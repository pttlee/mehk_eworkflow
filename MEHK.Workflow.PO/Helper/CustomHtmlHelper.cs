﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Expressions;
using System.Web.Routing;
using System.Text;
using System.Web.Mvc.Html;

namespace MEHK.Workflow.PO.Helper
{
	public static class CustomHtmlHelper
	{

		//public static IHtmlString CustomTextArea<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string text, bool enable)
		public static MvcHtmlString CustomHelperForTextArea<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, bool Enable)
		{
			var fieldName = ExpressionHelper.GetExpressionText(expression);
			var fullBindingName = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(fieldName);
			var fieldId = TagBuilder.CreateSanitizedId(fullBindingName);

			var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
			var value = metadata.Model;

			TagBuilder tag = new TagBuilder("textarea");
			tag.Attributes.Add("name", fullBindingName);
			tag.Attributes.Add("id", fieldId);
			tag.Attributes.Add("cols", "20");
			tag.Attributes.Add("rows", "6");
			tag.Attributes.Add("style", "width:100%");
			tag.AddCssClass("form-control");
			tag.AddCssClass("k-textbox");

			if (value != null)
			{
				tag.InnerHtml = value.ToString();
			}

			var validationAttributes = html.GetUnobtrusiveValidationAttributes(fullBindingName, metadata);
			foreach (var key in validationAttributes.Keys)
			{
				tag.Attributes.Add(key, validationAttributes[key].ToString());
			}

			if (Enable)
			{
				return new MvcHtmlString(tag.ToString());
			}
			else
			{
				if (value == null || String.IsNullOrEmpty(value.ToString()))
				{
					return new MvcHtmlString("<span class='form-control-plaintext'>N/A</span>");
				}
				else
				{
					return new MvcHtmlString("<div><label class='col-form-label'>" + value.ToString() + "</label></div>");
				}
			}
		}


		public static MvcHtmlString CustomHelperForTextBox<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, bool Enable)
		{
			var fieldName = ExpressionHelper.GetExpressionText(expression);
			var fullBindingName = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(fieldName);
			var fieldId = TagBuilder.CreateSanitizedId(fullBindingName);

			var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
			var value = metadata.Model;

			TagBuilder tag = new TagBuilder("input");
			tag.Attributes.Add("name", fullBindingName);
			tag.Attributes.Add("id", fieldId);
			tag.Attributes.Add("type", "text");
			tag.Attributes.Add("style", "width:100%");
			tag.AddCssClass("k-input");
			tag.AddCssClass("k-textbox");

			if (value != null)
			{
				//tag.InnerHtml = value.ToString();
				tag.Attributes.Add("value", value.ToString());
			}

			var validationAttributes = html.GetUnobtrusiveValidationAttributes(fullBindingName, metadata);
			foreach (var key in validationAttributes.Keys)
			{
				tag.Attributes.Add(key, validationAttributes[key].ToString());
			}

			if (Enable)
			{
				return new MvcHtmlString(tag.ToString());
			}
			else
			{
				if (value == null || String.IsNullOrEmpty(value.ToString()))
				{
					return new MvcHtmlString("<span class='form-control-plaintext'>N/A</span>");
				}
				else
				{
					return new MvcHtmlString("<div><label class='col-form-label'>" + value.ToString() + "</label></div>");
				}
			}
		}


		public static MvcHtmlString LabelWithColonFor<TModel, TValue>(
			this HtmlHelper<TModel> helper,
			Expression<Func<TModel, TValue>> expression)
		{
			var fieldName = ExpressionHelper.GetExpressionText(expression);
			var fullBindingName = helper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(fieldName);

			var metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);

			if (metadata.IsRequired)
			{
				return new MvcHtmlString(string.Format("{0}<span class='text-danger'>*</span>:", helper.DisplayNameFor(expression)));
			}
			else
			{
				return helper.LabelFor(expression, string.Format("{0}:",
							 helper.DisplayNameFor(expression)));
			}


		}

	}
}
﻿//var ProcessCode = 'NonBudgetCAPEX';
//var UploadTitle = 'Non Budget CAPEX Form';
var NumberCount = 1;

//var SOWUpdateClick = false;
//var ApiUrl = "http://192.168.1.227:8089/MEHK.CommonAPI/api/";

function iterationCopy(src) {
    let target = {};
    for (let prop in src) {
        if (src.hasOwnProperty(prop)) {
            target[prop] = src[prop];
        }
    }
    return target;
}


function getID() {

    var data = { ID: $("#ID").val() }
    return data;
}

function selectFile(e) {
}

function onUploadSuccess(e) {
}
function onUpload(e) {
    // pass the form Data

    //var fileDropDownList = $("#AttachmentType").data("kendoDropDownList");

    var data = {
        refid: $("#ID").val(),
        process: $("#EntertainmentGiftCode").val(),
        //CatName: fileDropDownList.text(),
        //FileCode: fileDropDownList.value(),
        remark:'',
        userID: $("#CurrentUserID").val(),
        attachmentType: "",
        title: "",
        GID: $("#GID").val(),
    }
    e.data = data;
}

function onUploadError(e) {

   
}

function onUploadComplete(e) {
    $("#DocumentTitle").val(''),
    $("#DocumentRemark").val('');

    // Reset the Title Textbox
}
function getUserFilterData() {
	var ddl = $("#Payee").data("kendoDropDownList");
	var MemberID= $("#MemberId").val();
	if (MemberID == "") {
		MemberID = 0;
	}
	return {
		text: ddl.filterInput.val(),
		MemberID: MemberID
	};
}

$(function () {
	var allowedExtensions = [".gif", ".jpg", ".png",".pdf"];
	var maxFileSize = 1048576 * 10;
	InitFiles($("#ID").val(), $("#GID").val(), $("#EntertainmentGiftCode").val());
	var uploadSetting = {};
	uploadSetting.allowedExtensions = allowedExtensions;
	uploadSetting.processCode = $("#EntertainmentGiftCode").val();
	uploadSetting.formID = $("#ID").val();
	uploadSetting.InvalidMaxFileSizeMessge = "File size maximum " + 10 + "M/per file"
	uploadSetting.maxFileSize = maxFileSize;
	uploadSetting.UploadControl = "#files";
	uploadSetting.TableArea = "#UploadedFiles";
	uploadSetting.UploadedFilesTable = "#UploadedFilesGrid";
	uploadSetting.GID = $("#GID").val();
	uploadSetting.TableColumnList = IninUploadMobileTalbeColumn('true', $("#GID").val(), $("#CurrentUserID").val());
	uploadSetting.TableField = InitFields();
	IninUpload(uploadSetting, selectFile, onUploadComplete, onUpload, onUploadSuccess, onUploadError);

	//	$("#IsNoDuplicationOfBudget").kendoMultiSelect({
	//		placeholder: "Select products...",
	//		dataTextField: "Description",
	//		dataValueField: "CostCenterId",
	//		autoBind: false,
	//		dataSource: dataSource
	//	});
	//}
	//var data = {
	//	SAMAccount: $("#SAMAccount").val(),
	//}
	//GetUserinfo($("#SAMAccount").val());
	//var db = Userinfo;
	//$.ajax({
	//	dataType: 'json',
	//	type: 'GET',
	//	url: ApiUrl +'Organization/Members?SAMAccount='+ $("#SAMAccount").val(),
	//	//data: JSON.stringify(data),
	//	success: function (data) {
	//		if (data!=null) {
	//			$("#MemberId").val(data[0].MemberId)
	//			$("#StaffID").html(data[0].StaffId)
	//			$("#StaffName").html(data[0].MemberName)
	//			$("#Division").html(data[0].Division)
	//			$("#Department").html(data[0].Department)
	//			var Payee = $("#Payee").data("kendoDropDownList");
	//			Payee.dataSource.read();
	//		}
			
	//	},
	//	failure: function (response) {
			
	//	}
	//});

    kendo.ui.validator.rules.mvcdate = function (input) {

        return !input.is("[data-val-date]") || input.val() === "" || kendo.parseDate(input.val(), DisplayDateOnlyFormat) !== null || kendo.parseDate(input.val(), DisplayMonthFormat) !== null;
    };

    //$(document).on("keypress", ":input:not(textarea)", function (event) {
    //    if (event.keyCode == 13) {
    //        event.preventDefault();
    //    }
    //});

    //var allowedExtensions = AllowedExtensionsSetting.match(/(?=\S)[^,]+?(?=\s*(,|$))/g);
    //var maxFileSize = 1048576 * MaxFileSizeSetting;

    CategoryText = "Attachment Type";


    //InitFiles($("#ID").val(), $("#GID").val(), ProcessCode);

    //var uploadSetting = {};
    //uploadSetting.allowedExtensions = allowedExtensions;
    //uploadSetting.maxFileSize = maxFileSize;
    //uploadSetting.UploadControl = "#files";
    //uploadSetting.TableArea = "#UploadedFiles";
    //uploadSetting.UploadedFilesTable = "#UploadedFilesGrid";
    //uploadSetting.GID = $("#GID").val();
    //uploadSetting.TableColumnList = InitUploadTableColumn($("#EnableDeleteFile").val(), $("#GID").val(), $("#CurrentUserID").val(), ["Category", "DocumentName", "UploadedBy", "UploadedDate"], ShowUploadAction);
    //uploadSetting.TableField = InitFields();

    //IninUpload(uploadSetting, selectFile, onUploadComplete, onUpload, onUploadSuccess, onUploadError);


    $("#btnDeleteDraft").click(function () {

        DialogMessage.ConfirmMessage(getConfirmTitle(event), '', function () {

            mApp.blockPage();

            var data = {
                ID: $("#ID").val(),
            }

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: 'DeleteDraft',
                data: JSON.stringify(data),
                success: function (data) {
                    if (data.Success) {

                        RemoveWindowOnBeforeUnloadListener();

                        DialogMessage.AlertMessageAndRedirect(data.Title,
                            data.Text,
                            'MyDraft');

                        mApp.unblockPage();
                    }
                    else {

                        DialogMessage.AlertMessage(data.Title,
                        data.Text);
                        mApp.unblockPage();
                    }
                },
                failure: function (response) {
                    mApp.unblockPage();
                }
            });
        });

        return false;
    });

    $("#btnSubmit").click(function () {

        var ConfirmTitle = getConfirmTitle(event);

        Submit(ConfirmTitle);

        return false;
    });

    $("#btnSave_PreApproval").click(function () {

            DialogMessage.ConfirmMessage(getConfirmTitle(event), '', function () {

                mApp.blockPage();

                var data = getFormData();

                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: 'POST',
                    url: 'Save_PreApproval',
                    data: JSON.stringify(data),
                    success: function (data) {

                        if (data.Success) {

                            //$("#ID").val(data.ID);

                            //SubmitChanges(data.ID, $("#GID").val(), $("#CurrentUserID").val(), ProcessCode);

                            DialogMessage.AlertMessage(data.Title,
                            data.Text);
                            mApp.unblockPage();

                            //$("#UploadedFilesGrid").data('kendoGrid').dataSource.read();
                            //$("#UploadedFilesGrid").data('kendoGrid').refresh();
                        }
                        else {
                            DialogMessage.AlertMessage(data.Title,
                            data.Text);
                            mApp.unblockPage();
                        }
                    },
                    failure: function (response) {
                        //swal('Draft Failed!');

                        mApp.unblockPage();
                    }
                });
            });
        

        return false;
    });

    $(".WorkflowAction").click(function () {

        var WFAction = $(this).data('wfaction');
        var FormAction = $(this).data('formaction');

        //var FormType = $("#FormType").val();
        //var RoleName = $("#RoleName").val();
        //var Step = $("#Step").val();

        var FormType = '';
        var RoleName = '';
        var Step = '';
        var RedirectURL = '../Home/MyTask';

        var PostURL = 'WorkflowAction';
        var headers = { __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() };
        var data = {
            K2SN: $("#K2SN").val(),
            ID: $("#ID").val(),
            Comment: $("#Comment").val(),
            FormType: FormType,
            RoleName: RoleName,
            WFAction: WFAction,
            FormAction: FormAction,
			Division:$("#Division").val(),
			Department:$("#Department").val(),
        };
        var AmountHKEntertain=0;
        var AmountHKGift = 0;
        var ReimbursementData = {};
        if (WFAction == 'ReimbursementSubmit' || (WFAction == 'Resubmit' && $("#ReIsCompleted").val() != false)) {
        	//var BudgetType = $("#BudgetType").val();
        	if ($("#BudgetType").val() == "Entertainment") {
        		AmountHKEntertain = $("#ResultAmount").val()
        	} else {
        		AmountHKGift = $("#ResultAmount").val()
        	}

        	var CostCenter = $("#IsNoDuplicationOfBudget").val().join(',');
			 ReimbursementData = {
        	//PaymentReimbursement: $('#SReimbursementForm').serialize()
        	PaymentReimbursement: {
        		PostingInstruction: $("#PaymentReimbursement_PostingInstruction").val(),
        		Purpose: $("#PaymentReimbursement_Purpose").val(),
        		CostCenter: CostCenter,
        		EventDateOn: $("#PaymentReimbursement_EventDateOn").data("kendoDatePicker").value(),
        		EventDateTo: $("#PaymentReimbursement_EventDateTo").data("kendoDatePicker").value(),
        		//EventDateOnString: $("#PaymentReimbursement_EventDateOnString").val(),
        		//EventDateToString: $("#PaymentReimbursement_EventDateToString").val(),
        		LateApplicationReason: $("#PaymentReimbursement_LateApplicationReason").val(),
        		NatureOfGift: $("#PaymentReimbursement_NatureOfGift").val(),
        		Guest: $("#PaymentReimbursement_Guest").val(),
        		GuestTotal: $("#PaymentReimbursement_GuestTotal").val(),
        		Company: $("#PaymentReimbursement_Company").val(),
        		Attendant: $("#PaymentReimbursement_Attendant").val(),
        		AttendantTotal: $("#PaymentReimbursement_AttendantTotal").val(),

        		Currency: $("#PaymentReimbursement_Currency").val(),
        		Amount: $("#PaymentReimbursement_Amount").val(),
        		ExchangeRate: $("#PaymentReimbursement_ExchangeRate").val(),
        		AmountHKEntertain: AmountHKEntertain,
        		AmountHKGift: AmountHKGift,
        		AmountExceedReason: $("#PaymentReimbursement_AmountExceedReason").val(),
        		PaymentMethod: $("input:radio[name=PaymentMethod]:checked").val(),
        		PayTo: $("#PaymentReimbursement_PayTo").val(),
        		NoOfDocumentOrReceiptAttached: $("#PaymentReimbursement_NoOfDocumentOrReceiptAttached").val()

        	}
        }
        }
       
       
        
        if (WFAction == 'ReimbursementSubmit') {
        	//var dOn = $("#PaymentReimbursement_EventDateOn").data("kendoDatePicker").value();
        	//var dTo = $("#PaymentReimbursement_EventDateTo").data("kendoDatePicker").value();
			//$("#PaymentReimbursement_EventDateOnString").val(kendo.toString(dOn, "yyyy") + "-" + kendo.toString(dOn, "MM")+"-" + kendo.toString(dOn, "dd"))
			//$("#PaymentReimbursement_EventDateToString").val(kendo.toString(dTo, "yyyy") + "-" + kendo.toString(dTo, "MM")+"-" + kendo.toString(dTo, "dd"))
			

        	

        	data = Object.assign(data, ReimbursementData);
        }
        if (WFAction == 'Resubmit') {
		var ReIsCompleted=$("#ReIsCompleted").val();
            //data = getFormData();
			if(ReIsCompleted==false){
        	data = Object.assign(data, getFormData());
			}else{
			//var dOn = $("#PaymentReimbursement_EventDateOn").data("kendoDatePicker").value();
        	//var dTo = $("#PaymentReimbursement_EventDateTo").data("kendoDatePicker").value();
			//$("#PaymentReimbursement_EventDateOnString").val(kendo.toString(dOn, "yyyy") + "-" + kendo.toString(dOn, "MM")+"-" + kendo.toString(dOn, "dd"))
			//$("#PaymentReimbursement_EventDateToString").val(kendo.toString(dTo, "yyyy") + "-" + kendo.toString(dTo, "MM")+"-" + kendo.toString(dTo, "dd"))

        	
			data = Object.assign(data, ReimbursementData);
			}

            //data = {...data, ...getFormData()};

                //data.K2SN = $("#K2SN").val();
                //data.FormType = FormType;
                //data.RoleName = RoleName;
                //data.WFAction = WFAction;
                //data.FormAction = FormAction;

                WorkflowAction(getConfirmTitle(event), '', PostURL, RedirectURL, data);

            } else if (WFAction == 'Return') {
                if (validateComment()) {

                    WorkflowAction('', getConfirmTitle(event), PostURL, RedirectURL, data);
                }
            }
            else {
                WorkflowAction(getConfirmTitle(event), '', PostURL, RedirectURL, data);
            }

        return false;

    });

    $("#btnCancel").click(function () {

        RemoveWindowOnBeforeUnloadListener();

        DialogMessage.ConfirmMessage(ConfirmLeavePageTitle, '', function () {

            if (history.length == 1) {
                window.close();
            }
            else {
                window.history.back();
            }

            //window.close();
        });

        return false;
    });

    $("#btnCancelWithoutConfirm").click(function () {

        //window.history.back();
        RemoveWindowOnBeforeUnloadListener();

        if (history.length == 1) {
            window.close();
        }
        else {
            window.history.back();
        }

        return false;
    });

    $("#btnRelease").click(function () {

        //mApp.blockPage();

        var data = {
            SN: $("#K2SN").val(),
        }

        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: 'Release',
            data: JSON.stringify(data),
            success: function (data) {
                if (data.Success) {

                    RemoveWindowOnBeforeUnloadListener();

                    location.href = 'MyTask';

                    //mApp.unblockPage();
                }
                else {
                    location.href = 'MyTask';
                    //DialogMessage.AlertMessage(data.Title,
                    //data.Text);

                    //mApp.unblockPage();
                }
            },
            failure: function (response) {
                location.href = 'MyTask';
                //mApp.unblockPage();
            }
        });

    });

    $("#btnSave").click(function () {

        if (IsMaxUploadedSizeValid()) {
            DialogMessage.ConfirmMessage(getConfirmTitle(event), '', function () {

                mApp.blockPage();

                var data = getFormData();

                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: 'POST',
                    url: 'Save',
                    data: JSON.stringify(data),
                    success: function (data) {

                        if (data.Success) {

                            $("#ID").val(data.ID);

                            SubmitChanges(data.ID, $("#GID").val(), $("#CurrentUserID").val(), ProcessCode);

                            DialogMessage.AlertMessage(data.Title,
                            data.Text);
                            mApp.unblockPage();

                            $("#UploadedFilesGrid").data('kendoGrid').dataSource.read();
                            $("#UploadedFilesGrid").data('kendoGrid').refresh();
                        }
                        else {
                            DialogMessage.AlertMessage(data.Title,
                            data.Text);
                            mApp.unblockPage();
                        }
                    },
                    failure: function (response) {
                        //swal('Draft Failed!');

                        mApp.unblockPage();
                    }
                });
            });
        }

        return false;
    });

    $("#btnDraft_PreApproval").click(function () {


        DialogMessage.ConfirmMessage(getConfirmTitle(event), '', function () {

            mApp.blockPage();

            var data = getFormData();

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: 'Draft',
                data: JSON.stringify(data),
                success: function (data) {

                    if (data.Success) {

                        $("#ID").val(data.ID);

                        DialogMessage.AlertMessage(data.Title,
                        data.Text);
                        mApp.unblockPage();
                    }
                    else {
                        DialogMessage.AlertMessage(data.Title,
                        data.Text);
                        mApp.unblockPage();
                    }
                },
                failure: function (response) {
                    mApp.unblockPage();
                }
            });
        });


        return false;
    });
});

function Submit(ConfirmTitle) {

    var validator = $("#Form").kendoValidator().data("kendoValidator");

    if (FormValidation('PreApproval')) {

        if (validator.validate()) {

            DialogMessage.ConfirmMessage(ConfirmTitle, '', function () {

                mApp.blockPage();

                var data = getFormData();
                var headers = { __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() };

                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    headers: headers,
                    type: 'POST',
                    url: 'Submit',
                  
                    //data: JSON.stringify(data),
                    data: JSON.stringify(data),
                    success: function (data) {

                        if (data.Success) {

                            //SubmitChanges(data.ID, $("#GID").val(), $("#CurrentUserID").val(), ProcessCode);

                            RemoveWindowOnBeforeUnloadListener();

                            DialogMessage.AlertMessageAndRedirect(data.Title,
                                data.Text,
                                '../Home/MyTask');

                            mApp.unblockPage();
                        }
                        else {
                            DialogMessage.AlertMessage(data.Title,
                                    data.Text);

                            mApp.unblockPage();
                        }
                    },
                    failure: function (response) {

                        DialogMessage.AlertMessage('Submitted Failed!');

                        mApp.unblockPage();
                    }
                });
            });
        }
        else {
            mApp.scrollTo("#Form");

            swal({
                "title": "",
                "text": "There are some errors in your submission. Please correct them.",
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
        }
    }
}

function Resubmit(ConfirmTitle) {

    DialogMessage.ConfirmMessage(ConfirmTitle, '', function () {

        mApp.blockPage();

        var data = getFormData();

        data.SN = $("#K2SN").val();

        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: 'Resubmit',
            data: JSON.stringify(data),
            success: function (data) {

                if (data.Success) {

                    SubmitChanges(data.ID, $("#GID").val(), $("#CurrentUserID").val(), ProcessCode);

                    RemoveWindowOnBeforeUnloadListener();

                    DialogMessage.AlertMessageAndRedirect(data.Title,
						data.Text,
						'MyTask');

                    mApp.unblockPage();
                }
                else {
                    DialogMessage.AlertMessage(data.Title,
						data.Text);

                    mApp.unblockPage();
                }
            },
            failure: function (response) {

                //swal('Submitted Failed!');
                DialogMessage.AlertMessage('Submitted Failed!');

                mApp.unblockPage();
            }
        });
    });

}

function getFormData() {

    //var GID = $("#GID").val();

    var PreApproval = {
        Purpose: $("#PreApproval_Purpose").val(),
        EventDateOn: $("#PreApproval_EventDateOn").data("kendoDatePicker").value(),
        EventDateTo: $("#PreApproval_EventDateTo").data("kendoDatePicker").value(),
        LateApplicationReason: $("#PreApproval_LateApplicationReason").val(),
        NatureOfGift: $("#PreApproval_NatureOfGift").val(),
        Guest: $("#PreApproval_Guest").val(),
        GuestTotal: $("#PreApproval_GuestTotal").data("kendoNumericTextBox").value(),
        Company: $("#PreApproval_Company").val(),
        Attendant: $("#PreApproval_Attendant").val(),
        AttendantTotal: $("#PreApproval_AttendantTotal").data("kendoNumericTextBox").value(),
    }

    var data = {
        ID: $("#ID").val(),
        __RequestVerificationToken: $("[name='__RequestVerificationToken']").val(),
        PreApproval: PreApproval,
        BudgetType: $("input:radio[name=BudgetType]:checked").val(),
        BudgetAmount: $("#BudgetAmount").data("kendoNumericTextBox").value(),
        Payee: $("#Payee").data("kendoDropDownList").value(),
        //EndorsedBy: $("#EndorsedBy").data("kendoDropDownList").value(),
        //RequestedBy: $("#RequestedBy").data("kendoDropDownList").value(),
        //RequestedParty: $("#RequestedParty").data("kendoDropDownList").value(),
        ////CostCentre: $("#CostCentre").data("kendoMultiSelect").value(),
        //TargetCompletionOfTheProposedWorks: $("#TargetCompletionOfTheProposedWorks").data("kendoDatePicker").value(),
        //InfluenceOfProjectCompletionByProposedWork: $("#InfluenceOfProjectCompletionByProposedWork").data("kendoDropDownList").value(),
        //InfluenceOfProjectCompletionByProposedWorkMonth: $("#InfluenceOfProjectCompletionByProposedWorkMonth").data("kendoNumericTextBox").value(),
        //ProcurementStatus: $("#ProcurementStatus").data("kendoDropDownList").value(),
        //OriginalContractContingency: $("#OriginalContractContingency").data("kendoNumericTextBox").value(),
        //ContingencyAlreadySpentWithAI: $("#ContingencyAlreadySpentWithAI").data("kendoNumericTextBox").value(),
        //RequestForUsingRemainingContractContingencyItem: $("#RequestForUsingRemainingContractContingencyItemGrid").data("kendoGrid").dataSource.data(),

        //TargetCompletionDateEarlierThanSubmitReason: $("#TargetCompletionDateEarlierThanSubmitReason").data("kendoDropDownList").value(),
        //TargetCompletionDateEarlierThanSubmitReasonOther: $("#TargetCompletionDateEarlierThanSubmitReasonOther").val(),
      
    }

    return data;
}




function validateComment() {
    var valid = false;

    if ($("#Comment").val()) {
        valid = true;
    }
    else {
        DialogMessage.AlertMessage('', CommentRequired);
    }

    return valid;
}

function ApprovalHistoryDataBound(e) {
    if (this.dataSource.total() === 0) {
        $("#ApprovalHistory").hide();
    }
    else {
        $("#ApprovalHistory").show();
    }
}

function ApprovalCommentDataBound(e) {
    if (this.dataSource.total() === 0) {
        $("#ApprovalComment").hide();
    }
    else {
        $("#ApprovalComment").show();
    }
}

function IsUploadedFiles() {

    var Valid = $("#UploadedFilesGrid").data("kendoGrid").dataSource.total() > 0;

    if (!Valid) {
        DialogMessage.AlertMessage('', UploadedFilesRequired);
    }

    return Valid;
}

function IsMaxUploadedSizeValid() {

    var Valid = true;
    var data = $("#UploadedFilesGrid").data("kendoGrid").dataSource.data();
    var TotalFileSize = 0;

    for (i = 0; i < data.length; i++) {
        TotalFileSize += data[i].FileSize;
    }

    if (TotalFileSize > (TotalMaxFileSize * 1048576)) {
        Valid = false;
    }

    if (!Valid) {
        DialogMessage.AlertMessage('', ExceedMaxTotalFileSize);
    }

    return Valid;
}

function FormValidation(type) {
    //return true;
    var Valid = true;
    var data;

    data = getFormData();

    //if (type == 'Project') {
    //    data = getFormData();
    //}

    var Message = '';
    //var ValidationMessage_LateApplicationReasonRequired = '@Resources.Resource_EntertainmentGiftValidation.LateApplicationReasonRequired';
    //var ValidationMessage_EventDateToCanNotLaterThanEventDateOn = '@Resources.Resource_EntertainmentGiftValidation.EventDateToCanNotLaterThanEventDateOn';

    if (data.PreApproval.EventDateOn && CheckApplicationDateLaterThanEventDate() && !data.PreApproval.LateApplicationReason)
    {
        Message += ValidationMessage.LateApplicationReasonRequired + '<br>';
    }

    if (data.PreApproval.EventDateOn && data.PreApproval.EventDateTo &&
        (
             data.PreApproval.EventDateOn > data.PreApproval.EventDateTo 
        ))
    {
        Message += ValidationMessage.EventDateToCanNotLaterThanEventDateOn + '<br>';
    }

    if (!data.BudgetType)
    {
        Message += ValidationMessage.BudgetTypeRequired + '<br>';
    }

    if (data.BudgetType == 'Entertainment' && !data.PreApproval.Attendant)
    {
        Message += ValidationMessage.AttendantRequired + '<br>';
    }

    //if (type == 'Project') {
        
    //    if (!CanNotHaveNegativeAmountIfOnlyApplySOWValid) {
    //        DialogMessage.AlertMessage('', "Can not have negative amount if only apply SOW");
    //        return false;
    //    }
    //}

    if (Message)
    {
        DialogMessage.AlertMessageUsingHTML('', Message);

        Valid = false;


    }



    //var Valid = true;

    //if (!Valid) {
    //    DialogMessage.AlertFormErrorMessage();
    //}

    return Valid;
}

function WorkflowAction(Title, Text, PostURL, Redirect, data) {
    DialogMessage.ConfirmMessage(Title, Text, function () {

        mApp.blockPage();

        //var data = {
        //    SN: $("#K2SN").val(),
        //    ID: $("#ID").val(),
        //    Comment: $("#Comment").val(),
        //}
        var headers = { __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() };
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            headers: headers,
            dataType: 'json',
            type: 'POST',
            url: PostURL,
            data: JSON.stringify(data),
            success: function (data) {
                if (data.Success) {
                	SubmitChanges($("#ID").val(), $("#GID").val(), $("#CurrentUserID").val(), $("#EntertainmentGiftCode").val());
                    RemoveWindowOnBeforeUnloadListener();

                    DialogMessage.AlertMessageAndRedirect(data.Title,
                        data.Text,
                        Redirect);

                    mApp.unblockPage();
                }
                else {
                    DialogMessage.AlertMessage(data.Title,
                    data.Text);

                    mApp.unblockPage();
                }
            },
            failure: function (response) {
                mApp.unblockPage();
            }
        });
    });
}

function formatCurrency(Amount) {
    //return '$' + Amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return '' + Amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

//ConfirmTitle = ConfirmTitle.replace('{0}', $(event.target).text().toLowerCase());

function getConfirmTitle(event) {
    return ConfirmTitle.replace('{0}', $(event.target).text().toLowerCase());
}

function GetFileTypeIsValid(CatName) {
    var data = $("#UploadedFilesGrid").data("kendoGrid").dataSource.data();
    var isValid = false;
    for (i = 0; i < data.length; i++) {
        if (data[i].CatName == CatName) {
            isValid = true;
            break;
        }
    }

    return isValid;
}

function changeNewLine(text) {
    var regexp = new RegExp('\n', 'g');
    return text.replace(regexp, '<br>');
}


function CheckApplicationDateLaterThanEventDate()
{
    var Temp = $("#PreApproval_EventDateOn").data("kendoDatePicker").value();

    var EventDate = new Date(Temp.getFullYear(), Temp.getMonth(), Temp.getDay());

    return ApplicationDate > EventDate;
}

function PreApprovalEventDateOnChange() {


    if (CheckApplicationDateLaterThanEventDate())
    {
        $("#divLateApplicationReason").show();
    }
    else
    {
        $("#divLateApplicationReason").hide();

        $("#PreApproval_LateApplicationReason").val();
    }


}
function setResultAmount() {
	var Amount = $("#PaymentReimbursement_Amount").val();
	var ExchangeRate = $("#PaymentReimbursement_ExchangeRate").val();
	var ResultAmount = Amount * ExchangeRate;
	$("#ResultAmount").val(ResultAmount);
}
$("#UploadedFilesbut").click(function () {
	SubmitChanges($("#ID").val(), $("#GID").val(), $("#CurrentUserID").val(), $("#EntertainmentGiftCode").val());
})
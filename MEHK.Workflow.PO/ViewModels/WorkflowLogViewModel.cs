﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MEHK.Workflow.PO.ViewModels
{
	public class WorkflowLogViewModel
	{

		public string Role { get; set; }

		public string Department { get; set; }

		public string Position { get; set; }

		public string Name { get; set; }

		public int UserID { get; set; }

		public string Action { get; set; }

		public int ActionID { get; set; }


		[Display(Name = "Action Date")]
		public DateTime ActionDate { get; set; }

		public string Comment { get; set; }


	}
}
﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MEHK.Workflow.BLL;
using MEHK.Workflow.Data.Enum;
using MEHK.Workflow.PO.ViewModels;
using Resources;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MEHK.Workflow.Data.Model;

namespace MEHK.Workflow.PO.Controllers
{
	public class HomeController : MEHKBase
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult MyDraft()
		{
			SetBreadcrumbs(Resource_Common.MyDraft);

			return View();
		}

		public ActionResult MyTask()
		{
			SetBreadcrumbs(Resource_Common.MyTask);

			return View();
		}

		public ActionResult MySubmission()
		{
			SetBreadcrumbs(Resource_Common.MySubmission);

			var TypeList = CommonBLL.GetFormTypes().Select(n => new SelectListItem() { Text = n.Description, Value = n.Code });
			ViewBag.TypeList = TypeList;

			var statusList = CommonBLL.GetStatusGroups()
				.OrderBy(n => n.SortOrder)
				.Select(n => new SelectListItem() { Text = n.Name, Value = n.ID.ToString() });

			ViewBag.StatusList = statusList;

			ViewBag.SearchFunction = "GetMySubmission";

			return View("Search");

			//return View();
		}

		public ActionResult Search()
		{
			SetBreadcrumbs("Home", "Search - All");

			var TypeList = CommonBLL.GetFormTypes().Select(n => new SelectListItem() { Text = n.Description, Value = n.Code });
			ViewBag.TypeList = TypeList;

			var statusList = CommonBLL.GetStatusGroups()
				.OrderBy(n => n.SortOrder)
				.Select(n => new SelectListItem() { Text = n.Name, Value = n.ID.ToString() });

			ViewBag.StatusList = statusList;

			ViewBag.SearchFunction = "GetSearch";

			//CommonSearch();

			//var searchAllInfoList = EClaimsEntites.MyActionSettings.Where(q => q.SearchAllEnabled && !q.IsDeleted).ToList();

			//ViewBag.Mode = "SearchAll";

			return View();
		}


		public ActionResult GetMySubmission([DataSourceRequest]DataSourceRequest request
				, string workflow = null
				, string serialNo = null
				, string requestBy = null
				, string creator = null
				, string applicantStaffNo = null
				, int? statusId = null
				, DateTime? submissionFrom = null
				, DateTime? submissionTo = null
				, string mode = null
				)
		{
			var MEHKWorkflowEntities = new MEHKWorkflowEntities();

			var result = new List<SearchGridViewModel>();

			var TypeList = CommonBLL.GetFormTypes();
			if (!String.IsNullOrEmpty(workflow))
			{
				TypeList = TypeList.Where(n => n.Code == workflow).ToList();
			}

			foreach (var wfInfo in TypeList)
			{
				try
				{
					var sql = CommonBLL.GetFormTypeMySubmissionSQL(wfInfo.Code);

					var list = MEHKWorkflowEntities.Database
							.SqlQuery<SearchGridViewModel>(
							$"{sql} @user, @lang, @serialNo, @requestBy, @creator, @applicantStaffNo, @statusId, @submissionFrom, @submissionTo",
							new SqlParameter("user", GetUserInfo.MemberId),
							new SqlParameter("lang", ""),
							new SqlParameter("serialNo", (object)serialNo ?? DBNull.Value),
							new SqlParameter("requestBy", (object)requestBy ?? DBNull.Value),
							new SqlParameter("creator", (object)creator ?? DBNull.Value),
							new SqlParameter("applicantStaffNo", (object)applicantStaffNo ?? DBNull.Value),
							new SqlParameter("statusId", (object)statusId ?? DBNull.Value),
							new SqlParameter("submissionFrom", (object)submissionFrom ?? DBNull.Value),
							new SqlParameter("submissionTo", (object)submissionTo ?? DBNull.Value)
							).ToList();

					result.AddRange(list);
				}
				catch (Exception ex)
				{
					this.CreateErrorLog(ex);
				}
			}

			result = result.OrderByDescending(q => q.SubmissionDate).ToList();

			return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetSearch([DataSourceRequest]DataSourceRequest request
			, string workflow = null
			, string serialNo = null
			, string requestBy = null
			, string creator = null
			, string applicantStaffNo = null
			, int? statusId = null
			, DateTime? submissionFrom = null
			, DateTime? submissionTo = null
			, string mode = null
			)
		{
			var MEHKWorkflowEntities = new MEHKWorkflowEntities();


			//var searchAllInfoList = EClaimsEntites.MyActionSettings.Where(q => q.SearchAllEnabled && !q.IsDeleted).ToList();

			//var userRightList = OrganizationBLL.GetUserRight(GetCurrentUserID).Select(n => n.RightName).ToArray();

			//string rightStr = String.Join(";", userRightList);

			var result = new List<SearchGridViewModel>();

			var TypeList = CommonBLL.GetFormTypes();
			if (!String.IsNullOrEmpty(workflow))
			{
				TypeList = TypeList.Where(n => n.Code == workflow).ToList();
			}

			//var searchTarget = String.IsNullOrEmpty(workflow) ? searchAllInfoList : searchAllInfoList.Where(q => q.ProcessCode == workflow).ToList();

			foreach (var wfInfo in TypeList)
			{
				try
				{
					//var spSearch = mode == "SearchAll" ? wfInfo.SPSearchAll : wfInfo.SPSearchMyActivity;
					//LanguageMange.GetCurrentCultureName()
					//rightStr
					// @workflow,
					//var sql = "";

					//if (wfInfo.Code == "EntertainmentGift")
					//{
					//    sql = "GetEntertainmentGiftSearch";
					//}

					var sql = CommonBLL.GetFormTypeSearchSQL(wfInfo.Code);

					var list = MEHKWorkflowEntities.Database
							.SqlQuery<SearchGridViewModel>(
							$"{sql} @user, @lang, @serialNo, @requestBy, @creator, @applicantStaffNo, @statusId, @submissionFrom, @submissionTo, @Right",
							new SqlParameter("user", 1000),
							new SqlParameter("lang", ""),
							//new SqlParameter("workflow", "EntertainmentGift"),
							new SqlParameter("serialNo", (object)serialNo ?? DBNull.Value),
							new SqlParameter("requestBy", (object)requestBy ?? DBNull.Value),
							new SqlParameter("creator", (object)creator ?? DBNull.Value),
							new SqlParameter("applicantStaffNo", (object)applicantStaffNo ?? DBNull.Value),
							new SqlParameter("statusId", (object)statusId ?? DBNull.Value),
							new SqlParameter("submissionFrom", (object)submissionFrom ?? DBNull.Value),
							new SqlParameter("submissionTo", (object)submissionTo ?? DBNull.Value),
							new SqlParameter("Right", "")
							).ToList();

					//var mySearchInfo = searchAllInfoList.Where(q => q.ProcessName == wfInfo.ProcessName).FirstOrDefault();
					//foreach (var item in list)
					//{
					//    if (mySearchInfo != null)
					//    {
					//        var tempUrl = String.Equals(item.Status, "Draft", StringComparison.OrdinalIgnoreCase) || String.Equals(item.Status, "Drafted", StringComparison.OrdinalIgnoreCase) ? mySearchInfo.DraftUrl : mySearchInfo.ViewUrl;
					//        foreach (var property in item.GetType().GetProperties())
					//        {
					//            var value = Convert.ToString(property.GetValue(item, null));
					//            tempUrl = tempUrl.Replace($"#{property.Name}#", value);
					//        }

					//        item.URL = tempUrl;
					//    }
					//}

					result.AddRange(list);
				}
				catch (Exception ex)
				{
					this.CreateErrorLog(ex);
				}
			}

			result = result.OrderByDescending(q => q.SubmissionDate).ToList();

			return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}


		public ActionResult GetWorkflowLog([DataSourceRequest]DataSourceRequest request, int FormID, int FormTypeID)
		{
			var list = CommonBLL.GetApprovalHistory(FormID, FormTypeID);

			return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetMyTasks([DataSourceRequest]DataSourceRequest request)
		{

			var list = CommonBLL.GetMyTasks();

			var k2UserID = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.k2UserID);

			var ProcessName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.GenenalProcessName);


			//todo set User Identity Name
			//  var ProcessItemList = WFControl.GetWorkList(GetUserInfo.SAMAccount, new string[] { ProcessName });
			var ProcessItemList = WFControl.GetWorkList(User.Identity.Name, new string[] { ProcessName });

			var result = (from task in list
						  join k2item in ProcessItemList
							  on task.ProcInstID equals (int?)k2item.ProcInstID
						  select new MyTaskGridViewModel()
						  {
							  ID = task.ID,
							  ProcInstID = k2item.ProcInstID,
							  SubmissionDate = task.SubmissionDate,
							  FormNo = task.FormNo,
							  //Title = task.Title,
							  FormStatus = task.FormStatus,
							  FormStatusDisplayName = task.FormStatusDisplayName,
							  K2ItemSerialNo = k2item.SN,
							  FormType = task.FormType,
							  FormTypeDisplayName = task.FormTypeDisplayName,
							  CreatedBy = task.CreatedBy
						  }).ToList();

			return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetMyDrafts([DataSourceRequest]DataSourceRequest request)
		{
			CommonBLL CommonBLL = new CommonBLL();

			var list = CommonBLL.GetMyDrafts().Select(n => new MyDraftGridViewModel()
			{
				ID = n.ID,
				FormType = n.FormType,
				FormTypeDisplayName = n.FormTypeDisplayName,
				FormStatus = n.FormStatus
			});

			return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
		}

		//public ActionResult About()
		//{
		//    ViewBag.Message = "Your application description page.";

		//    return View();
		//}

		//public ActionResult Contact()
		//{
		//    ViewBag.Message = "Your contact page.";

		//    return View();
		//}
	}
}
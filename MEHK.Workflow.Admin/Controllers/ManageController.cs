﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MEHK.Workflow.Data.Model;
using Kendo.Mvc.UI;
using MEHK.Workflow.Data.DTO;
using Kendo.Mvc.Extensions;
using RestSharp;
using System.Collections.Generic;
using MEHK.Workflow.CommonWeb.Controllers;
using MEHK.Workflow.BLL;
using MEHK.Workflow.Admin.Models;
using Newtonsoft.Json;
using MEHK.Workflow.Data.Const;
//using MEHK.Workflow.BLL.DTO;

namespace MEHK.Workflow.Admin.Controllers
{

    public class ManageController : BaseController
    {

        public ManageController()
        {
        }
		[MEHKAuthorize(PageRights = new string[] { RightConst.ACCESS_HR_ADMIN_PAGE_RIGHT, RightConst.ACCESS_IS_ADMIN_PAGE_RIGHT })]
		public ActionResult Index()
        {
			SetBreadcrumbs("Admin", "Index");
			ViewBag.CurrentUserID = GetUserInfo.MemberId;
            ViewBag.AppID = 1;
            ViewBag.ProcessID = 1;
            SetBreadcrumbs("Organization Units");
			
			APIAccessBLL apiBLL = new APIAccessBLL();
			Dictionary<string, object> dir = new Dictionary<string, object>();
			dir.Add("SAMACCOUNT", GetUserInfo.SAMAccountWithDomain.Split('\\')[1]);
			dir.Add("AppID", CommonConstcs.AppID);
			List<Right> Rights = apiBLL.Fun_CommonAPICALL(@"Common/GetUserRight/", dir, new Right());

			Right RightModel = Rights.Where(e => e.RightName == RightConst.ACCESS_IS_ADMIN_PAGE_RIGHT).FirstOrDefault();
			ViewBag.IsIT = false;
			if (RightModel != null)
			{
				ViewBag.IsIT = true;
			}

			return View();

        }
        public ActionResult About()
        {
			SetBreadcrumbs("Admin", "About");
			ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
			SetBreadcrumbs("Admin", "Contact");
			ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult UnitMemberGridList([DataSourceRequest] DataSourceRequest request, int UnitId)
        {
            List<Parameter> pms = new List<Parameter>();
            pms.Add(new Parameter() { Name = "UnitId", Value = UnitId });
            var UnitMembers = new APIAccessBLL().APICall("Members", "Organization", Method.GET, pms, new UserInfo());
            return Json(UnitMembers.ToDataSourceResult(request, ModelState));

        }
        //[AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnitMemberGridListCreate([DataSourceRequest] DataSourceRequest request, UserInfo CreateMember, int CreateUnitId)
        {
            //var results = new List<UserInfo>();
            List<Parameter> pms = new List<Parameter>();
            pms.Add(new Parameter() { Name = "UnitId", Value = CreateUnitId });
            pms.Add(new Parameter() { Name = "MemberId", Value = CreateMember.MemberId });

            List<UserInfo> UnitMembers = new APIAccessBLL().APICall("Members", "Organization", Method.POST, pms, new UserInfo());
            
            return Json(UnitMembers.ToDataSourceResult(request, ModelState));
            //  return Json(new[] { v }.ToDataSourceResult(request, ModelState));

        }
        [HttpPost]
        public ActionResult UpdatePostAndJobLevel(int MemberId,int JobLevelId,string Post)
        {
            #region update post jobLevel
            APIAccessBLL apiBLL = new APIAccessBLL();
            var UnitMembers = apiBLL.GetMemberById(MemberId);
            
            if (UnitMembers != null)
            {
                if (UnitMembers.JobLevelId != JobLevelId || UnitMembers.Post != Post)
                {
                    ManageViewModels mvs = new ManageViewModels()
                    {
                        MemberId = UnitMembers.MemberId,
                        MemberName = UnitMembers.MemberName,
                        LoginAccount = UnitMembers.LoginAccount,
                        StaffId = UnitMembers.StaffId,
                        SAMAccount = UnitMembers.SAMAccount,
                        SAPAccount = UnitMembers.SAPAccount,
                        Post = Post,
                        Phone = UnitMembers.Phone,
                        EmailAddress = UnitMembers.EmailAddress,
                        JobLevelId = JobLevelId,
                        //JobLevel = UnitMembers.JobLevel,
                        JobRank = UnitMembers.JobRank,
                        Remark = UnitMembers.Remark,
                        JobLevel = UnitMembers.JobLevel,
                        UnitId = UnitMembers.UnitId,
                    };
                    List<ManageViewModels> resultuf = new APIAccessBLL().APICall("Members", "Organization", Method.PUT, null, new ManageViewModels(), new
                    {
                        vms = mvs
                    });
                    
                }



            }

            #endregion
            return Json(UnitMembers, JsonRequestBehavior.AllowGet);
        }



        public ActionResult UnitMemberGridListUpdate([DataSourceRequest] DataSourceRequest request, UserInfo v)
        {

            return Json(new[] { v }.ToDataSourceResult(request, ModelState));

        }

        public ActionResult UnitMemberGridListDestroy([DataSourceRequest] DataSourceRequest request, int MemberId, int CreateUnitId)
        {

            List<Parameter> pms = new List<Parameter>();
            pms.Add(new Parameter() { Name = "UnitId", Value = CreateUnitId });
            pms.Add(new Parameter() { Name = "MemberId", Value = MemberId });
            var UnitMembers = new APIAccessBLL().APICall("Members", "Organization", Method.DELETE, pms, new UserInfo());

            return Json(UnitMembers.ToDataSourceResult(request, ModelState));
        }
    }
}
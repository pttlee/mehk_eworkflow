﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MEHK.Workflow.Admin.Models;
using MEHK.Workflow.CommonWeb.Controllers;
using Kendo.Mvc.UI;
using System.Collections.Generic;
using Kendo.Mvc.Extensions;
using MEHK.Workflow.BLL;
using RestSharp;
using MEHK.Workflow.Data.DTO;
using Newtonsoft.Json;
using System.Reflection;
using MEHK.Workflow.Data.Model;
using MEHK.Workflow.CommonWeb.ViewModels;
using MEHK.Workflow.Data.Const;
using MEHK.Workflow.Data.Enum;

namespace MEHK.Workflow.Admin.Controllers
{
    [MEHKAuthorize]
    public class AdminController : MEHKBase
    {


        public AdminController()
        {

        }
        [MEHKAuthorize(PageRights = new string[] { RightConst.ACCESS_HR_ADMIN_PAGE_RIGHT, RightConst.ACCESS_IS_ADMIN_PAGE_RIGHT, RightConst.ACCESS_ACCOUNT_ADMIN_PAGE_RIGHT })]
        public ActionResult Maintenance()
        {
            SetBreadcrumbs("Admin", "Maintenance");
            return View();
        }
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Manage");
        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_HR_ADMIN_PAGE_RIGHT)]
        public ActionResult Member()
        {
            SetBreadcrumbs("Admin", "Member");
            return View();
        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_HR_ADMIN_PAGE_RIGHT)]
        public ActionResult GetMember([DataSourceRequest]DataSourceRequest request)
        {
            List<ManageViewModels> result = new List<ManageViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            // var Manage = apiBLL.APICall("Members", "Organization", Method.GET, new List<Parameter>(), new UserInfo());
            var Manage = apiBLL.GetMemberList("Organization/GetMemberList/");
            //string json = JsonConvert.SerializeObject(Manage);
            //List<ManageViewModels> m = JsonConvert.DeserializeObject<List<ManageViewModels>>(json);
            result = Manage.Select(e => new ManageViewModels
            {
                MemberId = e.MemberId,
                MemberName = e.MemberName,
                LoginAccount = e.LoginAccount,
                StaffId = e.StaffId,
                SAMAccount = e.SAMAccount,
                SAPAccount = e.SAPAccount,
                Post = e.Post,
                Phone = e.Phone,
                EmailAddress = e.EmailAddress,
                JobLevelId = e.JobLevelId,
                JobLevel = e.JobLevel,
                JobRank = e.JobRank,
                Remark = e.Remark,
                UnitId = e.UnitId

            }).ToList();
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        //public ActionResult UpdateMember2([DataSourceRequest]DataSourceRequest request, ManageViewModels uf)
        //{
        //    List<ManageViewModels> result = new List<ManageViewModels>();
        //    List<ManageViewModels> ufl = new List<ManageViewModels>();
        //    List<Parameter> pms = new List<Parameter>();

        //    PropertyInfo[] ps = uf.GetType().GetProperties();
        //    foreach (PropertyInfo p in ps)
        //    {
        //        var value = p.GetValue(uf, null);
        //        pms.Add(new Parameter() { Name = p.Name, Value = value });
        //    }
        //    List<ManageViewModels> resultuf = new APIAccessBLL().APICall("Members", "Organization", Method.PUT, pms, new ManageViewModels());
        //    return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        //}
        public ActionResult UpdateMember([DataSourceRequest]DataSourceRequest request, ManageViewModels uf)
        {
            List<ManageViewModels> result = new List<ManageViewModels>();
            List<ManageViewModels> ufl = new List<ManageViewModels>();
            ufl.Add(uf);

            string json = JsonConvert.SerializeObject(uf);
            json = json.Replace(":null", ":\"\"");
            uf = JsonConvert.DeserializeObject<ManageViewModels>(json);

            List<ManageViewModels> resultuf = new APIAccessBLL().APICall("Members", "Organization", Method.PUT, null, new ManageViewModels(), new
            {
                vms = uf
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_IS_ADMIN_PAGE_RIGHT)]
        public ActionResult Group()
        {
            SetBreadcrumbs("Admin", "Group");
            return View();
        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_IS_ADMIN_PAGE_RIGHT)]
        public ActionResult Right()
        {
            SetBreadcrumbs("Admin", "Right");
            return View();
        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_IS_ADMIN_PAGE_RIGHT)]
        public ActionResult ApprovalMatrix()
        {
            SetBreadcrumbs("Admin", "ApprovalMatrix");
            return View();
        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_HR_ADMIN_PAGE_RIGHT)]
        public ActionResult ApprovalMatrixView()
        {
            SetBreadcrumbs("Admin", "ApprovalMatrixView");
            return View();
        }

        public JsonResult GetGroupMemberViewModelList(int nid, string bExist)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            //var Group = apiBLL.GetGroupMemberViewModelList("Organization/GetGroupMemberList/", nid);

            if (bExist == null)
            {
                return Json(apiBLL.GetGroupMemberViewModelList("Organization/GetGroupMemberList/", nid), JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (bExist == "0")
                {
                    return Json(apiBLL.GetGroupMemberViewModelList("Organization/GetGroupMemberList/", nid).Where(e => e.bExist == "False").ToList(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(apiBLL.GetGroupMemberViewModelList("Organization/GetGroupMemberList/", nid).Where(e => e.bExist == "True").ToList(), JsonRequestBehavior.AllowGet);
                }
            }
        }
        public JsonResult GetMemberGroupViewModelList(int nid, string bExist)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            if (bExist == null)
            {
                return Json(apiBLL.GetMemberGroupViewModelList("Organization/GetMemberGroupList/", nid), JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (bExist == "0")
                {
                    return Json(apiBLL.GetMemberGroupViewModelList("Organization/GetMemberGroupList/", nid).Where(e => e.bExist == "False").ToList(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(apiBLL.GetMemberGroupViewModelList("Organization/GetMemberGroupList/", nid).Where(e => e.bExist == "True").ToList(), JsonRequestBehavior.AllowGet);
                }
            }
        }
        public JsonResult GetGroupRightViewModelList(int nid, string bExist)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            if (bExist == null)
            {
                return Json(apiBLL.GetGroupRightList("Organization/GetGroupRightList/", nid), JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (bExist == "0")
                {
                    return Json(apiBLL.GetGroupRightList("Organization/GetGroupRightList/", nid).Where(e => e.bExist == "False").ToList(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(apiBLL.GetGroupRightList("Organization/GetGroupRightList/", nid).Where(e => e.bExist == "True").ToList(), JsonRequestBehavior.AllowGet);
                }
            }
        }
        public JsonResult GetRightGroupViewModelList(int nid, string bExist)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            if (bExist == null)
            {
                return Json(apiBLL.GetRightGroupList("Organization/GetRightGroupList/", nid), JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (bExist == "0")
                {
                    return Json(apiBLL.GetRightGroupList("Organization/GetRightGroupList/", nid).Where(e => e.bExist == "False").ToList(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(apiBLL.GetRightGroupList("Organization/GetRightGroupList/", nid).Where(e => e.bExist == "True").ToList(), JsonRequestBehavior.AllowGet);
                }
            }
        }
        public ActionResult GetUnit(string text)
        {

            List<Unit> resultList = new List<Unit>();
            //APIAccessBLL apiBLL = new APIAccessBLL();
            var CostCenters = APIAccessBLL.GetUnit(0);
            if (CostCenters != null)
            {
                resultList = CostCenters;
            }
            if (text != "" && text != null)
            {
                resultList = resultList.Where(e => e.Description.ToLower().IndexOf(text.Trim().ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateMember([DataSourceRequest] DataSourceRequest request, ManageViewModels uf)
        {


            string json = JsonConvert.SerializeObject(uf);
            json = json.Replace(":null", ":\"\"");
            uf = JsonConvert.DeserializeObject<ManageViewModels>(json);

            List<ManageViewModels> resultuf = new APIAccessBLL().APICall("CreateMember", "Organization", Method.PUT, null, new ManageViewModels(), new
            {
                vms = uf
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetGroup([DataSourceRequest]DataSourceRequest request)
        {
            List<GroupViewModels> result = new List<GroupViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            // var Manage = apiBLL.APICall("Members", "Organization", Method.GET, new List<Parameter>(), new UserInfo());
            var Group = apiBLL.GetGroup("Organization/GetGroup/");
            //string json = JsonConvert.SerializeObject(Manage);
            //List<ManageViewModels> m = JsonConvert.DeserializeObject<List<ManageViewModels>>(json);
            result = Group.Select(e => new GroupViewModels
            {
                ID = e.ID,
                GroupName = e.GroupName,
                Description = e.Description
            }).ToList();
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateGroup([DataSourceRequest] DataSourceRequest request, GroupViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<GroupViewModels>(json);

            List<GroupViewModels> resultuf = new APIAccessBLL().APICall("CreateGroup", "Organization", Method.PUT, null, new GroupViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult UpdateGroup([DataSourceRequest] DataSourceRequest request, GroupViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<GroupViewModels>(json);

            List<GroupViewModels> resultuf = new APIAccessBLL().APICall("UpdateGroup", "Organization", Method.PUT, null, new GroupViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult DeleteGroup([DataSourceRequest] DataSourceRequest request, GroupViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<GroupViewModels>(json);

            List<GroupViewModels> resultuf = new APIAccessBLL().APICall("DeleteGroup", "Organization", Method.DELETE, null, new GroupViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateGroupMember(string models)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var ret = apiBLL.UpdateGroupMemberList("Organization/UpdateGroupMemberList/", models);

            return Json(ret, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateMemberGroup(string models)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var ret = apiBLL.UpdateMemberGroupList("Organization/UpdateMemberGroupList/", models);

            return Json(ret, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateGroupRight(string models)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var ret = apiBLL.UpdateGroupRightList("Organization/UpdateGroupRightList/", models);

            return Json(ret, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateRightGroup(string models)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var ret = apiBLL.UpdateRightGroupList("Organization/UpdateRightGroupList/", models);

            return Json(ret, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetRight([DataSourceRequest]DataSourceRequest request)
        {
            List<RightViewModels> result = new List<RightViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetRight("Organization/GetRight/");
            result = Right.Select(e => new RightViewModels
            {
                ID = e.ID,
                RightName = e.RightName,
                Description = e.Description,
                AppID = e.AppID,
                AppName = e.AppName
            }).ToList();
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateRight([DataSourceRequest] DataSourceRequest request, RightViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<RightViewModels>(json);

            List<RightViewModels> resultuf = new APIAccessBLL().APICall("CreateRight", "Organization", Method.PUT, null, new RightViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            //return Json(resultuf.FirstOrDefault(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateRight([DataSourceRequest] DataSourceRequest request, RightViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<RightViewModels>(json);

            List<RightViewModels> resultuf = new APIAccessBLL().APICall("UpdateRight", "Organization", Method.PUT, null, new RightViewModels(), new
            {
                vms = model
            });
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetRight("Organization/GetRight/");
            //resultuf = Right.Where(e=>e.ID==model.ID).Select(e => new RightViewModels
            //{
            //	ID = e.ID,
            //	RightName = e.RightName,
            //	Description = e.Description,
            //	AppID = e.AppID,
            //	AppName = e.AppName
            //}).ToList();
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            //return Json(resultuf.FirstOrDefault(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteRight([DataSourceRequest] DataSourceRequest request, RightViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<RightViewModels>(json);

            List<RightViewModels> resultuf = new APIAccessBLL().APICall("DeleteRight", "Organization", Method.DELETE, null, new RightViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetApplication()
        {
            List<ApplicationViewModels> result = new List<ApplicationViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetApplication("Organization/GetApplication/");
            result = Right.Select(e => new ApplicationViewModels
            {
                ID = e.ID,
                AppName = e.AppName
            }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProcessOption(int processID)
        {
            List<ProcessOptionViewModels> result = new List<ProcessOptionViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetProcessOption("Organization/GetProcessOption/", processID);
            result = Right.Select(e => new ProcessOptionViewModels
            {
                ProcessID = e.ProcessID,
                ApprovalMartixOptionCode = e.ApprovalMartixOptionCode,
                ApprovalMartixOptionName = e.ApprovalMartixOptionName
            }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRoleORJobLevel(string RoleORJobLevel)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            // if (RoleORJobLevel == "JL")
            // {
            //     APIAccessBLL apiBLL = new APIAccessBLL();
            //     var Right = apiBLL.GetJobLevel("Organization/GetJobLeveln/");
            //     result = Right.Select(e => new ApprovalCriteriaViewModels
            //     {
            //         ApprovalCriteriaID = e.JobLevelId,
            //         ApprovalCriteriaName = e.JobLevel,
            //         Remark = "JL"
            //     }).ToList();
            // }
            //else if (RoleORJobLevel == "R")
            // {
            //     APIAccessBLL apiBLL = new APIAccessBLL();
            //     var Right = apiBLL.GetRoleList("Organization/GetRoleList/");
            //     result = Right.Select(e => new ApprovalCriteriaViewModels
            //     {
            //         ApprovalCriteriaID = e.RoletId,
            //         ApprovalCriteriaName = e.RoleName,
            //         Remark = "R"
            //     }).ToList();
            // }
            if (RoleORJobLevel == "JL")
            {
                APIAccessBLL apiBLL = new APIAccessBLL();
                var Right = apiBLL.GetJobLevel("Organization/GetJobLeveln/");
                result = Right.Select(e => new SelectListItem
                {
                    Value = e.JobLevelId.ToString(),
                    Text = e.JobLevel
                }).ToList();
            }
            else if (RoleORJobLevel == "R")
            {
                APIAccessBLL apiBLL = new APIAccessBLL();
                var Right = apiBLL.GetRoleList("Organization/GetRoleList/");
                result = Right.Select(e => new SelectListItem
                {
                    Value = e.RoletId.ToString(),
                    Text = e.RoleName,
                }).ToList();
            }


            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListJobLevel()
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var result = apiBLL.GetJobLevel("Organization/GetJobLeveln/").Select(e => new SelectListItem
            {
                Value = e.JobLevelId.ToString(),
                Text = e.JobLevel
            }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetApprovalMartix([DataSourceRequest]DataSourceRequest request)
        {
            List<ApprovalMartixViewModels> result = new List<ApprovalMartixViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetApprovalMartix("Organization/GetApprovalMartix/");
            List<ApprovalLevel> ApprovalLevelList = apiBLL.GetApprovalLevel("Organization/GetApprovalLevel/");
            result = Right.Select(e => new ApprovalMartixViewModels
            {
                ApprovalMartixId = e.ApprovalMartixId,
                ApprovalMartixName = e.ApprovalMartixName,
                ProcessId = e.ProcessId,
                ProcessName = e.ProcessName,
                UnitId = e.UnitId == null ? 0 : (int)e.UnitId,
				UnitIdText= e.UnitId == null ? "" : ((int)e.UnitId).ToString(),

				UnitName = e.UnitName,
                RoleId = e.RoleId,
                RoleName = e.RoleName,
                Priority = e.Priority,
                CopyDetailFrom = e.CopyDetailFrom,
                ApprovalMartixOptionCode = e.ApprovalMartixOptionCode,
                ApprovalMartixOptionName = e.ApprovalMartixOptionName,
                ApprovalMartixDetailList = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList),
                Supervisor1_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[0].ApprovalCriteria,
                Supervisor1_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[0].ApprovalCriteriaType,
                Supervisor2_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[1].ApprovalCriteria,
                Supervisor2_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[1].ApprovalCriteriaType,
                AssistantManger_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[2].ApprovalCriteria,
                AssistantManger_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[2].ApprovalCriteriaType,
                DepartmentManager_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[3].ApprovalCriteria,
                DepartmentManager_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[3].ApprovalCriteriaType,
                SeniorManager_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[4].ApprovalCriteria,
                SeniorManager_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[4].ApprovalCriteriaType,
                DepartmentGeneralManager_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[5].ApprovalCriteria,
                DepartmentGeneralManager_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[5].ApprovalCriteriaType,
                GeneralManager_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[6].ApprovalCriteria,
                GeneralManager_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[6].ApprovalCriteriaType,
                GPAD_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[7].ApprovalCriteria,
                GPAD_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[7].ApprovalCriteriaType,
                MD_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[8].ApprovalCriteria,
                MD_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[8].ApprovalCriteriaType,
                ACVerification_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[9].ApprovalCriteria,
                ACVerification_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[9].ApprovalCriteriaType,
                ACVerification2_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[10].ApprovalCriteria,
                ACVerification2_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[10].ApprovalCriteriaType,
                ACApproval_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[11].ApprovalCriteria,
                ACApproval_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[11].ApprovalCriteriaType,
                HRVerification_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[12].ApprovalCriteria,
                HRVerification_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[12].ApprovalCriteriaType,
                HRVerification2_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[13].ApprovalCriteria,
                HRVerification2_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[13].ApprovalCriteriaType,
                HRApprova_Criteria = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[14].ApprovalCriteria,
                HRApprova_CriteriaType = getApprovalMartixDetailView(e.ApprovalMartixDetailList, ApprovalLevelList)[14].ApprovalCriteriaType

                //  ApprovalMartixDetailList=e

            }).ToList();

            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetApprovalMartixList(int ProcessId, string ApprovalMartixOptionCode, int? UnitId = 0)
        {
            bool Success = true;
            //ApprovalMartixOptionCode = ApprovalMartixOptionCode == "" ? null : ApprovalMartixOptionCode;
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetApprovalMartix("Organization/GetApprovalMartix/");
            List<ApprovalLevel> ApprovalLevelList = apiBLL.GetApprovalLevel("Organization/GetApprovalLevel/");
            List<ApprovalMartixViewModels> result = new List<ApprovalMartixViewModels>();
            result = Right.Select(e => new ApprovalMartixViewModels
            {
                ApprovalMartixId = e.ApprovalMartixId,
                ApprovalMartixName = e.ApprovalMartixName,
                ProcessId = e.ProcessId,
                ProcessName = e.ProcessName,
                UnitId = e.UnitId == null ? 0 : (int)e.UnitId,
                UnitName = e.UnitName,
                RoleId = e.RoleId,
                RoleName = e.RoleName,
                Priority = e.Priority,
                CopyDetailFrom = e.CopyDetailFrom,
                ApprovalMartixOptionCode = e.ApprovalMartixOptionCode,
                ApprovalMartixOptionName = e.ApprovalMartixOptionName
            }).ToList();
            int Count = result.Where(e => e.ProcessId == ProcessId
        && e.UnitId == UnitId
        && e.ApprovalMartixOptionCode == ApprovalMartixOptionCode).Count();

            if (Count == 0)
            {
                Success = false;
            }

            return Json(new MessageViewModel() { Success = Success, Title = "" });
        }
        public List<ApprovalMartixDetailGridModel> getApprovalMartixDetailView(List<ApprovalMartixDetailModel> list, List<ApprovalLevel> Right)
        {


            List<ApprovalMartixDetailGridModel> ViewList = new List<ApprovalMartixDetailGridModel>();

            //Models.ApprovalMartixDetailModel model1 = new Models.ApprovalMartixDetailModel();
            //model1.ApprovalLevelCode = " Supervisor1";
            //var ApprovalLevel1 = Right.Where(e => e.ApprovalLevelCode == " Supervisor1").FirstOrDefault();
            //model1.ApprovalLevelId = ApprovalLevel1 == null ? 0 : ApprovalLevel1.ApprovalLevelId;
            //ViewList.Add(model1);
            foreach (var item in Right)
            {
                ApprovalMartixDetailGridModel model = new ApprovalMartixDetailGridModel();
                bool exist = false;
                foreach (var item2 in list)
                {
                    if (item2.ApprovalLevelCode == item.ApprovalLevelCode)
                    {
                        exist = true;
                        model.ApprovalLevelId = item2.ApprovalLevelId;
                        model.ApprovalLevelCode = item2.ApprovalLevelCode;
                        model.ApprovalLevelName = item2.ApprovalLevelName;
                        model.ApprovalCriteriaType = item2.ApprovalCriteriaType;
                        model.ApprovalCriteria = item2.ApprovalCriteria;

                    }
                }
                if (!exist)
                {
                    model.ApprovalLevelId = item.ApprovalLevelId;
                    model.ApprovalLevelCode = item.ApprovalLevelCode;
                    model.ApprovalLevelName = item.ApprovalLevelName;
                    model.ApprovalCriteriaType = "";
                    model.ApprovalCriteria = "";
                }

                ViewList.Add(model);
            }


            return ViewList;
        }
        public List<ApprovalMartixDetailGridModel> getApprovalMartixDetailGridView(List<ApprovalMartixDetailGridModel> list, List<ApprovalLevel> Right)
        {


            List<ApprovalMartixDetailGridModel> ViewList = new List<ApprovalMartixDetailGridModel>();
            foreach (var item in Right)
            {
                ApprovalMartixDetailGridModel model = new ApprovalMartixDetailGridModel();
                bool exist = false;
                foreach (var item2 in list)
                {
                    if (item2.ApprovalLevelId == item.ApprovalLevelId)
                    {
                        exist = true;
                        model.ApprovalLevelId = item2.ApprovalLevelId;
                        model.ApprovalLevelCode = item2.ApprovalLevelCode;
                        model.ApprovalLevelName = item2.ApprovalLevelName;
                        model.ApprovalCriteriaType = item2.ApprovalCriteriaType;
                        model.ApprovalCriteria = item2.ApprovalCriteria;

                    }
                }
                if (!exist)
                {
                    model.ApprovalLevelId = item.ApprovalLevelId;
                    model.ApprovalLevelCode = item.ApprovalLevelCode;
                    model.ApprovalLevelName = item.ApprovalLevelName;
                    model.ApprovalCriteriaType = "";
                    model.ApprovalCriteria = "";
                }

                ViewList.Add(model);
            }


            return ViewList;
        }

        public List<ApprovalMartixDetailGridModel> steApprovalMartixDetailGridModelList(ApprovalMartixViewModels db)
        {
            List<ApprovalMartixDetailGridModel> list = new List<ApprovalMartixDetailGridModel>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetApprovalLevel("Organization/GetApprovalLevel/");
            ApprovalMartixDetailGridModel model1 = new ApprovalMartixDetailGridModel();
            model1.ApprovalMartixId = db.ApprovalMartixId;
            model1.ApprovalLevelId = Right[0].ApprovalLevelId;
            model1.ApprovalCriteriaType = db.Supervisor1_CriteriaType;
            model1.ApprovalCriteria = db.Supervisor1_Criteria;
            list.Add(model1);
            ApprovalMartixDetailGridModel model2 = new ApprovalMartixDetailGridModel();
            model2.ApprovalMartixId = db.ApprovalMartixId;
            model2.ApprovalLevelId = Right[1].ApprovalLevelId;
            model2.ApprovalCriteriaType = db.Supervisor2_CriteriaType;
            model2.ApprovalCriteria = db.Supervisor2_Criteria;
            list.Add(model2);
            ApprovalMartixDetailGridModel model3 = new ApprovalMartixDetailGridModel();
            model3.ApprovalMartixId = db.ApprovalMartixId;
            model3.ApprovalLevelId = Right[2].ApprovalLevelId;
            model3.ApprovalCriteriaType = db.AssistantManger_CriteriaType;
            model3.ApprovalCriteria = db.AssistantManger_Criteria;
            list.Add(model3);
            ApprovalMartixDetailGridModel model4 = new ApprovalMartixDetailGridModel();
            model4.ApprovalMartixId = db.ApprovalMartixId;
            model4.ApprovalLevelId = Right[3].ApprovalLevelId;
            model4.ApprovalCriteriaType = db.DepartmentManager_CriteriaType;
            model4.ApprovalCriteria = db.DepartmentManager_Criteria;
            list.Add(model4);
            ApprovalMartixDetailGridModel model5 = new ApprovalMartixDetailGridModel();
            model5.ApprovalMartixId = db.ApprovalMartixId;
            model5.ApprovalLevelId = Right[4].ApprovalLevelId;
            model5.ApprovalCriteriaType = db.SeniorManager_CriteriaType;
            model5.ApprovalCriteria = db.SeniorManager_Criteria;
            list.Add(model5);
            ApprovalMartixDetailGridModel model6 = new ApprovalMartixDetailGridModel();
            model6.ApprovalMartixId = db.ApprovalMartixId;
            model6.ApprovalLevelId = Right[5].ApprovalLevelId;
            model6.ApprovalCriteriaType = db.DepartmentGeneralManager_CriteriaType;
            model6.ApprovalCriteria = db.DepartmentGeneralManager_Criteria;
            list.Add(model6);
            ApprovalMartixDetailGridModel model7 = new ApprovalMartixDetailGridModel();
            model7.ApprovalMartixId = db.ApprovalMartixId;
            model7.ApprovalLevelId = Right[6].ApprovalLevelId;
            model7.ApprovalCriteriaType = db.GeneralManager_CriteriaType;
            model7.ApprovalCriteria = db.GeneralManager_Criteria;
            list.Add(model7);
            ApprovalMartixDetailGridModel model8 = new ApprovalMartixDetailGridModel();
            model8.ApprovalMartixId = db.ApprovalMartixId;
            model8.ApprovalLevelId = Right[7].ApprovalLevelId;
            model8.ApprovalCriteriaType = db.GPAD_CriteriaType;
            model8.ApprovalCriteria = db.GPAD_Criteria;
            list.Add(model8);
            ApprovalMartixDetailGridModel model9 = new ApprovalMartixDetailGridModel();
            model9.ApprovalMartixId = db.ApprovalMartixId;
            model9.ApprovalLevelId = Right[8].ApprovalLevelId;
            model9.ApprovalCriteriaType = db.MD_CriteriaType;
            model9.ApprovalCriteria = db.MD_Criteria;
            list.Add(model9);
            ApprovalMartixDetailGridModel model10 = new ApprovalMartixDetailGridModel();
            model10.ApprovalMartixId = db.ApprovalMartixId;
            model10.ApprovalLevelId = Right[9].ApprovalLevelId;
            model10.ApprovalCriteriaType = db.ACVerification_CriteriaType;
            model10.ApprovalCriteria = db.ACVerification_Criteria;
            list.Add(model10);
            ApprovalMartixDetailGridModel model11 = new ApprovalMartixDetailGridModel();
            model11.ApprovalMartixId = db.ApprovalMartixId;
            model11.ApprovalLevelId = Right[10].ApprovalLevelId;
            model11.ApprovalCriteriaType = db.ACVerification2_CriteriaType;
            model11.ApprovalCriteria = db.ACVerification2_Criteria;
            list.Add(model11);
            ApprovalMartixDetailGridModel model12 = new ApprovalMartixDetailGridModel();
            model12.ApprovalMartixId = db.ApprovalMartixId;
            model12.ApprovalLevelId = Right[11].ApprovalLevelId;
            model12.ApprovalCriteriaType = db.ACApproval_CriteriaType;
            model12.ApprovalCriteria = db.ACApproval_Criteria;
            list.Add(model12);
            ApprovalMartixDetailGridModel model13 = new ApprovalMartixDetailGridModel();
            model13.ApprovalMartixId = db.ApprovalMartixId;
            model13.ApprovalLevelId = Right[12].ApprovalLevelId;
            model13.ApprovalCriteriaType = db.HRVerification_CriteriaType;
            model13.ApprovalCriteria = db.HRVerification_Criteria;
            list.Add(model13);
            ApprovalMartixDetailGridModel model14 = new ApprovalMartixDetailGridModel();
            model14.ApprovalMartixId = db.ApprovalMartixId;
            model14.ApprovalLevelId = Right[13].ApprovalLevelId;
            model14.ApprovalCriteriaType = db.HRVerification2_CriteriaType;
            model14.ApprovalCriteria = db.HRVerification2_Criteria;
            list.Add(model14);
            ApprovalMartixDetailGridModel model15 = new ApprovalMartixDetailGridModel();
            model15.ApprovalMartixId = db.ApprovalMartixId;
            model15.ApprovalLevelId = Right[14].ApprovalLevelId;
            model15.ApprovalCriteriaType = db.HRApprova_CriteriaType;
            model15.ApprovalCriteria = db.HRApprova_Criteria;
            list.Add(model15);

            return list;
        }

        public List<ApprovalMartixViewModels> getApprovalMartixDetailGridViewModel(List<ApprovalMartixViewModels> resultuf)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetApprovalMartix("Organization/GetApprovalMartix/");
            List<ApprovalLevel> ApprovalLevelList = apiBLL.GetApprovalLevel("Organization/GetApprovalLevel/");
			return resultuf = resultuf.Select(e => new ApprovalMartixViewModels
			{
				ApprovalMartixId = e.ApprovalMartixId,
				ApprovalMartixName = e.ApprovalMartixName,
				ProcessId = e.ProcessId,
				ProcessName = e.ProcessName,
				UnitId = e.UnitId,
				UnitIdText = e.UnitId != null ? e.UnitId.ToString():"",
                UnitName = e.UnitName,
                RoleId = e.RoleId,
                RoleName = e.RoleName,
                Priority = e.Priority,
                CopyDetailFrom = e.CopyDetailFrom,
                ApprovalMartixOptionCode = e.ApprovalMartixOptionCode,
                ApprovalMartixOptionName = e.ApprovalMartixOptionName,
                ApprovalMartixDetailList = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList),
                Supervisor1_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[0].ApprovalCriteria,
                Supervisor1_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[0].ApprovalCriteriaType,
                Supervisor2_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[1].ApprovalCriteria,
                Supervisor2_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[1].ApprovalCriteriaType,
                AssistantManger_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[2].ApprovalCriteria,
                AssistantManger_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[2].ApprovalCriteriaType,
                DepartmentManager_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[3].ApprovalCriteria,
                DepartmentManager_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[3].ApprovalCriteriaType,
                SeniorManager_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[4].ApprovalCriteria,
                SeniorManager_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[4].ApprovalCriteriaType,
                DepartmentGeneralManager_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[5].ApprovalCriteria,
                DepartmentGeneralManager_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[5].ApprovalCriteriaType,
                GeneralManager_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[6].ApprovalCriteria,
                GeneralManager_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[6].ApprovalCriteriaType,
                GPAD_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[7].ApprovalCriteria,
                GPAD_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[7].ApprovalCriteriaType,
                MD_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[8].ApprovalCriteria,
                MD_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[8].ApprovalCriteriaType,
                ACVerification_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[9].ApprovalCriteria,
                ACVerification_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[9].ApprovalCriteriaType,
                ACVerification2_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[10].ApprovalCriteria,
                ACVerification2_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[10].ApprovalCriteriaType,
                ACApproval_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[11].ApprovalCriteria,
                ACApproval_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[11].ApprovalCriteriaType,
                HRVerification_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[12].ApprovalCriteria,
                HRVerification_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[12].ApprovalCriteriaType,
                HRVerification2_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[13].ApprovalCriteria,
                HRVerification2_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[13].ApprovalCriteriaType,
                HRApprova_Criteria = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[14].ApprovalCriteria,
                HRApprova_CriteriaType = getApprovalMartixDetailGridView(e.ApprovalMartixDetailList, ApprovalLevelList)[14].ApprovalCriteriaType

                //  ApprovalMartixDetailList=e

            }).ToList();
        }
        public ActionResult CreateApprovalMartix([DataSourceRequest] DataSourceRequest request, ApprovalMartixViewModels model)
        {
			if (model.UnitIdText != null)
			{
				model.UnitId = int.Parse(model.UnitIdText);
			}

            model.ApprovalMartixDetailList = steApprovalMartixDetailGridModelList(model);

            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<ApprovalMartixViewModels>(json);

            List<ApprovalMartixViewModels> resultuf = new APIAccessBLL().APICall("CreateApprovalMartix", "Organization", Method.PUT, null, new ApprovalMartixViewModels(), new
            {
                vms = model
            });
            resultuf = getApprovalMartixDetailGridViewModel(resultuf).ToList();
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult UpdateApprovalMartix([DataSourceRequest] DataSourceRequest request, ApprovalMartixViewModels model)
        {
			if (model.UnitIdText != null)
			{
				model.UnitId = int.Parse(model.UnitIdText);
			}
			model.ApprovalMartixDetailList = steApprovalMartixDetailGridModelList(model);
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<ApprovalMartixViewModels>(json);

            List<ApprovalMartixViewModels> resultuf = new APIAccessBLL().APICall("UpdateApprovalMartix", "Organization", Method.PUT, null, new ApprovalMartixViewModels(), new
            {
                vms = model
            });
            resultuf = getApprovalMartixDetailGridViewModel(resultuf).ToList();
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult DeleteApprovalMartix([DataSourceRequest] DataSourceRequest request, ApprovalMartixViewModels model)
        {
            model.ApprovalMartixDetailList = steApprovalMartixDetailGridModelList(model);
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<ApprovalMartixViewModels>(json);

            List<ApprovalMartixViewModels> resultuf = new APIAccessBLL().APICall("DeleteApprovalMartix", "Organization", Method.DELETE, null, new ApprovalMartixViewModels(), new
            {
                vms = model
            });
            resultuf = getApprovalMartixDetailGridViewModel(resultuf).ToList();
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProcessList()
        {
            List<ProcessListViewModels> result = new List<ProcessListViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetProcessList("Organization/GetProcessList/");
            result = Right.Select(e => new ProcessListViewModels
            {
                ProcessId = e.ProcessId,
                ProcessName = e.ProcessName,
                Code = e.Code
            }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUnitList(string text)
        {
            List<UnitViewModels> result = new List<UnitViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetUnitList("Organization/GetUnitList/").OrderBy(e => e.UnitName);
            result = Right.Select(e => new UnitViewModels
            {
                UnitId = e.UnitId,
                UnitIdText = e.UnitId.ToString(),
                UnitName = e.UnitName
            }).ToList();
            if (text != "" && text != null)
            {
                result = result.Where(e => e.UnitName.ToLower().IndexOf(text.Trim().ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRoleList()
        {
            List<RoleViewModels> result = new List<RoleViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetRoleList("Organization/GetRoleList/");
            result = Right.Select(e => new RoleViewModels
            {
                RoletId = e.RoletId,
                RoleName = e.RoleName,
                Remark = e.Remark
            }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetApprovalMartixOption()
        {
            List<ApprovalMartixOptionViewModels> result = new List<ApprovalMartixOptionViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetApprovalMartixOption("Organization/GetApprovalMartixOption/");
            result = Right.Select(e => new ApprovalMartixOptionViewModels
            {
                ApprovalMartixOptionCode = e.ApprovalMartixOptionCode,
                ApprovalMartixOptionName = e.ApprovalMartixOptionName
            }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetApprovalCriteriaType()
        {
            List<SelectListItem> result = new List<SelectListItem>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetApprovalCriteriaType("Organization/GetApprovalCriteriaType/");
            result = Right.Select(e => new SelectListItem
            {
                Text = e.ApprovalCriteriaType1,
                Value = e.ApprovalCriteriaType1
            }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_ACCOUNT_ADMIN_PAGE_RIGHT)]
        public ActionResult CostCenter()
        {
            SetBreadcrumbs("Admin", "CostCenter");
            return View();
        }
        public ActionResult GetCostCenter([DataSourceRequest]DataSourceRequest request)
        {
            List<CostCenterViewModels> result = new List<CostCenterViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var CostCenter = apiBLL.GetCostCenter("Organization/GetCostCenter/");
            result = CostCenter.Select(e => new CostCenterViewModels
            {
                CostCenterId = e.CostCenterId,
                Code = e.Code,
                Description = e.Description,
                PIC = e.PIC
            }).ToList();
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateCostCenter([DataSourceRequest] DataSourceRequest request, CostCenterViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<CostCenterViewModels>(json);

            List<CostCenterViewModels> resultuf = new APIAccessBLL().APICall("CreateCostCenter", "Organization", Method.PUT, null, new CostCenterViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult UpdateCostCenter([DataSourceRequest] DataSourceRequest request, CostCenterViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<CostCenterViewModels>(json);

            List<CostCenterViewModels> resultuf = new APIAccessBLL().APICall("UpdateCostCenter", "Organization", Method.PUT, null, new CostCenterViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult DeleteCostCenter([DataSourceRequest] DataSourceRequest request, CostCenterViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<CostCenterViewModels>(json);

            List<CostCenterViewModels> resultuf = new APIAccessBLL().APICall("DeleteCostCenter", "Organization", Method.DELETE, null, new CostCenterViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_IS_ADMIN_PAGE_RIGHT)]
        public ActionResult Role()
        {
            SetBreadcrumbs("Admin", "Role");
            return View();
        }
        public ActionResult GetRoleGridList([DataSourceRequest]DataSourceRequest request)
        {
            List<RoleViewModels> result = new List<RoleViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var Right = apiBLL.GetRoleList("Organization/GetRoleList/");
            result = Right.Select(e => new RoleViewModels
            {
                RoletId = e.RoletId,
                RoleName = e.RoleName,
                Remark = e.Remark
            }).ToList();
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateRole([DataSourceRequest] DataSourceRequest request, RoleViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<RoleViewModels>(json);

            List<RoleViewModels> resultuf = new APIAccessBLL().APICall("CreateRole", "Organization", Method.PUT, null, new RoleViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult UpdateRole([DataSourceRequest] DataSourceRequest request, RoleViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<RoleViewModels>(json);

            List<RoleViewModels> resultuf = new APIAccessBLL().APICall("UpdateRole", "Organization", Method.PUT, null, new RoleViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult DeleteRole([DataSourceRequest] DataSourceRequest request, RoleViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<RoleViewModels>(json);

            List<RoleViewModels> resultuf = new APIAccessBLL().APICall("DeleteRole", "Organization", Method.DELETE, null, new RoleViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRoleMemberViewModelList(int nid, string bExist)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            if (bExist == null)
            {
                return Json(apiBLL.GetRoleMemberList("Organization/GetRoleMemberList/", nid), JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (bExist == "0")
                {
                    return Json(apiBLL.GetRoleMemberList("Organization/GetRoleMemberList/", nid).Where(e => e.bExist == "False").ToList(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(apiBLL.GetRoleMemberList("Organization/GetRoleMemberList/", nid).Where(e => e.bExist == "True").ToList(), JsonRequestBehavior.AllowGet);
                }
            }
        }
        public JsonResult UpdateRoleMemberList(string models)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var ret = apiBLL.UpdateGroupMemberList("Organization/UpdateRoleMemberList/", models);


            return Json(ret, JsonRequestBehavior.AllowGet);
        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_IS_ADMIN_PAGE_RIGHT)]
        public ActionResult SiteMenu()
        {
            SetBreadcrumbs("Admin", "SiteMenu");
            return View();
        }
        public JsonResult GetSiteMenuParent(string text, int? ID = 0)
        {
            List<SiteMenuViewModels> result = new List<SiteMenuViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var SiteMenu = apiBLL.GetSiteMenu("Organization/GetSiteMenu/");
            result = SiteMenu.Select(e => new SiteMenuViewModels
            {
                ID = e.ID,
                PageName = e.PageName,
            }).ToList();
            if (text != "" && text != null)
            {
                result = result.Where(e => e.PageName.ToLower().IndexOf(text.Trim().ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            if (ID != 0)
            {
                result = result.Where(e => e.ID != ID).ToList();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSiteMenu([DataSourceRequest]DataSourceRequest request)
        {
            List<SiteMenuViewModels> result = new List<SiteMenuViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var SiteMenu = apiBLL.GetSiteMenu("Organization/GetSiteMenu/");
            result = SiteMenu.Select(e => new SiteMenuViewModels
            {
                ID = e.ID,
                PageName = e.PageName,
                PageDisplayName = e.PageDisplayName,
                PageNameDisplayName_CN = e.PageNameDisplayName_CN,
                URL = e.URL,
                //AppID = e.AppID,
                //AppName = e.AppName,
                ParentID = e.ParentID == null ? 0 : (int)e.ParentID,
                ParentIDStr = e.ParentID + "",

                ParentName = e.ParentName
            }).ToList();
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateSiteMenu([DataSourceRequest] DataSourceRequest request, SiteMenuViewModels model)
        {
            model.ParentID = model.ParentIDStr == null ? 0 : Convert.ToInt32(model.ParentIDStr);
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<SiteMenuViewModels>(json);

            List<SiteMenuViewModels> resultuf = new APIAccessBLL().APICall("CreateSiteMenu", "Organization", Method.PUT, null, new SiteMenuViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult UpdateSiteMenu([DataSourceRequest] DataSourceRequest request, SiteMenuViewModels model)
        {
            model.ParentID = model.ParentIDStr == null ? 0 : Convert.ToInt32(model.ParentIDStr);
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<SiteMenuViewModels>(json);

            List<SiteMenuViewModels> resultuf = new APIAccessBLL().APICall("UpdateSiteMenu", "Organization", Method.PUT, null, new SiteMenuViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult DeleteSiteMenu([DataSourceRequest] DataSourceRequest request, SiteMenuViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<SiteMenuViewModels>(json);

            List<SiteMenuViewModels> resultuf = new APIAccessBLL().APICall("DeleteSiteMenu", "Organization", Method.DELETE, null, new SiteMenuViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSiteMenuRightViewModelList(int nid, string bExist)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            if (bExist == null)
            {
                return Json(apiBLL.GetSiteMenuRightList("Organization/GetSiteMenuRightList/", nid), JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (bExist == "0")
                {
                    return Json(apiBLL.GetSiteMenuRightList("Organization/GetSiteMenuRightList/", nid).Where(e => e.bExist == "False").ToList(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(apiBLL.GetSiteMenuRightList("Organization/GetSiteMenuRightList/", nid).Where(e => e.bExist == "True").ToList(), JsonRequestBehavior.AllowGet);
                }
            }
        }
        public JsonResult UpdateSiteMenuRightList(string models)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var ret = apiBLL.UpdateGroupMemberList("Organization/UpdateSiteMenuRightList/", models);


            return Json(ret, JsonRequestBehavior.AllowGet);
        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_HR_ADMIN_PAGE_RIGHT)]
        public ActionResult EmailTemplate()
        {
            SetBreadcrumbs("Admin", "EmailTemplate");
            return View();
        }
        public List<EmailTemplateViewModels> getEmailTemplateList()
        {
            List<EmailTemplateViewModels> resultuf = new List<EmailTemplateViewModels>();
            try
            {
                AdminBLL bll = new AdminBLL();
                resultuf = bll.GetEmailTemplate().Select(e => new EmailTemplateViewModels
                {
                    ID = e.ID,
                    ProcessCode = e.ProcessCode,
                    ProcessName = e.ProcessName,
                    Subject = e.Subject,
                    Body = e.Body,
                    SubjectView = e.Subject.Length > 20 ? e.Subject.Substring(0, 20) + "..." : e.Subject,
                    BodyView = e.Body.Length > 20 ? e.Body.Substring(0, 20) + "..." : e.Body,
                    ContentType = e.ContentType,
                }).ToList();
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return resultuf;
        }
        public ActionResult GetEmailTemplate([DataSourceRequest]DataSourceRequest request)
        {
            List<EmailTemplateViewModels> resultuf = new List<EmailTemplateViewModels>();
            resultuf = getEmailTemplateList();
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult CreateEmailTemplate([DataSourceRequest] DataSourceRequest request, EmailTemplateViewModels model)
        {
            List<EmailTemplateViewModels> resultuf = new List<EmailTemplateViewModels>();
            try
            {
                EmailTemplate db = new EmailTemplate();
                db.ID = model.ID;
                db.ProcessCode = model.ProcessCode;
                db.StepID = -1;
                db.Subject = model.Subject;
                db.Body = model.Body;
                db.ContentType = model.ContentType;
                db.IsDeleted = false;
                AdminBLL bll = new AdminBLL();
                db = bll.CreateEmailTemplate(db);
                model = getEmailTemplateList().Where(e => e.ID == db.ID).FirstOrDefault();
                resultuf.Add(model);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult UpdateEmailTemplate([DataSourceRequest] DataSourceRequest request, EmailTemplateViewModels model)
        {
            List<EmailTemplateViewModels> resultuf = new List<EmailTemplateViewModels>();
            try
            {
                EmailTemplate db = new EmailTemplate();
                db.ID = model.ID;
                db.ProcessCode = model.ProcessCode;
                db.StepID = -1;
                db.Subject = model.Subject;
                db.Body = model.Body;
                db.ContentType = model.ContentType;
                db.IsDeleted = false;
                AdminBLL bll = new AdminBLL();
                db = bll.UpdateEmailTemplate(db);
                model = getEmailTemplateList().Where(e => e.ID == db.ID).FirstOrDefault();
                resultuf.Add(model);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult DeleteEmailTemplate([DataSourceRequest] DataSourceRequest request, EmailTemplateViewModels model)
        {
            List<EmailTemplateViewModels> resultuf = new List<EmailTemplateViewModels>();
            try
            {
                EmailTemplate db = new EmailTemplate();
                db.ID = model.ID;
                db.ProcessCode = model.ProcessCode;
                db.StepID = -1;
                db.Subject = model.Subject;
                db.Body = model.Body;
                db.ContentType = model.ContentType;
                db.IsDeleted = true;
                AdminBLL bll = new AdminBLL();
                db = bll.UpdateEmailTemplate(db);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_ACCOUNT_ADMIN_PAGE_RIGHT)]
        public ActionResult Currency()
        {
            SetBreadcrumbs("Admin", "Currency");
            return View();
        }
        public List<CurrencyViewModels> GetCurrencyList()
        {
            List<CurrencyViewModels> resultuf = new List<CurrencyViewModels>();
            try
            {
                AdminBLL bll = new AdminBLL();
                resultuf = bll.GetCurrency().Select(e => new CurrencyViewModels
                {
                    CurrencyID = e.CurrencyID,
                    CurrencyName = e.CurrencyName,
                    SortSeq = e.SortSeq == null ? 1 : (int)e.SortSeq,
                    ExchangeRate = e.ExchangeRate,
                    Remark = e.Remark
                }).OrderBy(e => e.SortSeq).ToList();
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return resultuf;
        }
        public ActionResult GetCurrency([DataSourceRequest]DataSourceRequest request)
        {
            List<CurrencyViewModels> resultuf = new List<CurrencyViewModels>();
            resultuf = GetCurrencyList();
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult CreateCurrency([DataSourceRequest] DataSourceRequest request, CurrencyViewModels model)
        {
            List<CurrencyViewModels> resultuf = new List<CurrencyViewModels>();
            try
            {
                //int SortSeq = 1;
                //CurrencyViewModels Currency = GetCurrencyList().OrderByDescending(e => e.SortSeq).FirstOrDefault();
                //if (Currency != null)
                //{
                //    SortSeq = (Currency.SortSeq == null ? 0 : (int)Currency.SortSeq) + 1;
                //}
                Currency db = new Currency();
                db.CurrencyID = model.CurrencyID;
                db.CurrencyName = model.CurrencyName;
                db.SortSeq = model.SortSeq;
                db.ExchangeRate = model.ExchangeRate;
                db.Remark = model.Remark;
                db.CreatedDate = DateTime.Now;
                AdminBLL bll = new AdminBLL();
                db = bll.CreateCurrency(db);
                model = GetCurrencyList().Where(e => e.CurrencyID == db.CurrencyID).FirstOrDefault();
                resultuf.Add(model);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult UpdateCurrency([DataSourceRequest] DataSourceRequest request, CurrencyViewModels model)
        {
            List<CurrencyViewModels> resultuf = new List<CurrencyViewModels>();
            try
            {
                Currency db = new Currency();
                db.CurrencyID = model.CurrencyID;
                db.CurrencyName = model.CurrencyName;
                db.SortSeq = model.SortSeq;
                db.ExchangeRate = model.ExchangeRate;
                db.Remark = model.Remark;
                db.ModifiedDate = DateTime.Now;
                AdminBLL bll = new AdminBLL();
                db = bll.UpdateCurrency(db);
                model = GetCurrencyList().Where(e => e.CurrencyID == db.CurrencyID).FirstOrDefault();
                resultuf.Add(model);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult DeleteCurrency([DataSourceRequest] DataSourceRequest request, CurrencyViewModels model)
        {
            List<CurrencyViewModels> resultuf = new List<CurrencyViewModels>();
            try
            {
                Currency db = new Currency();
                db.CurrencyID = model.CurrencyID;
                db.CurrencyName = model.CurrencyName;
                db.SortSeq = model.SortSeq;
                db.ExchangeRate = model.ExchangeRate;
                db.Remark = model.Remark;
                db.ModifiedDate = DateTime.Now;
                AdminBLL bll = new AdminBLL();
                db = bll.DeleteCurrency(db);
                // model = GetCurrencyList().Where(e => e.CurrencyID == db.CurrencyID).FirstOrDefault();
                //resultuf.Add(model);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_HR_ADMIN_PAGE_RIGHT)]
        public ActionResult SystemParameter()
        {
            SetBreadcrumbs("Admin", "SystemParameter");
            return View();
        }

        public ActionResult GetListSystemParameter([DataSourceRequest]DataSourceRequest request)
        {
            List<SystemParameterViewModels> result = new List<SystemParameterViewModels>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            result = apiBLL.GetListSystemParameter("Organization/GetListSystemParameter/");

            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateSystemParameter([DataSourceRequest] DataSourceRequest request, SystemParameterViewModels model)
        {
            string json = JsonConvert.SerializeObject(model);
            json = json.Replace(":null", ":\"\"");
            model = JsonConvert.DeserializeObject<SystemParameterViewModels>(json);

            List<SystemParameterViewModels> resultuf = new APIAccessBLL().APICall("UpdateSystemParameter", "Organization", Method.PUT, null, new SystemParameterViewModels(), new
            {
                vms = model
            });
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_HR_ADMIN_PAGE_RIGHT)]
        public ActionResult AlternativePayee()
        {
            SetBreadcrumbs("Admin", "AlternativePayee");
            return View();

        }
        public ActionResult GetMemberList(string text)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var resultList = apiBLL.GetMember().ToList();
            if (text != "" && text != null)
            {
                resultList = resultList.Where(e => e.MemberName.ToLower().IndexOf(text.Trim().ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetMemberById(int id)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var resultList = apiBLL.GetMemberById(id);
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListBpxMemberAlternativePayee(int nid, string bExist)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            if (bExist == null)
            {
                return Json(apiBLL.GetListBpxMemberAlternativePayee(nid), JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (bExist == "0")
                {
                    var list = apiBLL.GetListBpxMemberAlternativePayee(nid).Where(e => e.bExist == "False").ToList();
                    return Json(list, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(apiBLL.GetListBpxMemberAlternativePayee(nid).Where(e => e.bExist == "True").ToList(), JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult UpdateAlternativePayee(string models)
        {
            List<AlternativePayeeViewModel> ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AlternativePayeeViewModel>>(models);
            APIAccessBLL apiBLL = new APIAccessBLL();
            ret = apiBLL.UpdateListAlternativePayee(ret);

            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRightsById([DataSourceRequest]DataSourceRequest request, string SAMACCOUNT)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            Dictionary<string, object> dir = new Dictionary<string, object>();
            dir.Add("SAMACCOUNT", SAMACCOUNT);
            dir.Add("AppID", CommonConstcs.AppID);
            if (string.IsNullOrEmpty(SAMACCOUNT))
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var result = apiBLL.Fun_CommonAPICALL(@"Common/GetUserRight/", dir, new Right()).ToList();

            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
		//[MEHKAuthorize(PageRight = RightConst.ACCESS_HR_ADMIN_PAGE_RIGHT)]
		[MEHKAuthorize(PageRights = new string[] { RightConst.ACCESS_HR_ADMIN_PAGE_RIGHT, RightConst.ACCESS_IS_ADMIN_PAGE_RIGHT, RightConst.ACCESS_ACCOUNT_ADMIN_PAGE_RIGHT })]
		public ActionResult EntertainmentGiftLimit()
        {
            SetBreadcrumbs("Admin", "EntertainmentGiftLimit");
            return View();

        }
        public ActionResult GetEntertainmentGiftLimit([DataSourceRequest]DataSourceRequest request)
        {
            List<EntertainmentGiftLimitViewModels> resultuf = new List<EntertainmentGiftLimitViewModels>();

            resultuf = GetEntertainmentGiftLimitList();

            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public List<EntertainmentGiftLimitViewModels> GetEntertainmentGiftLimitList()
        {
            List<EntertainmentGiftLimitViewModels> resultuf = new List<EntertainmentGiftLimitViewModels>();
            AdminBLL bll = new AdminBLL();
            try
            {
                resultuf = bll.GetEntertainmentGiftLimit().Select(e => new EntertainmentGiftLimitViewModels
                {
                    ID = e.ID,
                    Text = e.Text,
                    EffectiveDate = e.EffectiveDate,
                    Value = e.Value

                }).ToList();
            }
            catch (Exception e)
            {

                this.CreateErrorLog(e);
            }
            return resultuf;
        }
        public ActionResult UpdateEntertainmentGiftLimit([DataSourceRequest] DataSourceRequest request, EntertainmentGiftLimitViewModels model)
        {
            List<EntertainmentGiftLimitViewModels> resultuf = new List<EntertainmentGiftLimitViewModels>();
            try
            {
                EntertainmentGiftLimit db = new EntertainmentGiftLimit();
                db.ID = model.ID;
                db.Text = model.Text;
                db.Value = model.Value;
                db.EffectiveDate = model.EffectiveDate;

                AdminBLL bll = new AdminBLL();
                db = bll.UpdateEntertainmentGiftLimit(db);
                model = GetEntertainmentGiftLimitList().Where(e => e.ID == db.ID).FirstOrDefault();
                resultuf.Add(model);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_IS_ADMIN_PAGE_RIGHT)]
        public ActionResult Log()
        {
            SetBreadcrumbs("Admin", "Log");
            return View();

        }
        public ActionResult GetLog([DataSourceRequest]DataSourceRequest request)
        {
            List<LogViewModels> resultuf = new List<LogViewModels>();
            AdminBLL bll = new AdminBLL();
            try
            {
                resultuf = bll.GetLog().Select(e => new LogViewModels
                {
                    ID = e.ID,
                    Date = e.Date,
                    LogType = e.LogType,
                    LogPriority = e.LogPriority,
                    ErrorCode = e.ErrorCode,
                    StackTrace = e.StackTrace,
                    Message = e.Message,
                    Source = e.Source,
                    Remark = e.Remark,
                }).ToList();

            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
		[MEHKAuthorize(PageRight = RightConst.ACCESS_IS_ADMIN_PAGE_RIGHT)]
		public ActionResult ReAssignment()
        {
            SetBreadcrumbs("Admin", "Re-Assignment");
            return View();
        }
        public ActionResult GetAssignMemberList(string text, int? MemberID)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var resultList = apiBLL.GetMember().ToList();
            if (text != "" && text != null)
            {
                resultList = resultList.Where(e => e.MemberName.ToLower().IndexOf(text.Trim().ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            if (MemberID != null)
            {
                resultList = resultList.Where(e => e.MemberId != MemberID).ToList();
            }
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateReAssignment(string Workflow, int CurrentTaskOwner, int AssignTo)
        {
            try
            {
                //string ProcessList = ConfigurationManager.AppSettings["ProcessList"];
                //if (string.IsNullOrEmpty(ProcessList))
                //{
                //	return Json("ProcessName is null", JsonRequestBehavior.AllowGet);
                //}
                //List<string> list = ProcessList.Split(';').ToList();
                //list = list.Where(e => e != "").ToList();


                APIAccessBLL apiBLL = new APIAccessBLL();
                var Right = apiBLL.GetProcessList("Organization/GetProcessList/");
                List<string> list = Right.Where(e => e.Code == Workflow).Select(e => e.K2ProcessName).ToList();

                UserInfo user = apiBLL.GetMemberById(CurrentTaskOwner);
                UserInfo AssignUser = apiBLL.GetMemberById(AssignTo);
                string UserADName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.loginDomain) + "\\" + user.SAMAccount;
                string AssignUserADName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.loginDomain) + "\\" + AssignUser.SAMAccount;
                string result = WFControl.Delegation(UserADName, AssignUserADName, list);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json("success", JsonRequestBehavior.AllowGet);
        }
        [MEHKAuthorize(PageRight = RightConst.ACCESS_ACCOUNT_ADMIN_PAGE_RIGHT)]
        public ActionResult AnnualBudget()
        {
            SetBreadcrumbs("Admin", "AnnualBudget");
            return View();
        }
        public ActionResult GetBudgetYearList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            int Year = DateTime.Now.Year;
            for (int i = 10; i > 0; i--)
            {
                list.Add(new SelectListItem { Value = Year - i + "", Text = Year - i + "" });
            }
            for (int i = 0; i < 10; i++)
            {
                list.Add(new SelectListItem { Value = Year + i + "", Text = Year + i + "" });
            }

            //list.Add(new SelectListItem { Value = Year + "", Text = Year + "" });


            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAnnualBudgetVersionTypeList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            AdminBLL bll = new AdminBLL();
            List<AnnualBudgetVersionType> typeList = bll.GetAnnualBudgetVersionType();
            foreach (var item in typeList)
            {
                list.Add(new SelectListItem { Value = item.Code, Text = item.Name });
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetvDivisionList(string text)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var resultList = apiBLL.GetvDivisionList().ToList();
            if (text != "" && text != null)
            {
                resultList = resultList.Where(e => e.UnitName.ToLower().IndexOf(text.Trim().ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }
        public string GetvDivisionName(string DivisionId)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            var resultList = apiBLL.GetvDivisionList().ToList();
            if (!string.IsNullOrEmpty(DivisionId))
            {
                var result = resultList.Where(e => e.UnitId == Convert.ToInt32(DivisionId));
                if (result!=null&& result.Count()>0)
                {
                    return result.FirstOrDefault().UnitName;
                }
            }
            return null;
        }
        public ActionResult GetAnnualBudget([DataSourceRequest]DataSourceRequest request)
        {
            List<AnnualBudgetViewModels> resultuf = new List<AnnualBudgetViewModels>();

            resultuf = GetAnnualBudgetList();

            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public JsonResult DduplicationnnualBudget(string BudgetYear, string AnnualBudgetVersionCode, string DivisionID, int? ID)
        {
            bool Success = false;
            List<AnnualBudgetViewModels> annualBudgetModel = GetAnnualBudgetList().Where(e => e.DivisionID == DivisionID && e.BudgetYear == BudgetYear && e.AnnualBudgetVersionCode == AnnualBudgetVersionCode).ToList();
            if (annualBudgetModel != null && annualBudgetModel.Count > 0)
            {
                if (ID != annualBudgetModel[0].ID)
                {
                    Success = true;
                }

            }
            return Json(new MessageViewModel() { Success = Success, Title = "" });
        }
        public List<AnnualBudgetViewModels> GetAnnualBudgetList()
        {
            List<AnnualBudgetViewModels> resultuf = new List<AnnualBudgetViewModels>();
            AdminBLL bll = new AdminBLL();
            try
            {
                resultuf = bll.GetAnnualBudget().Select(e => new AnnualBudgetViewModels
                {
                    ID = e.ID,
                    BudgetYear = e.BudgetYear,
                    DivisionID = e.DivisionID,
                    Division = GetvDivisionName(e.DivisionID),
                    AnnualBudgetVersionCode = e.AnnualBudgetVersionCode,
                    Version = e.AnnualBudgetVersionCode,
                    EntertainBudget = e.EntertainBudget,
                    GiftBudget = e.GiftBudget,
                    CreatedDate = e.CreatedDate,
                    ModifiedDate = e.ModifiedDate,
                    ModifiedBy = e.ModifiedBy,

                }).ToList();
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return resultuf;
        }

        public ActionResult CreateAnnualBudget([DataSourceRequest] DataSourceRequest request, AnnualBudgetViewModels model)
        {
            List<AnnualBudgetViewModels> resultuf = new List<AnnualBudgetViewModels>();
            try
            {
                AnnualBudget db = new AnnualBudget();
                db.ID = model.ID;
                db.BudgetYear = model.BudgetYear;
                db.DivisionID = model.DivisionID;
                db.AnnualBudgetVersionCode = model.AnnualBudgetVersionCode;
                db.EntertainBudget = model.EntertainBudget;
                db.GiftBudget = model.GiftBudget;
                db.CreatedDate = DateTime.Now;
                AdminBLL bll = new AdminBLL();
                db = bll.CreateAnnualBudget(db);
                model = GetAnnualBudgetList().Where(e => e.DivisionID == db.DivisionID && e.BudgetYear == db.BudgetYear && e.AnnualBudgetVersionCode == db.AnnualBudgetVersionCode).FirstOrDefault();
                resultuf.Add(model);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult UpdateAnnualBudget([DataSourceRequest] DataSourceRequest request, AnnualBudgetViewModels model)
        {
            List<AnnualBudgetViewModels> resultuf = new List<AnnualBudgetViewModels>();
            try
            {
                AnnualBudget db = new AnnualBudget();
                db.ID = model.ID;
                db.BudgetYear = model.BudgetYear;
                db.DivisionID = model.DivisionID;
                db.AnnualBudgetVersionCode = model.AnnualBudgetVersionCode;
                db.EntertainBudget = model.EntertainBudget;
                db.GiftBudget = model.GiftBudget;
                db.ModifiedDate = DateTime.Now;
                AdminBLL bll = new AdminBLL();
                db = bll.UpdateAnnualBudget(db);
                model = GetAnnualBudgetList().Where(e => e.DivisionID == db.DivisionID && e.BudgetYear == db.BudgetYear && e.AnnualBudgetVersionCode == db.AnnualBudgetVersionCode).FirstOrDefault();
                resultuf.Add(model);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }
        public ActionResult DeleteAnnualBudget([DataSourceRequest] DataSourceRequest request, AnnualBudgetViewModels model)
        {
            List<AnnualBudgetViewModels> resultuf = new List<AnnualBudgetViewModels>();
            try
            {
                AnnualBudget db = new AnnualBudget();
                db.ID = model.ID;
                db.BudgetYear = model.BudgetYear;
                db.DivisionID = model.DivisionID;
                db.AnnualBudgetVersionCode = model.AnnualBudgetVersionCode;
                db.EntertainBudget = model.EntertainBudget;
                db.GiftBudget = model.GiftBudget;
                db.CreatedDate = DateTime.Now;
                AdminBLL bll = new AdminBLL();
                db = bll.DeleteAnnualBudget(db);
                // model = GetCurrencyList().Where(e => e.CurrencyID == db.CurrencyID).FirstOrDefault();
                //resultuf.Add(model);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delegation()
        {
            ViewBag.UserName = GetUserInfo.MemberName;
            ViewBag.UserID = GetUserInfo.MemberId;
            SetBreadcrumbs("Admin", "Delegation");
            return View();
        }
    }
}
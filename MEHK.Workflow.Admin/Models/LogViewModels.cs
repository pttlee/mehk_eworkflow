﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Admin.Models
{
	public class LogViewModels
    {
        public int ID { get; set; }
        public System.DateTime Date { get; set; }
        public string LogType { get; set; }
        public string LogPriority { get; set; }
        public string ErrorCode { get; set; }
        public string StackTrace { get; set; }
        public string Message { get; set; }
        public string Source { get; set; }
        public string Remark { get; set; }
    }
}
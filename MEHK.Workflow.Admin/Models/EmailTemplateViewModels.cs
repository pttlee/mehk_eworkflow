﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Admin.Models
{
	public class EmailTemplateViewModels
	{
		public int ID { get; set; }
		public string ProcessCode { get; set; }
		//public Nullable<int> StepID { get; set; }
		public string Subject { get; set; }
		public string Body { get; set; }
		public string SubjectView { get; set; }
		public string BodyView { get; set; }
		public string ContentType { get; set; }
		public string ProcessName { get; set; }
	}
}
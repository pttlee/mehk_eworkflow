﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Admin.Models
{
	public class GroupViewModels
	{
		public int ID { get; set; }
		public string GroupName { get; set; }
		public string Description { get; set; }
	}
}
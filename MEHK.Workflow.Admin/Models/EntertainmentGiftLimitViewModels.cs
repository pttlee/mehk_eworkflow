﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Admin.Models
{
	public class EntertainmentGiftLimitViewModels
    {
        public int ID { get; set; }
        public string Value { get; set; }
        public Nullable<decimal> Text { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
    }
}
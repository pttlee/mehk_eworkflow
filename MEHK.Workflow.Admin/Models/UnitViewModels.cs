﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Admin.Models
{
	public class UnitViewModels
	{
		public int UnitId { get; set; }
		public string UnitIdText { get; set; }
		public string UnitName { get; set; }
		public int UnitTypeId { get; set; }
		public string Description { get; set; }
	}
}
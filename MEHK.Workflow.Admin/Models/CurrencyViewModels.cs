﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Admin.Models
{
	public class CurrencyViewModels
	{
		public int CurrencyID { get; set; }
		public string CurrencyName { get; set; }
		public int SortSeq { get; set; }
		public decimal? ExchangeRate { get; set; }
		public string Remark { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Admin.Models
{
	public class ProcessListViewModels
	{
		public int ProcessId { get; set; }
		public string Code { get; set; }
		public string ProcessName { get; set; }
	}
}
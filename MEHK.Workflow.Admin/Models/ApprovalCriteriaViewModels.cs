﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Admin.Models
{
	public class  ApprovalCriteriaViewModels
	{
		public int ApprovalCriteriaID { get; set; }
		public string ApprovalCriteriaName { get; set; }
		public string Remark { get; set; }
	}
}
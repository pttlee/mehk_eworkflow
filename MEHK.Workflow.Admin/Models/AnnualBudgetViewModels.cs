﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Admin.Models
{
	public class AnnualBudgetViewModels
    {
        public int ID { get; set; }
        public string BudgetYear { get; set; }
        public string DivisionID { get; set; }
        public string Division { get; set; }
        public string AnnualBudgetVersionCode { get; set; }
        public string Version { get; set; }
        public Nullable<decimal> EntertainBudget { get; set; }
        public Nullable<decimal> GiftBudget { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Admin.Models
{
	public class ProcessOptionViewModels
    {
		public int ProcessID { get; set; }
		public string ApprovalMartixOptionCode { get; set; }
        public string ApprovalMartixOptionName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Admin.Models
{
	public class ApprovalMartixOptionViewModels
	{
		public string ApprovalMartixOptionCode { get; set; }
		public string ApprovalMartixOptionName { get; set; }
	}
}
﻿using MEHK.Workflow.EntertainmentGift.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MEHK.Workflow.BLL;
using MEHK.Workflow.Data.Model;
using System.Net;
using MEHK.Workflow.Data.Enum;
using Resources;
using System.Resources;
using System.Globalization;
using System.Collections;
using System.Reflection;
using K25Lib;
using System.Transactions;
using MEHK.Workflow.Data.DTO;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.IO;
using MEHK.Workflow.SAPService;
using MEHK.Workflow.Data.Const;
using System.Text.RegularExpressions;
using MEHK.Workflow.BLL.Authorize;
namespace MEHK.Workflow.EntertainmentGift.Controllers
{
    public class EntertainmentGiftController : MEHKBase
    {
        //// GET: EntertainmentGift
        //public ActionResult Index()
        //{
        //    return View();
        //}


        public int GetFormTypeID()
        {
            return CommonBLL.GetFormTypeID(FormTypes.EntertainmentGift);
        }



        public void EntertainmentGiftControllers()
        {


            //this.ProcessName = "";

            //FormType = Data.Enum.FormType.EntertainmentGift;

        }

        public void InitData()
        {
            ResourceManager MyResourceClass = new ResourceManager("MEHK.Workflow.Resources.Resources.Resource.EntertainmentGiftValidation", typeof(Resource_EntertainmentGiftValidation).Assembly);

            ResourceSet resourceSet = MyResourceClass.GetResourceSet(CultureInfo.CurrentUICulture, true, true);

            ViewBag.ValidationMessage = resourceSet;

            ViewBag.FormTypeID = GetFormTypeID();
        }

        [MEHKAuthorize(PageRight = RightConst.CREATE_ENTERTAINMENT_GIT_RIGHT)]
        public ActionResult Create()
        {
            ViewData["ViewmodelType"] = "Create";
            ViewBag.FormID = 0;
          //  APIAccessBLL apiBLL = GetAPIBLL;

            ViewBag.EntertainmentGiftCode = EntertainmentGiftCode;

            InitData();

            var model = new EntertainmentGiftFormViewModel();
            ViewBag.EntertainmentAmountText = EntertainmentAmount.ToString("N");
            ViewBag.GiftAmountText = GiftAmount.ToString("N");
			ViewBag.EntertainmentAmount = EntertainmentAmount;
			ViewBag.GiftAmount = GiftAmount;


			//model.StaffID = "0267";
			//model.StaffName = "Melody Chan";
			//model.Division = "LD Division";
			//model.Department = "Sales HA Department";

			model.SAMAccount = GetUserADName;

            ViewBag.MemberId = GetUserInfo.MemberId;
            model.StaffID = GetUserInfo.StaffId;
            model.StaffName = GetUserInfo.MemberName;
            model.Division = GetUserInfo.Division;
            model.Department = GetUserInfo.Department;


            model.UserInfo = GetCommonUserInfo();
            model.UserInfo.ViewMode = "Create";

            model.ApplicationDate = DateTime.Now;
            model.SubmissionDate = DateTime.Now;

            model.PaymentReimbursement = new EntertainmentPaymentReimbursementViewModel();
            model.PreApproval = new EntertainmentPreApprovalViewModel();

            model.FormStatus = Resources.Resource_Form.NewRequest;
            //model.EnablePaymentReimbursementPart = true;



            //InitDropDownList(0, BudgetID.Value, record.ApplicantDepartmentName, false, false);

            SetBreadcrumbs(Resource_EntertainmentGift.EntertainmentAndGift, Resource_Common.Create);
            //InitFileSettings();

            //NonBudgetCapexFormViewModel model = new NonBudgetCapexFormViewModel();
            ////model.FormType = CapexType.Project.ToString();
            //model.ID = 0;
            //model.CurrentUserID = GetCurrentUserID;
            //model.GID = Guid.NewGuid();
            //model.AdditionalBudgetGID = Guid.NewGuid();
            //model.EnableDeleteFile = true;
            //model.CapexProfileID = BudgetID.Value;
            //model.SubmissionDate = DateTime.Now;


            //model.EnableForm = true;
            //model.EnableUpload = true;

            ////model.FormStatus = "New Request";

            //ViewBag.HasDifferentBetweenFormCapexProfileDataAndLatestCapexProfileData = false;

            //model.FormStatus = Resources.Resource_Common.NewRequest;

            model.ActionList = new List<string>()
                {
                    FormActions.Submit.ToString(),
                    FormActions.SaveAsDraft_PreApproval.ToString(),
                };


            model.EnableForm = true;
            model.GID = Guid.NewGuid();

            ViewBag.ApprovalMartixOptionCode = " ";
            return View(model);
        }
        [EntertainmentGiftAuthorize]
        public ActionResult Edit(int? ID, string SN = "", string Mode = "")
        {
            ViewBag.WS_Currency_URL = WS_Currency_URL;
            ViewBag.WS_PostJV_URL = WS_PostJV_URL;
            ViewBag.WS_eFiling_URL = WS_eFiling_URL;
            ViewData["ViewmodelType"] = "Edit";
            ViewBag.EntertainmentAmounTextt = EntertainmentAmount.ToString("N");
            ViewBag.GiftAmountText = GiftAmount.ToString("N");
            ViewBag.EntertainmentAmount = EntertainmentAmount;
            ViewBag.GiftAmount = GiftAmount;
		

			InitData();
            var UserInfo = GetUserInfo;
            bool EnableForm = false;
            var EntertainmentGiftBLL = new EntertainmentGiftBLL();
            var model = new EntertainmentGiftFormViewModel();

            ViewBag.EntertainmentGiftCode = EntertainmentGiftCode;

            if (!ID.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var record = EntertainmentGiftBLL.Get(ID.Value);
            var ReimburseRecord = EntertainmentGiftBLL.GetReimburseRecord(ID.Value);

			List<CostCentersViewModel> CostCentersDropDownList = new List<CostCentersViewModel>();
			if (record != null)
			{
				//int MemberID = record.MemberID != null && record.MemberID != "" ? int.Parse(record.MemberID) : 0;
				int PayeeID = record.Payee != null ? (int)record.Payee : 0;
				CostCentersDropDownList = GetCostCentersModelList(PayeeID);
			}
			var CostCentersList = CostCentersDropDownList.Select(n => new SelectListItem { Text = n.Description, Value = n.Code.ToString() })
			.OrderBy(n => n.Text)
			.ToList();

			ViewBag.CostCentersDropDownList = CostCentersList;
			ViewBag.PostKeyDropDownList = new List<SelectListItem>()
			{
				new SelectListItem { Text = "40", Value = "Dr" },
				new SelectListItem { Text = "31", Value = "Cr" }
			};

			if (ReimburseRecord != null)
            {
                record.EntertainmentGiftReimburseRecord = ReimburseRecord;
            }
            ViewBag.FormID = record.ID;

            MapDBRecordToEntertainmentGiftPreApprovalFormViewModel(record, model);

            var CurrentStep = GeneralWorkflowBLL.GetCurrentStep(record.ID, GetFormTypeID());
            ViewBag.ReIsCompleted = false;


            if (!String.IsNullOrEmpty(SN))
            {
                switch (CurrentStep)
                {
                    case WorkflowSteps.PreApproval_Supervisor1:
                    case WorkflowSteps.PreApproval_Supervisor2:
                    case WorkflowSteps.PreApproval_AM:
                    case WorkflowSteps.PreApproval_DM:
                    case WorkflowSteps.PreApproval_SM:
                    case WorkflowSteps.PreApproval_DGM:
                    case WorkflowSteps.PreApproval_GM:
                    case WorkflowSteps.PreApproval_GPAD:
                    case WorkflowSteps.PreApproval_MD:

                    case WorkflowSteps.Reimbursement_Supervisor1:
                    case WorkflowSteps.Reimbursement_Supervisor2:
                    case WorkflowSteps.Reimbursement_AM:
                    case WorkflowSteps.Reimbursement_DM:
                    case WorkflowSteps.Reimbursement_SM:
                    case WorkflowSteps.Reimbursement_DGM:
                    case WorkflowSteps.Reimbursement_GM:
                    case WorkflowSteps.Reimbursement_GPAD:
                    case WorkflowSteps.Reimbursement_MD:
                    case WorkflowSteps.Reimbursement_ACRV1:
                    case WorkflowSteps.Reimbursement_ACRV2:
                    case WorkflowSteps.Reimbursement_ACAP:
                    case WorkflowSteps.Reimbursement_HRRV1:
                    case WorkflowSteps.Reimbursement_HRRV2:
                    case WorkflowSteps.Reimbursement_HRAP:

                        model.ActionList = new List<string>(){
                                FormActions.Approve.ToString(),
                                FormActions.Return.ToString()
                            };

                        model.EnableComment = true;

                        break;

                    case WorkflowSteps.Reimbursement_Requester:
                        if (record.IsCompleted != true)
                        {
                            model.ActionList = new List<string>()
                        {
                            FormActions.ReimbursementSubmit.ToString(),
                            FormActions.SaveAsDraft_Reimbursement.ToString(),
                        };
                        }
                        else
                        {
                            model.ActionList = new List<string>()
                            {

                                FormActions.Resubmit.ToString(),
                            FormActions.SaveAsDraft_Reimbursement.ToString(),
                            };
                        }
                        model.EnableComment = false;

                        break;
                    case WorkflowSteps.PreApproval_Requester:
                        model.EnablePreApprovalPart = true;
                        model.EnablePaymentReimbursementPart = false;
                        model.ActionList = new List<string>()
                        {
                            FormActions.Resubmit.ToString(),
                            FormActions.Cancel.ToString(),
                        };
                        model.EnableComment = false;

                        break;
                    case WorkflowSteps.Empty:
                        //&& record.FormStatusID
                        if (record.IsCompleted == false)
                        {
                            model.EnablePreApprovalPart = true;
                            model.EnablePaymentReimbursementPart = false;
                            ViewBag.ReIsCompleted = false;

                        }
                        else
                        {
                            model.EnablePaymentReimbursementPart = true;
                            model.EnablePreApprovalPart = false;
                            ViewBag.ReIsCompleted = true;

                        }
                        //model.ActionList = new List<string>()
                        //{
                        //    FormActions.Resubmit.ToString(),
                        //    FormActions.Cancel.ToString(),
                        //};
                        break;

                }
            }
            else
            {
                if ((model.UserInfo.FormNo == null || String.IsNullOrEmpty(model.UserInfo.FormNo)) && Mode == "Draft")
                {
                    model.EnablePreApprovalPart = true;
                    model.EnablePaymentReimbursementPart = false;
                    model.ActionList = new List<string>()
                        {
                            FormActions.Submit.ToString(),
                        };
                    model.EnableComment = false;
                }
            }

            if (CurrentStep == WorkflowSteps.Reimbursement_Requester)
            {
                model.EnablePaymentReimbursementPart = true;
            }


           

            if (!String.IsNullOrEmpty(SN))
            {
                SetBreadcrumbs(Resource_EntertainmentGift.EntertainmentAndGift, Resource_Common.MyTask);
                model.K2SN = SN;
            }

            EntertainmentGiftRecordStatusCode currentStatus;
            Enum.TryParse(EntertainmentGiftBLL.GetStatus(record.FormStatusID.Value).FormStatus, out currentStatus);

            switch (currentStatus)
            {
                case EntertainmentGiftRecordStatusCode.Draft:

                    model.SubmissionDate = DateTime.Now;
                    model.ApplicationDate = DateTime.Now;

                    break;

            }


          



            if (currentStatus == EntertainmentGiftRecordStatusCode.Returned && record.CreatedBy.ToLower() == UserInfo.SAMAccount.ToLower())
            {
                if (CurrentStep == WorkflowSteps.Empty)
                {
                    //model.EnableForm = true;
                    //model.EnableUpload = true;
                    // model.EnablePreApprovalPart = true;

                    //model.RoleName = "Creator";

                    //  model.ActionList = new List<string>() { "Resubmit", "Save_PreApproval", "Cancel" };
                }
                else
                {
                    //model.ShowFinancialReturn = true;
                    //model.ShowInvestmentAppraisal = true;
                    //model.ShowBudgetStatus = true;
                    //model.ShowApprovalStatusTable = true;
                }
            }

            if (!String.IsNullOrEmpty(Mode))
            {
                if (Mode == "View")
                {
                    //model.EnableForm = false;
                    EnableForm = false;
                    model.EnableUpload = false;
                    model.EnableComment = false;
                    model.EnablePaymentReimbursementPart = false;
                    model.EnablePreApprovalPart = false;
                    model.ActionList = new List<string>() { };

                    SetBreadcrumbs(Resource_EntertainmentGift.EntertainmentAndGift, Resource_Common.ViewDetails);
                }
            }

            //todo Change Created By
            if (String.IsNullOrEmpty(SN) && String.IsNullOrEmpty(Mode) &&
                currentStatus == EntertainmentGiftRecordStatusCode.Draft
                && record.CreatedBy.ToLower() == UserInfo.SAMAccount.ToLower()
               )
            {
                //model.EnableForm = true;
                // && record.CreatedBy == ""
                EnableForm = true;
                model.EnableUpload = true;

                //model.ActionList = new List<string>() { "Submit", "SaveAsDraft", "Delete" };

                model.ActionList = new List<string>()
                    {
                        FormActions.Submit.ToString(),
                        FormActions.SaveAsDraft_PreApproval.ToString(),
                        FormActions.Delete.ToString(),
                    };

                SetBreadcrumbs(Resource_EntertainmentGift.EntertainmentAndGift, Resource_Common.MyDraft);
            }


            //EnableForm = true;
            model.EnableForm = EnableForm;
            model.Mode = Mode;
            model.GID = Guid.NewGuid();
            model.CurrentUserID = UserInfo.MemberId;
            ViewBag.PaymentReimbursementGridShow = false;
            ViewBag.PaymentReimbursementGridEdit = false;
            ViewBag.ApprovalMatrixGridShow = true;
            /**case WorkflowSteps.Reimbursement_ACRV:
					case WorkflowSteps.Reimbursement_ACAP:**/
            //if ()
            //{
            //	ViewBag.PaymentReimbursementGridShow = true;
            //}
            if (CurrentStep == WorkflowSteps.Empty && model.UserInfo.FormNo != null)
            {
                ViewBag.ApprovalMatrixGridShow = false;
            }

            if (CurrentStep == WorkflowSteps.Reimbursement_ACRV1 ||
                CurrentStep == WorkflowSteps.Reimbursement_ACRV2 ||
                CurrentStep == WorkflowSteps.Reimbursement_ACAP ||
                (CurrentStep == WorkflowSteps.Empty && model.UserInfo.FormNo != null))
            {
               
                ViewBag.PaymentReimbursementGridShow = true;
				/////

				
				var list = EntertainmentGiftBLL.GetVoucher(record.ID);
				if (list.Count() == 0)
				{
					decimal Amount = 0;
					List<PaymentReimbursementGridViewModel> result = new List<PaymentReimbursementGridViewModel>();
					if (ReimburseRecord != null)
					{
						record.EntertainmentGiftReimburseRecord = ReimburseRecord;
					}

				

					if (model.BudgetType == ClaimTypes.Entertainment.ToString())
					{
						Amount = model.PaymentReimbursement.AmountHKEntertain != null ?
							(decimal)model.PaymentReimbursement.AmountHKEntertain : 0;
					}
					else
					{
						Amount = model.PaymentReimbursement.AmountHKGift != null ?
							(decimal)model.PaymentReimbursement.AmountHKGift : 0;
					}
					int count = model.PaymentReimbursement.CostCenterList.Count();
					foreach (var item in model.PaymentReimbursement.CostCenterList)
					{
						PaymentReimbursementGridViewModel resultDRModel = new PaymentReimbursementGridViewModel();
						resultDRModel.ID = 0;
						resultDRModel.Centre = item.Code;
						resultDRModel.AcctCode = "405500701";
						resultDRModel.Dr_Cr = "Dr";
						resultDRModel.Payee_AcctType = "Entertatiment";
						resultDRModel.PostKey = "40";
						resultDRModel.Amount = Amount == 0 ? 0 : Amount / count;
						result.Add(resultDRModel);
						PaymentReimbursementGridViewModel resultModel = new PaymentReimbursementGridViewModel();
						resultModel.ID = 0;
						//resultModel.Centre = model.PaymentReimbursement.CostCenterCode;
						resultModel.Centre = item.Code;
						resultModel.AcctCode = "S" + model.UserInfo.StaffID;
						resultModel.Dr_Cr = "Cr";
						resultModel.Payee_AcctType = model.UserInfo.PayeeName;
						resultModel.PostKey = "31";
						//resultModel.Amount = Amount;
						resultModel.Amount = Amount == 0 ? 0 : Amount / count;
						result.Add(resultModel);
					}

					List<Voucher> voucherList = PaymentReimbursementListToDBRecord(result, record.ID);
					EntertainmentGiftBLL.SetVoucher(voucherList);
				}
				////


				int PayeeID = 0;
                if (record.Payee != null)
                {
					PayeeID = (int)record.Payee;
                }
				
				ApprovalMartixDetailViewModel ApprovalMartix = GetApprovalMatrix(PayeeID, "R").Where(e => e.ApprovalLevelCode == "ACRV1").FirstOrDefault();
                if (CurrentStep == WorkflowSteps.Reimbursement_ACRV1 && Mode != "View")
                {
                    
					foreach (var item in ApprovalMartix.Approvers)
					{
						if (item.ToLower() == GetUserADName.ToLower())
						{
							ViewBag.PaymentReimbursementGridEdit = true;
						}
					}
				}


            }
            ViewBag.ApprovalMartixOptionCode = "";
            if (model.BudgetType == ClaimTypes.Entertainment.ToString())
            {
                if (model.BudgetAmount >= EntertainmentAmount)
                {
                    ViewBag.ApprovalMartixOptionCode = "O";
                }
                else
                {
                    ViewBag.ApprovalMartixOptionCode = "B";
                }
            }
            else
            {
                if (model.BudgetAmount >= GiftAmount)
                {
                    ViewBag.ApprovalMartixOptionCode = "O";
                }
                else
                {
                    ViewBag.ApprovalMartixOptionCode = "B";
                }
            }

            ViewBag.CurrentStep = CurrentStep;
            ViewBag.SAPShow = false;
            if (record.IsCompleted == true && CurrentStep == WorkflowSteps.Empty)
            {
                ViewBag.SAPShow = true;
            }
            WFControl.OpenItem(UserInfo.SAMAccountWithDomain, SN);
            return View(model);
        }
        public ActionResult GetPaymentReimbursementGridViewModel([DataSourceRequest]DataSourceRequest request, int FormID)
        {
            EntertainmentGiftFormViewModel model = new EntertainmentGiftFormViewModel();
            int Number = 1;
            List<PaymentReimbursementGridViewModel> result = new List<PaymentReimbursementGridViewModel>();
			EntertainmentGiftBLL EntertainmentGiftBLL = new EntertainmentGiftBLL();
			var list = EntertainmentGiftBLL.GetVoucher(FormID);
			result = DBListToPaymentReimbursementList(list);
            
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
		public List<PaymentReimbursementGridViewModel> DBListToPaymentReimbursementList(List<Voucher> list)
		{
			List<PaymentReimbursementGridViewModel> resultuf = new List<PaymentReimbursementGridViewModel>();
			int Number = 1;
			foreach (var item in list)
			{
				PaymentReimbursementGridViewModel resultDRModel = new PaymentReimbursementGridViewModel();
				resultDRModel.ID = item.ID;
				resultDRModel.Centre = item.Centre;
				resultDRModel.AcctCode = item.AcctCode;
				resultDRModel.Number = Number;
				resultDRModel.Dr_Cr = item.DrCr;
				resultDRModel.Payee_AcctType = item.PayeeAcctType;
				resultDRModel.PostKey = item.PostKey;
				resultDRModel.Amount = item.Amount != null ? (decimal)item.Amount : 0;
				resultDRModel.Text = item.Text;
				Number++;
				resultuf.Add(resultDRModel);
			}
			return resultuf;
		}
		public ActionResult CreatePaymentReimbursementGridViewModel([DataSourceRequest] DataSourceRequest request, PaymentReimbursementGridViewModel model,int FormID)
		{
			List<PaymentReimbursementGridViewModel> list = new List<PaymentReimbursementGridViewModel>();
			list.Add(model);
			List<Voucher> voucherList = PaymentReimbursementListToDBRecord(list, FormID);
			EntertainmentGiftBLL EntertainmentGiftBLL = new EntertainmentGiftBLL();
			var resultuf = EntertainmentGiftBLL.createVoucher(voucherList.FirstOrDefault());

			list = DBListToPaymentReimbursementList(EntertainmentGiftBLL.GetVoucher(resultuf.FormID != null ? (int)resultuf.FormID : 0))
				.Where(e=>e.ID== resultuf.ID).ToList();

			return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

		}

		public ActionResult UpdatePaymentReimbursementGridViewModel([DataSourceRequest] DataSourceRequest request, PaymentReimbursementGridViewModel model, int FormID)
		{
			List<PaymentReimbursementGridViewModel> list = new List<PaymentReimbursementGridViewModel>();
			list.Add(model);
			List<Voucher> voucherList = PaymentReimbursementListToDBRecord(list, FormID);
			EntertainmentGiftBLL EntertainmentGiftBLL = new EntertainmentGiftBLL();
			var resultuf = EntertainmentGiftBLL.updateVoucher(voucherList.FirstOrDefault());
			list = DBListToPaymentReimbursementList(EntertainmentGiftBLL.GetVoucher(resultuf.FormID != null ? (int)resultuf.FormID : 0))
				.Where(e => e.ID == resultuf.ID).ToList(); ;
			return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

		}

		public ActionResult DeletePaymentReimbursementGridViewModel([DataSourceRequest] DataSourceRequest request, PaymentReimbursementGridViewModel model, int FormID)
		{
			List<PaymentReimbursementGridViewModel> list = new List<PaymentReimbursementGridViewModel>();
			list.Add(model);
			List<Voucher> voucherList = PaymentReimbursementListToDBRecord(list, FormID);
			EntertainmentGiftBLL EntertainmentGiftBLL = new EntertainmentGiftBLL();
			Voucher voucher = voucherList.FirstOrDefault();
			voucher.IsDeleted = true;
			 EntertainmentGiftBLL.updateVoucher(voucher);
			return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

		}


		//public string GetNextApproverdisplayName(string NextApprover )
		//{
		//	string NextApproverdisplayName = "";
		//	if (NextApprover != null)
		//	{
		//		APIAccessBLL apiBLL = new APIAccessBLL();
		//		NextApprover = NextApprover.Split(';').FirstOrDefault();
		//		var NextApprovermodel = apiBLL.GetMember(@"Organization/Members/", NextApprover.Split('\\')[1]).FirstOrDefault();

		//		if (NextApprovermodel != null)
		//		{
		//			NextApproverdisplayName = NextApprovermodel.MemberName;
		//		}

		//	}
		//	return NextApproverdisplayName;
		//}
		[ValidateHeaderAntiForgeryTokenAttribute]
        public ActionResult WorkflowAction(string WFAction, string FormAction, string Comment, EntertainmentGiftFormViewModel model, List<PaymentReimbursementGridViewModel> PaymentReimbursementList, string pdfString = "")
        {
            try
            {

                //if (!string.IsNullOrEmpty(model.PaymentReimbursement.EventDateOnString))
                //{
                //	DateTime dt = DateTime.Now;
                //	DateTime.TryParse(model.PaymentReimbursement.EventDateOnString, out dt);
                //	model.PaymentReimbursement.EventDateOn = dt;
                //}
                //if (!string.IsNullOrEmpty(model.PaymentReimbursement.EventDateToString))
                //{
                //	DateTime dt = DateTime.Now;
                //	DateTime.TryParse(model.PaymentReimbursement.EventDateToString, out dt);
                //	model.PaymentReimbursement.EventDateTo = dt;
                //}

                var EntertainmentGiftBLL = new EntertainmentGiftBLL();
                var CommonBLL = new CommonBLL();
                ApprovalActionHistoryActions ApprovalActionHistoryAction = ApprovalActionHistoryActions.Empty;
                WorkflowActions WorkflowAction;
                bool Success = true;
                string Message = "Submitted";
                string Text = "";
                //string ProcessCode = "AC04";
                string ProcessCode = EntertainmentGiftCode;
                int StepID = -1;
                string ActionType = "PendingCase";
                string step = "";
				string NextWFAction = "";

				Dictionary<string, object> _Datafields = new Dictionary<string, object>();
                Enum.TryParse(FormAction, out ApprovalActionHistoryAction);
                Enum.TryParse(WFAction, out WorkflowAction);

                APIAccessBLL apiBLL = new APIAccessBLL();
                var record = EntertainmentGiftBLL.Get(model.ID);
				step = model.Step;
				var CurrentStep = GeneralWorkflowBLL.GetCurrentStep(record.ID, GetFormTypeID());
                var NextStep = GeneralWorkflowBLL.GetNextStep(record.ID, GetFormTypeID());
                var NextStepID = GeneralWorkflowBLL.GetNextStepID(record.ID, GetFormTypeID());
                var LastStep = GeneralWorkflowBLL.CheckLastStep(record.ID, GetFormTypeID());
                //StepID = NextStepID.HasValue ? (int)NextStepID : -1;
                GeneralWorkflowBLL.ResetWFActivityDetailFromStepName(record.ID, GetFormTypeID(), CurrentStep.ToString());
                ApprovalMartixViewModel approverModelReimbursement = new ApprovalMartixViewModel();
                ApprovalMartixViewModel approverModelPreApproval = new ApprovalMartixViewModel();
                approverModelReimbursement = GetApprovalMatrixFromAPI(model, FlowType.Reimbursement, record.MemberID);
                approverModelPreApproval = GetApprovalMatrixFromAPI(model, FlowType.PreApproval, record.MemberID);
                string NextApprover = GeneralWorkflowBLL.GetNextStepUser(record.ID, GetFormTypeID()).ADName;
				string ApprovalCriteriaType = GeneralWorkflowBLL.GetNextStepUser(record.ID, GetFormTypeID()).ApprovalCriteriaType;
				string NextApproverdisplayName = "";
				if (NextApprover != null)
				{
					NextApprover = new CommonBLL().GetDelegationNextApprover(ApprovalCriteriaType, NextApprover);
					NextApproverdisplayName = EntertainmentGiftBLL.GetNextApproverdisplayName(NextApprover, ApprovalCriteriaType);
					
				}
                //--------------------------------------------------------
                using (TransactionScope scope = new TransactionScope())
                {
					//GeneralWorkflowBLL.CompleteTask(model.ID, GetFormTypeID(), User.Identity.Name, WFAction);
					string UserIdentityName = new CommonBLL().GetAppIdentityName(model.ID, GetFormTypeID());
					GeneralWorkflowBLL.CompleteTask(model.ID, GetFormTypeID(), UserIdentityName, WFAction);

                    switch (WorkflowAction)
                    {
                        case WorkflowActions.Approve:
							NextWFAction = WorkflowActions.Approve.ToString();
							if (LastStep)
                            {
                                ActionType = "ApprovedCase";
                                _Datafields[GeneralWFDataFields.HasNextStep.ToString()] = "False";
                                _Datafields[GeneralWFDataFields.StatusCode.ToString()] = EntertainmentGiftRecordStatusCode.Approved.ToString();
								
								NextWFAction = "Approved";
							}
                            else
                            {
                                //if (NextStep == WorkflowSteps.Reimbursement_Requester)
                                //{
                                //    record.FormStatusID = EntertainmentGiftBLL.GetStatusID(EntertainmentGiftRecordStatusCode.Filling_Reimbursement);

                                //    EntertainmentGiftBLL.SaveRecord(record);
                                //}
                                _Datafields[GeneralWFDataFields.HasNextStep.ToString()] = "True";
                                _Datafields[GeneralWFDataFields.NextApprover.ToString()] = NextApprover;
								
								NextApproverdisplayName = EntertainmentGiftBLL.GetNextApproverdisplayName(NextApprover,ApprovalCriteriaType);

							}
                            if (CurrentStep == WorkflowSteps.Reimbursement_ACAP)
                            {
                                pdfString = pdfString.Replace("data:application/pdf;base64,", "");
                                // Dictionary<string, byte[]> dictionaryByte = EntertainmentGiftBLL.GetFilesList(ProcessCode, model.ID);
                                //string filePathList = GenPDF.MergeMemoStream(pdfString, record.FormNo, dictionaryByte);
                                byte[] file = GenPDF.MergeMemoStream(pdfString, record.FormNo);
                                int memberID = 0;
                                int.TryParse(record.MemberID, out memberID);
                                EntertainmentGiftBLL.InsertPDFFile(ProcessID, record.ID, memberID, record.FormNo + "_Formpdf.pdf", file.Length, file);
                                MainService sapServices = new MainService(WS_eFiling_URL);
                                sapServices.SaveFileListToSAP(record.ID.ToString(), VitalDocUserName, VitalDocUserPassword, "997181", record.FormNo, "", "", "", record.FormNo);
                                NextWFAction = "Approved";
                            }
							if (NextStep == WorkflowSteps.Reimbursement_Requester)
							{
								NextWFAction = WorkflowActions.ReimbursementSubmit.ToString();
							}

							break;
                        case WorkflowActions.Return:
                            ActionType = "ReturnedCase";
							NextWFAction = WorkflowActions.Resubmit.ToString();

							GeneralWorkflowBLL.DeleteWFActivity(record.ID, GetFormTypeID());
                            if ((bool)record.IsCompleted)
                            {
                                if (CurrentStep == WorkflowSteps.Reimbursement_ACAP)
                                {
                                    var approverModel = GetACAPReturnApprovalMatrix(approverModelReimbursement);
                                    NextApprover = EntertainmentGiftBLL.InsertWFActivity(record, approverModel, FlowType.Reimbursement, GetUserInfo);

                                    WFAction = WorkflowActions.Approve.ToString();
									NextWFAction = WorkflowActions.Approve.ToString();

									ApprovalCriteriaType = GeneralWorkflowBLL.GetNextStepUser(record.ID, GetFormTypeID()).ApprovalCriteriaType;

									NextApproverdisplayName = EntertainmentGiftBLL.GetNextApproverdisplayName(NextApprover, ApprovalCriteriaType);
								}
                                else
                                {
                                    NextApprover = EntertainmentGiftBLL.InsertActivityForReturn(record, WorkflowSteps.Reimbursement_Requester.ToString());

									ApprovalCriteriaType = GeneralWorkflowBLL.GetNextStepUser(record.ID, GetFormTypeID()).ApprovalCriteriaType;

									NextApproverdisplayName = EntertainmentGiftBLL.GetNextApproverdisplayName(NextApprover, ApprovalCriteriaType);
								}
								
							}
                            else
                            {

                                NextApprover = EntertainmentGiftBLL.InsertActivityForReturn(record, WorkflowSteps.PreApproval_Requester.ToString());

								ApprovalCriteriaType = GeneralWorkflowBLL.GetNextStepUser(record.ID, GetFormTypeID()).ApprovalCriteriaType;

								NextApproverdisplayName = EntertainmentGiftBLL.GetNextApproverdisplayName(NextApprover, ApprovalCriteriaType);
							}
							NextApprover = new CommonBLL().GetDelegationNextApprover(ApprovalCriteriaTypeEnum.JL.ToString(), NextApprover);
							_Datafields[GeneralWFDataFields.HasNextStep.ToString()] = "True";
                            _Datafields[GeneralWFDataFields.NextApprover.ToString()] = NextApprover;
                            break;

                        case WorkflowActions.ReimbursementSubmit:
                        case WorkflowActions.Resubmit:
							NextWFAction = WorkflowActions.Approve.ToString();
							if (CurrentStep == WorkflowSteps.Reimbursement_Requester)
                            {
                                if (record.IsCompleted.HasValue && (bool)record.IsCompleted == true)
                                {
                                    WFAction = WorkflowActions.Resubmit.ToString();
                                }
                                else
                                {
                                    WFAction = WorkflowActions.Approve.ToString();

                                }
                                EntertainmentGiftReimburseRecord ReimburseRecord = ReimburseRecordModel(record, model);
                                //  record.FormStatusID = EntertainmentGiftBLL.GetStatusID(EntertainmentGiftRecordStatusCode.Approving_Reimbursement);
                                record.IsCompleted = true;
                                record.EntertainmentGiftReimburseRecord = ReimburseRecord;
                                record.LateReason = model.PaymentReimbursement.LateApplicationReason;

								EntertainmentGiftBLL.SaveRecord(record);
                                GeneralWorkflowBLL.DeleteWFActivity(record.ID, GetFormTypeID());
                                NextApprover = EntertainmentGiftBLL.InsertWFActivity(record, approverModelReimbursement, FlowType.Reimbursement, GetUserInfo);

								ApprovalCriteriaType = GeneralWorkflowBLL.GetNextStepUser(record.ID, GetFormTypeID()).ApprovalCriteriaType;

								NextApproverdisplayName = EntertainmentGiftBLL.GetNextApproverdisplayName(NextApprover, ApprovalCriteriaType);
							}
                            else
                            {
                                record = MapEntertainmentGiftPreApprovalFormViewModelToDBRecord(model, record);
								// record.FormStatusID = EntertainmentGiftBLL.GetStatusID(EntertainmentGiftRecordStatusCode.Approving_PreApproval);
								
								EntertainmentGiftBLL.SaveRecord(record);
                                NextApprover = EntertainmentGiftBLL.InsertWFActivity(record, approverModelPreApproval, FlowType.PreApproval, GetUserInfo);

								ApprovalCriteriaType = GeneralWorkflowBLL.GetNextStepUser(record.ID, GetFormTypeID()).ApprovalCriteriaType;

								NextApproverdisplayName = EntertainmentGiftBLL.GetNextApproverdisplayName(NextApprover, ApprovalCriteriaType);
							}
							NextApprover = new CommonBLL().GetDelegationNextApprover(ApprovalCriteriaTypeEnum.JL.ToString(), NextApprover);
							_Datafields[GeneralWFDataFields.HasNextStep.ToString()] = "True";
                            _Datafields[GeneralWFDataFields.NextApprover.ToString()] = NextApprover;
                            break;

                        case WorkflowActions.Withdraw:
							//record.FormStatusID = EntertainmentGiftBLL.GetStatusID(EntertainmentGiftRecordStatusCode.Cancelled);
							//EntertainmentGiftBLL.SaveRecord(record);
							NextWFAction = "Terminated";
							_Datafields[GeneralWFDataFields.StatusCode.ToString()] = EntertainmentGiftRecordStatusCode.Terminated.ToString();
                            break;
                    }
					///////
					 record = EntertainmentGiftBLL.Get(model.ID);
                    if(CurrentStep ==WorkflowSteps.Reimbursement_ACAP )
                    {
                        record.Step = GetUserInfo.MemberName + "-" + EntertainmentGiftRecordStatusCode.Approved.ToString() + "-" + step;
                    }
                    else 
                    {
                        record.Step = NextApproverdisplayName + "-" + NextWFAction + "-" + step;
                    }
					
					EntertainmentGiftBLL.SaveRecord(record);
					///////
					CommonBLL.InsertApprovalHistroy(model.ID, FormTypes.EntertainmentGift, GetUserInfo,
                        ApprovalActionHistoryAction,
                        ApprovalActionHistoryRoles.AccountApproval,
                        Comment,
                        ApprovalActionHistorySections.PreApproval.ToString());


                    //if (WFAction == WorkflowActions.ReimbursementSubmit.ToString())
                    //{
                    //    WFAction = WorkflowActions.Approve.ToString();
                    //    //WFAction = WorkflowActions.ReimbursementSubmit.ToString();
                    //}
               

                    //if (PaymentReimbursementList != null && PaymentReimbursementList.Count > 0)
                    //{
                    //    List<Voucher> voucherList = PaymentReimbursementListToDBRecord(PaymentReimbursementList, model.ID);
                    //    EntertainmentGiftBLL.SetVoucher(voucherList, model.ID);
                    //}
                    if (model.PaymentReimbursement != null && model.PaymentReimbursement.SAPDocNo != null)
                    {
                        EntertainmentGiftBLL.upadteSAP(model.ID, model.PaymentReimbursement.SAPDocNo, model.PaymentReimbursement.SAPRemark, model.PaymentReimbursement.PostDate);
                    }


                    var emailmodel = EmailNotifactionBLL.GetEmailContent(ProcessCode, StepID, ActionType, FormTypes.EntertainmentGift.ToString(), NextStep.ToString(), record.FormNo, record.StaffName, "#");
                    if (emailmodel != null && IsDev)
                    {
                        _Datafields[GeneralWFDataFields.Subject.ToString()] = emailmodel.Subject;
                        _Datafields[GeneralWFDataFields.Body.ToString()] = emailmodel.Body;
                        _Datafields[GeneralWFDataFields.ToUsers.ToString()] = NextApprover;
                    }
                    else
                    {
                        //skip send email
                        _Datafields[GeneralWFDataFields.ToUsers.ToString()] = "";
                    }

                    WFControl.ExecuteWorkflowAction(GetUserInfo.SAMAccountWithDomain, model.K2SN, WFAction, _Datafields);

                    //WFControl.ExecuteWorkflowAction(User.Identity.Name, model.K2SN, WFAction, _Datafields);

                    scope.Complete();
                }

                return Json(new { Success = true, Title = Message });
            }
            catch (Exception ex)
            {
                this.CreateErrorLog(ex);

                return Json(new { Success = false, Title = Resources.Resource_DialogMessage.UpdatedFailMessage });
            }
        }
        public EntertainmentGiftReimburseRecord ReimburseRecordModel(EntertainmentGiftRecord record, EntertainmentGiftFormViewModel model)
        {
            EntertainmentGiftReimburseRecord ReimburseRecord = new EntertainmentGiftReimburseRecord();
            ReimburseRecord.EntertainmentGiftRecordID = record.ID;
            ReimburseRecord.PostingInstruction = model.PaymentReimbursement.PostingInstruction;
            ReimburseRecord.Purpose = model.PaymentReimbursement.Purpose;
            ReimburseRecord.EventOn = model.PaymentReimbursement.EventDateOn;
            ReimburseRecord.EventTo = model.PaymentReimbursement.EventDateTo;

            ReimburseRecord.Place_NatureOfGift = model.PaymentReimbursement.NatureOfGift;
            ReimburseRecord.Guests = model.PaymentReimbursement.Guest;
            ReimburseRecord.GuestTotal = model.PaymentReimbursement.GuestTotal;
            ReimburseRecord.Company = model.PaymentReimbursement.Company;
            ReimburseRecord.Attendants = model.PaymentReimbursement.Attendant;
            ReimburseRecord.AttendantTotal = model.PaymentReimbursement.AttendantTotal;
            ReimburseRecord.CostCenter = model.PaymentReimbursement.CostCenter;

			var CurrencyModel = new EntertainmentGiftBLL().GetCurrencyList().ToList();
			if (Regex.IsMatch(model.PaymentReimbursement.Currency, @"^[+-]?\d*$"))
			{
				int Currency = int.Parse(model.PaymentReimbursement.Currency);
				if (CurrencyModel.Where(e => e.CurrencyID == Currency).ToList().Count() > 0)
				{
					ReimburseRecord.Currency = model.PaymentReimbursement.Currency;
				}
				else
				{
					ReimburseRecord.CurrencyTest = model.PaymentReimbursement.Currency;
					ReimburseRecord.Currency = "";
				}
			}
			else
			{
				ReimburseRecord.Currency = "";
				ReimburseRecord.CurrencyTest = model.PaymentReimbursement.Currency;
			}
			

			

			ReimburseRecord.Amount = model.PaymentReimbursement.Amount;
            ReimburseRecord.ExchangeRate = model.PaymentReimbursement.ExchangeRate;
            ReimburseRecord.AmountHKEntertain = model.PaymentReimbursement.AmountHKEntertain;
            ReimburseRecord.AmountHKGift = model.PaymentReimbursement.AmountHKGift;
            ReimburseRecord.AmountExceedReason = model.PaymentReimbursement.AmountExceedReason;
            ReimburseRecord.PaymentMethod = model.PaymentReimbursement.PaymentMethod;
            ReimburseRecord.PayTo = model.PaymentReimbursement.PayTo;
            ReimburseRecord.NoOfAttachment = model.PaymentReimbursement.NoOfDocumentOrReceiptAttached;

            return ReimburseRecord;
        }
        public void MapDBRecordToEntertainmentGiftPreApprovalFormViewModel(EntertainmentGiftRecord model, EntertainmentGiftFormViewModel viewModel)
        {

            //viewModel.Purpose = model.Purpose;
            //viewModel.EventDateOn = model.EventOn;
            //viewModel.EventDateTo = model.EventTo;
            //viewModel.LateApplicationReason = model.LateReason;
            //viewModel.NatureOfGift = model.Place_NatureOfGift;
            //viewModel.Guest = model.Guests;
            //viewModel.GuestTotal = model.GuestTotal;
            //viewModel.Company = model.Company;
            //viewModel.Attendant = model.Attendants;
            //viewModel.AttendantTotal = model.AttendantTotal;
            viewModel.BudgetType = model.BudgetType;

            if (viewModel.BudgetType == EntertainmentGiftBudgetTypes.Entertainment.ToString())
            {
                viewModel.BudgetAmount = model.EntertainBudget;
            }
            else if (viewModel.BudgetType == EntertainmentGiftBudgetTypes.Gift.ToString())
            {
                viewModel.BudgetAmount = model.GiftBudget;
            }
            UserInfoModel UserInfo = new UserInfoModel();

            UserInfo.ViewMode = "Edit";
            //viewModel.FormStatus = model.EntertainmentGiftRecordStatu.Description;
            UserInfo = new UserInfoModel();
            UserInfo.FormNo = model.FormNo;
            UserInfo.Division = model.Division;
            UserInfo.Department = model.Department;
            UserInfo.StaffID = model.StaffID;
            UserInfo.SubmissionDate = model.SubmissionDate;
            UserInfo.StaffName = model.StaffName;
            UserInfo.FormStatus = EntertainmentGiftBLL.GetStatus(model.FormStatusID.Value).Description;
            UserInfo.ApplicationDate = model.ApplicationDate;
            viewModel.ApplicationDate = model.ApplicationDate;
            int PayeeID = model.Payee != null ? (int)model.Payee : 0;
			//var GetMember = new EntertainmentGiftBLL().GetMemberList(PayeeID).FirstOrDefault();
			int MemberID = model.MemberID != null && model.MemberID != "" ? int.Parse(model.MemberID) : 0;
			var GetMember = new EntertainmentGiftBLL().GetMemberList(MemberID).Where(e=>e.MemberId== PayeeID).FirstOrDefault();
			if (GetMember != null)
            {
                UserInfo.PayeeName = GetMember.MemberName;
                UserInfo.StaffID = GetMember.StaffId;

            }
            else
            {
                UserInfo.PayeeName = model.StaffName;
                UserInfo.StaffID = model.StaffID;
            }

            viewModel.UserInfo = UserInfo;
            //viewModel.SubmissionDate = model.SubmissionDate;

            viewModel.PreApproval = new EntertainmentPreApprovalViewModel();
            viewModel.PaymentReimbursement = new EntertainmentPaymentReimbursementViewModel();

            viewModel.PreApproval.Purpose = model.Purpose;
            viewModel.PreApproval.EventDateOn = model.EventOn;
            viewModel.PreApproval.EventDateTo = model.EventTo;
            viewModel.PreApproval.LateApplicationReason = model.LateReason;
            viewModel.PreApproval.NatureOfGift = model.Place_NatureOfGift;
            viewModel.PreApproval.Guest = model.Guests;
            viewModel.PreApproval.GuestTotal = model.GuestTotal;
            viewModel.PreApproval.Company = model.Company;
            viewModel.PreApproval.Attendant = model.Attendants;
            viewModel.PreApproval.AttendantTotal = model.AttendantTotal;
            viewModel.IsCompleted = model.IsCompleted == null ? false : (bool)model.IsCompleted;

            viewModel.Payee = model.Payee;


            if (model.EntertainmentGiftReimburseRecord != null)
            {
                viewModel.PaymentReimbursement.PostingInstruction = model.EntertainmentGiftReimburseRecord.PostingInstruction;
                viewModel.PaymentReimbursement.Purpose = model.EntertainmentGiftReimburseRecord.Purpose;
                viewModel.PaymentReimbursement.EventDateOn = model.EntertainmentGiftReimburseRecord.EventOn;
                viewModel.PaymentReimbursement.EventDateTo = model.EntertainmentGiftReimburseRecord.EventTo;
                viewModel.PaymentReimbursement.LateApplicationReason = model.LateReason;
                viewModel.PaymentReimbursement.NatureOfGift = model.EntertainmentGiftReimburseRecord.Place_NatureOfGift;
                viewModel.PaymentReimbursement.Guest = model.EntertainmentGiftReimburseRecord.Guests;
                viewModel.PaymentReimbursement.GuestTotal = model.EntertainmentGiftReimburseRecord.GuestTotal;
                viewModel.PaymentReimbursement.Company = model.EntertainmentGiftReimburseRecord.Company;
                viewModel.PaymentReimbursement.Attendant = model.EntertainmentGiftReimburseRecord.Attendants;
                viewModel.PaymentReimbursement.AttendantTotal = model.EntertainmentGiftReimburseRecord.AttendantTotal;

				if (model.EntertainmentGiftReimburseRecord.Currency != null && model.EntertainmentGiftReimburseRecord.Currency != "")
				{
					int CurrencyID = int.Parse(model.EntertainmentGiftReimburseRecord.Currency);
					var CurrencyModel = new EntertainmentGiftBLL().GetCurrencyList().Where(e => e.CurrencyID == CurrencyID).FirstOrDefault();
					if (CurrencyModel != null)
					{
						viewModel.PaymentReimbursement.CurrencyName = CurrencyModel.CurrencyName;
					}
				}
				else
				{
					if (model.EntertainmentGiftReimburseRecord.CurrencyTest != "" && model.EntertainmentGiftReimburseRecord.CurrencyTest != null)
					{
						viewModel.PaymentReimbursement.CurrencyName = model.EntertainmentGiftReimburseRecord.CurrencyTest;
					}
				}

                viewModel.PaymentReimbursement.Currency = model.EntertainmentGiftReimburseRecord.Currency;

                viewModel.PaymentReimbursement.Amount = model.EntertainmentGiftReimburseRecord.Amount;
                viewModel.PaymentReimbursement.ExchangeRate = model.EntertainmentGiftReimburseRecord.ExchangeRate;
                viewModel.PaymentReimbursement.AmountHKEntertain = model.EntertainmentGiftReimburseRecord.AmountHKEntertain;
                viewModel.PaymentReimbursement.AmountHKGift = model.EntertainmentGiftReimburseRecord.AmountHKGift;
                viewModel.PaymentReimbursement.AmountExceedReason = model.EntertainmentGiftReimburseRecord.AmountExceedReason;
                viewModel.PaymentReimbursement.PaymentMethod = model.EntertainmentGiftReimburseRecord.PaymentMethod;
                viewModel.PaymentReimbursement.PayTo = model.EntertainmentGiftReimburseRecord.PayTo;
                viewModel.PaymentReimbursement.NoOfDocumentOrReceiptAttached = model.EntertainmentGiftReimburseRecord.NoOfAttachment;
                viewModel.PaymentReimbursement.SAPDocNo = model.EntertainmentGiftReimburseRecord.SAPDocNo;
                viewModel.PaymentReimbursement.SAPRemark = model.EntertainmentGiftReimburseRecord.SAPRemark;
				viewModel.PaymentReimbursement.PostDate = model.EntertainmentGiftReimburseRecord.PostDate;
				viewModel.PaymentReimbursement.PostDateText = model.EntertainmentGiftReimburseRecord.PostDate!=null?
					((DateTime)model.EntertainmentGiftReimburseRecord.PostDate).ToString(Resources.Resource_Common.DisplayDateOnlyFormat) :"";

				viewModel.PaymentReimbursement.SAPCleaningDocNo = model.EntertainmentGiftReimburseRecord.SAPCleaningDocNo;
				viewModel.PaymentReimbursement.CleaningDate = model.EntertainmentGiftReimburseRecord.CleaningDate;
				viewModel.PaymentReimbursement.CleaningDateText = model.EntertainmentGiftReimburseRecord.CleaningDate != null ?
					((DateTime)model.EntertainmentGiftReimburseRecord.CleaningDate).ToString(Resources.Resource_Common.DisplayDateOnlyFormat) : "";



				if (model.EntertainmentGiftReimburseRecord.CostCenter != "" && model.EntertainmentGiftReimburseRecord.CostCenter != null)
                {
                    string CostCenterName = "";
                    string CostCenterCode = "";
                    string[] sArray = model.EntertainmentGiftReimburseRecord.CostCenter.Split(',');
                    List<CostCentersViewModel> CostCenterList = new List<CostCentersViewModel>(); /*GetCostCentersModelList();*/
                    foreach (var item in sArray)
                    {
                        int CostCenterId = int.Parse(item);
                        //GetMemberListViewModel userModel = GetCreatedByModel(model.CreatedBy);
       //                  PayeeID = 0;
       //                 if (model.Payee != null)
       //                 {
							//PayeeID = (int) model.Payee;
       //                 }

                        //int CreatedByID = int.Parse(model.CreatedBy);


                        CostCentersViewModel CostCentersModel = GetCostCentersModelList(PayeeID).Where(e => e.CostCenterId == CostCenterId).FirstOrDefault();
                        if (CostCentersModel != null)
                        {
                            CostCenterName += CostCentersModel.Description + ",";
                            CostCenterCode += CostCentersModel.Code + ",";
                            CostCenterList.Add(CostCentersModel);
                        }
                    }
                    if (CostCenterName != "")
                    {
                        CostCenterName = CostCenterName.Substring(0, (CostCenterName.Length - 1));
                    }
                    if (CostCenterCode != "")
                    {
                        CostCenterCode = CostCenterCode.Substring(0, (CostCenterCode.Length - 1));
                    }
                    viewModel.PaymentReimbursement.CostCenterName = CostCenterName;
                    viewModel.PaymentReimbursement.CostCenterCode = CostCenterCode;
                    viewModel.PaymentReimbursement.CostCenterList = CostCenterList;
                }
            }


        }


        public EntertainmentGiftRecord MapEntertainmentGiftPreApprovalFormViewModelToDBRecord(EntertainmentGiftFormViewModel viewModel, EntertainmentGiftRecord model)
        {
            if (model == null)
            {
                model = new EntertainmentGiftRecord();
            }

            //record.Payee = viewModel.Payee;

            model.BudgetType = viewModel.BudgetType;

            if (viewModel.BudgetType == EntertainmentGiftBudgetTypes.Entertainment.ToString())
            {
                model.EntertainBudget = viewModel.BudgetAmount;
            }
            else if (viewModel.BudgetType == EntertainmentGiftBudgetTypes.Gift.ToString())
            {
                model.GiftBudget = viewModel.BudgetAmount;
            }

            model.Purpose = viewModel.PreApproval.Purpose;
            model.EventOn = viewModel.PreApproval.EventDateOn;
            model.EventTo = viewModel.PreApproval.EventDateTo;
            model.LateReason = viewModel.PreApproval.LateApplicationReason;
            model.Place_NatureOfGift = viewModel.PreApproval.NatureOfGift;
            model.Guests = viewModel.PreApproval.Guest;
            model.GuestTotal = viewModel.PreApproval.GuestTotal;
            model.Company = viewModel.PreApproval.Company;
            model.Attendants = viewModel.PreApproval.Attendant;
            model.AttendantTotal = viewModel.PreApproval.AttendantTotal;
			model.Payee = viewModel.Payee != null ? viewModel.Payee : 0;

			//model.Payee = 237;
			if (model.ID == 0)
            {
                model.Division = viewModel.UserInfo.Division;
                model.Department = viewModel.UserInfo.Department;
                model.StaffID = viewModel.UserInfo.StaffID;
                model.StaffName = viewModel.UserInfo.StaffName;
            }
            return model;
        }

        public ActionResult Save_PreApproval(EntertainmentGiftFormViewModel viewModel)
        {
            try
            {
                EntertainmentGiftBLL EntertainmentGiftBLL = new EntertainmentGiftBLL();
                CommonBLL CommonBLL = new CommonBLL();

                var record = EntertainmentGiftBLL.Get(viewModel.ID);

                record = MapEntertainmentGiftPreApprovalFormViewModelToDBRecord(viewModel, record);

                EntertainmentGiftBLL.SaveRecord(record);

                return Json(new { Success = true, ID = record.ID, Title = Resources.Resource_DialogMessage.SaveSuccessMessage });
            }
            catch (Exception ex)
            {
                this.CreateErrorLog(ex);

                return Json(new { Success = false, Title = Resources.Resource_DialogMessage.UpdatedFailMessage });
            }
        }


        public ActionResult Draft(EntertainmentGiftFormViewModel viewModel)
        {
            try
            {
                EntertainmentGiftBLL EntertainmentGiftBLL = new EntertainmentGiftBLL();
                CommonBLL CommonBLL = new CommonBLL();

                var record = EntertainmentGiftBLL.Get(viewModel.ID);
                int FormStatusID = EntertainmentGiftBLL.GetStatusID(Data.Enum.EntertainmentGiftRecordStatusCode.Draft);
                var UserInfo = GetUserInfo;
                if (viewModel.ID != 0 && FormStatusID != record.FormStatusID)
                {
                    EntertainmentGiftReimburseRecord ReimburseRecord = ReimburseRecordModel(record, viewModel);
                    record.EntertainmentGiftReimburseRecord = ReimburseRecord;
                    record.LateReason = viewModel.PaymentReimbursement.LateApplicationReason;
                }
                else
                {
                    record = MapEntertainmentGiftPreApprovalFormViewModelToDBRecord(viewModel, record);
                    record.FormStatusID = FormStatusID;
                    record.CreatedBy = UserInfo.SAMAccountWithDomain;
                }

                EntertainmentGiftBLL.SaveRecord(record);

                //CommonBLL.InsertApprovalHistroy(record.ID, FormType, 0, )

                return Json(new { Success = true, ID = record.ID, Title = Resources.Resource_DialogMessage.DraftSuccessMessage });
            }
            catch (Exception ex)
            {
                this.CreateErrorLog(ex);

                return Json(new { Success = false, Title = Resources.Resource_DialogMessage.UpdatedFailMessage });
            }
        }

        public bool CheckApplicationValid(EntertainmentGiftFormViewModel view, ref string message)
        {
            return true;
        }
        private Dictionary<string, object> SetWorkflowVariable(int formID, int formTypeID, string NextApprover)
        {
            Dictionary<string, object> _Datafields = new Dictionary<string, object>();

            _Datafields[GeneralWFDataFields.FormID.ToString()] = formID.ToString();
            _Datafields[GeneralWFDataFields.FormTypeID.ToString()] = formTypeID.ToString();
            _Datafields[GeneralWFDataFields.HasNextStep.ToString()] = "True";
            _Datafields[GeneralWFDataFields.NextApprover.ToString()] = NextApprover;
            _Datafields[GeneralWFDataFields.StatusCode.ToString()] = EntertainmentGiftRecordStatusCode.InProgress.ToString();
            //_Datafields["FormID"] = formID.ToString();
            //_Datafields["RequestID"] = FormNo;
            //_Datafields["CurrentWFActivityID"] = FirstWFActivityID;
            //_Datafields["HasNextStep"] = true;
            //_Datafields["ProcessName"] = ProcessName;
            //_Datafields["StaffID"] = 0;
            //_Datafields["UserID"] = 0;
            //_Datafields["ActivityGroupID"] = 0;

            //if (CapexType != CapexType.Utilization)
            //{
            //    _Datafields["ApprovedEmailCode"] = "NonBudgetCapexApproved";
            //}

            //_Datafields["ApprovedEmailCode"] = "NonBudgetCapexApproved";
            //_Datafields["PendingEmailCode"] = NonBudgetCapexBLL.GetPendingEmailCode(FormNo, ClientAction.Submitted.ToString(), true);
            ////_Datafields["PendingEmailCode"] = "NonBudgetCapexPendingCase";
            //_Datafields["RejectEmailCode"] = "NonBudgetCapexRejected";
            //_Datafields["ReturnEmailCode"] = "NonBudgetCapexReturned";

            //_Datafields["CCUser"] = " ";
            //_Datafields["BCCUser"] = " ";

            ////_Datafields["UserID"]

            //if (!String.IsNullOrEmpty(RequesterUserName))
            //{
            //    _Datafields["Requester"] = RequesterUserName;
            //    _Datafields["Receiver"] = RequesterUserName + ";" + Applicant.DomainName + @"\" + Applicant.SAMAccountName;
            //}

            return _Datafields;
        }

        private int StartWorkflow(EntertainmentGiftRecord model, string FormNo, string NextApprover)
        {
            //UserInfo ApplicantUserInfo = OrganizationBLL.GetUser(ApplicantUserID);
            var EntertainmentGiftBLL = new EntertainmentGiftBLL();

            Dictionary<string, object> _Datafields = SetWorkflowVariable(model.ID, GetFormTypeID(), NextApprover);

            var emailmodel = EmailNotifactionBLL.GetEmailContent(EntertainmentGiftCode, -1, "PendingCase", FormTypes.EntertainmentGift.ToString(), "Preapproval_Submit", model.FormNo, GetUserInfo.MemberName, "#");
            if (emailmodel != null)
            {
                _Datafields[GeneralWFDataFields.Subject.ToString()] = emailmodel.Subject;
                _Datafields[GeneralWFDataFields.Body.ToString()] = emailmodel.Body;
                _Datafields[GeneralWFDataFields.ToUsers.ToString()] = NextApprover;
            }
            WorkflowData data = new WorkflowData();
            data.CentralRecordNum = "MEHK  : " + model.ID;
            //data.K2WorkflowInstanceUser = "k2admin";
            data.K2WorkflowInstanceUser = User.Identity.Name;

            int procInstID = WFControl.StartWorkflow(data, _Datafields, ProcessName);
            //int procInstID = WFControl.StartWorkflow(data, _Datafields);

            return procInstID;

        }

        //[TheLinkAuthorize(PageRights = new string[] { "CreateNonBudgetCAPEX_Project" })]
        private ApprovalMartixViewModel GetApprovalMatrixFromAPI(EntertainmentGiftFormViewModel viewModel, FlowType ftype, string Memberid)
        {
            ApprovalMartixViewModel approverModel = new ApprovalMartixViewModel();
            string Option = "";
            if (ftype == FlowType.PreApproval)
            {
                if (viewModel.BudgetType == ClaimTypes.Entertainment.ToString())
                {
                    if (viewModel.BudgetAmount >= EntertainmentAmount)
                    {
                        Option = "O";
                    }
                    else
                    {
                        Option = "B";
                    }
                }
                else
                {
                    if (viewModel.BudgetAmount >= GiftAmount)
                    {
                        Option = "O";
                    }
                    else
                    {
                        Option = "B";
                    }
                }
            }
            else
            {
                Option = "R";
            }
            APIAccessBLL apiBLL = new APIAccessBLL();
            approverModel = apiBLL.GetApprovers("Organization/ApprovalMartixApprovers/", Memberid.ToString(), EntertainmentGiftCode, Option);
            return approverModel;

        }

        private ApprovalMartixViewModel GetACAPReturnApprovalMatrix(ApprovalMartixViewModel modle)
        {

            var details = new List<ApprovalMartixDetailViewModel>();
            details = modle.Details.Where(p => FlowType.Reimbursement.ToString() + "_" + p.ApprovalLevelCode == WorkflowSteps.Reimbursement_ACRV1.ToString()
           || FlowType.Reimbursement.ToString() + "_" + p.ApprovalLevelCode == WorkflowSteps.Reimbursement_ACRV2.ToString()
           || FlowType.Reimbursement.ToString() + "_" + p.ApprovalLevelCode == WorkflowSteps.Reimbursement_ACAP.ToString()).ToList();
            modle.Details = details;
            return modle;
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [ValidateHeaderAntiForgeryTokenAttribute]
        public ActionResult Submit(EntertainmentGiftFormViewModel viewModel)
        {
            var EntertainmentGiftBLL = new EntertainmentGiftBLL();
            var UserInfo = GetUserInfo;
            try
            {
                string Message = "";

                if (CheckApplicationValid(viewModel, ref Message))
                {
                    var record = EntertainmentGiftBLL.Get(viewModel.ID);

                    record = MapEntertainmentGiftPreApprovalFormViewModelToDBRecord(viewModel, record);

                    record.SubmissionDate = DateTime.Now;
                    record.ApplicationDate = DateTime.Now;
                    record.MemberID = UserInfo.MemberId.ToString();
                    record.CreatedBy = UserInfo.SAMAccountWithDomain;
                    record.ModifiedBy = UserInfo.SAMAccountWithDomain;
					record.DepartmentID = UserInfo.UnitId;
					//record.FormStatusID = EntertainmentGiftBLL.GetStatusID(EntertainmentGiftRecordStatusCode.Approving_PreApproval);
					record.FormStatusID = EntertainmentGiftBLL.GetStatusID(EntertainmentGiftRecordStatusCode.InProgress);
					ApprovalMartixViewModel approverModel = GetApprovalMatrixFromAPI(viewModel, FlowType.PreApproval, record.MemberID);
                    record = EntertainmentGiftBLL.Submit(record, UserInfo, StartWorkflow, approverModel, FlowType.PreApproval);

                    string submitSuccessText = string.Format(Resources.Resource_DialogMessage.SubmitSuccessText, record.FormNo);
                    return Json(new
                    {
                        Success = true,
                        ID = record.ID,
                        Title = Resources.Resource_DialogMessage.SubmitSuccessTitle,
                        Text = submitSuccessText
                    });
                }
                else
                {
                    return Json(new
                    {
                        Success = false,
                        Title = Resources.Resource_DialogMessage.Warning,
                        Text = Message
                    });
                }


            }
            catch (Exception ex)
            {
                this.CreateErrorLog(ex);

                return Json(new { Success = false, Title = Resources.Resource_DialogMessage.UpdatedFailMessage });
            }
        }
        public ActionResult GetMemberList(string text, int? MemberID)
        {
            int id = 0;
            if (MemberID != null)
            {
                id = (int)MemberID;
            }
            List<GetMemberListViewModel> resultList = new EntertainmentGiftBLL().GetMemberList(id).Select(e => new GetMemberListViewModel
            {
                MemberId = e.MemberId != null ? (int)e.MemberId : 0,
                MemberName = e.MemberName
            }).ToList();
            //
            GetMemberListViewModel model = new GetMemberListViewModel();
            model.MemberId = GetUserInfo.MemberId;
            model.MemberName = GetUserInfo.MemberName;
            resultList.Add(model);
            //
            if (text != "" && text != null)
            {
                resultList = resultList.Where(e => e.MemberName.ToLower().IndexOf(text.Trim().ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }
        public List<CostCentersViewModel> GetCostCentersModelList(int MemberId)
        {
            List<CostCentersViewModel> list = new List<CostCentersViewModel>();

            APIAccessBLL apiBLL = new APIAccessBLL();
            var CostCenters = apiBLL.GetCostCenters("Organization/CostCenters/", MemberId);
            if (CostCenters.Count() > 0)
            {
                list = CostCenters.Where(e => e.IsDeleted == false).Select(e => new CostCentersViewModel
                {
                    Description = e.Description,
                    CostCenterId = e.CostCenterId,
                    Code = e.Code
                }).ToList();
            }
            return list;
        }
        public JsonResult GetCostCentersList(int ID)
        {
            List<CostCentersViewModel> list = new List<CostCentersViewModel>();
			var EntertainmentGiftBLL = new EntertainmentGiftBLL();
			var record = EntertainmentGiftBLL.Get(ID);
			int Payee = record.Payee != null ? (int)record.Payee : 0;
			//list = GetCostCentersModelList(GetUserInfo.MemberId);
			list = GetCostCentersModelList(Payee);
			return Json(list, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCurrencyList(string text)
        {
            List<CurrencyViewModel> list = new List<CurrencyViewModel>();

            list = new EntertainmentGiftBLL().GetCurrencyList().Select(e => new CurrencyViewModel
            {
                CurrencyID = e.CurrencyID,
                CurrencyName = e.CurrencyName
            }).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public List<ApprovalMartixDetailViewModel> GetApprovalMatrix(int MemberId, string ApprovalMartixOptionCode)
        {
            string ProcessCode = EntertainmentGiftCode;
            List<ApprovalMartixDetailViewModel> result = new List<ApprovalMartixDetailViewModel>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var CostCenters = apiBLL.GetApprovers("Organization/ApprovalMartixApprovers/", MemberId.ToString(), ProcessCode, ApprovalMartixOptionCode);
            if (CostCenters != null)
            {
                if (CostCenters.Details != null)
                {
                    result = CostCenters.Details.ToList();
                }
            }
            return result;

        }
        //public ActionResult GetApprovalMatrixGrid([DataSourceRequest]DataSourceRequest request, int MemberId, string ApprovalMartixOptionCode)
        //{
        //	string ProcessCode = EntertainmentGiftCode;
        //	List<ApprovalMartixGridViewModel> result = new List<ApprovalMartixGridViewModel>();
        //	APIAccessBLL apiBLL = new APIAccessBLL();
        //	var CostCenters = apiBLL.GetApprovers("Organization/ApprovalMartixApprovers/", MemberId.ToString(), ProcessCode, ApprovalMartixOptionCode);
        //	if (CostCenters != null)
        //	{
        //		if (CostCenters.Details != null)
        //		{
        //			result = CostCenters.Details.Select(e => new ApprovalMartixGridViewModel
        //			{
        //				ApprovalLevelCode = e.ApprovalLevelCode,
        //				ApprovalLevelName = e.ApprovalLevelName,
        //				Approvers = e.Approvers,
        //				ApproversString = string.Join(",", e.Approvers)
        //			}).ToList();
        //		}
        //	}

        //	return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        //}
        //public List<ApprovalMartixGridViewModel> GetApprovalMatrixGrid( int MemberId, string ApprovalMartixOptionCode)
        //{
        //	string ProcessCode = EntertainmentGiftCode;
        //	List<ApprovalMartixGridViewModel> result = new List<ApprovalMartixGridViewModel>();
        //	APIAccessBLL apiBLL = new APIAccessBLL();
        //	var CostCenters = apiBLL.GetApprovers("Organization/ApprovalMartixApprovers/", MemberId.ToString(), ProcessCode, ApprovalMartixOptionCode);
        //	if (CostCenters != null)
        //	{
        //		result = CostCenters.Details.Select(e => new ApprovalMartixGridViewModel
        //		{
        //			ApprovalLevelCode = e.ApprovalLevelCode,
        //			ApprovalLevelName = e.ApprovalLevelName,
        //			Approvers = e.Approvers,
        //			ApproversString = string.Join(",", e.Approvers)
        //		}).ToList();
        //	}

        //	return result;
        //}
        public List<Voucher> PaymentReimbursementListToDBRecord(List<PaymentReimbursementGridViewModel> list, int FormID)
        {
            List<Voucher> resultList = new List<Voucher>();
            var UserInfo = GetUserInfo;
            resultList = list.Select(e => new Voucher
            {
                ID = e.ID,
                FormID = FormID,
                DrCr = e.Dr_Cr,
				//PostKey = e.PostKey,
				PostKey = e.Dr_Cr=="Dr"?"40":"31",
				AcctCode = e.AcctCode,
                PayeeAcctType = e.Payee_AcctType,
                Amount = e.Amount,
                Centre = e.Centre,
                Text = e.Text,
                CreatedBy = UserInfo.MemberId,
                CreatedDate = DateTime.Now,
                ModifiedBy = UserInfo.MemberId,
                ModifiedDate = DateTime.Now,
				IsDeleted=false
            }).ToList();
            return resultList;
        }
        public ActionResult GetExchangeRate(string CurrencyName)
        {
            string result = "";
            var model = new EntertainmentGiftBLL().GetCurrencyList().Where(e => e.CurrencyName == CurrencyName).FirstOrDefault();
            if (model != null)
            {
                result = model.ExchangeRate != null ? model.ExchangeRate.ToString() : "";
            }
            return Json(result);
        }
        public GetMemberListViewModel GetCreatedByModel(string CreatedByAD)
        {
            string CreatedBy = CreatedByAD.Contains('\\') ? CreatedByAD.Split('\\')[1] : CreatedByAD;
			APIAccessBLL apiBLL = new APIAccessBLL();
			var resultList = apiBLL.GetMember().Where(e => e.SAMAccount.ToLower() == CreatedBy.ToLower())
				.Select(e => new GetMemberListViewModel
				{
					MemberId = e.MemberId,
					MemberName = e.MemberName
				}).FirstOrDefault(); 

			//GetMemberListViewModel userModel = new EntertainmentGiftBLL().GetMemberList(0)
   //             .Where(e => e.MemberName == CreatedBy)
   //             .Select(e => new GetMemberListViewModel
   //             {
   //                 MemberId = e.MemberId != null ? (int)e.MemberId : 0,
   //                 MemberName = e.MemberName
   //             }).FirstOrDefault();
            return resultList;
        }

        public ActionResult PrintForm(string responseHistory_printStream__business, string formid)
        {

            try
            {
                responseHistory_printStream__business = responseHistory_printStream__business.Replace("data:application/pdf;base64,", "");

                //MemoryStream workStream = EDocBaseBLL.MergeMemoStream(responseHistory_printStream__business, RE_path);
                string filePath = "";
                return Json(new MessageViewModel
                {
                    Success = true,
                    Title = filePath,
                    Text = filePath
                });
            }
            catch (Exception ex)
            {
                this.CreateErrorLog(ex);
                return Json(new MessageViewModel
                {
                    Success = false,
                    Title = ex.Message,
                    Text = ex.InnerException.Message
                });
            }


        }

        [HttpPost]
        public JsonResult BatchApproval(List<BatchAprrovalViewModel> batchApprovalViewModel)
        {
            MessageViewModel modle = new MessageViewModel();
            foreach(var item in batchApprovalViewModel)
            {

            }
            return Json(modle, JsonRequestBehavior.AllowGet);
              
        }
		public ActionResult UpdatePaymentReimbursement([DataSourceRequest] DataSourceRequest request, PaymentReimbursementGridViewModel model)
		{
			List<PaymentReimbursementGridViewModel> resultuf = new List<PaymentReimbursementGridViewModel>();

			resultuf.Add(model);
			return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

		}
		
		public ActionResult CostCentersDropDownList(string text,int ID)
		{
			List<SelectListItem> resultList = new List<SelectListItem>();

			var EntertainmentGiftBLL = new EntertainmentGiftBLL();
			var record = EntertainmentGiftBLL.Get(ID);
			if (record != null)
			{
				//int MemberID = record.MemberID != null && record.MemberID != "" ? int.Parse(record.MemberID) : 0;
				int Payee = record.Payee != null ? (int)record.Payee : 0;
				resultList = GetCostCentersModelList(Payee).Select(e => new SelectListItem
				{
					Text=e.Description,
					Value=e.Code
				}).ToList();
			}
			if (text != "" && text != null)
			{
				resultList = resultList.Where(e => e.Text.ToLower().IndexOf(text.Trim().ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
			}
			return Json(resultList, JsonRequestBehavior.AllowGet);
		}
		public ActionResult GetBudget(int formID,string BudgetType)
		{
			var model = new CommonBLL().GetBudget(formID);
			if (model != null)
			{
				var EntertainmentGiftBLL = new EntertainmentGiftBLL();
				decimal ResultAmount = 0;
				var record = EntertainmentGiftBLL.Get(formID);
				var ReimburseRecord = EntertainmentGiftBLL.GetReimburseRecord(formID);
				if (record.IsCompleted == false)
				{
					if (BudgetType == ClaimTypes.Entertainment.ToString())
					{
						ResultAmount = record.EntertainBudget != null ? (decimal)record.EntertainBudget : 0;
					}
					else
					{
						ResultAmount = record.GiftBudget != null ? (decimal)record.GiftBudget : 0;
					}
				}
				else
				{
					if (BudgetType == ClaimTypes.Entertainment.ToString())
					{
						ResultAmount = ReimburseRecord.AmountHKEntertain != null ? (decimal)ReimburseRecord.AmountHKEntertain : 0;
					}
					else
					{
						ResultAmount = ReimburseRecord.AmountHKGift != null ? (decimal)ReimburseRecord.AmountHKGift : 0;
					}
				}

				string BudgetYear = DateTime.Now.Year.ToString();
				if (record.SubmissionDate != null)
				{
					BudgetYear = ((DateTime)record.SubmissionDate).Year.ToString();
				}
				var GetAnnualBudgetModel = new CommonBLL().GetAnnualBudget(record.DepartmentID + "", BudgetYear);
				if (GetAnnualBudgetModel != null)
				{
					if (BudgetType == ClaimTypes.Entertainment.ToString())
					{
						if (GetAnnualBudgetModel.EntertainBudget <( model.EntertainBudget+ ResultAmount))
						{
							return Json(new { Success = false, Title = "" });
						}
					}
					else
					{
						if (GetAnnualBudgetModel.GiftBudget < (model.GiftBudget+ ResultAmount))
						{
							return Json(new { Success = false, Title = "" });
						}
					}
				}
				else
				{
					return Json(new { Success = false, Title = "No annual budget" });
				}
			}
			return Json(new { Success = true, Title = "" });
		}
	}
}
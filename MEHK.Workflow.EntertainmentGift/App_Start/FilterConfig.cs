﻿using System.Web;
using System.Web.Mvc;

namespace MEHK.Workflow.EntertainmentGift
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
		}
	}
}

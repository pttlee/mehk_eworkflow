﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.EntertainmentGift.ViewModels
{
    public class MyDraftGridViewModel
    {

        public int ID { get; set; }
        public string FormType { get; set; }

        [Display(Name = "Form Type")]
        public string FormTypeDisplayName { get; set; }

        [Display(Name = "Status")]
        public string FormStatus { get; set; }
    }
}
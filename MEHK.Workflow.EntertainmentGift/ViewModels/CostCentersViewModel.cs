﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.EntertainmentGift.ViewModels
{
	public class CostCentersViewModel
	{
		public int CostCenterId { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public string PIC { get; set; }
		public string CurrencyCode { get; set; }
		public bool IsDeleted { get; set; }
		public string Owner { get; set; }

	}
}
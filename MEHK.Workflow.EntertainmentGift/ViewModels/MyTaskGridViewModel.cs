﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.EntertainmentGift.ViewModels
{
    public class MyTaskGridViewModel
    {
        public int ID { get; set; }

        public int ProcInstID { get; set; }

        //[Display(Name = "Non-Budget CAPEX Status")]
        //public string NonBudgetCapexStatus { get; set; }

        public string FormStatus { get; set; }

        [Display(Name ="Status")]
        public string FormStatusDisplayName { get; set; }

        [Display(Name = "Form No")]
        public string FormNo { get; set; }


        public  string FormType { get; set; }

        [Display(Name = "Form Type")]
        public string FormTypeDisplayName { get; set; }

        //[Display(Name = nameof(Resource_CapexNonBudget.SearchResultCapexFormTitle), ResourceType = typeof(Resource_CapexNonBudget))]
        //public string Title { get; set; }

        [Display(Name = nameof(Resource_Common.SubmissionDate), ResourceType = typeof(Resource_Common))]
        public DateTime? SubmissionDate { get; set; }

        public string K2ItemSerialNo { get; set; }
        public string CreatedBy { get; set; }


    }
}
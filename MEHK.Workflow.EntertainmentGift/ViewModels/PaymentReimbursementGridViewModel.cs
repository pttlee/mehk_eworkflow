﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MEHK.Workflow.EntertainmentGift.ViewModels
{
	public class PaymentReimbursementGridViewModel
	{
		public int ID { get; set; }
		public int Number { get; set; }
		[DataMember]
		[Required(ErrorMessage = "Dr_Cr is required")]
		public string Dr_Cr { get; set; }
		public string PostKey { get; set; }
		public string AcctCode { get; set; }
		public string Payee_AcctType { get; set; }
		public decimal Amount { get; set; }
		[DataMember]
		[Required(ErrorMessage = "Centre is required")]
		public string Centre { get; set; }
		public string Text { get; set; } 
	}
}
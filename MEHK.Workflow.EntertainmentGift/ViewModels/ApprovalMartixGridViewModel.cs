﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.EntertainmentGift.ViewModels
{
	public class ApprovalMartixGridViewModel
	{
		public string ApprovalLevelCode { get; set; }
		public string ApprovalLevelName { get; set; }
		public List<string> Approvers { get; set; }
		public string ApproversString { get; set; }
	}
}
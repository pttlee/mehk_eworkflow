﻿//var ProcessCode = 'NonBudgetCAPEX';
//var UploadTitle = 'Non Budget CAPEX Form';
var NumberCount = 1;

//var SOWUpdateClick = false;
//var ApiUrl = "http://192.168.1.227:8089/MEHK.CommonAPI/api/";

function iterationCopy(src) {
    let target = {};
    for (let prop in src) {
        if (src.hasOwnProperty(prop)) {
            target[prop] = src[prop];
        }
    }
    return target;
}


function getID() {

    var data = { ID: $("#ID").val() }
    return data;
}

function selectFile(e) {
}

function onUploadSuccess(e) {
}
function onUpload(e) {
    // pass the form Data

    //var fileDropDownList = $("#AttachmentType").data("kendoDropDownList");

    var data = {
        refid: $("#ID").val(),
        process: $("#EntertainmentGiftCode").val(),
        //CatName: fileDropDownList.text(),
        //FileCode: fileDropDownList.value(),
        remark: '',
        userID: $("#CurrentUserID").val(),
        attachmentType: "",
        title: "",
        GID: $("#GID").val(),
    }
    e.data = data;
}

function onUploadError(e) {


}

function onUploadComplete(e) {
    $("#DocumentTitle").val(''),
        $("#DocumentRemark").val('');

    // Reset the Title Textbox
}
function getUserFilterData() {
    var ddl = $("#Payee").data("kendoDropDownList");
    var MemberID = $("#MemberId").val();
    if (MemberID == "") {
        MemberID = 0;
    }
    return {
        text: ddl.filterInput.val(),
        MemberID: MemberID
    };
}
var _pdfstring;
$(function () {
    //InitListener();
    if ($("#CurrentStep").val() == Reimbursement_ACAP) {
        GetPDFString();
    }
    var allowedExtensions = [".gif", ".jpg", ".png", ".pdf",".txt",".docx",".xlsx",".xls"];
    var maxFileSize = 1048576 * 10;
    InitFiles($("#ID").val(), $("#GID").val(), $("#EntertainmentGiftCode").val());
    var uploadSetting = {};
    uploadSetting.allowedExtensions = allowedExtensions;
    uploadSetting.processCode = $("#EntertainmentGiftCode").val();
    uploadSetting.formID = $("#ID").val();
    uploadSetting.InvalidMaxFileSizeMessge = "File size maximum " + 10 + "M/per file"
    uploadSetting.maxFileSize = maxFileSize;
    uploadSetting.UploadControl = "#files";
    uploadSetting.TableArea = "#UploadedFiles";
    uploadSetting.UploadedFilesTable = "#UploadedFilesGrid";
    uploadSetting.GID = $("#GID").val();
    uploadSetting.TableColumnList = IninUploadMobileTalbeColumn('true', $("#GID").val(), $("#CurrentUserID").val());
    uploadSetting.TableField = InitFields();
    IninUpload(uploadSetting, selectFile, onUploadComplete, onUpload, onUploadSuccess, onUploadError);
    if (ViewmodelType == "Edit") {
        var AprrovalHistorySetting = {};
        AprrovalHistorySetting.FormID = FormID;
        AprrovalHistorySetting.FormTypeID = FormTypeID;
        AprrovalHistorySetting.DateFormat = DateFormat;
        AprrovalHistorySetting.UploadedFilesTable = "#WorkflowLog"
        BindAprrovalHistoryGrid(AprrovalHistorySetting)
    }
    //	$("#IsNoDuplicationOfBudget").kendoMultiSelect({
    //		placeholder: "Select products...",
    //		dataTextField: "Description",
    //		dataValueField: "CostCenterId",
    //		autoBind: false,
    //		dataSource: dataSource
    //	});
    //}
    //var data = {
    //	SAMAccount: $("#SAMAccount").val(),
    //}
    //GetUserinfo($("#SAMAccount").val());
    //var db = Userinfo;
    //$.ajax({
    //	dataType: 'json',
    //	type: 'GET',
    //	url: ApiUrl +'Organization/Members?SAMAccount='+ $("#SAMAccount").val(),
    //	//data: JSON.stringify(data),
    //	success: function (data) {
    //		if (data!=null) {
    //			$("#MemberId").val(data[0].MemberId)
    //			$("#StaffID").html(data[0].StaffId)
    //			$("#StaffName").html(data[0].MemberName)
    //			$("#Division").html(data[0].Division)
    //			$("#Department").html(data[0].Department)
    //			var Payee = $("#Payee").data("kendoDropDownList");
    //			Payee.dataSource.read();
    //		}

    //	},
    //	failure: function (response) {

    //	}
    //});

    kendo.ui.validator.rules.mvcdate = function (input) {

        return !input.is("[data-val-date]") || input.val() === "" || kendo.parseDate(input.val(), DisplayDateOnlyFormat) !== null || kendo.parseDate(input.val(), DisplayMonthFormat) !== null;
    };

    //$(document).on("keypress", ":input:not(textarea)", function (event) {
    //    if (event.keyCode == 13) {
    //        event.preventDefault();
    //    }
    //});

    //var allowedExtensions = AllowedExtensionsSetting.match(/(?=\S)[^,]+?(?=\s*(,|$))/g);
    //var maxFileSize = 1048576 * MaxFileSizeSetting;

    CategoryText = "Attachment Type";


    //InitFiles($("#ID").val(), $("#GID").val(), ProcessCode);

    //var uploadSetting = {};
    //uploadSetting.allowedExtensions = allowedExtensions;
    //uploadSetting.maxFileSize = maxFileSize;
    //uploadSetting.UploadControl = "#files";
    //uploadSetting.TableArea = "#UploadedFiles";
    //uploadSetting.UploadedFilesTable = "#UploadedFilesGrid";
    //uploadSetting.GID = $("#GID").val();
    //uploadSetting.TableColumnList = InitUploadTableColumn($("#EnableDeleteFile").val(), $("#GID").val(), $("#CurrentUserID").val(), ["Category", "DocumentName", "UploadedBy", "UploadedDate"], ShowUploadAction);
    //uploadSetting.TableField = InitFields();

    //IninUpload(uploadSetting, selectFile, onUploadComplete, onUpload, onUploadSuccess, onUploadError);


    $("#btnDeleteDraft").click(function () {

        DialogMessage.ConfirmMessage(getConfirmTitle(event), '', function () {

            mApp.blockPage();

            var data = {
                ID: $("#ID").val(),
            }

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: 'DeleteDraft',
                data: JSON.stringify(data),
                success: function (data) {
                    if (data.Success) {

                        RemoveWindowOnBeforeUnloadListener();

                        DialogMessage.AlertMessageAndRedirect(data.Title,
                            data.Text,
                            'MyDraft');

                        mApp.unblockPage();
                    }
                    else {

                        DialogMessage.AlertMessage(data.Title,
                            data.Text);
                        mApp.unblockPage();
                    }
                },
                failure: function (response) {
                    mApp.unblockPage();
                }
            });
        });

        return false;
    });
    function checkDate(dateStr) {
        var reg = /^[1-9]\d{3}(\/)(0[1-9]|1[0-2])(\/)(0[1-9]|[1-2][0-9]|3[0-1])$/;
        var regExp = new RegExp(reg);
        if (!regExp.test(dateStr)) {
            return false;
        } else {
            return true;
        }
    }
    function GetApprovalMartixOptionCode() {
    	var BudgetType = "";
    	var ApprovalMartixOptionCode = "";
    	if ($("#ID").val() != "0" && $("#EnablePreApprovalPart").val().toUpperCase() == "FALSE") {
    		BudgetType = $("#BudgetType").val()
    	} else {
    		BudgetType = $("input:radio[name=BudgetType]:checked").val();
    	}
    	if ($("#ReimbursementSubmit").val() == "true") {
    		ApprovalMartixOptionCode = "R"
    	} else {
    		if (BudgetType == "Entertainment") {
    			if ($("#BudgetAmount").val() >= EntertainmentAmount) {
    				ApprovalMartixOptionCode = "O"
    			} else {
    				ApprovalMartixOptionCode = "B"
    			}
    		} else {
    			if ($("#BudgetAmount").val() >= GiftAmount) {
    				ApprovalMartixOptionCode = "O"
    			} else {
    				ApprovalMartixOptionCode = "B"
    			}
    		}
    	}
    	return ApprovalMartixOptionCode;
    }
    function isApprovalMatrix() {
    	var isNoApproval = "false";
    	var Payee = $("#Payee").val();
    	var ApprovalMartixOptionCode = GetApprovalMartixOptionCode();
    	$.ajax({
    		type: 'POST',
    		url: "../../MEHK.dashboard/Home/GetApprovalMatrixGrid",
    		cache: false,
    		async: false,
    		data: {
    			MemberId: Payee,
    			ApprovalMartixOptionCode: ApprovalMartixOptionCode
    		},
    		success: function (data) {
    			if (data != null) {

    				if (data.Data.length == 0) {
    					isNoApproval= "false";
    				} else {
    					isNoApproval= "true";
    				}


    			}
    			else {
    				isNoApproval= "false";
    			}
    		},
    		failure: function (response) {
    			isNoApproval= "false";
    		}
    	});
    	return isNoApproval;
    }
    $("#btnSubmit").click(function () {

        var ConfirmTitle = getConfirmTitle(event);
        var EventDateOn = $("#PreApproval_EventDateOn").val();
        var EventDateTo = $("#PreApproval_EventDateTo").val();
        if (EventDateOn != "" && EventDateTo != "") {
            if (!checkDate(EventDateOn)) {
                swal({ title: "Incorrect format! " })
                return false;
            }
            if (!checkDate(EventDateTo)) {
                swal({ title: "Incorrect format! " })
                return false;
            }
        }
        var isNoApproval = isApprovalMatrix();
        if (isNoApproval=="true") {
        	Submit(ConfirmTitle);
        } else {
        	swal({ title: "No Approval Matrix! " })
        	return false;
			}

      
        //Submit(ConfirmTitle);

        return false;
    });

    $("#btnSave_PreApproval").click(function () {

        DialogMessage.ConfirmMessage(getConfirmTitle(event), '', function () {

            mApp.blockPage();

            var data = getFormData();

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: 'Save_PreApproval',
                data: JSON.stringify(data),
                success: function (data) {

                    if (data.Success) {

                        //$("#ID").val(data.ID);

                        //SubmitChanges(data.ID, $("#GID").val(), $("#CurrentUserID").val(), ProcessCode);

                        DialogMessage.AlertMessage(data.Title,
                            data.Text);
                        mApp.unblockPage();

                        //$("#UploadedFilesGrid").data('kendoGrid').dataSource.read();
                        //$("#UploadedFilesGrid").data('kendoGrid').refresh();
                    }
                    else {
                        DialogMessage.AlertMessage(data.Title,
                            data.Text);
                        mApp.unblockPage();
                    }
                },
                failure: function (response) {
                    //swal('Draft Failed!');

                    mApp.unblockPage();
                }
            });
        });


        return false;
    });

    $(".WorkflowAction").click(function () {
        var ret = true;

        var WFAction = $(this).data('wfaction');
        var FormAction = $(this).data('formaction');

        //var FormType = $("#FormType").val();
        //var RoleName = $("#RoleName").val();
        //var Step = $("#Step").val();

        var FormType = '';
        var RoleName = '';
        var Step = '';
        var RedirectURL = '../../MEHK.dashboard/Home/MyTask';

        var PostURL = 'WorkflowAction';
        var headers = { __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() };
        var data = {
            K2SN: $("#K2SN").val(),
            ID: $("#ID").val(),
            Comment: $("#Comment").val(),
            Step: $("#Step").val(),
            FormType: FormType,
            RoleName: RoleName,
            WFAction: WFAction,
            FormAction: FormAction,
            Payee: $("#Payee").val(),
            UserInfo: {
                //Division: $("#Division").val(),
                //StaffID: $("#StaffID").val(),
                //Department: $("#Department").val(),
                //StaffName: $("#StaffName").val(),
                Division: $("#UserInfo_Division").val(),
                Department: $("#UserInfo_Department").val(),
                StaffName: $("#UserInfo_StaffName").val(),
                StaffName: $("#UserInfo_StaffName").val(),
            },
            pdfString: _pdfstring

        }
      

        //data = Object.assign(data, UserInfo);
        var AmountHKEntertain = 0;
        var AmountHKGift = 0;
        var ReimbursementData = {};
        if (WFAction == 'ReimbursementSubmit' || (WFAction == 'Resubmit' && $("#ReIsCompleted").val() != false)) {
            //var BudgetType = $("#BudgetType").val();
            if ($("#BudgetType").val() == "Entertainment") {
                AmountHKEntertain = $("#ResultAmount").val()
            } else {
                AmountHKGift = $("#ResultAmount").val()
            }

            var CostCenter = $("#IsNoDuplicationOfBudget").val().join(',');
            if (CostCenter == "" || CostCenter == null) {
                swal({ title: "Please select Cost Centre " })
                return false;
            }
            var EventDateOn = $("#PaymentReimbursement_EventDateOn").val();
            var EventDateTo = $("#PaymentReimbursement_EventDateTo").val();
            if (EventDateOn != "" && EventDateTo != "") {
            	if (!checkDate(EventDateOn)) {
            		swal({ title: "Incorrect format! " })
            		return false;
            	}
            	if (!checkDate(EventDateTo)) {
            		swal({ title: "Incorrect format! " })
            		return false;
            	}
            }
            var Amount = $("#PaymentReimbursement_Amount").val();
            if (Amount <= 0) {
            	swal({ title: "Amount cannot be less than 0! " })
            	return false;
            }
            var ExchangeRate = $("#PaymentReimbursement_ExchangeRate").val();
            if (ExchangeRate <= 0) {
            	swal({ title: "Exchange Rate cannot be less than 0! " })
            	return false;
            }
            var isNoApproval = isApprovalMatrix();
            if (isNoApproval != "true") {
            	swal({ title: "No Approval Matrix! " })
            	return false;
            }

            ReimbursementData = {
                //PaymentReimbursement: $('#SReimbursementForm').serialize()
                PaymentReimbursement: {
                    PostingInstruction: $("#PaymentReimbursement_PostingInstruction").val(),
                    Purpose: $("#PaymentReimbursement_Purpose").val(),
                    CostCenter: CostCenter,
                    EventDateOn: $("#PaymentReimbursement_EventDateOn").data("kendoDatePicker").value(),
                    EventDateTo: $("#PaymentReimbursement_EventDateTo").data("kendoDatePicker").value(),
                    //EventDateOnString: $("#PaymentReimbursement_EventDateOnString").val(),
                    //EventDateToString: $("#PaymentReimbursement_EventDateToString").val(),
                    LateApplicationReason: $("#PreApproval_LateApplicationReason").val(),
                    NatureOfGift: $("#PaymentReimbursement_NatureOfGift").val(),
                    Guest: $("#PaymentReimbursement_Guest").val(),
                    GuestTotal: $("#PaymentReimbursement_GuestTotal").val(),
                    Company: $("#PaymentReimbursement_Company").val(),
                    Attendant: $("#PaymentReimbursement_Attendant").val(),
                    AttendantTotal: $("#PaymentReimbursement_AttendantTotal").val(),

                    Currency: $("#PaymentReimbursement_Currency").val(),
                    Amount: $("#PaymentReimbursement_Amount").val(),
                    ExchangeRate: $("#PaymentReimbursement_ExchangeRate").val(),
                    AmountHKEntertain: AmountHKEntertain,
                    AmountHKGift: AmountHKGift,
                    AmountExceedReason: $("#PaymentReimbursement_AmountExceedReason").val(),
                    PaymentMethod: $("input:radio[name=PaymentMethod]:checked").val(),
                    PayTo: $("#PaymentReimbursement_PayTo").val(),
                    NoOfDocumentOrReceiptAttached: $("#PaymentReimbursement_NoOfDocumentOrReceiptAttached").val()

                }
            }
        }



        if (WFAction == 'ReimbursementSubmit') {
            //var dOn = $("#PaymentReimbursement_EventDateOn").data("kendoDatePicker").value();
            //var dTo = $("#PaymentReimbursement_EventDateTo").data("kendoDatePicker").value();
            //$("#PaymentReimbursement_EventDateOnString").val(kendo.toString(dOn, "yyyy") + "-" + kendo.toString(dOn, "MM")+"-" + kendo.toString(dOn, "dd"))
            //$("#PaymentReimbursement_EventDateToString").val(kendo.toString(dTo, "yyyy") + "-" + kendo.toString(dTo, "MM")+"-" + kendo.toString(dTo, "dd"))




        	//data = $.extendn(data, ReimbursementData);
        	data = $.extend(data, ReimbursementData);
        }
        if (WFAction == 'Resubmit') {
            var IsCompleted = $("#IsCompleted").val();
            if (IsCompleted != null && IsCompleted.toLowerCase() == "true") {
                if ($("#BudgetType").val() == "Entertainment") {
                    AmountHKEntertain = $("#ResultAmount").val()
                } else {
                    AmountHKGift = $("#ResultAmount").val()
                }
                var CostCenter = $("#IsNoDuplicationOfBudget").val().join(',');
                if (CostCenter == "" || CostCenter == null) {
                    swal({ title: "Please select Cost Centre " })
                    return false;
                }
               
                var EventDateOn = $("#PaymentReimbursement_EventDateOn").val();
                var EventDateTo = $("#PaymentReimbursement_EventDateTo").val();
                if (EventDateOn != "" && EventDateTo != "") {
                	if (!checkDate(EventDateOn)) {
                		swal({ title: "Incorrect format! " })
                		return false;
                	}
                	if (!checkDate(EventDateTo)) {
                		swal({ title: "Incorrect format! " })
                		return false;
                	}
                }
                var Amount = $("#PaymentReimbursement_Amount").val();
                if (Amount <= 0) {
                	swal({ title: "Amount cannot be less than 0! " })
                	return false;
                }
                var ExchangeRate = $("#PaymentReimbursement_ExchangeRate").val();
                if (ExchangeRate <= 0) {
                	swal({ title: "Exchange Rate cannot be less than 0! " })
                	return false;
                }
                ReimbursementData = {
                    //PaymentReimbursement: $('#SReimbursementForm').serialize()
                    PaymentReimbursement: {
                        PostingInstruction: $("#PaymentReimbursement_PostingInstruction").val(),
                        Purpose: $("#PaymentReimbursement_Purpose").val(),
                        CostCenter: CostCenter,
                        EventDateOn: $("#PaymentReimbursement_EventDateOn").data("kendoDatePicker").value(),
                        EventDateTo: $("#PaymentReimbursement_EventDateTo").data("kendoDatePicker").value(),
                        //EventDateOnString: $("#PaymentReimbursement_EventDateOnString").val(),
                        //EventDateToString: $("#PaymentReimbursement_EventDateToString").val(),
                        LateApplicationReason: $("#PreApproval_LateApplicationReason").val(),
                        NatureOfGift: $("#PaymentReimbursement_NatureOfGift").val(),
                        Guest: $("#PaymentReimbursement_Guest").val(),
                        GuestTotal: $("#PaymentReimbursement_GuestTotal").val(),
                        Company: $("#PaymentReimbursement_Company").val(),
                        Attendant: $("#PaymentReimbursement_Attendant").val(),
                        AttendantTotal: $("#PaymentReimbursement_AttendantTotal").val(),

                        Currency: $("#PaymentReimbursement_Currency").val(),
                        Amount: $("#PaymentReimbursement_Amount").val(),
                        ExchangeRate: $("#PaymentReimbursement_ExchangeRate").val(),
                        AmountHKEntertain: AmountHKEntertain,
                        AmountHKGift: AmountHKGift,
                        AmountExceedReason: $("#PaymentReimbursement_AmountExceedReason").val(),
                        PaymentMethod: $("input:radio[name=PaymentMethod]:checked").val(),
                        PayTo: $("#PaymentReimbursement_PayTo").val(),
                        NoOfDocumentOrReceiptAttached: $("#PaymentReimbursement_NoOfDocumentOrReceiptAttached").val()

                    }
                }
                //data = Object.assign(data, ReimbursementData);
                data = $.extend(data, ReimbursementData);
            } else {
                var ReIsCompleted = $("#ReIsCompleted").val();
                //data = getFormData();
                if (ReIsCompleted == false) {
                	data = $.extend(getFormData(), data);
                } else {
                    //var dOn = $("#PaymentReimbursement_EventDateOn").data("kendoDatePicker").value();
                    //var dTo = $("#PaymentReimbursement_EventDateTo").data("kendoDatePicker").value();
                    //$("#PaymentReimbursement_EventDateOnString").val(kendo.toString(dOn, "yyyy") + "-" + kendo.toString(dOn, "MM")+"-" + kendo.toString(dOn, "dd"))
                    //$("#PaymentReimbursement_EventDateToString").val(kendo.toString(dTo, "yyyy") + "-" + kendo.toString(dTo, "MM")+"-" + kendo.toString(dTo, "dd"))


                	data = $.extend(data, ReimbursementData);
                }
            }

            //data = {...data, ...getFormData()};

            //data.K2SN = $("#K2SN").val();
            //data.FormType = FormType;
            //data.RoleName = RoleName;
            //data.WFAction = WFAction;
            //data.FormAction = FormAction;

            WorkflowAction(getConfirmTitle(event), '', PostURL, RedirectURL, data);

        } else if (WFAction == 'Return') {
            if (validateComment()) {

                WorkflowAction('', getConfirmTitle(event), PostURL, RedirectURL, data);
            }
        }
        else {
            var PaymentReimbursementGridEdit = $("#PaymentReimbursementGridEdit").val();
            //if(PaymentReimbursementGridEdit=="true"){
            if ($("#CurrentStep").val() == Reimbursement_ACRV1 || $("#CurrentStep").val() == Reimbursement_ACRV2) {
                var GridData = $("#PaymentReimbursementGrid").data('kendoGrid').dataSource._data;
                //var PaymentReimbursementList=[];
                //for (var i = 0; i < data.length; i++) {
                //	PaymentReimbursementList.push(data[i])
            	//}
                var PaymentReimbursementData = {
                    PaymentReimbursementList: GridData
                }
                data = $.extend(data, PaymentReimbursementData);
            }

            if (PaymentReimbursementGridEdit == "false" && $("#CurrentStep").val() == Reimbursement_ACAP) {
                Needpdf = true;
				
			var PaymentReimbursement_PostDate=$("#PaymentReimbursement_PostDate").val()
				if(PaymentReimbursement_PostDate==""){
				
					swal({ title: "Please select PostDate " })
                            return false;
				}
                var GridData = $("#PaymentReimbursementGrid").data('kendoGrid').dataSource._data;
                var jvVector = JSON.stringify(GridData)
                var date = new Date();
                var year = date.getFullYear(); //获取当前年份   
                var mon = date.getMonth() + 1; //获取当前月份   
                var da = date.getDate(); //获取当前日  
                var _date = year + "/" + mon + "/" + da;
                var SAPDOC = "";
                var Message = "";
                //console.log(jvVector)
                //mApp.unblockPage()
                mApp.blockPage();

                $.ajax({
                    type: "Post", //Post传参
                    //url: 'http://localhost:8088/sapcall.asmx/postJV',  //服务地址
                    url: WS_PostJV_URL,  //服务地址
                    data: { formID: $("#FormNo").val(), docDate: _date, postDate: _date, tskID: $("#ID").val(), jvVector: jvVector },
                    async: false,
                    success: function (r) {
                        var result = $.parseJSON(r)

                        if (result.SAPDOC == "" || result.SAPDOC == null) {
                            Message = result.Message;
                            swal({ title: Message })
                            return false;
                        } else {
                            SAPDOC = result.SAPDOC;
                            Message = result.Message;
                            var PaymentReimbursementData = {
                                PaymentReimbursement: {
                                    SAPRemark: Message,
                                    SAPDocNo: SAPDOC,
                                	PostDate:$("#PaymentReimbursement_PostDate").val()
                                }

                            }

                            data = $.extend(data, PaymentReimbursementData);

                        }

                    },
                    error: function (e) {
                         swal({ title: "Call SAP Failed" })

                        mApp.unblockPage();
                        console.log(e.statusText);
                        ret = false;

                    }
                })


            }
            if (ret) {
            	var Title = "";
            	if ($("#CurrentStep").val() == "PreApproval_GM" || $("#CurrentStep").val() == Reimbursement_GM ) {
            		$.ajax({
            			type: "Post",
            			url: "GetBudget",
            			data: {
            				formID: $("#ID").val(),
            				BudgetType: $("#BudgetType").val()
            			},
            			async: false,
            			success: function (r) {
            				if (r.Success == false) {
            					if (r.Title == "") {
            						Title = "Over annual budget"
            					} else {
            						Title = r.Title;
            					}
            				}
            				
            			},
            			error: function (e) {
            				swal({ title: "error" })

            				mApp.unblockPage();
            				return false;

            			}
            		})
            	}

            	WorkflowAction(Title,  getConfirmTitle(event), PostURL, RedirectURL, data);
				
            }
        }

        return false;

    });
    function GetPDFString() {
        var responseHistory_printStream__business = "";
        var RE_printStream = "";

        var scaledown = 0.6;
        if (window.innerWidth <= 800) {
            scaledown = 1
        }
        kendo.drawing
          .drawDOM($("#printform"), {
              paperSize: "A4",
              margin: { top: "1cm", bottom: "1cm" },
              scale: 0.6,
              height: 500,
              multiPage: true
          })

          .then(function (root) {
              //kendo.drawing.pdf.saveAs(root, "exportFile.pdf");
              return kendo.drawing.exportPDF(root);
          }).done(function (data) {

              $('#Form').removeClass('PrintPDF').trigger('change');
              $('.k-grid').removeClass('PrintPDFgrid').trigger('change');
              var postdata = {};
              //var FileGUID = $("#AppDocUploadedFilesGrid .flaticon-download").attr("onclick");
              //FileGUID = FileGUID.split('"');
              //FileGUID = FileGUID[1];
              formid = $('#FormNo').val();

              postdata.responseHistory_printStream__business = data;
              // postdata.formid = formid;

              _pdfstring = data;
              //var json = JSON.stringify(postdata);
              //var req = new XMLHttpRequest();

              //req.open("POST", "PrintForm", true);
              //req.setRequestHeader("Content-type", 'application/json; charset=utf-8');
              //req.responseType = "blob";

              //req.onload = function (event) {
              //    if (this.status === 404 || this.status === 500) {


              //        return;
              //    }



              //}


              //req.send(json);
          });
    }
    $("#btnCancel").click(function () {

        RemoveWindowOnBeforeUnloadListener();

        DialogMessage.ConfirmMessage(ConfirmLeavePageTitle, '', function () {

            if (history.length == 1) {
                window.close();
            }
            else {
                window.history.back();
            }

            //window.close();
        });

        return false;
    });

    $("#btnCancelWithoutConfirm").click(function () {

        //window.history.back();
        RemoveWindowOnBeforeUnloadListener();

        if (history.length == 1) {
            window.close();
        }
        else {
            window.history.back();
        }

        return false;
    });

    $("#btnRelease").click(function () {

        //mApp.blockPage();

        var data = {
            SN: $("#K2SN").val(),
        }

        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: 'Release',
            data: JSON.stringify(data),
            success: function (data) {
                if (data.Success) {

                    RemoveWindowOnBeforeUnloadListener();

                    location.href = 'MyTask';

                    //mApp.unblockPage();
                }
                else {
                    location.href = 'MyTask';
                    //DialogMessage.AlertMessage(data.Title,
                    //data.Text);

                    //mApp.unblockPage();
                }
            },
            failure: function (response) {
                location.href = 'MyTask';
                //mApp.unblockPage();
            }
        });

    });

    $("#btnSave").click(function () {

        if (IsMaxUploadedSizeValid()) {
            DialogMessage.ConfirmMessage(getConfirmTitle(event), '', function () {

                mApp.blockPage();

                var data = getFormData();

                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: 'POST',
                    url: 'Save',
                    data: JSON.stringify(data),
                    success: function (data) {

                        if (data.Success) {

                            $("#ID").val(data.ID);

                            SubmitChanges(data.ID, $("#GID").val(), $("#CurrentUserID").val(), ProcessCode);

                            DialogMessage.AlertMessage(data.Title,
                                data.Text);
                            mApp.unblockPage();

                            $("#UploadedFilesGrid").data('kendoGrid').dataSource.read();
                            $("#UploadedFilesGrid").data('kendoGrid').refresh();
                        }
                        else {
                            DialogMessage.AlertMessage(data.Title,
                                data.Text);
                            mApp.unblockPage();
                        }
                    },
                    failure: function (response) {
                        //swal('Draft Failed!');

                        mApp.unblockPage();
                    }
                });
            });
        }

        return false;
    });

    $("#btnDraft_PreApproval").click(function () {


        DialogMessage.ConfirmMessage(getConfirmTitle(event), '', function () {

            mApp.blockPage();

            var data = getFormData();

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: 'Draft',
                data: JSON.stringify(data),
                success: function (data) {

                    if (data.Success) {

                        $("#ID").val(data.ID);

                        DialogMessage.AlertMessage(data.Title,
                            data.Text);
                        mApp.unblockPage();
                    }
                    else {
                        DialogMessage.AlertMessage(data.Title,
                            data.Text);
                        mApp.unblockPage();
                    }
                },
                failure: function (response) {
                    mApp.unblockPage();
                }
            });
        });


        return false;
    });
    $("#btnDraft_ReimbursementSubmit").click(function () {
        var WFAction = $(this).data('wfaction');
        var FormAction = $(this).data('formaction');
        var FormType = '';
        var RoleName = '';
        var Step = '';
        var RedirectURL = '../../MEHK.dashboard/Home/MyTask';

        var PostURL = 'WorkflowAction';
        var headers = { __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() };
        var data = {
            K2SN: $("#K2SN").val(),
            ID: $("#ID").val(),
            Comment: $("#Comment").val(),
            FormType: FormType,
            RoleName: RoleName,
            WFAction: WFAction,
            FormAction: FormAction,
            Division: $("#Division").val(),
            Department: $("#Department").val(),
            Payee: $("#Payee").val(),
            StaffID: $("#StaffID").val(),
            StaffName: $("#StaffName").val(),

        }

        var AmountHKEntertain = 0;
        var AmountHKGift = 0;
        var ReimbursementData = {};

        if ($("#BudgetType").val() == "Entertainment") {
            AmountHKEntertain = $("#ResultAmount").val()
        } else {
            AmountHKGift = $("#ResultAmount").val()
        }

        var CostCenter = $("#IsNoDuplicationOfBudget").val().join(',');
        ReimbursementData = {
            PaymentReimbursement: {
                PostingInstruction: $("#PaymentReimbursement_PostingInstruction").val(),
                Purpose: $("#PaymentReimbursement_Purpose").val(),
                CostCenter: CostCenter,
                EventDateOn: $("#PaymentReimbursement_EventDateOn").data("kendoDatePicker").value(),
                EventDateTo: $("#PaymentReimbursement_EventDateTo").data("kendoDatePicker").value(),
                LateApplicationReason: $("#PreApproval_LateApplicationReason").val(),
                NatureOfGift: $("#PaymentReimbursement_NatureOfGift").val(),
                Guest: $("#PaymentReimbursement_Guest").val(),
                GuestTotal: $("#PaymentReimbursement_GuestTotal").val(),
                Company: $("#PaymentReimbursement_Company").val(),
                Attendant: $("#PaymentReimbursement_Attendant").val(),
                AttendantTotal: $("#PaymentReimbursement_AttendantTotal").val(),

                Currency: $("#PaymentReimbursement_Currency").val(),
                Amount: $("#PaymentReimbursement_Amount").val(),
                ExchangeRate: $("#PaymentReimbursement_ExchangeRate").val(),
                AmountHKEntertain: AmountHKEntertain,
                AmountHKGift: AmountHKGift,
                AmountExceedReason: $("#PaymentReimbursement_AmountExceedReason").val(),
                PaymentMethod: $("input:radio[name=PaymentMethod]:checked").val(),
                PayTo: $("#PaymentReimbursement_PayTo").val(),
                NoOfDocumentOrReceiptAttached: $("#PaymentReimbursement_NoOfDocumentOrReceiptAttached").val()

            }
        }
        data = $.extend(data, ReimbursementData);

        DialogMessage.ConfirmMessage(getConfirmTitle(event), '', function () {

            mApp.blockPage();



            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: 'Draft',
                data: JSON.stringify(data),
                success: function (data) {

                    if (data.Success) {

                        $("#ID").val(data.ID);
                        SubmitChanges($("#ID").val(), $("#GID").val(), $("#CurrentUserID").val(), $("#EntertainmentGiftCode").val());
                        DialogMessage.AlertMessage(data.Title,
                            data.Text);
                        mApp.unblockPage();
                    }
                    else {
                        DialogMessage.AlertMessage(data.Title,
                            data.Text);
                        mApp.unblockPage();
                    }
                },
                failure: function (response) {
                    mApp.unblockPage();
                }
            });
        });


        return false;
    });
    if ($("#ApprovalMatrixGridShow").val() == "true") {
    	SetApprovalMartixOptionCode();
    }
});
//$("#btnTest").click(function ()
//    {
//    var GridData = $("#PaymentReimbursementGrid").data('kendoGrid').dataSource._data;
//    var jvVector = JSON.stringify(GridData)
//    var date = new Date();
//    var year = date.getFullYear(); //获取当前年份   
//    var mon = date.getMonth() + 1; //获取当前月份   
//    var da = date.getDate(); //获取当前日  
//    var _date = year + "/" + mon + "/" + da;
//    var SAPDOC = "";
//    var Message = "";
//    //console.log(jvVector)
//    $.ajax({
//        type: "Post", //Post传参
//        url: 'http://localhost:8088/sapcall.asmx/postJV',  //服务地址
//        data: { formID: $("#FormNo").val(), docDate: _date, postDate: _date, tskID: $("#ID").val(), jvVector: jvVector },
//        success: function (result) {
//            // 调用成功后
//            //  var d = $.parseJSON(result)
//            console.log(result);
//            if (result.SAPDOC == "" || result.SAPDOC == null) {
//                return false
//            } else {
//                SAPDOC = result.SAPDOC;
//                Message = result.Message
//            }

//        },
//        error: function (e) {

//            console.log(e.status);
//            //  p.innerHTML += "参数:" + status:"+ e.status ;
//        }
//    })
//    });

function Submit(ConfirmTitle) {
    var data = getFormData();
    var validator = $("#Form").kendoValidator().data("kendoValidator");

    if (FormValidation('PreApproval')) {

        if (validator.validate()) {

            DialogMessage.ConfirmMessage(ConfirmTitle, '', function () {

                mApp.blockPage();


                var headers = { __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() };

                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    headers: headers,
                    type: 'POST',
                    url: 'Submit',

                    //data: JSON.stringify(data),
                    data: JSON.stringify(data),
                    success: function (data) {

                        if (data.Success) {

                            //SubmitChanges(data.ID, $("#GID").val(), $("#CurrentUserID").val(), ProcessCode);

                            RemoveWindowOnBeforeUnloadListener();

                            DialogMessage.AlertMessageAndRedirect(data.Title,
                                data.Text,
                                '../../MEHK.dashboard/Home/MyTask');

                            mApp.unblockPage();
                        }
                        else {
                            DialogMessage.AlertMessage(data.Title,
                                data.Text);

                            mApp.unblockPage();
                        }
                    },
                    failure: function (response) {

                        DialogMessage.AlertMessage('Submitted Failed!');

                        mApp.unblockPage();
                    }
                });
            });
        }
        else {
            mApp.scrollTo("#Form");

            swal({
                "title": "",
                "text": "There are some errors in your submission. Please correct them.",
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
        }
    }
}

function Resubmit(ConfirmTitle) {

    DialogMessage.ConfirmMessage(ConfirmTitle, '', function () {

        mApp.blockPage();

        var data = getFormData();

        data.SN = $("#K2SN").val();

        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: 'Resubmit',
            data: JSON.stringify(data),
            success: function (data) {

                if (data.Success) {

                    SubmitChanges(data.ID, $("#GID").val(), $("#CurrentUserID").val(), ProcessCode);

                    RemoveWindowOnBeforeUnloadListener();

                    DialogMessage.AlertMessageAndRedirect(data.Title,
                        data.Text,
                        'MyTask');

                    mApp.unblockPage();
                }
                else {
                    DialogMessage.AlertMessage(data.Title,
                        data.Text);

                    mApp.unblockPage();
                }
            },
            failure: function (response) {

                //swal('Submitted Failed!');
                DialogMessage.AlertMessage('Submitted Failed!');

                mApp.unblockPage();
            }
        });
    });

}

function getFormData() {

    //var GID = $("#GID").val();

    var PreApproval = {
        Purpose: $("#PreApproval_Purpose").val(),
        EventDateOn: $("#PreApproval_EventDateOn").data("kendoDatePicker").value(),
        EventDateTo: $("#PreApproval_EventDateTo").data("kendoDatePicker").value(),
        LateApplicationReason: $("#PreApproval_LateApplicationReason").val(),
        NatureOfGift: $("#PreApproval_NatureOfGift").val(),
        Guest: $("#PreApproval_Guest").val(),
        GuestTotal: $("#PreApproval_GuestTotal").data("kendoNumericTextBox").value(),
        Company: $("#PreApproval_Company").val(),
        Attendant: $("#PreApproval_Attendant").val(),
        AttendantTotal: $("#PreApproval_AttendantTotal").data("kendoNumericTextBox").value(),

    }

    var data = {
        ID: $("#ID").val(),
        __RequestVerificationToken: $("[name='__RequestVerificationToken']").val(),
        PreApproval: PreApproval,
        BudgetType: $("input:radio[name=BudgetType]:checked").val(),
        BudgetAmount: $("#BudgetAmount").data("kendoNumericTextBox").value(),
        Payee: $("#Payee").val(),

        //StaffID: $("#StaffID").val(),
        //StaffName: $("#StaffName").val(),
        //Division: $("#Division").val(),
        //Department: $("#Department").val(),
        UserInfo: {
            Division: $("#Division").val(),
            StaffID: $("#StaffID").val(),
            Department: $("#Department").val(),
            StaffName: $("#StaffName").val(),
        }
        //EndorsedBy: $("#EndorsedBy").data("kendoDropDownList").value(),
        //RequestedBy: $("#RequestedBy").data("kendoDropDownList").value(),
        //RequestedParty: $("#RequestedParty").data("kendoDropDownList").value(),
        ////CostCentre: $("#CostCentre").data("kendoMultiSelect").value(),
        //TargetCompletionOfTheProposedWorks: $("#TargetCompletionOfTheProposedWorks").data("kendoDatePicker").value(),
        //InfluenceOfProjectCompletionByProposedWork: $("#InfluenceOfProjectCompletionByProposedWork").data("kendoDropDownList").value(),
        //InfluenceOfProjectCompletionByProposedWorkMonth: $("#InfluenceOfProjectCompletionByProposedWorkMonth").data("kendoNumericTextBox").value(),
        //ProcurementStatus: $("#ProcurementStatus").data("kendoDropDownList").value(),
        //OriginalContractContingency: $("#OriginalContractContingency").data("kendoNumericTextBox").value(),
        //ContingencyAlreadySpentWithAI: $("#ContingencyAlreadySpentWithAI").data("kendoNumericTextBox").value(),
        //RequestForUsingRemainingContractContingencyItem: $("#RequestForUsingRemainingContractContingencyItemGrid").data("kendoGrid").dataSource.data(),

        //TargetCompletionDateEarlierThanSubmitReason: $("#TargetCompletionDateEarlierThanSubmitReason").data("kendoDropDownList").value(),
        //TargetCompletionDateEarlierThanSubmitReasonOther: $("#TargetCompletionDateEarlierThanSubmitReasonOther").val(),

    }

    return data;
}




function validateComment() {
    var valid = false;

    if ($("#Comment").val()) {
        valid = true;
    }
    else {
        DialogMessage.AlertMessage('', CommentRequired);
    }

    return valid;
}

function ApprovalHistoryDataBound(e) {
    if (this.dataSource.total() === 0) {
        $("#ApprovalHistory").hide();
    }
    else {
        $("#ApprovalHistory").show();
    }
}

function ApprovalCommentDataBound(e) {
    if (this.dataSource.total() === 0) {
        $("#ApprovalComment").hide();
    }
    else {
        $("#ApprovalComment").show();
    }
}

function IsUploadedFiles() {

    var Valid = $("#UploadedFilesGrid").data("kendoGrid").dataSource.total() > 0;

    if (!Valid) {
        DialogMessage.AlertMessage('', UploadedFilesRequired);
    }

    return Valid;
}

function IsMaxUploadedSizeValid() {

    var Valid = true;
    var data = $("#UploadedFilesGrid").data("kendoGrid").dataSource.data();
    var TotalFileSize = 0;

    for (i = 0; i < data.length; i++) {
        TotalFileSize += data[i].FileSize;
    }

    if (TotalFileSize > (TotalMaxFileSize * 1048576)) {
        Valid = false;
    }

    if (!Valid) {
        DialogMessage.AlertMessage('', ExceedMaxTotalFileSize);
    }

    return Valid;
}

function FormValidation(type) {
    //return true;
    var Valid = true;
    var data;

    data = getFormData();

    //if (type == 'Project') {
    //    data = getFormData();
    //}

    var Message = '';
    //var ValidationMessage_LateApplicationReasonRequired = '@Resources.Resource_EntertainmentGiftValidation.LateApplicationReasonRequired';
    //var ValidationMessage_EventDateToCanNotLaterThanEventDateOn = '@Resources.Resource_EntertainmentGiftValidation.EventDateToCanNotLaterThanEventDateOn';

    if (data.PreApproval.EventDateOn && CheckApplicationDateLaterThanEventDate() && !data.PreApproval.LateApplicationReason) {
        Message += ValidationMessage.LateApplicationReasonRequired + '<br>';
    }

    if (data.PreApproval.EventDateOn && data.PreApproval.EventDateTo &&
        (
            data.PreApproval.EventDateOn > data.PreApproval.EventDateTo
        )) {
        Message += ValidationMessage.EventDateToCanNotLaterThanEventDateOn + '<br>';
    }

    if (!data.BudgetType) {
        Message += ValidationMessage.BudgetTypeRequired + '<br>';
    }

    if (data.BudgetType == 'Entertainment' && !data.PreApproval.Attendant) {
        Message += ValidationMessage.AttendantRequired + '<br>';
    }

    //if (type == 'Project') {

    //    if (!CanNotHaveNegativeAmountIfOnlyApplySOWValid) {
    //        DialogMessage.AlertMessage('', "Can not have negative amount if only apply SOW");
    //        return false;
    //    }
    //}

    if (Message) {
        DialogMessage.AlertMessageUsingHTML('', Message);

        Valid = false;


    }



    //var Valid = true;

    //if (!Valid) {
    //    DialogMessage.AlertFormErrorMessage();
    //}

    return Valid;
}

function WorkflowAction(Title, Text, PostURL, Redirect, data) {
    DialogMessage.ConfirmMessage(Title, Text, function () {

        mApp.blockPage();

        //var data = {
        //    SN: $("#K2SN").val(),
        //    ID: $("#ID").val(),
        //    Comment: $("#Comment").val(),
        //}
        var headers = { __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() };
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            headers: headers,
            dataType: 'json',
            type: 'POST',
            url: PostURL,
            data: JSON.stringify(data),
            success: function (data) {
                if (data.Success) {
                    SubmitChanges($("#ID").val(), $("#GID").val(), $("#CurrentUserID").val(), $("#EntertainmentGiftCode").val());
                    RemoveWindowOnBeforeUnloadListener();

                    DialogMessage.AlertMessageAndRedirect(data.Title,
                        data.Text,
                        Redirect);

                    mApp.unblockPage();
                }
                else {
                    DialogMessage.AlertMessage(data.Title,
                        data.Text);

                    mApp.unblockPage();
                }
            },
            failure: function (response) {
                mApp.unblockPage();
            }
        });
    });
}

function formatCurrency(Amount) {
    //return '$' + Amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return '' + Amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

//ConfirmTitle = ConfirmTitle.replace('{0}', $(event.target).text().toLowerCase());

function getConfirmTitle(event) {
    return ConfirmTitle.replace('{0}', $(event.target).text().toLowerCase());
}

function GetFileTypeIsValid(CatName) {
    var data = $("#UploadedFilesGrid").data("kendoGrid").dataSource.data();
    var isValid = false;
    for (i = 0; i < data.length; i++) {
        if (data[i].CatName == CatName) {
            isValid = true;
            break;
        }
    }

    return isValid;
}

function changeNewLine(text) {
    var regexp = new RegExp('\n', 'g');
    return text.replace(regexp, '<br>');
}


function CheckApplicationDateLaterThanEventDate(Temp) {
	//var Temp = $("#PreApproval_EventDateOn").data("kendoDatePicker").value();
	//var da = Temp.getFullYear() + "/" + Temp.getMonth() + "/" + Temp.getDay();
	//var EventDate = new Date(Temp.getFullYear(), Temp.getMonth(), Temp.getDay());


	//var bool = ApplicationDate > EventDate
	//return bool;
	////
	//var Temp = $("#PreApproval_EventDateOn").data("kendoDatePicker").value();
	var ApplicationDate = new Date();
	var SubmissionDatehtml = $("#SubmissionDate").html();
	if (SubmissionDatehtml != "") {
		ApplicationDate = new Date(SubmissionDatehtml)
	}
	var bool = ApplicationDate > Temp
	return bool;
}

function PreApprovalEventDateOnChange() {

	var Temp = $("#PreApproval_EventDateOn").data("kendoDatePicker").value();
	if (CheckApplicationDateLaterThanEventDate(Temp)) {
		$("#divLateApplicationReason").show();
	}
	else {
		$("#divLateApplicationReason").hide();

		$("#PreApproval_LateApplicationReason").val("");
	}


}
function PaymentPreApprovalEventDateOnChange() {

	var Temp = $("#PaymentReimbursement_EventDateOn").data("kendoDatePicker").value();
	if (CheckApplicationDateLaterThanEventDate(Temp)) {
		$("#divPaymentLateApplicationReason").show();
	}
	else {
		$("#divPaymentLateApplicationReason").hide();

		$("#PreApproval_LateApplicationReason").val("");
	}


}
function setResultAmount() {
    var Amount = $("#PaymentReimbursement_Amount").val();
    var ExchangeRate = $("#PaymentReimbursement_ExchangeRate").val();
    var ResultAmount = Amount * ExchangeRate;
    //$("#ResultAmount").val(ResultAmount);
    $("#ResultAmount").data("kendoNumericTextBox").value(ResultAmount);
}
$("#UploadedFilesbut").click(function () {
    SubmitChanges($("#ID").val(), $("#GID").val(), $("#CurrentUserID").val(), $("#EntertainmentGiftCode").val());
})
function PaymentReimbursementGridData() {
    return {
    	FormID: $("#ID").val(),
        //CurrentStep: $("#CurrentStep").val()
    };
}
$("[name=BudgetType]").click(function () {
    SetApprovalMartixOptionCode()
    //alert($("input:radio[name=BudgetType]:checked").val())
})


$("#UseSystemRate").click(function () {
	
    GetExchangeRate();
    //alert(isClick);
})
function GetExchangeRate() {
	//alert($("#PaymentReimbursement_Currency").val())
    var isClick = $("#UseSystemRate").prop('checked');
    if (isClick) {
        RequestWebService();
        //2019/8/26
        //	var CurrencyName = $("#PaymentReimbursement_Currency").data("kendoDropDownList").text()
        //$.ajax({
        //  	type: 'POST',
        //  	url: "GetExchangeRate",
        //  	cache: false,
        //  	//async: false,
        //  	data: {CurrencyName: CurrencyName},
        //  	success: function (data) {
        //  		if (data != null) {
        //  			$("#PaymentReimbursement_ExchangeRate").data("kendoNumericTextBox").value(data);
        //  			setResultAmount();
        //  		}
        //  		else {

        //  		}
        //  	},
        //  	failure: function (response) {

        //  	}
        //  });
    }
}

function RequestWebService() {
    //    debugger;
	var cur = $("#PaymentReimbursement_Currency").data("kendoComboBox").text();
    var date = new Date();
    var year = date.getFullYear(); //获取当前年份   
    var mon = date.getMonth() + 1; //获取当前月份   
    var da = date.getDate(); //获取当前日  
    var _dateStr = year + "/" + mon + "/" + da;
    
    $.ajax({
        type: "Post", //Post传参
        //url: 'http://localhost:8088/sapcall.asmx/getExchangeRate',  //服务地址
        url: WS_Currency_URL,  //服务地址
        data: { Currency: cur, TranslationDate: _dateStr },

        success: function (result) {
            // 调用成功后
            var d = $.parseJSON(result)
            console.log(d);
            //p.innerHTML += d.Rate;
            $("#PaymentReimbursement_ExchangeRate").data("kendoNumericTextBox").value(d.Rate);
            setResultAmount();
        },
        error: function (e) {

        }
    })


};

$("#btnPrint").click(function () {
    var responseHistory_printStream__business = "";
    var RE_printStream = "";

    var scaledown = 0.6;
    if (window.innerWidth <= 800) {
        scaledown = 1
    }
    kendo.drawing
      .drawDOM($("#printform"), {
          paperSize: "A4",
          margin: { top: "1cm", bottom: "1cm" },
          scale: 0.6,
          height: 500,
          multiPage: true
      })

      .then(function (root) {
          //kendo.drawing.pdf.saveAs(root, "exportFile.pdf");
          return kendo.drawing.exportPDF(root);
      }).done(function (data) {

          $('#Form').removeClass('PrintPDF').trigger('change');
          $('.k-grid').removeClass('PrintPDFgrid').trigger('change');
          var postdata = {};
          //var FileGUID = $("#AppDocUploadedFilesGrid .flaticon-download").attr("onclick");
          //FileGUID = FileGUID.split('"');
          //FileGUID = FileGUID[1];
          formid = $('#FormNo').val();

          postdata.responseHistory_printStream__business = data;
          postdata.formid = formid;

          var json = JSON.stringify(postdata);
          var req = new XMLHttpRequest();

          req.open("POST", "PrintForm", true);
          req.setRequestHeader("Content-type", 'application/json; charset=utf-8');
          req.responseType = "blob";

          req.onload = function (event) {
              if (this.status === 404 || this.status === 500) {


                  return;
              }



          }


          req.send(json);
      });

    return false;
});
function getCentreFilterData() {
	var ddl = $("#Centre").data("kendoDropDownList");
	var ID = $("#ID").val();
	return {
		text: ddl.filterInput.val(),
		ID: ID
	}
}
function getCentreFilterMultiSelectData() {
	var ID = $("#ID").val();
	return {
		ID: ID
	}
}
function DrCrChange(e) {
var Text= $("#Dr_Cr").val();
	if (Text == "Dr") {
				$("#PostKey").val("40")
			} else {
				$("#PostKey").val("31")
			}
}

function PaymentReimbursementGridSave() {
	var Number=$("#Number").val();
	
	var GridData = $("#PaymentReimbursementGrid").data('kendoGrid').dataSource._data;
	if (Number == 0) {
		Number = GridData.length;
		$("#Number").val(Number);
	}
	console.log(GridData.length);
	

}

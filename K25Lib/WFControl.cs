﻿using System;
using System.Collections.Generic;
using System.Linq;
using SourceCode.Workflow.Client;
using System.Configuration;
using CommonHelper;

namespace K25Lib
{
    public class WFControl
    {
        private static string K2DomainName;
        private static string K2WorkflowInstance;
        private static string K2Server;
        private static string k2UserID;
        private static string k2Password;
        private static string loginDomain;
        private static string k2WindowsDomain;
        public WFControl(string _K2DomainName, string _K2Server, string _k2UserID, string _k2Password, string _loginDomain, string _k2WindowsDomain)
        {

            K2DomainName = _K2DomainName;

            K2Server = _K2Server;
            k2UserID = _k2UserID;

            k2Password = _k2Password;
            loginDomain = _loginDomain;
            k2WindowsDomain = _k2WindowsDomain;
        }

        private bool isDev
        {
            get
            {
                return ConfigurationManager.AppSettings["isDEV"] == "T" ? true : false;
            }
        }


        private bool isTestEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["isTestEmail"] == "T" ? true : false;
            }
        }

        private string GetK2ConnectionString
        {
            get
            {


                /*
                SCConnectionStringBuilder K2ConnStringBuilder = new SCConnectionStringBuilder();
                K2ConnStringBuilder.Host = K2Server; //K2 server name or the name of the DNS entry poiting to the K2 Farm
                K2ConnStringBuilder.Authenticate = true; //specify whether to authenticate the user's credentials against the security provider. This is usually set to true
                K2ConnStringBuilder.UserID = ConfigurationManager.AppSettings["k2UserID"]; //a specific username
                K2ConnStringBuilder.Password = ConfigurationManager.AppSettings["k2Password"];   //the user's password, unencrypted
                K2ConnStringBuilder.Port = 5252;  //if K2 was configured to run on a non-standard port, you can specify the port number here
                K2ConnStringBuilder.IsPrimaryLogin = true; //this is normally set to true, unless you are using cached security credentials
                K2ConnStringBuilder.SecurityLabelName = "K2"; //if using a different security provider, specify the label of the security provider here
                K2ConnStringBuilder.WindowsDomain = ConfigurationManager.AppSettings["k2WindowsDomain"];
                return K2ConnStringBuilder.ToString();
                */

                var k2Connent = "Integrated=False;IsPrimaryLogin=True;Authenticate=True;EncryptedPassword=False;Host=#k2Host#;UserID=#k2UserId#;Password=#k2Password#;Port=5252;SecurityLabelName=K2;WindowsDomain=#k2WindowsDomain#";
                k2Connent = k2Connent.Replace("#k2Host#", K2Server).Replace("#k2UserId#", k2UserID).Replace("#k2Password#", k2Password).Replace("#k2WindowsDomain#", k2WindowsDomain);
                return k2Connent;

            }

        }


        //public int StartWorkflow(WorkflowData _data, List<Dictionary<string,object>> _Datafields)

        public int StartWorkflow(WorkflowData _data, Dictionary<string, object> _Datafields, string StartProcessName)
        {
            int nInstanceID = 0;
            try
            {
                using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
                {
                    if (isDev)
                    {
                        k2Conn.Open(K2Server);
                    }
                    else
                    {
                        k2Conn.Open(K2Server, GetK2ConnectionString);
                        _data.K2WorkflowInstanceUser = ToK2Value(_data.K2WorkflowInstanceUser);
                    }


                    k2Conn.ImpersonateUser(ToK2Value(_data.K2WorkflowInstanceUser));
                    //instantiate the workflow
                    ProcessInstance k2Proc = k2Conn.CreateProcessInstance(StartProcessName);
                    //start the workflow
                    k2Proc.Folio = _data.CentralRecordNum;



                    foreach (var v in _Datafields)
                    {
                        k2Proc.DataFields[v.Key].Value = v.Value;
                    }


                    //foreach (var v in _Datafields)
                    //{
                    //    k2Proc.DataFields[v.Keys.ToString()].Value = v.Values;
                    //}


                    k2Conn.StartProcessInstance(k2Proc);
                    nInstanceID = k2Proc.ID;

                    //close the connection
                    k2Conn.Close();
                    return nInstanceID;

                }
            }
            catch (Exception ex)
            {
                throw ex;
                return nInstanceID;
            }
        }


        public void ExecuteWorkflowAction(string UserIdentityName, string SN, string Action, Dictionary<string, object> _Datafields)
        {
            try
            {
                using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
                {

                    if (isDev)
                    {
                        k2Conn.Open(K2Server);
                    }
                    else
                    {
                        k2Conn.Open(K2Server, GetK2ConnectionString);
                    }
                    k2Conn.ImpersonateUser(ToK2Value(UserIdentityName));
                    //Worklist wl = k2Conn.OpenWorklist();

                    WorklistItem wli = k2Conn.OpenWorklistItem(SN);
                    ProcessInstance pi = wli.ProcessInstance;
                    foreach (var v in _Datafields)
                    {
                        pi.DataFields[v.Key].Value = v.Value;
                    }

                    pi.Update();
                    wli.Actions[Action].Execute();
                    k2Conn.Close();
                }

            }
            catch (Exception e)
            {
                throw;
            }
        }




        //public bool Approve(WorkflowData _data)
        //{
        //    bool _bResult = true;

        //    try
        //    {
        //        using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
        //        {
        //            if (isDev)
        //            {
        //                k2Conn.Open(K2Server);
        //            }
        //            else
        //            {
        //                k2Conn.Open(K2Server, GetK2ConnectionString);
        //            }
        //            k2Conn.ImpersonateUser(ToK2Value(_data.LoginName));

        //            WorklistItem wi = k2Conn.OpenWorklistItem(_data.SN);
        //            ProcessInstance pi = wi.ProcessInstance;


        //            wi.Actions[_data.Action].Execute();
        //            k2Conn.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        return false;
        //    }

        //    return _bResult;
        //}
        public void ReturnToCreatorByK2Admin(string[] ProcessName, int ProcInstID)
        {
            var list = GetWorkList(k2UserID, ProcessName);

            var SN = "";

            foreach (var item in list)
            {
                if (item.ProcInstID == ProcInstID)
                {
                    //K2SN = item.
                    SN = item.SN;

                    break;
                }
            }

            //ExecuteWorkflowAction(k2UserID, SN, )


            //LinkCommonBLL.DeleteWFActivity(record.SerialNo);
        }

        public string GetSerialNo(string[] ProcessName, int ProcInstID, string UserIdentityName)
        {
            var SN = "";

            var list = GetWorkList(UserIdentityName, ProcessName);

            foreach (var item in list)
            {
                if (item.ProcInstID == ProcInstID)
                {
                    SN = item.SN;

                    break;
                }
            }

            return SN;

        }

        public List<WorkflowData> GetWorkList(string UserIdentityName, string[] ProcessName)
        {
            using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
            {

                if (isDev)
                {
                    k2Conn.Open(K2Server);
                }
                else
                {
                    k2Conn.Open(K2Server, GetK2ConnectionString);
                }
                k2Conn.ImpersonateUser(ToK2Value(UserIdentityName));


                WorklistCriteria wc = new WorklistCriteria();

                for (int i = 0; i < ProcessName.Length; i++)
                {
                    if (i == 0)
                    {
                        wc.AddFilterField(WCField.ProcessFullName, WCCompare.Equal, ProcessName[i]);
                    }
                    else
                    {
                        wc.AddFilterField(WCLogical.Or, WCField.ProcessFullName, WCCompare.Equal, ProcessName[i]);
                    }
                }

                //wc.AddFilterField(WCField.WorklistItemOwner, WCCompare.Equal, ToK2Value(UserIdentityName));

                Worklist wl = k2Conn.OpenWorklist(wc);

                List<WorkflowData> _wfds = new List<WorkflowData>();
                foreach (SourceCode.Workflow.Client.WorklistItem wli in wl)
                {

                    if (wli.AllocatedUser.ToUpper() == (ToK2Value(UserIdentityName)).ToUpper())
                    {
                        WorkflowData _wfd = new WorkflowData
                        {
                            SN = wli.SerialNumber,
                            ProcessName = wli.ProcessInstance.FullName,
                            ActivityName = wli.ActivityInstanceDestination.Name,
                            EventName = wli.EventInstance.Name,
                            StartDate = wli.EventInstance.StartDate,
                            CentralRecordNum = wli.ProcessInstance.Folio,
                            ApproveURL = wli.Data.ToString(),
                            ProcInstID = wli.ProcessInstance.ID
                        };
                        _wfds.Add(_wfd);
                    }

                }
                //string K2WorkflowPayment = ConfigurationManager.AppSettings["K2WorkflowInstance"].ToString();
                //string K2WorkflowContract = ConfigurationManager.AppSettings["K2WorkflowInstanceContract"].ToString();
                //return _wfds.Where(t => t.ProcessName == K2WorkflowPayment || t.ProcessName == K2WorkflowContract).ToList();

                //return _wfds.Where(t => ProcessName.Contains(t.ProcessName)).ToList();
                return _wfds.ToList();
            }
        }


        //public List<WorkflowData> GetWorkList(WorkflowData _data)
        //      {
        //          using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
        //          {

        //              if (isDev)
        //              {
        //                  k2Conn.Open(K2Server);
        //              }
        //              else
        //              {
        //                  k2Conn.Open(K2Server, GetK2ConnectionString);
        //              }
        //              k2Conn.ImpersonateUser(ToK2Value(_data.LoginName));
        //              Worklist wl = k2Conn.OpenWorklist();
        //              List<WorkflowData> _wfds = new List<WorkflowData>();
        //              foreach (WorklistItem wli in wl)
        //              {
        //                  if (wli.AllocatedUser.ToUpper() == (ToK2Value(_data.LoginName)).ToUpper())
        //                  {
        //                      WorkflowData _wfd = new WorkflowData
        //                      {
        //                          SN = wli.SerialNumber,
        //                          ProcessName = wli.ProcessInstance.FullName,
        //                          ActivityName = wli.ActivityInstanceDestination.Name,
        //                          EventName = wli.EventInstance.Name,
        //                          StartDate = wli.EventInstance.StartDate,
        //                          CentralRecordNum = wli.ProcessInstance.Folio,
        //                          ApproveURL = wli.Data.ToString(),

        //                      };
        //                      _wfds.Add(_wfd);
        //                  }

        //              }
        //              string K2WorkflowPayment = ConfigurationManager.AppSettings["K2WorkflowInstance"].ToString();
        //              string K2WorkflowContract = ConfigurationManager.AppSettings["K2WorkflowInstanceContract"].ToString();
        //              return _wfds.Where(t => t.ProcessName == K2WorkflowPayment || t.ProcessName == K2WorkflowContract).ToList();
        //          }
        //      }

        //public void Resubmit(WorkflowData _data, List<Dictionary<string, object>> _Datafields)
        //{
        //    try
        //    {
        //        using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
        //        {

        //            if (isDev)
        //            {
        //                k2Conn.Open(K2Server);
        //            }
        //            else
        //            {
        //                k2Conn.Open(K2Server, GetK2ConnectionString);
        //            }
        //            k2Conn.ImpersonateUser(ToK2Value(_data.LoginName));
        //            Worklist wl = k2Conn.OpenWorklist();

        //            WorklistItem wli = k2Conn.OpenWorklistItem(_data.SN);
        //            ProcessInstance pi = wli.ProcessInstance;
        //            foreach (var v in _Datafields)
        //            {
        //                pi.DataFields[v.Keys.ToString()].Value = v.Values;
        //            }


        //            pi.Update();
        //            wli.Actions[_data.Action].Execute();
        //            k2Conn.Close();
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //    }
        //}


        public string GetTaskName(string SN, string[] ProcessName, string UserIdentityName)
        {
            string TaskName = "";

            try
            {
                using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
                {

                    if (isDev)
                    {
                        k2Conn.Open(K2Server);
                    }
                    else
                    {
                        k2Conn.Open(K2Server, GetK2ConnectionString);
                    }
                    k2Conn.ImpersonateUser(ToK2Value(UserIdentityName));

                    WorklistItem wi = k2Conn.OpenWorklistItem(SN);

                    TaskName = wi.ActivityInstanceDestination.Name;
                }
            }
            catch (Exception ex)
            {
                //throw;

            }

            return TaskName;

        }

        public string GetWFDataFieldValue(string SN, string FieldName, string UserIdentityName)
        {
            string result = "";

            using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
            {

                if (isDev)
                {
                    k2Conn.Open(K2Server);
                }
                else
                {
                    k2Conn.Open(K2Server, GetK2ConnectionString);
                }
                k2Conn.ImpersonateUser(ToK2Value(UserIdentityName));

                WorklistItem wi = k2Conn.OpenWorklistItem(SN);
                result = wi.ProcessInstance.DataFields[FieldName].Value.ToString();
                k2Conn.Close();
            }

            return result;
        }

        public bool CheckWFLastStep(WorkflowData _data)
        {

            bool bcheck = false;
            try
            {
                using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
                {

                    if (isDev)
                    {
                        k2Conn.Open(K2Server);
                    }
                    else
                    {
                        k2Conn.Open(K2Server, GetK2ConnectionString);
                    }
                    k2Conn.ImpersonateUser(ToK2Value(_data.LoginName));

                    WorklistItem wi = k2Conn.OpenWorklistItem(_data.SN);

                    string ActivityName = wi.ActivityInstanceDestination.Name;
                    string lv = "";
                    lv = wi.ProcessInstance.DataFields["lv"].Value.ToString();
                    k2Conn.Close();

                    if (ActivityName != string.Empty && lv != string.Empty)
                    {
                        if (ActivityName.Contains(lv))
                        {
                            bcheck = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {


            }

            return bcheck;

        }

        public bool CheckSN(WorkflowData _data, string sFolio)
        {
            bool _bResult = false;
            try
            {
                using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
                {
                    if (isDev)
                    {
                        k2Conn.Open(K2Server);
                    }
                    else
                    {
                        k2Conn.Open(K2Server, GetK2ConnectionString);
                    }
                    k2Conn.ImpersonateUser(ToK2Value(_data.LoginName));

                    WorklistItem wi = k2Conn.OpenWorklistItem(_data.SN);
                    ProcessInstance pi = wi.ProcessInstance;
                    if (wi.Status == WorklistStatus.Available || wi.Status == WorklistStatus.Open)
                    {
                        if (pi.Folio == sFolio)
                        {
                            _bResult = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return _bResult;
        }
        public void OpenItem(string UserIdentityName, string SNNum)
        {

            try
            {
                using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
                {
                    if (isDev)
                    {
                        k2Conn.Open(K2Server);
                    }
                    else
                    {
                        k2Conn.Open(K2Server, GetK2ConnectionString);
                    }

                    k2Conn.ImpersonateUser(ToK2Value(UserIdentityName));

                    WorklistItem wi = k2Conn.OpenWorklistItem(SNNum);

                }
            }
            catch (Exception ex)
            {

            }

        }

        public string ToK2Value(string target)
        {
            return (!String.IsNullOrEmpty(target) && !target.ToLower().Contains("k2:")) ? String.Format("K2:{0}", target) : target;
        }
        //public bool CheckIsLastAction(WorkflowData _data)
        //{
        //    bool _bool = false;
        //    var v = GetActionsList(_data);
        //    foreach (var _v in v)
        //    {
        //        if (_v.ToString().ToUpper() == "COMPLETE")
        //        {
        //            _bool = true;
        //        }
        //    }
        //    return _bool;
        //}



        public bool ReleaseWorkItem(string UserIdentityName, string SN)
        {
            bool Success = false;

            try
            {
                using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
                {

                    if (isDev)
                    {
                        k2Conn.Open(K2Server);
                    }
                    else
                    {
                        k2Conn.Open(K2Server, GetK2ConnectionString);
                    }
                    k2Conn.ImpersonateUser(ToK2Value(UserIdentityName));
                    //Worklist wl = k2Conn.OpenWorklist();

                    WorklistItem wli = k2Conn.OpenWorklistItem(SN);
                    wli.Release();

                    k2Conn.Close();
                }

                Success = true;

            }
            catch (Exception e)
            {
                throw;
            }

            return Success;
        }


        public string  Delegation(string oldADUser, string newADUser, List<string> ProcessName)
        {
			int Tasklength = 0;

			using (SourceCode.Workflow.Client.Connection k2Conn = new Connection())
            {

                if (isDev)
                {
                    k2Conn.Open(K2Server);
                }
                else
                {
                    k2Conn.Open(K2Server, GetK2ConnectionString);
                }
                k2Conn.ImpersonateUser(ToK2Value(oldADUser));


                WorklistCriteria wc = new WorklistCriteria();

                for (int i = 0; i < ProcessName.Count; i++)
                {
                    if (i == 0)
                    {
                        wc.AddFilterField(WCField.ProcessFullName, WCCompare.Equal, ProcessName[i]);
                    }
                    else
                    {
                        wc.AddFilterField(WCLogical.Or, WCField.ProcessFullName, WCCompare.Equal, ProcessName[i]);

                    }
                }



                Worklist wl = k2Conn.OpenWorklist(wc);

                List<WorkflowData> _wfds = new List<WorkflowData>();
                foreach (SourceCode.Workflow.Client.WorklistItem _wli in wl)
                {

                    if (_wli.AllocatedUser.ToUpper() == (ToK2Value(oldADUser)).ToUpper())
                    {
                        try
                        {
                            var wli = k2Conn.OpenWorklistItem(_wli.SerialNumber);
                            wli.Release();
                            wli.Redirect(newADUser); //Example:- K2\\UserA
                                                     //code to delegate
                            SourceCode.Workflow.Client.Destination _Destination = new SourceCode.Workflow.Client.Destination();
                            //Allow actions to delegated user
                            foreach (SourceCode.Workflow.Client.Action _action in wli.Actions)
                            {
                                _Destination.AllowedActions.Add(_action.Name);
                            }
                            _Destination.DestinationType = SourceCode.Workflow.Client.DestinationType.User;
                            _Destination.Name = newADUser; //Example:- K2\\UserA
                            wli.Delegate(_Destination);
							Tasklength++;

						}
                        catch (Exception ex)
                        {

                        }
                        //code to redirect

                        //}

                    }
                    //string K2WorkflowPayment = ConfigurationManager.AppSettings["K2WorkflowInstance"].ToString();
                    //string K2WorkflowContract = ConfigurationManager.AppSettings["K2WorkflowInstanceContract"].ToString();
                    //return _wfds.Where(t => t.ProcessName == K2WorkflowPayment || t.ProcessName == K2WorkflowContract).ToList();

                    //return _wfds.Where(t => ProcessName.Contains(t.ProcessName)).ToList();
                   
                }
            }
            return Tasklength+"";

        }
    }
}

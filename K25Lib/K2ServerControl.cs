﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SourceCode.Workflow.Management;
using SourceCode.Workflow.Management.Criteria;

namespace K25Lib
{
    public  class K2ServerControl
    {
        //private static string K2DomainName;
        //private static string K2WorkflowInstance;
        //private static string K2Server;
        //private static string k2UserID;
        //private static string k2Password;
        //private static string loginDomain;
        //private static string k2WindowsDomain;
        //public K2ServerControl(string _K2DomainName, string _K2Server, string _k2UserID, string _k2Password, string _loginDomain, string _k2WindowsDomain)
        //{

        //    K2DomainName = _K2DomainName;

        //    K2Server = _K2Server;
        //    k2UserID = _k2UserID;

        //    k2Password = _k2Password;
        //    loginDomain = _loginDomain;
        //    k2WindowsDomain = _k2WindowsDomain;
        //}

        private static string GetK2ConnectionString
        {
            get
            {
                SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder builder = new SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder();
                builder.Authenticate = true; //whether to authenticate the user's credentials against the security provider, usually true
                builder.Host = "PTTK2FIVE"; //name of the K2 host server, or the name of the DNS entry pointing to the K2 Farm
                builder.Port = 5555; //use port 5252 for SourceCode.Workflow.Client connections, port 5555 for everything else
                builder.Integrated = true; //true = use the logged on user, false = use the specified user
                builder.IsPrimaryLogin = true; //true = re-authenticate user, false = use cached security credentials
                builder.SecurityLabelName = "K2"; //the name of the security label to use for authentication
              
                return builder.ConnectionString;
			}

        } 

        public static string  DelegationUser(string oldADUser,string newADUser,List<string> ProcessFullName)
        {
            string result = "";
            WorkflowManagementServer connectServver = new WorkflowManagementServer();
            try
            {
                connectServver.CreateConnection();
                connectServver.Connection.Open(GetK2ConnectionString);
                WorklistCriteria wc = new WorklistCriteria();
                WorklistCriteriaFilter filters = new WorklistCriteriaFilter();

                filters.AddRegularFilter(WorklistFields.Destination, Comparison.Like, "%" + oldADUser + "%");

                foreach (string str in ProcessFullName)
                {
                    filters.AddRegularFilter(WorklistFields.ProcessFullName, Comparison.Like, "%" + ProcessFullName + "%", RegularFilter.FilterCondition.OR);
                }

           

                WorklistItems items = connectServver.GetWorklistItems(filters);
				

				foreach (WorklistItem item in items)
                {
                    if (item.Status == WorklistItem.WorklistStatus.Open || item.Status == WorklistItem.WorklistStatus.Allocated || item.Status == WorklistItem.WorklistStatus.Sleep)
                    {
                        connectServver.ReleaseWorklistItem(item.ID);
                    }
                }
				int count = 0;
                foreach (WorklistItem item in items)
                {
					bool ret = connectServver.RedirectWorklistItem(oldADUser, newADUser, item.ProcInstID, item.ActInstDestID, item.ID);
                 
                    if (ret)
					{
						count++;
					}

                }
				result = count.ToString();

			}
            catch(Exception ex)
            {
                result = ex.ToString();
            }
            finally
            {
                if (connectServver.Connection != null && connectServver.Connection.IsConnected)
                {
                    connectServver.Connection.Close();
                }
            }

            return result;
        }


        public static string DelegationUserTest(string oldADUser, string newADUser, List<string> ProcessFullName)
        {
            string result = "";
            WorkflowManagementServer connectServver = new WorkflowManagementServer();
            try
            {
                connectServver.CreateConnection();
                connectServver.Connection.Open(GetK2ConnectionString);
                WorklistCriteria wc = new WorklistCriteria();
                WorklistCriteriaFilter filters = new WorklistCriteriaFilter();

                filters.AddRegularFilter(WorklistFields.Destination, Comparison.Like, "%" + oldADUser + "%");

                foreach (string str in ProcessFullName)
                {
                    filters.AddRegularFilter(WorklistFields.ProcessFullName, Comparison.Like, "%" + ProcessFullName + "%", RegularFilter.FilterCondition.OR);
                }

                WorklistItems items = connectServver.GetWorklistItems(filters);

                result = items.Count.ToString();

            }
            catch (Exception ex)
            {
                result = ex.ToString();
            }
            finally
            {
                if (connectServver.Connection != null && connectServver.Connection.IsConnected)
                {
                    connectServver.Connection.Close();
                }
            }

            return result;
        }

    }
}

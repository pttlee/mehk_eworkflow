﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using MEHK.Workflow.Data.DTO;
using MEHK.Workflow.Data.Const;
namespace MEHK.Workflow.BLL
{
    public class MEHKAuthorize: AuthorizeAttribute
    {
        public string PageRight { get; set; }
        public string[] PageRights { get; set; }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);

            //filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "General", action = "NoPermission", area = "" }));
            filterContext.Result = new RedirectResult("/MEHK.DashBoard/Error/NoPermission");
        }
        public dynamic UserRight(List<Right> userRightList)
        {
            dynamic userRight = new System.Dynamic.ExpandoObject();
       

            foreach (var right in userRightList)
            {
                AddProperty(userRight, right.RightName, true);
            }

            return userRight;
        }
        public void AddProperty(System.Dynamic.ExpandoObject expando, string propertyName, object propertyValue)
        {
            // ExpandoObject supports IDictionary so we can extend it like this
            var expandoDict = expando as IDictionary<string, object>;
            if (expandoDict.ContainsKey(propertyName))
            {
                expandoDict[propertyName] = propertyValue;
            }
            else
            {
                expandoDict.Add(propertyName, propertyValue);
            }
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
          

            if (String.IsNullOrEmpty(PageRight) && PageRights == null)
            {
                return true;
            }
            APIAccessBLL apiBLL = new APIAccessBLL();
            Dictionary<string, object> dir = new Dictionary<string, object>();
            dir.Add("SAMACCOUNT", httpContext.User.Identity.Name.Split('\\')[1]);
            dir.Add("AppID", CommonConstcs.AppID);
            var Rights = apiBLL.Fun_CommonAPICALL(@"Common/GetUserRight/", dir, new Right());
            var userRight = UserRight(Rights);
            bool result = false;

            //var userRight = UserRight(httpContext);

            var pageRightsList = PageRights != null ? PageRights.ToList() : new List<string>();
            if (!String.IsNullOrEmpty(PageRight))
            {
                pageRightsList.Add(PageRight);
            }

            try
            {
                foreach (var pageRight in pageRightsList)
                {
                    result = IsPropertyExist(userRight, pageRight);
                    if (result)
                    {
                        break;
                    }
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        public bool IsPropertyExist(dynamic settings, string name)
        {
            if (settings is System.Dynamic.ExpandoObject)
            {
                return ((IDictionary<string, object>)settings).ContainsKey(name);
            }
            return settings.GetType().GetProperty(name) != null;
        }
    }
}

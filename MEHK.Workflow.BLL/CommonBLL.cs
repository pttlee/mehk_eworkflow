﻿//using MEHK.Workflow.BLL.DTO;
using MEHK.Workflow.Data.DTO;
using MEHK.Workflow.Data.Enum;
using MEHK.Workflow.Data.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;

namespace MEHK.Workflow.BLL
{
    public class CommonBLL
    {


        public static string GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey AppKey)
        {
            //    var Value = "";

            //using (var db = new MEHKWorkflowEntities())
            //{
            //    Value = db.OrgManagement_SystemParameter.Where(n => n.AppKey == AppKey.ToString())
            //        .Select(n => n.Value)
            //        .FirstOrDefault();
            //}
            //return Value;
            try
            {
                return APIAccessBLL.GetOrgManagementSystemParameter(AppKey.ToString());
            }
            catch (Exception ex)
            {
                CreateErrorLog(ex, "GetOrgManagementSystemParameter");
                return "";
            }

        }

        public static int GetFormTypeID(FormTypes FormType)
        {
            var ID = 0;

            using (var db = new MEHKWorkflowEntities())
            {
                ID = db.FormTypes.Where(n => n.Code == FormType.ToString())
                    .Select(n => n.ID)
                    .FirstOrDefault();
            }

            return ID;

        }

        public static int GetWFStepID(string Step)
        {
            var ID = 0;

            using (var db = new MEHKWorkflowEntities())
            {
                ID = db.WFSteps.Where(n => n.Step == Step.ToString())
                    .Select(n => n.ID)
                    .FirstOrDefault();

            }

            return ID;

        }

        public static string GetSerialNumberWithoutYear(string workflowCode)
        {
            string ID = "";
            try
            {
                using (var DB = new MEHKWorkflowEntities())
                {
                    var v = DB.GetNextNumWithoutYear(workflowCode).ToList().FirstOrDefault();
                    if (v != null)
                    {
                        ID = String.Format("{0}{1}", workflowCode, v.ToString());
                    }
                }
            }
            catch (DbEntityValidationException ex)
            {

            }

            return ID;
        }

        public static List<WorkflowLogViewModel> GetApprovalHistory(int FormID, int FormTypeID)
        {
            var list = new List<WorkflowLogViewModel>();

            using (var db = new MEHKWorkflowEntities())
            {
                list = db.Database
                    .SqlQuery<WorkflowLogViewModel>("GetWorkflowHistory @FormID, @FormTypeID",
                    new SqlParameter("FormID", FormID),
                    new SqlParameter("FormTypeID", FormTypeID)).ToList();
            }

            return list;
        }

        public static int GetApprovalActionHistoryActionID(ApprovalActionHistoryActions Action)
        {
            var ID = 0;

            using (var db = new MEHKWorkflowEntities())
            {
                ID = db.ApprovalActionHistoryActions.Where(n => n.Code == Action.ToString())
                    .Select(n => n.ID)
                    .FirstOrDefault();

            }

            return ID;

        }

        public static int GetApprovalActionHistoryRoleID(ApprovalActionHistoryRoles Role)
        {
            var ID = 0;

            using (var db = new MEHKWorkflowEntities())
            {
                ID = db.ApprovalActionHistoryActions.Where(n => n.Code == Role.ToString())
                    .Select(n => n.ID)
                    .FirstOrDefault();

            }

            return ID;

        }

        public static void InsertApprovalHistroy(int FormID, FormTypes FormType, UserInfo UserInfo, ApprovalActionHistoryActions Action, ApprovalActionHistoryRoles Role, string Comment, string Section)
        {
            //OrganizationBLL OrganizationBLL = new OrganizationBLL();

            int FormTypeID = CommonBLL.GetFormTypeID(FormType);

            using (var db = new MEHKWorkflowEntities())
            {
                //var UserInfo = OrganizationBLL.GetUser(UserID);
                //var ActionID = db.NonBudgetCapexActions.Where(n => n.Code == ActionName).Select(n => n.ID).FirstOrDefault();
                var ActionID = GetApprovalActionHistoryActionID(Action);
                var ApprovalHistoryRoleID = GetApprovalActionHistoryRoleID(Role);

                var ApprovalActionHistory = new ApprovalActionHistory();
                //ApprovalActionHistory.FormType = FormType;
                ApprovalActionHistory.FormID = FormID;
                ApprovalActionHistory.FormTypeID = FormTypeID;
                ApprovalActionHistory.UserID = UserInfo.MemberId;
                ApprovalActionHistory.StaffID = UserInfo.StaffId;
                ApprovalActionHistory.UserName = UserInfo.MemberName;
                //ApprovalActionHistory.UserPosition = UserInfo.JobTitle;
                //ApprovalActionHistory.UserDepartmentName = UserInfo.Department;
                ApprovalActionHistory.ActionID = ActionID;
                ApprovalActionHistory.ApprovalHistoryRoleID = ApprovalHistoryRoleID;
                //ApprovalActionHistory.RoleName = RoleName;
                ApprovalActionHistory.Section = Section;
                ApprovalActionHistory.Comment = Comment;
                ApprovalActionHistory.IsDeleted = false;

                //ApprovalActionHistory.CreatedBy = UserInfo.UserID;
                ApprovalActionHistory.CreatedBy = UserInfo.MemberId;
                ApprovalActionHistory.CreatedDate = DateTime.Now;

                db.Entry(ApprovalActionHistory).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();

            }
        }
        public static void CreateErrorLog(Log log)
        {
            using (var DB = new MEHKWorkflowEntities())
            {
                DB.Entry(log).State = System.Data.Entity.EntityState.Added;
                DB.SaveChanges();
            }
        }
        public static void CreateErrorLog(Exception ex)
        {
            using (var DB = new MEHKWorkflowEntities())
            {
                Log Log = new Log();
                Log.Date = DateTime.Now;
                Log.ErrorCode = "";
                Log.LogPriority = "";
                Log.LogType = "Error";
                Log.Message = ex.Message;
                Log.StackTrace = ex.ToString();
                Log.Remark = "";
                Log.Source = "";
                DB.Entry(Log).State = System.Data.Entity.EntityState.Added;
                DB.SaveChanges();
            }
        }
        public static void CreateErrorLog(Exception ex, string Source)
        {

            using (var DB = new MEHKWorkflowEntities())
            {
                Log Log = new Log();
                Log.Date = DateTime.Now;
                Log.ErrorCode = "";
                Log.LogPriority = "";
                Log.LogType = "Error";
                Log.Message = ex.Message;
                Log.StackTrace = ex.ToString();
                Log.Remark = "";
                Log.Source = Source;
                DB.Entry(Log).State = System.Data.Entity.EntityState.Added;
                DB.SaveChanges();
            }
        }

        public static List<FormType> GetFormTypes()
        {
            var results = new List<FormType>();

            using (var DB = new MEHKWorkflowEntities())
            {
                results = DB.FormTypes.ToList();
            }

            return results;
        }

        public static List<StatusGroup> GetStatusGroups()
        {
            var results = new List<StatusGroup>();

            using (var DB = new MEHKWorkflowEntities())
            {
                results = DB.StatusGroups.Where(n => n.IsDeleted == false).ToList();
            }

            return results;
        }

        public List<MyDraftsModel> GetMyDrafts()
        {
            var list = new List<MyDraftsModel>();

            using (var DB = new MEHKWorkflowEntities())
            {

                //int UserID, string lang
                //list = DB.Database
                //    .SqlQuery<MyDraftsModel>("GetDraftRecord @user, @lang",
                //    new SqlParameter("user", UserID),
                //    new SqlParameter("lang", lang)).ToList();\

                list = DB.Database
                    .SqlQuery<MyDraftsModel>("GetDraftRecord").ToList();
            }

            return list;
        }

        public static List<MyTasksModel> GetMyTasks()
        {
            var list = new List<MyTasksModel>();

            using (var db = new MEHKWorkflowEntities())
            {
                list = db.Database
                    .SqlQuery<MyTasksModel>("GetMyTasks").ToList();

                //list = db.Database
                //    .SqlQuery<MyTasksModel>("GetMyTasks @user, @lang",
                //    new SqlParameter("user", UserID),
                //    new SqlParameter("lang", lang)).ToList();
            }

            return list;
        }

        public static string GetFormTypeSearchSQL(string FormTypeCode)
        {
            string result = "";

            switch (FormTypeCode)
            {
                case "EntertainmentGift":

                    result = "GetEntertainmentGiftSearch";

                    break;


                case "PO":

                    result = "GetPOSearch";
                    break;


                default:
                    break;
            }

            return result;
        }

        public static string GetFormTypeMySubmissionSQL(string FormTypeCode)
        {
            string result = "";

            switch (FormTypeCode)
            {
                case "EntertainmentGift":

                    result = "GetEntertainmentGiftMySubmission";

                    break;


                default:
                    break;
            }

            return result;
        }

        public static string GetSystemvalue(string Key)
        {
            string _value = "";
            using (var db = new MEHKWorkflowEntities())
            {
                var sys = db.SystemParameters.Where(p => p.AppKey == Key).ToList();
                if (sys.Count() > 0)
                {
                    _value = sys.FirstOrDefault().Value;
                }
            }
            return _value;
        }
        public static EntertainmentGiftLimit GetEntertainmentAndGift(string value)
        {
            EntertainmentGiftLimit result = new EntertainmentGiftLimit();
            using (var db = new MEHKWorkflowEntities())
            {
                result = db.EntertainmentGiftLimits.Where(e => e.Value == value &&
                DateTime.Compare((e.EffectiveDate != null ? (DateTime)e.EffectiveDate : DateTime.Now), DateTime.Now) < 0
                ).ToList().OrderByDescending(e => e.EffectiveDate).FirstOrDefault();

            }
            return result;
        }

        public static EmailTemplate GetEmailTemplate(string ProcessCode, int StepID, string ContentType)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                var _template = db.EmailTemplates.Where(p => p.ProcessCode == ProcessCode && p.StepID == StepID && p.ContentType == ContentType && p.IsDeleted == false).FirstOrDefault();
                return _template;
            }


        }
        public Delegation CreateDelegation(Delegation model)
        {
            using (var DB = new MEHKWorkflowEntities())
            {
                DB.Entry(model).State = System.Data.Entity.EntityState.Added;
                DB.SaveChanges();
                return model;
            }
        }
        public List<DelegationViewModel> GetDelegation()
        {
            using (var db = new MEHKWorkflowEntities())
            {
                return db.Delegations.Select(e => new DelegationViewModel
                {
                    Id = e.ID,
                    UserName = e.UserName,
                    AssignUserName = e.AssignUserName,
                    FormDate = e.FormDate,
                    ToDate = e.ToDate,
					AssignUserADName=e.AssignUserADName,
					UserADName=e.UserADName,
                    ProcessCode=e.ProcessCode,
                    ProcessName=e.ProcessName,

                }).ToList();
            }

        }
        public List<DelegationViewModel> GetDelegationByUserID(int? userID)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                return db.Delegations.Where(p=>p.UserID==userID).Select(e => new DelegationViewModel
                {
                    Id = e.ID,
                    UserName = e.UserName,
                    AssignUserName = e.AssignUserName,
                    FormDate = e.FormDate,
                    ToDate = e.ToDate,
                    AssignUserADName = e.AssignUserADName,
                    UserADName = e.UserADName,
                    ProcessCode = e.ProcessCode,
                    ProcessName = e.ProcessName,

                }).ToList();
            }

        }
        public List<DelegationViewModel> GetDelegationByCrateby(string Crateby)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                return db.Delegations.Where(p => p.CreatedBy == Crateby).Select(e => new DelegationViewModel
                {
                    Id = e.ID,
                    UserName = e.UserName,
                    AssignUserName = e.AssignUserName,
                    FormDate = e.FormDate,
                    ToDate = e.ToDate,
                    AssignUserADName = e.AssignUserADName,
                    UserADName = e.UserADName,
                    ProcessCode = e.ProcessCode,
                    ProcessName = e.ProcessName,

                }).ToList();
            }

        }
        public List<DelegationViewModel> GetDelegationByID(int userID, int assingUserID, string processCode)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                return db.Delegations.Where(e => e.UserID == userID && e.AssignUserID == assingUserID && e.ProcessCode == processCode).Select(e => new DelegationViewModel
                {
                    Id = e.ID,
                    UserName = e.UserName,
                    AssignUserName = e.AssignUserName,
                    FormDate = e.FormDate,
                    ToDate = e.ToDate,
                }).ToList();
            }

        }
        public Delegation DeleteDelegation(Delegation model)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                db.Entry(model).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
                return model;
            }
        }
		public bool isAcrvOrAcap(int ID)
		{
			int FormTypeID = GetFormTypeID(FormTypes.EntertainmentGift);
			var CurrentStep = GeneralWorkflowBLL.GetCurrentStep(ID, FormTypeID);
			if (CurrentStep == WorkflowSteps.Reimbursement_ACAP
				|| CurrentStep == WorkflowSteps.Reimbursement_ACRV1
				|| CurrentStep == WorkflowSteps.Reimbursement_ACRV2)
			{
				return true;
			}
			else { return false; }

		}
        public List<Unit> GetUserUnitAndSubUnit(int UnitID)
        {

            APIAccessBLL apiBLL = new APIAccessBLL();
            Dictionary<string, object> dir = new Dictionary<string, object>();
            dir.Add("Id", UnitID);

            var unit = apiBLL.Fun_CommonAPICALL("Organization/GetUnitAndSubUnit/", dir, new Unit());
            return unit;

        }
		public List<UserInfo> GetUnitUserList(int unitId)
		{
			List<UserInfo> result = new List<UserInfo>();
			APIAccessBLL apiBLL = new APIAccessBLL();
			var Manage = apiBLL.APICall("Members", "Organization", Method.GET, new List<Parameter>(), new UserInfo());
			Manage = Manage.Where(e => e.UnitId == unitId).ToList();
			return Manage;


		}
		public  string GetDelegationNextApprover(string ApprovalCriteriaType, string NextApprover)
		{
			string resNextApprover = "";
			string[] list = NextApprover.Split(';');
			foreach (var item in list)
			{
				if (ApprovalCriteriaType == ApprovalCriteriaTypeEnum.JL.ToString())
				{
					List<DelegationViewModel> Delegation = GetDelegation().Where(e => e.UserADName == item).ToList();
					if (Delegation != null && Delegation.Count > 0)
					{
						for (int i = 0; i < Delegation.Count(); i++)
						{
							if (DateTime.Now >= Delegation[i].FormDate && DateTime.Now <= Delegation[i].ToDate)
							{
								resNextApprover += Delegation[i].AssignUserADName+";";
							}
						}
					}
				}
			}
			if (resNextApprover != "")
			{
				resNextApprover = resNextApprover.Substring(0, resNextApprover.Length - 1);
			}
			else
			{
				resNextApprover = NextApprover;
			}
						return resNextApprover;
		}
		public string GetAppIdentityName(int FormID,int FormTypeID)
		{
			string AppIdentityName = "";
			using (var db = new MEHKWorkflowEntities())
			{
				var WFActivity = db.WFActivities.Where(n => n.Completed != true
					&& n.FormID == FormID
					&& n.FormTypeID == FormTypeID).OrderBy(n => n.ID).FirstOrDefault();
				var Details = db.WFActivityDetails.Where(e => e.WFActivityID == WFActivity.ID).FirstOrDefault();
				if (Details != null)
				{
					AppIdentityName = Details.UserName;
				}
			}
			return AppIdentityName;
		}
		public GetBudget_Result GetBudget(int FormID)
		{
			GetBudget_Result model = new GetBudget_Result();
			using (var db = new MEHKWorkflowEntities())
			{
				model = db.GetBudget(FormID).FirstOrDefault();
				return model;
			}
		}
		public AnnualBudget GetAnnualBudget(string DivisionID,string BudgetYear)
		{
			AnnualBudget model = null;
			using (var db = new MEHKWorkflowEntities())
			{
				List<AnnualBudget> list = db.AnnualBudgets.Where(e => e.DivisionID == DivisionID&& e.BudgetYear==BudgetYear).ToList();
				if (list.Count > 0)
				{
					if (list.Count > 1)
					{
						model = list.Where(e => e.AnnualBudgetVersionCode == "RB").FirstOrDefault();

					}
					else
					{
						model = list.Where(e => e.AnnualBudgetVersionCode == "OB").FirstOrDefault();
					}
				}
				return model;
			}
		}

    
    }
}

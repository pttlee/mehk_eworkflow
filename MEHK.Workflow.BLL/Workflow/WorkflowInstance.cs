﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.Workflow.Data.DTO;
using MEHK.Workflow.Data.Const;
using K25Lib;
using MEHK.Workflow.Data.Enum;
namespace MEHK.Workflow.BLL.Workflow
{
   public static class WorkflowInstance
    {
        private static string K2DomainName = "";
        private static string K2Server = "";
        private static string k2UserID = "";
        private static string k2Password = "";
        private static string loginDomain = "";
        private static string k2WindowsDomain = "";
        static string  ControllerName = "";

        public static WFControl WFControl
        {
            get
            {
                K2DomainName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.K2DomainName);
                K2Server = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.K2Server);
                k2UserID = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.k2UserID);
                k2Password = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.k2Password);
                loginDomain = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.loginDomain);
                k2WindowsDomain = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.k2WindowsDomain);
                CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.GenenalProcessName);
                WFControl WFControl = new WFControl(K2DomainName, K2Server, k2UserID, k2Password, loginDomain, k2WindowsDomain);

                //WFControl WFControl = new WFControl(K2DomainName, ProcessName, K2Server, k2UserID, k2Password, loginDomain, k2WindowsDomain);

                //WFControl WFControl = new WFControl(K2DomainName, CapexProcessName, K2Server, k2UserID, k2Password, loginDomain, k2WindowsDomain);

                return WFControl;
            }
        }
    }
}

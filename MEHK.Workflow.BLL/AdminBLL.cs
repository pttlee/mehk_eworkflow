﻿using MEHK.Workflow.Data.DTO;
using MEHK.Workflow.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.BLL
{
    public class AdminBLL
    {
        public List<GetEmailTemplateView_Result> GetEmailTemplate()
        {
            using (var db = new MEHKWorkflowEntities())
            {
                return db.GetEmailTemplateView().Where(e => e.IsDeleted == false).ToList();
            }
        }
        public EmailTemplate CreateEmailTemplate(EmailTemplate model)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                model = db.EmailTemplates.Add(model);
                db.SaveChanges();
                return model;
            }
        }
        public EmailTemplate UpdateEmailTemplate(EmailTemplate model)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return model;
            }
        }
        public List<Currency> GetCurrency()
        {
            using (var db = new MEHKWorkflowEntities())
            {
                return db.Currencies.ToList();
            }
        }
        public Currency CreateCurrency(Currency model)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                model = db.Currencies.Add(model);
                db.SaveChanges();
                return model;
            }
        }
        public Currency UpdateCurrency(Currency model)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); ;
                return model;
            }
        }

        public Currency DeleteCurrency(Currency model)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                db.Entry(model).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges(); ;
                return model;
            }
        }
        public List<EntertainmentGiftLimit> GetEntertainmentGiftLimit()
        {
            using (var db = new MEHKWorkflowEntities())
            {
                return db.EntertainmentGiftLimits.ToList();
            }
        }
        public EntertainmentGiftLimit UpdateEntertainmentGiftLimit(EntertainmentGiftLimit model)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); 
                return model;
            }
        }
        public List<Log> GetLog()
        {
            using (var db = new MEHKWorkflowEntities())
            {
                return db.Logs.ToList();
            }
        }

        public List<AnnualBudget> GetAnnualBudget()
        {
            using (var db = new MEHKWorkflowEntities())
            {
                return db.AnnualBudgets.ToList();
            }
        }
        public AnnualBudget CreateAnnualBudget(AnnualBudget model)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                model = db.AnnualBudgets.Add(model);
                db.SaveChanges();
                return model;
            }
        }
        public AnnualBudget UpdateAnnualBudget(AnnualBudget model)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); 
                return model;
            }
        }

        public AnnualBudget DeleteAnnualBudget(AnnualBudget model)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                db.Entry(model).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges(); 
                return model;
            }
        }


        public List<AnnualBudgetVersionType> GetAnnualBudgetVersionType()
        {
            using (var db = new MEHKWorkflowEntities())
            {
                return db.AnnualBudgetVersionTypes.ToList();
            }
        }
    }
}

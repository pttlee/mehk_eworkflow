﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.IO;
using System.Configuration;
using MEHK.Workflow.Data.Enum;
using MEHK.Workflow.BLL;
using CommonHelper;

namespace MEHK.Workflow.BLL
{
    public static class GenPDF
    {
        //public static  string  MergeMemoStream(string responseHistory_printStream_business, string formNumber, Dictionary<string, byte[]> attachmentList)
        //{

        //    Dictionary<string, byte[]> d = new Dictionary<string, byte[]>();
        //    string PdfTempPath = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.PdfTempPath);
        //    string path = PdfTempPath + formNumber;
        //    string filePath="";
        //    string pdffilePath = path + "\\" + formNumber + "_Form.pdf";
        //    try
        //    {
        //        if (!Directory.Exists(path))
        //        {
        //            Directory.CreateDirectory(path);
        //        }
        //        XSize size = PageSizeConverter.ToSize(PdfSharp.PageSize.A4);
        //        PdfDocument pdfdoc = new PdfDocument();
        //        PdfPage FirstPage = pdfdoc.AddPage();


        //        pdfdoc.Save(pdffilePath);
        //        byte[] responseHistory_printStream_business_bytes = Convert.FromBase64String(responseHistory_printStream_business);
        //        MemoryStream responseHistory_printStream__business_Stream = new MemoryStream(responseHistory_printStream_business_bytes);

        //        File.WriteAllBytes(pdffilePath, responseHistory_printStream_business_bytes);
        //        string attachmentPath = "";
        //        foreach (var item in attachmentList)
        //        {
        //            string _itemPath= path + "\\" + formNumber + "_" + item.Key;
        //            attachmentPath = attachmentPath + _itemPath + ";";
        //            WriteByteToFile(item.Value, _itemPath);
        //        }
        //        filePath = attachmentPath + pdffilePath;
        //    }
        //    catch (Exception ex)
        //    {
        //        //CreateErrorLog(ex);
        //        CommonBLL.CreateErrorLog(ex, "MergeMemoStream");
        //      //  throw new Exception("1_" + ex.Message);
        //    }
        //    return filePath.Length>0? filePath.Substring(0,filePath.Length-1):"";
        //}
        public static bool WriteByteToFile(byte[] pReadByte, string fileName)
        {
            FileStream pFileStream = null;
            BinaryWriter bw = null;
            try
            {
                byte[] decbytes = CompressHelper.Decompress(pReadByte);
                pFileStream = new FileStream(fileName, FileMode.OpenOrCreate);
                pFileStream.Write(decbytes, 0, decbytes.Length);


            }
            catch
            {
                return false;
            }
            finally
            {
                if (bw != null)
                    pFileStream.Close();
                if (pFileStream != null)
                    pFileStream.Close();
            }
            return true;
        }

        public static byte[] MergeMemoStream(string responseHistory_printStream_business, string formNumber)
        {
            byte[] responseHistory_printStream_business_bytes =null;
            Dictionary<string, byte[]> d = new Dictionary<string, byte[]>();
            string PdfTempPath = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.PdfTempPath);
            string path = PdfTempPath + formNumber;
            string filePath = "";
            string pdffilePath = path + "\\" + formNumber + "_Form.pdf";
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                XSize size = PageSizeConverter.ToSize(PdfSharp.PageSize.A4);
                PdfDocument pdfdoc = new PdfDocument();
                PdfPage FirstPage = pdfdoc.AddPage();


                pdfdoc.Save(pdffilePath);
                responseHistory_printStream_business_bytes = Convert.FromBase64String(responseHistory_printStream_business);

            }
            catch (Exception ex)
            {
                //CreateErrorLog(ex);
                CommonBLL.CreateErrorLog(ex, "MergeMemoStream");
                //  throw new Exception("1_" + ex.Message);
                return responseHistory_printStream_business_bytes;

            }

            return CompressHelper.Compress(responseHistory_printStream_business_bytes);


        }

    }
}

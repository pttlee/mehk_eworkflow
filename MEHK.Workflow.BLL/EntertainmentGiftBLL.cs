﻿using MEHK.Workflow.Data.Enum;
using MEHK.Workflow.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using MEHK.Workflow.Data.DTO;
//using MEHK.Workflow.BLL.DTO;

namespace MEHK.Workflow.BLL
{
    public class EntertainmentGiftBLL
    {

        public EntertainmentGiftRecord Get(int ID)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                var record = db.EntertainmentGiftRecords
                    //.Include(nameof(EntertainmentGiftRecordStatu))
                    .Where(n => n.ID == ID).FirstOrDefault();

                return record;
            }
        }

        public EntertainmentGiftRecord Submit(EntertainmentGiftRecord Model, UserInfo userinfoModle, Func<EntertainmentGiftRecord, string, string, int> startWorkFlow, ApprovalMartixViewModel approverModel, FlowType _ftypee)
        {
            var EntertainmentGiftBLL = new EntertainmentGiftBLL();


            using (var db = new MEHKWorkflowEntities())
            using (TransactionScope scope = new TransactionScope())
            {
                Dictionary<string, object> _Datafields = new Dictionary<string, object>();

                //var record = CreateOrUpdateRecord(viewModel, userInfo, SaveType.Submit);

                var FormNo = CommonBLL.GetSerialNumberWithoutYear("AC04-");

                Model.FormNo = FormNo;
                Model.IsCompleted = false;
                var record = SaveRecord(Model);



                //viewModel.ID = record.ID;

                //int FirstWFActivityID = 0;

                //InsertWFActivity(record, out FirstWFActivityID);

                //InsertApprovalHistroy(record.ID, CapexType.Project.ToString(), userInfo.UserID, "Submit", "Creator", "");
                //int formID, string FormNo, string NextApprover

                string NextApprover = EntertainmentGiftBLL.InsertWFActivity(record, approverModel, _ftypee, userinfoModle);

				//Ren 10_30_Delegation
				List<string> lstStep = new List<string>();
				APIAccessBLL apiBLL = new APIAccessBLL();
				var Manage = apiBLL.GetMembers("Organization/Members/");
				var payeeUser = Manage.Where(e => e.MemberId == record.Payee).FirstOrDefault();
				string payeeSAMAccount = "";
				if (payeeUser != null)
				{
					payeeSAMAccount = payeeUser.SAMAccount;
					//payeeSAMAccount = "GM";
				}
				var ApprovalMatrix = GetApprovalMartix(approverModel, _ftypee, out lstStep, payeeSAMAccount);
				var approvers = ApprovalMatrix.Where(p => p.ApproversFulList== NextApprover).FirstOrDefault();
				 NextApprover = new CommonBLL().GetDelegationNextApprover(approvers.ApprovalCriteriaType, NextApprover);


				CommonBLL.InsertApprovalHistroy(record.ID, FormTypes.EntertainmentGift, userinfoModle,
                    ApprovalActionHistoryActions.Submit,
                    ApprovalActionHistoryRoles.Applicant,
                    "",
                    ApprovalActionHistorySections.PreApproval.ToString());

                int procInstID = startWorkFlow(record, "", NextApprover);
			string	NextWFAction = WorkflowActions.Approve.ToString();
				string step = "PreApproval";

				if (NextApprover == record.CreatedBy)
				{
					NextWFAction = WorkflowActions.ReimbursementSubmit.ToString();
					step = "Reimbursement";
				}
				record.Step= NextApprover + "-" + NextWFAction + "-" + step;
				SaveRecord(record);

				if (procInstID != 0)
                {
                    record.ProcInstID = procInstID;
                    db.Entry(record).State = System.Data.Entity.EntityState.Modified;

                    db.SaveChanges();

                    scope.Complete();
                }
                else
                {
                    throw new Exception();
                }

                return record;
            }
        }

        //UserInfo userInfo,
        public EntertainmentGiftRecord SaveRecord(EntertainmentGiftRecord record)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                record.ModifiedDate = DateTime.Now;

                if (record.ID != 0)
                {


                    db.Entry(record).State = System.Data.Entity.EntityState.Modified;


                }
                else
                {
                    record.CreatedDate = DateTime.Now;
                    db.Entry(record).State = System.Data.Entity.EntityState.Added;

                }

                db.SaveChanges();
                EntertainmentGiftReimburseRecord ReimburseRecord = db.EntertainmentGiftReimburseRecords.Where(e => e.EntertainmentGiftRecordID == record.ID).FirstOrDefault();
                if (ReimburseRecord != null)
                {
                    ReimburseRecord = record.EntertainmentGiftReimburseRecord;
                    //ReimburseRecord.CleaningDate = DateTime.Now;
                    db.Entry(ReimburseRecord).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    ReimburseRecord = new EntertainmentGiftReimburseRecord();
                    ReimburseRecord.EntertainmentGiftRecordID = record.ID;
                    //ReimburseRecord.PostingInstruction = record.PostingInstruction;
                    ReimburseRecord.Purpose = record.Purpose;
                    ReimburseRecord.EventOn = record.EventOn;
                    ReimburseRecord.EventTo = record.EventTo;

                    ReimburseRecord.Place_NatureOfGift = record.Place_NatureOfGift;
                    ReimburseRecord.Guests = record.Guests;
                    ReimburseRecord.GuestTotal = record.GuestTotal;
                    ReimburseRecord.Company = record.Company;
                    ReimburseRecord.Attendants = record.Attendants;
                    ReimburseRecord.AttendantTotal = record.AttendantTotal;
                    ReimburseRecord.Amount = 0;
                    ReimburseRecord.ExchangeRate = 0;
                    ReimburseRecord.AmountHKEntertain = 0;
                    ReimburseRecord.AmountHKGift = 0;
                    ReimburseRecord.AmountExceedReason = "";


                    //ReimburseRecord.CleaningDate = DateTime.Now;
                    db.Entry(ReimburseRecord).State = System.Data.Entity.EntityState.Added;
                }
                db.SaveChanges();

                return record;
            }
        }
        public EntertainmentGiftReimburseRecord SaveReimburseRecord(EntertainmentGiftReimburseRecord ReimburseRecord)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                //ReimburseRecord.CleaningDate = DateTime.Now;
                //EntertainmentGiftRecord record = db.EntertainmentGiftRecords.Where(e => e.ID == ReimburseRecord.EntertainmentGiftRecordID).FirstOrDefault();
                //record.EntertainmentGiftReimburseRecord = ReimburseRecord;
                db.Entry(ReimburseRecord).State = System.Data.Entity.EntityState.Added;

                db.SaveChanges();

                return ReimburseRecord;
            }
        }
        public EntertainmentGiftReimburseRecord GetReimburseRecord(int ID)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                var record = db.EntertainmentGiftReimburseRecords
                    //.Include(nameof(EntertainmentGiftRecordStatu))
                    .Where(n => n.EntertainmentGiftRecordID == ID).FirstOrDefault();

                return record;
            }
        }

        public static int GetStatusID(Data.Enum.EntertainmentGiftRecordStatusCode Status)
        {
            int Value = 0;

            using (var db = new MEHKWorkflowEntities())
            {
                Value = db.EntertainmentGiftRecordStatus.Where(n => n.FormStatus == Status.ToString()).Select(n => n.ID).FirstOrDefault();
            }

            return Value;
        }


        public static EntertainmentGiftRecordStatu GetStatus(int StatusID)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                var result = db.EntertainmentGiftRecordStatus.Where(n => n.ID == StatusID).FirstOrDefault();

                return result;
            }

        }

        public string InsertWFActivity(EntertainmentGiftRecord record, ApprovalMartixViewModel ApproverModel, FlowType ftype, UserInfo UserinfoModel)
        {
            ApproversDetails details = new ApproversDetails();
            string NextApprover = "";
            int FormTypeID = CommonBLL.GetFormTypeID(FormTypes.EntertainmentGift);

            List<string> lstStep = new List<string>();

			string payeeSAMAccount = "";
			APIAccessBLL apiBLL = new APIAccessBLL();
			var Manage = apiBLL.GetMembers("Organization/Members/");
			var payeeUser = Manage.Where(e => e.MemberId == record.Payee).FirstOrDefault();
			if (payeeUser != null)
			{
				payeeSAMAccount = payeeUser.SAMAccount;
				//payeeSAMAccount = "GM";
			}
			//ApproverModel.Details = ApproverModel.Details.Where(e =>
			//e.Approvers.Where(p => p != payeeSAMAccount).ToList().Count > 0).ToList();
			var ApprovalMatrix = GetApprovalMartix(ApproverModel, ftype, out lstStep, payeeSAMAccount);


			using (var DB = new MEHKWorkflowEntities())
			{
				int _i = 1;
				foreach (var Step in lstStep)
				{
					var approvers = ApprovalMatrix.Where(p => p.ApprovalLevelDisplayName == Step.ToString()).FirstOrDefault();
					var StepID = CommonBLL.GetWFStepID(Step);

					var WFActivity = new WFActivity();

					WFActivity.FormID = record.ID;

					WFActivity.FormTypeID = FormTypeID;
					WFActivity.WFStepID = StepID;
					WFActivity.Completed = false;

					DB.Entry(WFActivity).State = System.Data.Entity.EntityState.Added;
					DB.SaveChanges();
					if (approvers != null && approvers.Approvers.Count() > 0)
					{
                        //lee 2019-9-24
                        //foreach (var aduser in approvers.ADApprovers)
                        //{
                        //	var WFActivityDetail = new WFActivityDetail();

                        //	WFActivityDetail.WFActivityID = WFActivity.ID;
                        //	WFActivityDetail.FormID = record.ID;
                        //	WFActivityDetail.FormTypeID = FormTypeID;
                        //	WFActivityDetail.WFStepID = StepID;

                        //	WFActivityDetail.UserName = aduser;

                        //	WFActivityDetail.CreatedDate = DateTime.Now;

                        //	DB.Entry(WFActivityDetail).State = System.Data.Entity.EntityState.Added;
                        //	DB.SaveChanges();
                        //}
                        foreach (var aduser in approvers.ApproverDetails)
                        {
                            if ((ApprovalCriteriaTypeEnum)Enum.Parse(typeof(ApprovalCriteriaTypeEnum), approvers.ApprovalCriteriaType) == ApprovalCriteriaTypeEnum.JL && approvers.ApproversFulList.ToLower() == UserinfoModel.SAMAccountWithDomain.ToLower())
                            {

                            }
                            else
                            {
                                var WFActivityDetail = new WFActivityDetail();
                                WFActivityDetail.WFActivityID = WFActivity.ID;
                                WFActivityDetail.FormID = record.ID;
                                WFActivityDetail.FormTypeID = FormTypeID;
                                WFActivityDetail.WFStepID = StepID;
                                WFActivityDetail.UserName = aduser.ADName;
                                WFActivityDetail.UserDisplayName = aduser.DisplayName;
								WFActivityDetail.ApprovalCriteriaType = approvers.ApprovalCriteriaType;

								WFActivityDetail.CreatedDate = DateTime.Now;
                                DB.Entry(WFActivityDetail).State = System.Data.Entity.EntityState.Added;
                                DB.SaveChanges();
                                if (_i == 1)
                                {
                                    NextApprover = approvers.ApproversFulList;
                                }
                            }
                        }
      //                  if (_i == 1)
						//{
						//	//NextApprover = approvers.ApproversFulList;
						//	NextApprover = new CommonBLL().GetDelegationNextApprover(approvers.ApprovalCriteriaType, approvers.ApproversFulList);
						//}



						//i++;
					}
					if (Step == WorkflowSteps.Reimbursement_Requester.ToString() || Step == WorkflowSteps.PreApproval_Requester.ToString())
					{
						var WFActivityDetail = new WFActivityDetail();

						WFActivityDetail.WFActivityID = WFActivity.ID;
						WFActivityDetail.FormID = record.ID;
						WFActivityDetail.FormTypeID = FormTypeID;
						WFActivityDetail.ApprovalCriteriaType = ApprovalCriteriaTypeEnum.JL.ToString();
						WFActivityDetail.WFStepID = StepID;

						WFActivityDetail.UserName = record.CreatedBy;
						WFActivityDetail.CreatedDate = DateTime.Now;
						DB.Entry(WFActivityDetail).State = System.Data.Entity.EntityState.Added;
						DB.SaveChanges();
						if (_i == 1)
						{
							NextApprover = record.CreatedBy;
							//NextApprover = new CommonBLL().GetDelegationNextApprover(approvers.ApprovalCriteriaType, record.CreatedBy);
						}
					}
					_i++;
				}

				//if (_i == 1&& NextApprover=="")
				//{
				//	NextApprover = record.CreatedBy;
				//}

			}
            return NextApprover;
        }

        public string InsertActivityForReturn(EntertainmentGiftRecord record,string Step)
        {
           
            int FormTypeID = CommonBLL.GetFormTypeID(FormTypes.EntertainmentGift);


            using (var DB = new MEHKWorkflowEntities())
            {
                var StepID = CommonBLL.GetWFStepID(Step);
                var WFActivity = new WFActivity();

                WFActivity.FormID = record.ID;
                WFActivity.FormTypeID = FormTypeID;
                WFActivity.WFStepID = StepID;
                WFActivity.Completed = false;

                DB.Entry(WFActivity).State = System.Data.Entity.EntityState.Added;
                DB.SaveChanges();

                var WFActivityDetail = new WFActivityDetail();

                WFActivityDetail.WFActivityID = WFActivity.ID;
                WFActivityDetail.FormID = record.ID;
                WFActivityDetail.FormTypeID = FormTypeID;
                WFActivityDetail.WFStepID = StepID;

                WFActivityDetail.UserName = record.CreatedBy;

                WFActivityDetail.CreatedDate = DateTime.Now;

                DB.Entry(WFActivityDetail).State = System.Data.Entity.EntityState.Added;
                DB.SaveChanges();
            }
            return record.CreatedBy;
        }

        public List<GetMemberByMemberIDList_Result> GetMemberList(int MemberID)
        {
            List<GetMemberByMemberIDList_Result> result = new List<GetMemberByMemberIDList_Result>();
            using (var DB = new MEHKWorkflowEntities())
            {
                result = DB.GetMemberByMemberIDList(MemberID).ToList();
            }
            return result;
        }

        public List<ApprovalMartixDetailViewModel> GetApprovalMartix(ApprovalMartixViewModel ApproverModel, FlowType ftype, out List<string> lstStep,string payeeSAMAccount)
        {
            List<ApprovalMartixDetailViewModel> ApprovalMatrix = new List<ApprovalMartixDetailViewModel>();
            var addomain = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.loginDomain);
            lstStep = new List<string>();
            //if (bReturn && !bACAPReturn)
            //{
            //    if (ftype == FlowType.PreApproval)
            //    {
            //        lstStep.Add(WorkflowSteps.PreApproval_Requester.ToString());
            //    }
            //    else
            //    {
            //        lstStep.Add(WorkflowSteps.Reimbursement_Requester.ToString());
            //    }
            //}
            if (ApproverModel.Details.Count() > 0)
            {
                foreach (var details in ApproverModel.Details)
                {
					if (details.Approvers.Where(e => e != payeeSAMAccount).ToList().Count > 0)
					{

						if (details.Approvers.ToList().Count() > 0)
						{
							details.ApprovalLevelDisplayName = ftype.ToString() + "_" + details.ApprovalLevelCode;
							lstStep.Add(details.ApprovalLevelDisplayName);
							string fullname = "";
							details.ADApprovers = new List<string>();
							foreach (var u in details.Approvers)
							{
								if (u != payeeSAMAccount)
								{
									details.ADApprovers.Add(addomain + "\\" + u);
									fullname = fullname + addomain + "\\" + u + ";";
								}
							}

							details.ApproversFulList = fullname != "" ? fullname.Substring(0, fullname.Length - 1) : "";
						}
						else
						{
						}
					}

                }
                if (ftype == FlowType.PreApproval)
                {
                    lstStep.Add(WorkflowSteps.Reimbursement_Requester.ToString());
                }
                ApprovalMatrix = ApproverModel.Details;
            }



            return ApprovalMatrix;
        }
        public List<Currency> GetCurrencyList()
        {
            List<Currency> list = new List<Currency>();
            using (var DB = new MEHKWorkflowEntities())
            {
                list = DB.Currencies.OrderBy(e=>e.SortSeq).ToList();
            }
            return list;
        }
        public List<Voucher> SetVoucher(List<Voucher> list)
        {
            using (var db = new MEHKWorkflowEntities())
            {
				//List<Voucher> VouchersList = db.Vouchers.Where(e => e.FormID == FormID).ToList();
				//foreach (var item in VouchersList)
				//{
				//	item.IsDeleted = true;
				//	db.Entry(item).State = System.Data.Entity.EntityState.Modified;
				//	db.SaveChanges();
				//}

                foreach (var item in list)
                {
					if (item.ID != 0)
					{
						db.Entry(item).State = System.Data.Entity.EntityState.Modified;
						db.SaveChanges();
					}
					else
					{

						db.Entry(item).State = System.Data.Entity.EntityState.Added;
						db.SaveChanges();
					}
					
                }
            }
            return list;
        }
		//public List<Voucher> GetVoucherList(int FormId)
		//{
		//	List<Voucher> list = new List<Voucher>();
		//	using (var db = new MEHKWorkflowEntities())
		//	{
		//		list = db.Vouchers.Where(e => e.FormID == FormId && e.IsDeleted == false).ToList();
		//	}
		//	return list;
		//}
		public Voucher createVoucher(Voucher model)
		{
			Voucher res = new Voucher();
			using (var db = new MEHKWorkflowEntities())
			{
				//db.Entry(model).State = System.Data.Entity.EntityState.Added;
				res = db.Vouchers.Add(model);
				db.SaveChanges();
			}
			return res;
		}
		public Voucher updateVoucher(Voucher model)
		{
			using (var db = new MEHKWorkflowEntities())
			{
				db.Entry(model).State = System.Data.Entity.EntityState.Modified;
				db.SaveChanges();
			}
			return model;
		}
	
		public void upadteSAP(int FormID, string SAPDocNo, string SAPRemark,DateTime? PostDate)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                EntertainmentGiftReimburseRecord record = db.EntertainmentGiftReimburseRecords.Where(e => e.EntertainmentGiftRecordID == FormID).FirstOrDefault();
                record.SAPDocNo = SAPDocNo;
                record.SAPRemark = SAPRemark;
				record.PostDate = PostDate;

				db.Entry(record).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }
        public List<Voucher> GetVoucher(int ID)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                var record = db.Vouchers
                    .Where(n => n.FormID == ID&&n.IsDeleted==false).ToList();

                return record;
            }
        }

        public Dictionary<string, byte[]> GetFilesList(string processCode,int formID)
        {
            Dictionary<string, byte[]> dictionaryByte = new Dictionary<string, byte[]>();
            using (var db = new MEHKWorkflowEntities())
            {
                var record = db.GetFilesList(formID, processCode)
                 .ToList();
            
                foreach(var item in record)
                {
                    dictionaryByte.Add(item.FileName, item.FileData);
                }

              
            }
            return  dictionaryByte;
        }
        public Dictionary<string, byte[]> InsertPDFFile(int processID, int formID,int userID,string fileName,double filesize,byte[] filedata)
        {
            Dictionary<string, byte[]> dictionaryByte = new Dictionary<string, byte[]>();
            using (var db = new MEHKWorkflowEntities())
            {
                db.InsertFile(Guid.NewGuid(), processID, formID, userID, fileName, filesize, filedata);
            }
            return dictionaryByte;
        }
        public string GetStep(string NextApprover,string status,string type)
        {
           return  string.Format("{0}-{1}-{2}", NextApprover, status, type);
        }
		public string GetNextApproverdisplayName(string NextApprover,string ApprovalCriteriaType)
		{
			string NextApproverdisplayName = "";
			if (NextApprover != null)
			{
				APIAccessBLL apiBLL = new APIAccessBLL();
				List<string> ApproverList = NextApprover.Split(';').ToList();

				//NextApprover = NextApprover.Split(';').FirstOrDefault();
				foreach (var item in ApproverList)
				{
					var NextApprovermodel = apiBLL.GetMember(@"Organization/Members/", item.Split('\\')[1]).FirstOrDefault();

					if (NextApprovermodel != null)
					{

						//NextApproverdisplayName += new CommonBLL().GetDelegationNextApprover(ApprovalCriteriaType, NextApprovermodel.MemberName) + ",";
						NextApproverdisplayName += NextApprovermodel.MemberName+",";

					}
				}
				

			}
			if (NextApproverdisplayName != "") {
				NextApproverdisplayName = NextApproverdisplayName.Substring(0, NextApproverdisplayName.Length - 1);
					}
			return NextApproverdisplayName;
		}
		public string BatchUpdateEntertainmentGift(int ID,string UserName, UserInfo GetUserInfo)
        {
			string ret = "";
			try
			{
				var EntertainmentGiftBLL = new EntertainmentGiftBLL();
				var record = EntertainmentGiftBLL.Get(ID);
				string step = "";
				if (record.IsCompleted == true)
				{
					step = FormStep.Reimbursement.ToString();
				}
				else
				{
					step = FormStep.PreApproval.ToString();
				}
				string NextApproverdisplayName = "";
				string NextWFAction = WorkflowActions.Approve.ToString();
				string NextApprover = GeneralWorkflowBLL.GetNextStepUser(record.ID, CommonBLL.GetFormTypeID(FormTypes.EntertainmentGift)).ADName;
				string ApprovalCriteriaType = GeneralWorkflowBLL.GetNextStepUser(record.ID, CommonBLL.GetFormTypeID(FormTypes.EntertainmentGift)).ApprovalCriteriaType;
				if (NextApprover != null)
				{
					NextApproverdisplayName = GetNextApproverdisplayName(NextApprover, ApprovalCriteriaType);
					ret = NextApprover;
				}
				var NextStep = GeneralWorkflowBLL.GetNextStep(record.ID, CommonBLL.GetFormTypeID(FormTypes.EntertainmentGift));
				if (NextStep == WorkflowSteps.Reimbursement_Requester)
				{
					NextWFAction = WorkflowActions.ReimbursementSubmit.ToString();
				}
				GeneralWorkflowBLL.CompleteTask(record.ID, CommonBLL.GetFormTypeID(FormTypes.EntertainmentGift), UserName, "Approve");
				record.Step = NextApproverdisplayName + "-" + NextWFAction + "-" + step;
				EntertainmentGiftBLL.SaveRecord(record);
				var CurrentStep = GeneralWorkflowBLL.GetCurrentStep(record.ID, CommonBLL.GetFormTypeID(FormTypes.EntertainmentGift));
				GeneralWorkflowBLL.ResetWFActivityDetailFromStepName(record.ID, CommonBLL.GetFormTypeID(FormTypes.EntertainmentGift), CurrentStep.ToString());
				ApprovalActionHistoryActions ApprovalActionHistoryAction = ApprovalActionHistoryActions.Empty;
				Enum.TryParse("Approve", out ApprovalActionHistoryAction);
				CommonBLL.InsertApprovalHistroy(record.ID, FormTypes.EntertainmentGift, GetUserInfo,
					ApprovalActionHistoryAction,
					ApprovalActionHistoryRoles.AccountApproval,
					"",
					ApprovalActionHistorySections.PreApproval.ToString());

				
			}
			catch (Exception ex) {
				ret = "";
			}
			return ret;
		}

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using MEHK.Workflow.Data.DTO;
using MEHK.Workflow.Data.Const;
using MEHK.Workflow.BLL.Workflow;
using MEHK.Workflow.Data.Enum;
using MEHK.Workflow.Data.Model;
namespace MEHK.Workflow.BLL.Authorize
{
    public class EntertainmentGiftAuthorize : MEHKAuthorize
    {
        protected string ProcessName = "";
     

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            int FormID = 0;
            var EntertainmentGiftBLL = new EntertainmentGiftBLL();
            HttpRequestBase request = httpContext.Request;//定义传统request对象      
            var record = new EntertainmentGiftRecord();
            bool result = false;
            if (request.QueryString["ID"] == null)
            {
                return false;
            }
            else
            {
                int.TryParse(request.QueryString["ID"].ToString(), out FormID);
                record = EntertainmentGiftBLL.Get(FormID);
            }
            if (request.QueryString["SN"] != null)
            {
                this.ProcessName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.GenenalProcessName);
                string SN = request.QueryString["SN"].ToString();
                var ProcessItemList = WorkflowInstance.WFControl.GetWorkList(httpContext.User.Identity.Name, new string[] { ProcessName });
                result = ProcessItemList.Any(p => p.ProcInstID == record.ProcInstID);
            }
            else
            {

                APIAccessBLL apiBLL = new APIAccessBLL();
                Dictionary<string, object> dir = new Dictionary<string, object>();
                dir.Add("SAMACCOUNT", httpContext.User.Identity.Name.Split('\\')[1]);

                dir.Add("AppID", CommonConstcs.AppID);
                var Rights = apiBLL.Fun_CommonAPICALL(@"Common/GetUserRight/", dir, new Right());
                var _user = apiBLL.GetMember("Organization/Members/", httpContext.User.Identity.Name.Split('\\')[1]).FirstOrDefault();
                List<Unit> Unitlist = _user.BelowUnits;
                List<int> unitlistis = _user.BelowUnits.Select(e => e.UnitId).ToList();

                if (Rights.Any(p => p.RightName == RightConst.VIEWALL_ENTERTAINMENT_GIT_RIGHT))
                {
                    result = true;
                }
                else if (Rights.Any(p => p.RightName == RightConst.VIEWDEPARTMENT_ENTERTAINMENT_GIT_RIGHT))
                {
                    result = unitlistis.Any(p => p == record.DepartmentID);
                }
                else if (Rights.Any(p => p.RightName == RightConst.VIEWOWN_ENTERTAINMENT_GIT_RIGHT))
                {
                    if (record.Payee == _user.MemberId || record.CreatedBy.Split('\\').Length > 0 ? record.CreatedBy.Split('\\')[1] == _user.SAMAccount : false)
                    {
                        result = true;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using RestSharp;
using MEHK.Workflow.Data.DTO;
using Newtonsoft.Json;
using System.IO;
using System.Dynamic;
using CommonHelper;
using System.Reflection;
//using MEHK.Workflow.BLL.DTO;
using MEHK.Workflow.Data.Enum;
using MEHK.Workflow.Data.Model;
using System.Data.Entity;

namespace MEHK.Workflow.BLL
{
    public class APIAccessBLL
    {
        private static string ApiURL
        {
            get
            {
                //var a = HttpContext.Current.Server.MapPath(CommonBLL.GetSystemvalue("ApiPath"));
                // return CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.ApiPath);
                return CommonBLL.GetSystemvalue("ApiPath");
            }


            // set;
        }
        private static RestClient _restClient
        {
            get
            {

                return new RestClient(ApiURL)
                {
                    Authenticator = new RestSharp.Authenticators.NtlmAuthenticator()
                }; ;
            }
        }
        //public APIAccessBLL(string _apiurl)
        //{

        //    // ApiURL = _apiurl;
        //}
        public APIAccessBLL()
        {
            //ApiURL = CommonBLL.GetSystemvalue("ApiPath"); 
        }
        public List<UserInfo> GetMember(string apimethod, string SAMAccount)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("SAMAccount", SAMAccount);
            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<UserInfo> info = JsonConvert.DeserializeObject<List<UserInfo>>(content);
            return info;
        }

        public List<UserInfo> GetMember(string apimethod, Dictionary<string, string> dir)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            foreach (var str in dir)
            {
                _request.AddParameter(str.Key, str.Value);
            }
            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<UserInfo> info = JsonConvert.DeserializeObject<List<UserInfo>>(content);
            return info;
        }
        public List<CostCenters> GetCostCenters(string apimethod, int MemberId)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("MemberId", MemberId);
            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<CostCenters> list = JsonConvert.DeserializeObject<List<CostCenters>>(content);
            return list;
        }
        public List<ApprovalMartixViewModel> GetApprovalMartix(string apimethod, int MemberId, string ProcessCode, string ApprovalMartixOptionCode)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("MemberId", MemberId);
            _request.AddParameter("ProcessCode", ProcessCode);
            _request.AddParameter("ApprovalMartixOptionCode", ApprovalMartixOptionCode);
            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<ApprovalMartixViewModel> list = JsonConvert.DeserializeObject<List<ApprovalMartixViewModel>>(content);
            return list;
        }
        public ApprovalMartixViewModel GetApprovers(string apimethod, string MemberId, string ProcessCode, string ApprovalMartixOptionCode)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("ProcessCode", ProcessCode);
            _request.AddParameter("MemberId", MemberId);
            _request.AddParameter("ApprovalMartixOptionCode", ApprovalMartixOptionCode);
            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            ApprovalMartixViewModel list = JsonConvert.DeserializeObject<ApprovalMartixViewModel>(content);
            return list;
        }
        public static List<Unit> GetChildrenUnit(string apimethod, int UnitId)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("UnitId", UnitId);

            IRestResponse _respond = _restClient.Execute(_request);
            var content = _respond.Content;

            List<Unit> list = JsonConvert.DeserializeObject<List<Unit>>(content);
            return list;
        }
        public static List<Unit> GetUnit(int UnitId)
        {
            var client = _restClient;
            var _request = new RestRequest("Units", Method.GET);
            _request.AddParameter("UnitId", UnitId);

            IRestResponse _respond = _restClient.Execute(_request);
            var content = _respond.Content;

            List<Unit> list = JsonConvert.DeserializeObject<List<Unit>>(content);
            return list;
        }
        public static List<UnitType> GetAllUnitTypes()
        {
            var client = _restClient;
            var _request = new RestRequest("UnitTypes", Method.GET);
            _request.AddParameter("id", -1);

            IRestResponse _respond = _restClient.Execute(_request);
            var content = _respond.Content;

            List<UnitType> list = JsonConvert.DeserializeObject<List<UnitType>>(content);
            return list;
        }
        public static string GetOrgManagementSystemParameter(string Key)
        {
            var client = _restClient;
            var _request = new RestRequest("Common/SystemParameter", Method.GET);
            _request.AddParameter("Key", Key);

            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            string Value = JsonConvert.DeserializeObject<string>(content);
            return Value;
        }
        public List<T> APICall<T>(string EndPoint, string Controller, Method MethodType, List<Parameter> Params, T Object, Object BodyParams = null) where T : class
        {

            try
            {
                var client = _restClient;
                var restrequest = new RestRequest(Controller + "/" + EndPoint + "/", MethodType)
                {
                    RequestFormat = DataFormat.Json
                };
                restrequest.AddHeader("Cache-Control", "no-cache");

                if (Params != null)
                {
                    dynamic expandoObj = new ExpandoObject();
                    foreach (Parameter pm in Params)
                    {
                        restrequest.AddParameter(pm.Name, pm.Value);
                        DynamicHelper.AddProperty(expandoObj, pm.Name, pm.Value);
                    }
                    restrequest.AddBody(expandoObj);
                }

                if (BodyParams != null)
                {
                    restrequest.AddHeader("Content-type", "application/json");
                    PropertyInfo[] ps = BodyParams.GetType().GetProperties();
                    foreach (PropertyInfo p in ps)
                    {
                        var value = p.GetValue(BodyParams, null);
                        restrequest.AddJsonBody(value);
                    }

                }
                IRestResponse response = client.Execute(restrequest);
                var content = response.Content;
                //return new List<T>();
                return JsonConvert.DeserializeObject<List<T>>(content);
            }
            catch (Exception ex)
            {
                CommonBLL.CreateErrorLog(ex);
                return new List<T>();
            }

        }
        public List<UserInfo> GetMembers(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("additionalinfo", false);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<UserInfo> list = JsonConvert.DeserializeObject<List<UserInfo>>(content);
            return list;
        }
        public List<UserInfo> GetMemberList(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<UserInfo> list = JsonConvert.DeserializeObject<List<UserInfo>>(content);
            return list;
        }
        public List<Group> GetGroup(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", 0);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<Group> list = JsonConvert.DeserializeObject<List<Group>>(content);
            return list;
        }
        public List<GroupMemberViewModel> GetGroupMemberViewModelList(string apimethod, int nid)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("nid", nid);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<GroupMemberViewModel> list = JsonConvert.DeserializeObject<List<GroupMemberViewModel>>(content);
            return list;
        }
        public List<MemberGroupViewModel> GetMemberGroupViewModelList(string apimethod, int nid)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("nid", nid);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<MemberGroupViewModel> list = JsonConvert.DeserializeObject<List<MemberGroupViewModel>>(content);
            return list;
        }
        public List<GroupRightViewModel> GetGroupRightList(string apimethod, int nid)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("nid", nid);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<GroupRightViewModel> list = JsonConvert.DeserializeObject<List<GroupRightViewModel>>(content);
            return list;
        }
        public List<RightGroupViewModel> GetRightGroupList(string apimethod, int nid)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("nid", nid);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<RightGroupViewModel> list = JsonConvert.DeserializeObject<List<RightGroupViewModel>>(content);
            return list;
        }
        public List<GroupMemberViewModel> UpdateGroupMemberList(string apimethod, string models)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("models", models);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<GroupMemberViewModel> list = JsonConvert.DeserializeObject<List<GroupMemberViewModel>>(content);
            return list;
        }
        public List<MemberGroupViewModel> UpdateMemberGroupList(string apimethod, string models)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("models", models);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<MemberGroupViewModel> list = JsonConvert.DeserializeObject<List<MemberGroupViewModel>>(content);
            return list;
        }
        public List<GroupRightViewModel> UpdateGroupRightList(string apimethod, string models)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("models", models);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<GroupRightViewModel> list = JsonConvert.DeserializeObject<List<GroupRightViewModel>>(content);
            return list;
        }
        public List<RightGroupViewModel> UpdateRightGroupList(string apimethod, string models)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("models", models);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<RightGroupViewModel> list = JsonConvert.DeserializeObject<List<RightGroupViewModel>>(content);
            return list;
        }
        public List<Right> GetRight(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", 0);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<Right> list = JsonConvert.DeserializeObject<List<Right>>(content);
            return list;
        }

        public List<Application> GetApplication(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", 0);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<Application> list = JsonConvert.DeserializeObject<List<Application>>(content);
            return list;
        }
        public List<ApprovalMartix> GetApprovalMartix(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", 0);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<ApprovalMartix> list = JsonConvert.DeserializeObject<List<ApprovalMartix>>(content);
            return list;
        }
        public List<ProcessList> GetProcessList(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", 0);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<ProcessList> list = JsonConvert.DeserializeObject<List<ProcessList>>(content);
            return list;
        }
        public List<ProcessOption> GetProcessOption(string apimethod, int processID)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", processID);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<ProcessOption> list = JsonConvert.DeserializeObject<List<ProcessOption>>(content);
            return list;
        }

        public List<Unit> GetUnitList(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", 0);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<Unit> list = JsonConvert.DeserializeObject<List<Unit>>(content);
            return list;
        }
        public List<Role> GetRoleList(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", 0);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<Role> list = JsonConvert.DeserializeObject<List<Role>>(content);
            return list;
        }

        public List<JobLeveln> GetJobLevel(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", 0);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<JobLeveln> list = JsonConvert.DeserializeObject<List<JobLeveln>>(content);
            return list;
        }

        public List<ApprovalMartixOption> GetApprovalMartixOption(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Code", "");


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<ApprovalMartixOption> list = JsonConvert.DeserializeObject<List<ApprovalMartixOption>>(content);
            return list;
        }
        public List<ApprovalLevel> GetApprovalLevel(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", 0);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<ApprovalLevel> list = JsonConvert.DeserializeObject<List<ApprovalLevel>>(content);
            return list;
        }
        public List<ApprovalCriteriaType> GetApprovalCriteriaType(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<ApprovalCriteriaType> list = JsonConvert.DeserializeObject<List<ApprovalCriteriaType>>(content);
            return list;
        }
        public List<CostCenter> GetCostCenter(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", 0);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<CostCenter> list = JsonConvert.DeserializeObject<List<CostCenter>>(content);
            return list;
        }
        public List<RoleMemberViewModel> GetRoleMemberList(string apimethod, int nid)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("nid", nid);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<RoleMemberViewModel> list = JsonConvert.DeserializeObject<List<RoleMemberViewModel>>(content);
            return list;
        }
        public List<RoleMemberViewModel> UpdateRoleMemberList(string apimethod, string models)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("models", models);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<RoleMemberViewModel> list = JsonConvert.DeserializeObject<List<RoleMemberViewModel>>(content);
            return list;
        }
        public List<SiteMenu> GetSiteMenu(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", 0);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<SiteMenu> list = JsonConvert.DeserializeObject<List<SiteMenu>>(content);
            return list;
        }
        public List<SiteMenuRightViewModel> GetSiteMenuRightList(string apimethod, int nid)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("nid", nid);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<SiteMenuRightViewModel> list = JsonConvert.DeserializeObject<List<SiteMenuRightViewModel>>(content);
            return list;
        }
        public List<SiteMenuRightViewModel> UpdateSiteMenuRightList(string apimethod, string models)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("models", models);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<SiteMenuRightViewModel> list = JsonConvert.DeserializeObject<List<SiteMenuRightViewModel>>(content);
            return list;
        }
        public List<SystemParameterViewModels> GetListSystemParameter(string apimethod)
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            _request.AddParameter("Id", 0);


            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<SystemParameterViewModels> list = JsonConvert.DeserializeObject<List<SystemParameterViewModels>>(content);
            return list;
        }
        public List<UserInfo> GetMember()
        {
            var client = _restClient;
            var _request = new RestRequest("Organization/Members/", Method.GET);
            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<UserInfo> info = JsonConvert.DeserializeObject<List<UserInfo>>(content);
            return info;
        }
        public UserInfo GetMemberById(int MemberId)
        {
            var client = _restClient;
            var _request = new RestRequest("Organization/GetMemberById/", Method.GET);
            _request.AddParameter("Id", MemberId);
            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            UserInfo info = JsonConvert.DeserializeObject<UserInfo>(content);
            return info;
        }
        public List<vDivisionViewModel> GetvDivisionList()
        {
            var client = _restClient;
            var _request = new RestRequest("Organization/GetvDivisionList/", Method.GET);
            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<vDivisionViewModel> info = JsonConvert.DeserializeObject<List<vDivisionViewModel>>(content);
            return info;
        }
        public List<AlternativePayeeViewModel> GetListBpxMemberAlternativePayee(int nid)
        {
            List<AlternativePayeeViewModel> list = new List<AlternativePayeeViewModel>();
            using (var db = new MEHKWorkflowEntities())
            {
                list = db.GetListBpxMemberAlternativePayee(nid).Select(e => new AlternativePayeeViewModel
                {
                    MemberId = e.MemberId,
                    MemberName = e.MemberName,
                    bExist = e.bexist,
                    ID = nid
                }).ToList();
            }
            return list;
        }

        public List<AlternativePayeeViewModel> UpdateListAlternativePayee(List<AlternativePayeeViewModel> list)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                foreach (AlternativePayeeViewModel item in list)
                {
                    List<AlternativePayee> data = db.AlternativePayees.Where(e => e.AlternativePayee1 == item.MemberId && e.MemberID == item.ID).ToList();
                    AlternativePayee model = new AlternativePayee();
                    if (item.bExist.ToLower() == "false")
                    {
                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = true;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = false;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            model.MemberID = item.ID;
                            model.AlternativePayee1 = item.MemberId;
                            model.IsDeleted = false;
                            model = db.AlternativePayees.Add(model);
                            db.SaveChanges();
                        }
                    }
                }
            }
            return list;
        }

        public List<T> Fun_CommonAPICALL<T>(string apimethod, Dictionary<string,object> paraneters, T Object) where T : class
        {
            var client = _restClient;
            var _request = new RestRequest(apimethod, Method.GET);
            foreach (var item in paraneters)
            {
                _request.AddParameter(item.Key, item.Value);
            }
            IRestResponse _respond = client.Execute(_request);
            var content = _respond.Content;

            List<T> info = JsonConvert.DeserializeObject<List<T>>(content);
            return info;
        }

        public List<Right> GetUserRight(string samAccount,string appID)
        {
            Dictionary<string, object> dir = new Dictionary<string, object>();
            dir.Add("SAMACCOUNT", samAccount);
            dir.Add("AppID", appID);
            return Fun_CommonAPICALL(@"Common/GetUserRight/", dir, new Right());

        }
    }
}

﻿using MEHK.Workflow.Data.Enum;
using MEHK.Workflow.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.Workflow.Data.DTO;
namespace MEHK.Workflow.BLL
{
   public   class GeneralWorkflowBLL
    {
        public static void DeleteWFActivity(int FormID, int FormTypeID)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                var WFActivities = db.WFActivities.Where(n => n.FormID == FormID
                && n.FormTypeID == FormTypeID).ToList();

                foreach (var WF in WFActivities)
                {
                    db.Entry(WF).State = System.Data.Entity.EntityState.Deleted;

                    var WFActivityDetails = db.WFActivityDetails.Where(n => n.WFActivityID == WF.ID).ToList();

                    foreach (var WFDetail in WFActivityDetails)
                    {
                        db.Entry(WFDetail).State = System.Data.Entity.EntityState.Deleted;
                    }
                }

                db.SaveChanges();

            }
        }
        public static void DeleteCompletedWFActivity(int FormID, int FormTypeID)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                var WFActivities = db.WFActivities.Where(n => n.FormID == FormID
                && n.FormTypeID == FormTypeID && n.Completed==true).ToList();

                foreach (var WF in WFActivities)
                {
                    db.Entry(WF).State = System.Data.Entity.EntityState.Deleted;

                    var WFActivityDetails = db.WFActivityDetails.Where(n => n.WFActivityID == WF.ID).ToList();

                    foreach (var WFDetail in WFActivityDetails)
                    {
                        db.Entry(WFDetail).State = System.Data.Entity.EntityState.Deleted;
                    }
                }

                db.SaveChanges();

            }
        }
        public static void ResetWFActivityDetailFromStepName(int FormID, int FormTypeID, string StepCode)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                int WFActivityID = db.WFActivities.Where(n => n.FormID == FormID
                && n.FormTypeID == FormTypeID
                && n.WFStep.Step == StepCode).Select(n => n.ID).FirstOrDefault();

                var WFActivityDetails = db.WFActivityDetails.Where(n => n.FormID == FormID
                && n.FormTypeID == FormTypeID
                && n.WFActivityID >= WFActivityID).ToList();

                foreach (var item in WFActivityDetails)
                {
                    item.ActionBy = null;
                    item.ActionDate = null;
                    item.ActionName = null;

                    db.Entry(item).State = System.Data.Entity.EntityState.Modified;

                    var WFActivity = db.WFActivities.Where(n => n.ID == item.WFActivityID).FirstOrDefault();

                    WFActivity.Completed = false;
                    WFActivity.CompletedDate = null;

                    db.Entry(WFActivity).State = System.Data.Entity.EntityState.Modified;

                }

                db.SaveChanges();
            }
        }

        public static bool CheckLastStep(int FormID, int FormTypeID)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                //List<string> NotLastStep = new List<string>();

                var Count = db.WFActivities.Where(n => n.Completed != true
                && n.FormID == FormID
                && n.FormTypeID == FormTypeID).ToList().Count;
                //&& !NotLastStep.Contains(n.WFStep.Step)).ToList().Count;

                var HasReturned = db.WFActivityDetails.Any(n => n.FormID == FormID
                && n.FormTypeID == FormTypeID
                && n.ActionName == "Returned");

                if (Count == 1 && !HasReturned)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static WorkflowSteps GetCurrentStep(int FormID, int FormTypeID)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                WorkflowSteps result = WorkflowSteps.Empty;

                var Step = db.WFActivities.Where(n => n.Completed != true
                && n.FormID == FormID
                && n.FormTypeID == FormTypeID).OrderBy(n => n.ID).Select(n => n.WFStep.Step).FirstOrDefault();

                Enum.TryParse(Step, out result);

                return result;
            }
        }

        public static WorkflowSteps GetNextStep(int FormID, int FormTypeID)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                WorkflowSteps result = WorkflowSteps.Empty;

                var Step = db.WFActivities.Where(n => n.Completed != true
                && n.FormID == FormID
                && n.FormTypeID == FormTypeID).OrderBy(n => n.ID).Select(n => n.WFStep.Step).Skip(1).Take(1).FirstOrDefault();

                Enum.TryParse(Step, out result);

                return result;
            }


        }

        public static int? GetNextStepID(int FormID, int FormTypeID)
        {
            using (var db = new MEHKWorkflowEntities())
            {
              

                var Step = db.WFActivities.Where(n => n.Completed != true
                && n.FormID == FormID
                && n.FormTypeID == FormTypeID).OrderBy(n => n.ID).Select(n => n.WFStepID).Skip(1).Take(1).FirstOrDefault();

                

                return Step;
            }


        }
        public static ApproversDetails GetNextStepUser(int FormID, int FormTypeID)
        {
            ApproversDetails details = new ApproversDetails();
            string result = "";
            string displayName = "";
            using (var db = new MEHKWorkflowEntities())
            {
                var WFActivity = db.WFActivities.Where(n => n.Completed != true
                && n.FormID == FormID
                && n.FormTypeID == FormTypeID).OrderBy(n => n.ID).Skip(1).Take(1).FirstOrDefault();

                if (WFActivity != null)
                {
					string ApprovalCriteriaType = "";

					var WFActivityDetails = db.WFActivityDetails.Where(n => n.WFActivityID == WFActivity.ID);
					foreach (var v in WFActivityDetails)
					{
						result = result + v.UserName + ";";
                        displayName = displayName + v.UserDisplayName + ";";
						ApprovalCriteriaType = v.ApprovalCriteriaType;

					}
					//result = String.Join(";", WFActivityDetails.ToList());
					if (result.Length > 0)
					{
						result = result.Substring(0, result.Length - 1);
					}
                    if (displayName.Length > 0)
                    {
                        displayName = displayName.Substring(0, displayName.Length - 1);
                    }
                    details.ADName = result;
                    details.DisplayName = displayName;
					details.ApprovalCriteriaType = ApprovalCriteriaType;

				}
            }

            return details;
        }

        public static void CompleteTask(int FormID, int FormTypeID, string UserName, string ActionName)
        {
            using (var db = new MEHKWorkflowEntities())
            {
                var WFActivity = db.WFActivities.Where(n => n.Completed != true
                    && n.FormID == FormID
                    && n.FormTypeID == FormTypeID).OrderBy(n => n.ID).FirstOrDefault();



                if (WFActivity != null)
                {
                    var WFActivityDetail = db.WFActivityDetails
                        .Where(n => n.WFActivityID == WFActivity.ID &&  n.UserName.ToLower()==UserName.ToLower())
                        .OrderBy(n => n.ID).FirstOrDefault();

                    WFActivity.Completed = true;
                    WFActivity.CompletedDate = DateTime.Now;

                    WFActivityDetail.ActionBy = UserName;
                    WFActivityDetail.ActionDate = DateTime.Now;
                    WFActivityDetail.ActionName = ActionName;

                    db.Entry(WFActivity).State = System.Data.Entity.EntityState.Modified;
                    db.Entry(WFActivityDetail).State = System.Data.Entity.EntityState.Modified;
                }


                db.SaveChanges();
            }
        }
    }
}

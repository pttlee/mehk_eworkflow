﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.Workflow.Data.Enum;

namespace MEHK.Workflow.BLL.DTO
{
	public class UserInfo
	{


        public UserInfo()
        {

        }
        public UserInfo(vMember model, bool DisplayDepartmentAndDivision = false)
        {
            MemberId = model.MemberId;
            MemberName = model.MemberName;
            LoginAccount = model.LoginAccount;
            StaffId = model.StaffId;
            SAMAccount = model.SAMAccount;
            SAPAccount = model.SAPAccount;
            Phone = model.Phone;
            EmailAddress = model.EmailAddress;
            JobLevelId = model.JobLevelId;
            JobRank = model.JobRank;
            Remark = model.Remark;
            IsActive = model.IsActive;
            CreatedBy = model.CreatedBy;
            Created = model.Created;
            LastModifiedBy = model.LastModifiedBy;
            LastModifiedDate = model.LastModifiedDate;
            UnitId = model.UnitId.HasValue ? model.UnitId.Value : -1;
            _DisplayDepartmentDivision = DisplayDepartmentAndDivision;
            _AllUnitTypes = APIAccessBLL.GetAllUnitTypes();


        }
        private bool _DisplayDepartmentDivision { get; set; }
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public string LoginAccount { get; set; }
        public string StaffId { get; set; }
        public string SAMAccount { get; set; }
        public string SAPAccount { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
        public int JobLevelId { get; set; }
        public string JobRank { get; set; }
        public string Remark { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public string LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public int UnitId { get; set; }
        public List<Role> Roles { get; set; }
        public Unit Unit { get; set; }
        public List<Unit> AllUnits { get; set; }
        public string SAMAccountWithDomain { get; set; }
        private List<UnitType> _AllUnitTypes { get; set; }
        public string DisplaynameAndStaffNum
        {
            get
            {
                return $"{MemberName} ({StaffId})";
            }
        }
        public string Department
        {
            get
            {
                return _DisplayDepartmentDivision ? GetUnitNameByCode(MEHK.Workflow.Data.Enum.UnitCode.Department.ToString()) : string.Empty;
            }
        }
        public string Division
        {
            get
            {
                return _DisplayDepartmentDivision ? GetUnitNameByCode(MEHK.Workflow.Data.Enum.UnitCode.Division.ToString()) : string.Empty;
            }
        }
        public string GetUnitNameByCode(string Code)
        {
          
                var Type = _AllUnitTypes.Where(x => x.Code == Code).FirstOrDefault();
                if (Type == null)
                    return string.Empty;
                if (Unit.UnitTypeId == Type.UnitTypeId)
                    return Unit.UnitName;
                Unit _CurrentUnit = Unit;
                while (_CurrentUnit != null && _CurrentUnit.ParentUnitId.HasValue)
                {
                    int _CurrentUnitId = _CurrentUnit.ParentUnitId.Value;
                    _CurrentUnit = GetUnit(_CurrentUnitId);
                    if (_CurrentUnit != null && _CurrentUnit.UnitTypeId == Type.UnitTypeId)
                        return _CurrentUnit.UnitName;
                }
                return string.Empty;
            

        }
        public Unit GetUnit(int UnitId)
        {
            
            
            return APIAccessBLL.GetUnit(UnitId).FirstOrDefault(); ;

        }
        public string CreatedString
        {
            get
            {
                return Created.HasValue ? Created.Value.ToString("dd/MM/yyyy") : "";
            }
        }

    }



    public partial class Role
    {
        public int RoletId { get; set; }
        public string RoleName { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
    }

    public  class Unit
    {
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public int UnitTypeId { get; set; }
        public string Description { get; set; }
        public Nullable<int> ParentUnitId { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }

    public  class UnitType
    {
        public int UnitTypeId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}

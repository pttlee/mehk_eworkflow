﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.BLL.DTO
{
    using System;
    using System.Collections.Generic;

    public partial class vMember
    {
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public string LoginAccount { get; set; }
        public string StaffId { get; set; }
        public string SAMAccount { get; set; }
        public string SAPAccount { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
        public int JobLevelId { get; set; }
        public string JobRank { get; set; }
        public string Remark { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public string LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public Nullable<int> UnitId { get; set; }
        public string Roles { get; set; }
        public Nullable<int> JobLevel { get; set; }
    }
}

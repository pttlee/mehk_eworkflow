﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.Workflow.Data.DTO;
namespace MEHK.Workflow.BLL
{
    public class EmailNotifactionBLL
    {
        private static string _templatePath = @"email\EmailTemplate.html";
        private static string ReadHtmlFile(string path)
        {
            var html = System.IO.File.ReadAllText(path);
            return html;
        }

        public static EmailViewModle GetEmailContent(string ProcessCode, int? stepID, string ContentType, string WorkflowType, string WorkflowStep, string AppNum, string Applicant, string URL)
        {
            int _stepid = stepID.HasValue ? (int)stepID : -1;
            EmailViewModle modle = new EmailViewModle();

            var _email = CommonBLL.GetEmailTemplate(ProcessCode, _stepid, ContentType);
            //Subject
            string _subject = "";
            _subject = _email.Subject.Replace("#WorkflowType#", WorkflowType).Replace("#AppNum#", AppNum).Replace("#Applicant#", Applicant);

            //Body
            var _body = _email.Body;
            _body= _body.Replace("#WorkflowType#", WorkflowType).Replace("#WorkflowStep#", WorkflowStep).Replace("#AppNum#", AppNum).Replace("#Applicant#", Applicant).Replace("#URL#", URL);
            //_body.Replace("#Body#", _email.Body);

            modle.Body = _body;
            modle.Subject = _subject;

            return modle;

        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.Web.Routing;
using System.Web;
using System.Web.Mvc;
using MEHK.Workflow.Data.Model;
using K25Lib;
using MEHK.Workflow.Data.Enum;
using MEHK.Workflow.Data.DTO;
using System.Web.Helpers;
//using MEHK.Workflow.BLL.DTO;
using MEHK.Workflow.Data.Const;
using MEHK.Workflow.BLL.Culture;
using MEHK.Workflow.BLL.Workflow;
namespace MEHK.Workflow.BLL
{
  
    public class MEHKBase : Controller
    {

        //private string K2DomainName = "";
        //private string K2Server = "";
        //private string k2UserID = "";
        //private string k2Password = "";
        //private string loginDomain = "";
        //private string k2WindowsDomain = "";
        string ControllerName = "";
        protected string ProcessName = "";
        protected decimal EntertainmentAmount = 0;
        protected decimal GiftAmount = 0;
        private string _EntertainmentGiftCode = "";       
        private string RootURL="";
        public bool IsDev;
        public string WS_eFiling_URL = "";
        public string WS_Currency_URL = "";
        public string WS_PostJV_URL = "";
        public string VitalDocUserName = "";
        public string VitalDocUserPassword = "";
        private int _ProcessID = 1;
        public string EntertainmentGiftCode
        {
            get { return _EntertainmentGiftCode; }
        }
        public int ProcessID
        {
            get { return _ProcessID; }
        }
        public MEHKBase()
        {

            this.ProcessName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.GenenalProcessName);
            EntertainmentGiftLimit Entertainment = CommonBLL.GetEntertainmentAndGift("Entertainment");
            EntertainmentAmount = Entertainment != null ? (decimal)Entertainment.Text : 0;
			EntertainmentGiftLimit Gift = CommonBLL.GetEntertainmentAndGift("Gift");
			GiftAmount = Gift != null ? (decimal)Gift.Text : 0;
            _EntertainmentGiftCode = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.EntertainmentGift);
            //var AppID = 1;
            RootURL = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.WebBaseURL);
            WS_eFiling_URL= CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.WS_eFiling_URL);
            IsDev = ConfigurationManager.AppSettings["isDEV"].ToString() == "T" ? true : false;
            WS_Currency_URL = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.WS_Currency_URL);
            WS_PostJV_URL = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.WS_PostJV_URL);
            VitalDocUserName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.VitalDocUserName);
            VitalDocUserPassword = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.VitalDocUserPassword);
            //ViewData["CurrentUserID"] = GetUserInfo.MemberId;
            //ViewData["AppID"] = AppID;
        }

        protected override void Initialize(RequestContext requestContext)
        {

            base.Initialize(requestContext);
            string cultureName = GetCultureName();

            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");//"zh-CN"
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            ControllerName = requestContext.RouteData.Values["controller"].ToString();


            var url = RootURL+"/MEHK.DashBoard/Error/NoUserFoundInSystem";

            if (requestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                try
                {
                    var AppID =CommonConstcs.AppID;
                    ViewData["AppID"] = AppID;
                    ViewData["DisplayName"] = GetUserInfo.MemberName;
                    ViewData["CurrentUserID"] = GetUserInfo.MemberId;

                    if (!(GetUserInfo.IsActive.HasValue ? (bool)GetUserInfo.IsActive : false))
                    {
                        requestContext.HttpContext.Response.Redirect(url);
                    }
                }
                catch (Exception ex)
                {
                    CreateErrorLog(ex);
                    requestContext.HttpContext.Response.Redirect(url);
                }
            }
        }
        private string GetCultureName()
        {
            string cultureName = null;
            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
                cultureName = cultureCookie.Value;
            else
                cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ?
                        Request.UserLanguages[0] :  // obtain it from HTTP header AcceptLanguages
                        null;
            // Validate culture name

            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe
            return cultureName;
        }



        //public void SetCulture(string culture)
        //{
        //    // Validate input
        //    culture = CultureHelper.GetImplementedCulture(culture);
        //    // Save culture in a cookie
        //    HttpCookie cookie = Request.Cookies["_culture"];
        //    if (cookie != null)
        //        cookie.Value = culture;   // update cookie value
        //    else
        //    {
        //        cookie = new HttpCookie("_culture");
        //        cookie.Value = culture;
        //        cookie.Expires = DateTime.Now.AddYears(1);
        //    }
        //    Response.Cookies.Add(cookie);
        //}
        public UserInfoModel GetCommonUserInfo()
        {
            UserInfoModel m = new UserInfoModel();
            m.StaffID = GetUserInfo.StaffId;
            m.StaffName = GetUserInfo.MemberName;
            m.Division = GetUserInfo.Division;
            m.Department = GetUserInfo.Department;
            m.ApplicationDate = DateTime.Now;
            m.SubmissionDate = DateTime.Now;
            m.Payee = GetUserInfo.MemberId;
            return m;
        }
        protected void SetBreadcrumbs(params string[] breadcrumbs)
        {
            List<string> listBreadcrumbs = new List<string>();

            listBreadcrumbs = breadcrumbs.ToList();

            ViewData["listBreadcrumbs"] = listBreadcrumbs;
        }


        public void CreateErrorLog(Exception ex)
        {
    

            CommonBLL.CreateErrorLog(ex);
        }
        public void CreateErrorLog(Exception ex,string FunctionName)
        {
  
            CommonBLL.CreateErrorLog(ex, FunctionName);
        }

        public void CreateDebugLog(string Message, string Name = "")
        {
            CommonBLL CommonBLL = new CommonBLL();

            Log Log = new Log();
            Log.Date = DateTime.Now;
            Log.ErrorCode = "";
            Log.LogPriority = "";
            Log.LogType = "Debug";
            Log.Message = Message;
            Log.StackTrace = Name;
            Log.Remark = "";
            Log.Source = ControllerName;

            CommonBLL.CreateErrorLog(Log);
        }

        protected WFControl WFControl 
        {
           get
            {
                //K2DomainName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.K2DomainName);
                //K2Server = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.K2Server);
                //k2UserID = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.k2UserID);
                //k2Password = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.k2Password);
                //loginDomain = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.loginDomain);
                //k2WindowsDomain = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.k2WindowsDomain);

                //WFControl WFControl = new WFControl(K2DomainName, K2Server, k2UserID, k2Password, loginDomain, k2WindowsDomain);

                //WFControl WFControl = new WFControl(K2DomainName, CapexProcessName, K2Server, k2UserID, k2Password, loginDomain, k2WindowsDomain);

                return WorkflowInstance.WFControl;
            }
            
        }

        
        public string GetUserADName
        {
            get
            {
                return User.Identity.Name.Contains('\\') ? User.Identity.Name.Split('\\')[1] : User.Identity.Name;
            }
        }
        public UserInfo GetUserInfo
        {
            get
            {

                APIAccessBLL apiBLL = new APIAccessBLL();
                var _user = apiBLL.GetMember("Organization/Members/", GetUserADName);
                if (_user.Count() > 0)
                {
                    _user.FirstOrDefault().SAMAccountWithDomain = User.Identity.Name;
                    return _user.FirstOrDefault();

                }
                else
                {
                    return new UserInfo();
                }

            }
        }

    

        [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
        public sealed class ValidateHeaderAntiForgeryTokenAttribute : FilterAttribute, IAuthorizationFilter
        {
            public void OnAuthorization(AuthorizationContext filterContext)
            {
                if (filterContext == null)
                {
                    throw new ArgumentNullException("filterContext");
                }

                var httpContext = filterContext.HttpContext;
                var cookie = httpContext.Request.Cookies[AntiForgeryConfig.CookieName];
                AntiForgery.Validate(cookie != null ? cookie.Value : null, httpContext.Request.Headers["__RequestVerificationToken"]);
            }
        }
    }
}

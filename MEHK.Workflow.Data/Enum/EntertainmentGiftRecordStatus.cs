﻿namespace MEHK.Workflow.Data.Enum
{
    public enum EntertainmentGiftRecordStatusCode
    {
        Draft,
        Approving,
        Approved,
        Cancelled,
        Returned,
        Approving_PreApproval,
        Approving_Reimbursement,
        Filling_Reimbursement,
        //InProcess,
        Completed,
        Terminated,
		InProgress
	}
}

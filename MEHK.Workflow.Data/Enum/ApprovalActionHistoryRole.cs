﻿namespace MEHK.Workflow.Data.Enum
{
    public enum ApprovalActionHistoryRoles
    {
        Applicant,
        DepartmentHead,
        GeneralManager,
        GeneralPlanningAndAccountingDirector,
        ManagingDirector,
        AccountVerification,
        AccountApproval,
    }
}

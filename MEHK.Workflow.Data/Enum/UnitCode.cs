﻿
namespace MEHK.Workflow.Data.Enum
{
    public enum UnitCode
    {
        Company,
        Division,
        Department,
        DepartmentOrDivision,
        Section,
        Others,
    }

    public enum ApprovalCriteriaTypeEnum
    {
        JL,
        R
    }
}
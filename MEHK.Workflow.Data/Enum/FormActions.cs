﻿namespace MEHK.Workflow.Data.Enum
{
    public enum FormActions
    {
        Empty,
        Submit, 
        Delete,
        Cancel,
        Approve,
        Return,
        Reject,
        Resubmit,
        Close,
        Save_PreApproval,
        SaveAsDraft_PreApproval,
        ReimbursementSubmit,
        SaveAsDraft_Reimbursement,
    }
}

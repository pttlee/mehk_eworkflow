﻿
namespace MEHK.Workflow.Data.Enum
{
    public enum FlowType
    {
        PreApproval,
        Reimbursement
    }
}

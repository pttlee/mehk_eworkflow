﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.Enum
{
    public  enum EntertainmentGiftBudgetTypes
    {
        Entertainment,
        Gift
    }
}

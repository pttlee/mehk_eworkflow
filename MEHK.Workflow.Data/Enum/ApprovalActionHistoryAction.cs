﻿namespace MEHK.Workflow.Data.Enum
{
    public enum ApprovalActionHistoryActions
    {
        Empty,
        Submit,
        Cancel,
        Approve,
        Reject,
        Return,
        ReimbursementSubmit,
		Resubmit
    }
}

﻿namespace MEHK.Workflow.Data.Enum
{
    public enum WorkflowActions
    {
        Approve,
        Return,
        Resubmit,
        Withdraw,
        ReimbursementSubmit
    }
}

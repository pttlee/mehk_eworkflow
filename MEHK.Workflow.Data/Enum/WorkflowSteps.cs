﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.Enum
{
    public enum WorkflowSteps
    {
        Empty,
        PreApproval_Requester,
        PreApproval_Supervisor1,
        PreApproval_Supervisor2,
        PreApproval_AM,
        PreApproval_DM,
        PreApproval_SM,
        PreApproval_DGM,
        PreApproval_GM,
        PreApproval_GPAD,
        PreApproval_MD,
        PreApproval_ACRV,
        PreApproval_ACAP,
        PreApproval_HRRV,
        PreApproval_HRAP,
        Reimbursement_Requester,
        Reimbursement_Supervisor1,
        Reimbursement_Supervisor2,
        Reimbursement_AM,
        Reimbursement_DM,
        Reimbursement_SM,
        Reimbursement_DGM,
        Reimbursement_GM,
        Reimbursement_GPAD,
        Reimbursement_MD,
        Reimbursement_ACRV1,
        Reimbursement_ACRV2,
        Reimbursement_ACAP,
        Reimbursement_HRRV1,
        Reimbursement_HRRV2,
        Reimbursement_HRAP,
    }
}

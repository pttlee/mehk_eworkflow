﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.Const
{
    public static class RightConst
    {
        #region System
        public const string ACCESS_HR_ADMIN_PAGE_RIGHT = "ACCESS_HR_ADMIN_PAGE_RIGHT";
        public const string ACCESS_ACCOUNT_ADMIN_PAGE_RIGHT = "ACCESS_ACCOUNT_ADMIN_PAGE_RIGHT";
        public const string ACCESS_IS_ADMIN_PAGE_RIGHT = "ACCESS_IS_ADMIN_PAGE_RIGHT";
        #endregion
        #region AC04
        public const string CREATE_ENTERTAINMENT_GIT_RIGHT = "CREATE_ENTERTAINMENT_GIT_RIGHT";
        public const string VIEWOWN_ENTERTAINMENT_GIT_RIGHT = "VIEWOWN_ENTERTAINMENT_GIT_RIGHT";
        public const string VIEWDEPARTMENT_ENTERTAINMENT_GIT_RIGHT = "VIEWDEPARTMENT_ENTERTAINMENT_GIT_RIGHT";
        public const string VIEWALL_ENTERTAINMENT_GIT_RIGHT = "VIEWALL_ENTERTAINMENT_GIT_RIGHT";
        #endregion
    }
}

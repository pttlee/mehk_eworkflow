﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
    public class MyDraftsModel
    {
        public int ID { get; set; }
        public string FormType { get; set; }

        public string FormTypeDisplayName { get; set; }

        public string FormStatus { get; set; }
		public string CreatedBy { get; set; }
		public DateTime? CreatedDate { get; set; }

	}
}

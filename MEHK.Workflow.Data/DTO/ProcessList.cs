﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
	public class ProcessList
	{
		public int ProcessId { get; set; }
		public string Code { get; set; }
		public string ProcessName { get; set; }
		public string K2ProcessName { get; set; }
	}
}

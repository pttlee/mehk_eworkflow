﻿using System;

namespace MEHK.Workflow.Data.DTO
{
    public class WorkflowLogViewModel
    {

        public string Role { get; set; }

        public string Department { get; set; }

        public string Position { get; set; }

        public string Name { get; set; }

        public int UserID { get; set; }

        public string Action { get; set; }

        public int ActionID { get; set; }

        public DateTime ActionDate { get; set; }

        public string Comment { get; set; }


    }
}

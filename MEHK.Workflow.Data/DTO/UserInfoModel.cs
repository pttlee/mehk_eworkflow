﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
  public  class UserInfoModel
    {
		[Display(Name = "Form No")]
		public string FormNo { get; set; }
		[Display(Name = "Submission Date")]
		public DateTime? SubmissionDate { get; set; }
		[Display(Name = "Staff ID")]
		public string StaffID { get; set; }

		[Display(Name = "Staff Name")]
		public string StaffName { get; set; }

    
        public string Division { get; set; }

    
        public string Department { get; set; }

        public int? Payee { get; set; }
        public string PayeeName { get; set; }
		[Display(Name = "Form Status")]
		public string FormStatus { get; set; }
		[Display(Name = "Application Date")]
		public DateTime? ApplicationDate { get; set; }

        public string ViewMode { get; set; }

    }
}

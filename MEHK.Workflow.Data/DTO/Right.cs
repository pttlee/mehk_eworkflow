﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
	public class Right
	{
		public int ID { get; set; }
		public string RightName { get; set; }
		public string Description { get; set; }
		public int AppID { get; set; }
		public string AppName { get; set; }
	}
}

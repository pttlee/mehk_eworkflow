﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
    public class ApprovalMartixViewModel
    {
        public int ApprovalMartixId { get; set; }
        public string ApprovalMartixName { get; set; }
        public int ProcessId { get; set; }
        public int? UnitId { get; set; }
        public int? RoleId { get; set; }
        public int? CopyDetailFrom { get; set; }
        public string ApprovalMartixOptionCode { get; set; }
        //public decimal FromAmount { get; set; }
        //public decimal ToAmount { get; set; }
        public List<ApprovalMartixDetailViewModel> Details;
    }
    public class ApprovalMartixDetailViewModel
    {
        // ApprovalMartixDetail
        public int ApprovalMartixDetailId { get; set; }
		public int ApprovalMartixId { get; set; }
		public int ApprovalLevelId { get; set; }
		public string ApprovalCriteriaType { get; set; }
		public string ApprovalCriteria { get; set; }
		//[ApprovalLevel]
		public int Seq { get; set; }
		public string ApprovalLevelCode { get; set; }
		public string ApprovalLevelName { get; set; }
		public string ApprovalLevelDisplayName;
        public List<string> Approvers { get; set; }
		public List<string> ADApprovers;
        public string  ApproversFulList;
        public List<ApproversDetails> ApproverDetails;
    }

    public class ApproversDetails
    {

        public string ADName { get; set; }
        public string DisplayName { get; set; }
        public string ApprovalCriteriaType { get; set; }
    }
}

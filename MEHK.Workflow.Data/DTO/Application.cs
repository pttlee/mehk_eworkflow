﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
	public class Application
	{
		public int ID { get; set; }
		public string AppName { get; set; }
		public string AppCode { get; set; }
		public string Description { get; set; }
	}
}

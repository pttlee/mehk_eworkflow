﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
	public class Group
	{
		public int ID { get; set; }
		public string GroupName { get; set; }
		public string Description { get; set; }
	}
}

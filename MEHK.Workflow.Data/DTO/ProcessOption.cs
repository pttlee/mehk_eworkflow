﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
	public class ProcessOption
    {
		public int ProcessID { get; set; }
		public string ApprovalMartixOptionCode { get; set; }

        public string ApprovalMartixOptionName { get; set; }

    }
}

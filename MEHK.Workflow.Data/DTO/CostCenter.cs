﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
	public class CostCenter
	{
		public int CostCenterId { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public string PIC { get; set; }
	}
}

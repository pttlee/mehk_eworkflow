﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
	public class ApprovalLevel
	{
		public int ApprovalLevelId { get; set; }
		public int Seq { get; set; }
		public string ApprovalLevelCode { get; set; }
		public string ApprovalLevelName { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
    public class AlternativePayeeViewModel
    {
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public string bExist { get; set; }
        public int ID { get; set; }
    }
}

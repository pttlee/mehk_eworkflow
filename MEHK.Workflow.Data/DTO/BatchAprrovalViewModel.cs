﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
   public class BatchAprrovalViewModel
    {
        public string SNNum{get;set;}
        public string FormNum { get; set; }

        public int ID { get; set; }
    }
}

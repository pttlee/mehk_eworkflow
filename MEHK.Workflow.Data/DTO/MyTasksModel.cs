﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
    public class MyTasksModel
    {
        public int ID { get; set; }

        public int ProcInstID { get; set; }
        public int? StatusGroupID { get; set; }
        public string FormType { get; set; }

        public string FormTypeDisplayName { get; set; }

        public string FormStatus { get; set; }

        public string Payee { get; set; }
        public string Step { get; set; }

        public string FormStatusDisplayName { get; set; }

        public DateTime SubmissionDate { get; set; }

        public string FormNo { get; set; }
        public string CreatedBy { get; set; }

		//SubmissionDate = task.SubmissionDate,
		//                      FormNo = task.FormNo,s
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
	public class SiteMenu
	{
		public int ID { get; set; }
		public string PageName { get; set; }
		public string PageDisplayName { get; set; }
		public string PageNameDisplayName_CN { get; set; }
		public string URL { get; set; }
		public int AppID { get; set; }
		public string AppName { get; set; }
		public int? ParentID { get; set; }
		public string ParentName { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MEHK.Workflow.Data.DTO
{
	public class UserInfo
	{
	
		
        private bool _DisplayDepartmentDivision { get; set; }
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public string LoginAccount { get; set; }
        public string StaffId { get; set; }
        public string SAMAccount { get; set; }
        public string SAPAccount { get; set; }
        public string Post { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
        public int JobLevelId { get; set; }
		public int JobLevel { get; set; }
		public string JobRank { get; set; }
        public string Remark { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public string LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public int UnitId { get; set; }
        public List<Role> Roles { get; set; }
        public Unit Unit { get; set; }
        public List<Unit> AllUnits { get; set; }
		public List<Unit> BelowUnits { get; set; }

        private List<UnitType> _AllUnitTypes { get; set; }
        public string DisplaynameAndStaffNum
		{
			get
			{
				return $"{MemberName} ({StaffId})";
			}
		}
        public string Department
        {
            get;set;
        }
        public string Division
        {
            get;set;
        }


        public string SAMAccountWithDomain { get; set; }
        public string CreatedString { get; set; }
    }

    public partial class Role
    {
        public int RoletId { get; set; }
        public string RoleName { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
    }

    public partial class JobLeveln
    {
        public int JobLevelId { get; set; }
        public string RankingSequence { get; set; }
        public string Title { get; set; }
        public string JobLevel { get; set; }
        public string IsDeleted { get; set; }
    }

    public  class Unit
    {
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public int UnitTypeId { get; set; }
        public string Description { get; set; }
        public Nullable<int> ParentUnitId { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }

    public  class UnitType
    {
        public int UnitTypeId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}

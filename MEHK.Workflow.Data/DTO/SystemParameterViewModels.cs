﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Data.DTO
{
	public class SystemParameterViewModels
    {
        public string AppKey { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string GroupName { get; set; }
    }
}
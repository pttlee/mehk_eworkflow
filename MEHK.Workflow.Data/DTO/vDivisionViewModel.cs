﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
	public class vDivisionViewModel
    {
        public int UnitId { get; set; }
        public string UnitName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Data.DTO
{
	public class DelegationViewModel
    {
		public int Id { get; set; }
		public int UserID { get; set; }
        public string UserName { get; set; }
        public string UserADName { get; set; }
        public int AssignUserID { get; set; }
        public string AssignUserName { get; set; }
        public string AssignUserADName { get; set; }
        public string ProcessCode { get; set; }
        public string ProcessName { get; set; }
        public DateTime? FormDate { get; set; }
        public DateTime? ToDate { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.Data.DTO
{
	public class ApprovalCriteriaType
	{
		public string ApprovalCriteriaType1 { get; set; }
		public string Description { get; set; }
		public string CompareLogic { get; set; }
	}
}

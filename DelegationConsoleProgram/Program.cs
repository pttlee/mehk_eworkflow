﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using K25Lib;
using MEHK.Workflow.BLL;

using System.Configuration;
using MEHK.Workflow.Data.DTO;
using MEHK.Workflow.Data.Model;
using MEHK.Workflow.Data.Enum;

namespace DelegationConsoleProgram
{
    class Program
    {

		
		static void Main(string[] args)
        {
            Console.WriteLine("===================start");


            try
            {
                CommonBLL bll = new CommonBLL();
                List<DelegationViewModel> Delegation = bll.GetDelegation();

                // string ProcessName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.GenenalProcessName);
                string ProcessList = ConfigurationManager.AppSettings["ProcessList"];
                if (string.IsNullOrEmpty(ProcessList))
                {
                    Console.WriteLine("ProcessName is null");
                    return;
                }
                List<string> list = ProcessList.Split(';').ToList();
                list = list.Where(e => e != "").ToList();

                if (Delegation != null && Delegation.Count > 0)
                {
                    for (int i = 0; i < Delegation.Count(); i++)
                    {
                        if (DateTime.Now >= Delegation[i].FormDate && DateTime.Now <= Delegation[i].ToDate)
                        {
                            Console.WriteLine("UserADName :" + Delegation[i].UserADName);
                            Console.WriteLine("AssignUserADName :" + Delegation[i].AssignUserADName);
                            Console.WriteLine(list[0]);
                            if (!string.IsNullOrEmpty(Delegation[i].UserADName) && !string.IsNullOrEmpty(Delegation[i].AssignUserADName))
                            {
                                Console.WriteLine(WFControl.Delegation(Delegation[i].UserADName, Delegation[i].AssignUserADName, list));
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
				CommonBLL CommonBLL = new CommonBLL();

				Log Log = new Log();
				Log.Date = DateTime.Now;
				Log.ErrorCode = "";
				Log.LogPriority = "";
				Log.LogType = "Error";
				Log.Message = ex.Message;
				Log.StackTrace = ex.ToString();
				Log.Remark = "";
				Log.Source = "DelegationConsoleProgram";
				CommonBLL.CreateErrorLog(Log);

				Console.WriteLine("error");
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine("===================end");
            Console.ReadLine();
            //  K2ServerControl.DelegationUser("", "", "")

        }


        private static WFControl WFControl
        {
            get
            {

                #region old
                //string K2DomainName = "PTTK2FIVE";
                //string K2Server = "PTTK2FIVE";
                //string k2UserID = "pttlab\\k25admin";
                //string k2Password = "P@ssw0rd1234";
                //string loginDomain = "pttlab";
                //string k2WindowsDomain = "PTTK2FIVE";
                #endregion

                string K2DomainName = ConfigurationManager.AppSettings["K2DomainName"];
                string K2Server = ConfigurationManager.AppSettings["K2Server"];
                string k2UserID = ConfigurationManager.AppSettings["k2UserID"];
                string k2Password = ConfigurationManager.AppSettings["k2Password"];
                string loginDomain = ConfigurationManager.AppSettings["loginDomain"];
                string k2WindowsDomain = ConfigurationManager.AppSettings["k2WindowsDomain"];

                WFControl WFControl = new WFControl(K2DomainName, K2Server, k2UserID, k2Password, loginDomain, k2WindowsDomain);

                //WFControl WFControl = new WFControl(K2DomainName, CapexProcessName, K2Server, k2UserID, k2Password, loginDomain, k2WindowsDomain);

                return WFControl;
            }

        }
    }
}

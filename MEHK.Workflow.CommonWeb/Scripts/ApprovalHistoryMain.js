﻿function BindAprrovalHistoryGrid(AprrovalHistorySetting) {
	var FormID = AprrovalHistorySetting.FormID;
	var FormTypeID = AprrovalHistorySetting.FormTypeID;
	var DateFormat = AprrovalHistorySetting.DateFormat;
	$(AprrovalHistorySetting.UploadedFilesTable).kendoGrid({
		dataSource: {
			transport: {
				read: {
					url: "../../MEHK.dashboard/Home/GetWorkflowLog",
					dataType: "json",
					data: function () { return { FormID: FormID, FormTypeID: FormTypeID } },
					cache: false
				}
			},
			schema: {
				data: function (d) {
					console.log(d);
					return d.Data;
				},
				model: {
					fields: {
						Name: { type: "string" },
						Position: { type: "string" },
						Action: { type: "string" },
						ActionDate: { type: "date" },
						Comment: { type: "string" }
					}
				}
			},

		},
		scrollable: false,
		columns: [
				{
					field: "Name",
					title: "Name"
				}, {
					field: "Position",
					title: "Position",
				}, {
					field: "Action",
					title: "Action",
				}, {
					field: "ActionDate",
					title: "ActionDate",
					format: "{0:" + DateFormat + "}",
				}, {
					field: "Comment",
					title: "Comment",
				}
		],
		dataBound: function (e) {


		}
	});
}
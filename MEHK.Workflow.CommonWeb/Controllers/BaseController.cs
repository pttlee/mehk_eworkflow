﻿using MEHK.Workflow.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MEHK.Workflow.Data.Model;
using MEHK.Workflow.Data.Enum;
using MEHK.Workflow.Data.DTO;
using System.Web.Helpers;
using K25Lib;
using MEHK.Workflow.BLL.Workflow;
using MEHK.Workflow.EntertainmentGift.ViewModels;
using System.Transactions;
using MEHK.Workflow.SAPService;
using MEHK.Workflow.Data.Const;
//using MEHK.Workflow.BLL.DTO;

namespace MEHK.Workflow.CommonWeb.Controllers
{
    public class BaseController : MEHKBase
    {
        private string K2DomainName = "";
        private string K2Server = "";
        private string k2UserID = "";
        private string k2Password = "";
        private string loginDomain = "";
        private string k2WindowsDomain = "";
        string ControllerName = "";
        protected string ProcessName = "";
		protected MEHKWorkflowEntities MEHKWorkflowEntities = new MEHKWorkflowEntities();
		protected override void Initialize(RequestContext requestContext)
        {

            base.Initialize(requestContext);

            ControllerName = requestContext.RouteData.Values["controller"].ToString();


            var url = "/Error/NoUserFoundInSystem";

            if (requestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                try
                {
					var AppID = 1;
					ViewData["DisplayName"] = GetUserInfo.MemberName;
                    ViewData["CurrentUserID"] = GetUserInfo.MemberId;
					ViewData["AppID"] = AppID;
					if (!(GetUserInfo.IsActive.HasValue ? (bool)GetUserInfo.IsActive : false))
                    {
                        requestContext.HttpContext.Response.Redirect(url);
                    }
                }
                catch (Exception ex)
                {
                    CreateErrorLog(ex);
                    requestContext.HttpContext.Response.Redirect(url);
                }
            }
        }
        protected void SetBreadcrumbs(params string[] breadcrumbs)
        {
            List<string> listBreadcrumbs = new List<string>();

            listBreadcrumbs = breadcrumbs.ToList();

            ViewData["listBreadcrumbs"] = listBreadcrumbs;
        }

        public BaseController()
        {
            this.ProcessName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.GenenalProcessName);

            //var AppID =1;

            //ViewData["CurrentUserID"] = GetUserInfo.MemberId;
            //ViewData["AppID"] = AppID;


        }
 
        public string GetAPIPath
        {
            get
            {
                return CommonBLL.GetSystemvalue("ApiPath");
            }
        }
        public string GetUserADName
        {
            get
            {
                return User.Identity.Name.Contains('\\') ? User.Identity.Name.Split('\\')[1] : User.Identity.Name;
            }
        }
        public UserInfo GetUserInfo
        {
            get
            {

                APIAccessBLL apiBLL = new APIAccessBLL();
                var _user = apiBLL.GetMember("Organization/Members/", GetUserADName);
                if (_user.Count() > 0)
                {
                    _user.FirstOrDefault().SAMAccountWithDomain = User.Identity.Name;
                    return _user.FirstOrDefault();

                }
                else
                {
                    return new UserInfo();
                }

            }
        }

        protected WFControl WFControl
        {
            get
            {
                //K2DomainName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.K2DomainName);
                //K2Server = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.K2Server);
                //k2UserID = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.k2UserID);
                //k2Password = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.k2Password);
                //loginDomain = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.loginDomain);
                //k2WindowsDomain = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.k2WindowsDomain);

                //WFControl WFControl = new WFControl(K2DomainName,  K2Server, k2UserID, k2Password, loginDomain, k2WindowsDomain);

                //WFControl WFControl = new WFControl(K2DomainName, ProcessName, K2Server, k2UserID, k2Password, loginDomain, k2WindowsDomain);

                //WFControl WFControl = new WFControl(K2DomainName, CapexProcessName, K2Server, k2UserID, k2Password, loginDomain, k2WindowsDomain);

                return  WorkflowInstance.WFControl;
            }
        }
        public string CommonAPIBase
        {
            get
            {

                return System.Configuration.ConfigurationManager.AppSettings["CommonApiUrl"];
            }
        }
        public string OrganizationAPIBase
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["OrganizationApiUrl"];
            }
        }
		public int GetFormTypeID()
		{
			return CommonBLL.GetFormTypeID(FormTypes.EntertainmentGift);
		}

		public void MapDBRecordToEntertainmentGiftPreApprovalFormViewModel(EntertainmentGiftRecord model, EntertainmentGiftFormViewModel viewModel)
		{

			//viewModel.Purpose = model.Purpose;
			//viewModel.EventDateOn = model.EventOn;
			//viewModel.EventDateTo = model.EventTo;
			//viewModel.LateApplicationReason = model.LateReason;
			//viewModel.NatureOfGift = model.Place_NatureOfGift;
			//viewModel.Guest = model.Guests;
			//viewModel.GuestTotal = model.GuestTotal;
			//viewModel.Company = model.Company;
			//viewModel.Attendant = model.Attendants;
			//viewModel.AttendantTotal = model.AttendantTotal;
			viewModel.BudgetType = model.BudgetType;

			if (viewModel.BudgetType == EntertainmentGiftBudgetTypes.Entertainment.ToString())
			{
				viewModel.BudgetAmount = model.EntertainBudget;
			}
			else if (viewModel.BudgetType == EntertainmentGiftBudgetTypes.Gift.ToString())
			{
				viewModel.BudgetAmount = model.GiftBudget;
			}
			UserInfoModel UserInfo = new UserInfoModel();

			UserInfo.ViewMode = "Edit";
			//viewModel.FormStatus = model.EntertainmentGiftRecordStatu.Description;
			UserInfo = new UserInfoModel();
			UserInfo.FormNo = model.FormNo;
			UserInfo.Division = model.Division;
			UserInfo.Department = model.Department;
			UserInfo.StaffID = model.StaffID;
			UserInfo.SubmissionDate = model.SubmissionDate;
			UserInfo.StaffName = model.StaffName;
			UserInfo.FormStatus = EntertainmentGiftBLL.GetStatus(model.FormStatusID.Value).Description;
			UserInfo.ApplicationDate = model.ApplicationDate;
			viewModel.ApplicationDate = model.ApplicationDate;
			int PayeeID = model.Payee != null ? (int)model.Payee : 0;
			//var GetMember = new EntertainmentGiftBLL().GetMemberList(PayeeID).FirstOrDefault();
			int MemberID = model.MemberID != null && model.MemberID != "" ? int.Parse(model.MemberID) : 0;
			var GetMember = new EntertainmentGiftBLL().GetMemberList(MemberID).Where(e => e.MemberId == PayeeID).FirstOrDefault();
			if (GetMember != null)
			{
				UserInfo.PayeeName = GetMember.MemberName;
				UserInfo.StaffID = GetMember.StaffId;

			}
			else
			{
				UserInfo.PayeeName = model.StaffName;
				UserInfo.StaffID = model.StaffID;
			}

			viewModel.UserInfo = UserInfo;
			//viewModel.SubmissionDate = model.SubmissionDate;

			viewModel.PreApproval = new EntertainmentPreApprovalViewModel();
			viewModel.PaymentReimbursement = new EntertainmentPaymentReimbursementViewModel();

			viewModel.PreApproval.Purpose = model.Purpose;
			viewModel.PreApproval.EventDateOn = model.EventOn;
			viewModel.PreApproval.EventDateTo = model.EventTo;
			viewModel.PreApproval.LateApplicationReason = model.LateReason;
			viewModel.PreApproval.NatureOfGift = model.Place_NatureOfGift;
			viewModel.PreApproval.Guest = model.Guests;
			viewModel.PreApproval.GuestTotal = model.GuestTotal;
			viewModel.PreApproval.Company = model.Company;
			viewModel.PreApproval.Attendant = model.Attendants;
			viewModel.PreApproval.AttendantTotal = model.AttendantTotal;
			viewModel.IsCompleted = model.IsCompleted == null ? false : (bool)model.IsCompleted;

			viewModel.Payee = model.Payee;


			if (model.EntertainmentGiftReimburseRecord != null)
			{
				viewModel.PaymentReimbursement.PostingInstruction = model.EntertainmentGiftReimburseRecord.PostingInstruction;
				viewModel.PaymentReimbursement.Purpose = model.EntertainmentGiftReimburseRecord.Purpose;
				viewModel.PaymentReimbursement.EventDateOn = model.EntertainmentGiftReimburseRecord.EventOn;
				viewModel.PaymentReimbursement.EventDateTo = model.EntertainmentGiftReimburseRecord.EventTo;
				viewModel.PaymentReimbursement.LateApplicationReason = model.LateReason;
				viewModel.PaymentReimbursement.NatureOfGift = model.EntertainmentGiftReimburseRecord.Place_NatureOfGift;
				viewModel.PaymentReimbursement.Guest = model.EntertainmentGiftReimburseRecord.Guests;
				viewModel.PaymentReimbursement.GuestTotal = model.EntertainmentGiftReimburseRecord.GuestTotal;
				viewModel.PaymentReimbursement.Company = model.EntertainmentGiftReimburseRecord.Company;
				viewModel.PaymentReimbursement.Attendant = model.EntertainmentGiftReimburseRecord.Attendants;
				viewModel.PaymentReimbursement.AttendantTotal = model.EntertainmentGiftReimburseRecord.AttendantTotal;

				if (model.EntertainmentGiftReimburseRecord.Currency != null)
				{
					int CurrencyID = int.Parse(model.EntertainmentGiftReimburseRecord.Currency);
					var CurrencyModel = new EntertainmentGiftBLL().GetCurrencyList().Where(e => e.CurrencyID == CurrencyID).FirstOrDefault();
					if (CurrencyModel != null)
					{
						viewModel.PaymentReimbursement.CurrencyName = CurrencyModel.CurrencyName;
					}
				}

				viewModel.PaymentReimbursement.Currency = model.EntertainmentGiftReimburseRecord.Currency;

				viewModel.PaymentReimbursement.Amount = model.EntertainmentGiftReimburseRecord.Amount;
				viewModel.PaymentReimbursement.ExchangeRate = model.EntertainmentGiftReimburseRecord.ExchangeRate;
				viewModel.PaymentReimbursement.AmountHKEntertain = model.EntertainmentGiftReimburseRecord.AmountHKEntertain;
				viewModel.PaymentReimbursement.AmountHKGift = model.EntertainmentGiftReimburseRecord.AmountHKGift;
				viewModel.PaymentReimbursement.AmountExceedReason = model.EntertainmentGiftReimburseRecord.AmountExceedReason;
				viewModel.PaymentReimbursement.PaymentMethod = model.EntertainmentGiftReimburseRecord.PaymentMethod;
				viewModel.PaymentReimbursement.PayTo = model.EntertainmentGiftReimburseRecord.PayTo;
				viewModel.PaymentReimbursement.NoOfDocumentOrReceiptAttached = model.EntertainmentGiftReimburseRecord.NoOfAttachment;
				viewModel.PaymentReimbursement.SAPDocNo = model.EntertainmentGiftReimburseRecord.SAPDocNo;
				viewModel.PaymentReimbursement.SAPRemark = model.EntertainmentGiftReimburseRecord.SAPRemark;


				if (model.EntertainmentGiftReimburseRecord.CostCenter != "" && model.EntertainmentGiftReimburseRecord.CostCenter != null)
				{
					string CostCenterName = "";
					string CostCenterCode = "";
					string[] sArray = model.EntertainmentGiftReimburseRecord.CostCenter.Split(',');
					List<CostCentersViewModel> CostCenterList = new List<CostCentersViewModel>(); /*GetCostCentersModelList();*/
					foreach (var item in sArray)
					{
						int CostCenterId = int.Parse(item);
						//GetMemberListViewModel userModel = GetCreatedByModel(model.CreatedBy);
						

						//int CreatedByID = int.Parse(model.CreatedBy);


						CostCentersViewModel CostCentersModel = GetCostCentersModelList(PayeeID).Where(e => e.CostCenterId == CostCenterId).FirstOrDefault();
						if (CostCentersModel != null)
						{
							CostCenterName += CostCentersModel.Description + ",";
							CostCenterCode += CostCentersModel.Code + ",";
							CostCenterList.Add(CostCentersModel);
						}
					}
					if (CostCenterName != "")
					{
						CostCenterName = CostCenterName.Substring(0, (CostCenterName.Length - 1));
					}
					if (CostCenterCode != "")
					{
						CostCenterCode = CostCenterCode.Substring(0, (CostCenterCode.Length - 1));
					}
					viewModel.PaymentReimbursement.CostCenterName = CostCenterName;
					viewModel.PaymentReimbursement.CostCenterCode = CostCenterCode;
					viewModel.PaymentReimbursement.CostCenterList = CostCenterList;
				}
			}


		}
		public GetMemberListViewModel GetCreatedByModel(string CreatedByAD)
		{
			string CreatedBy = CreatedByAD.Contains('\\') ? CreatedByAD.Split('\\')[1] : CreatedByAD;
			//GetMemberListViewModel userModel = new EntertainmentGiftBLL().GetMemberList(0)
			//	.Where(e => e.MemberName == CreatedBy)
			//	.Select(e => new GetMemberListViewModel
			//	{
			//		MemberId = e.MemberId != null ? (int)e.MemberId : 0,
			//		MemberName = e.MemberName
			//	}).FirstOrDefault();
			APIAccessBLL apiBLL = new APIAccessBLL();
			var resultList = apiBLL.GetMember().Where(e => e.SAMAccount.ToLower() == CreatedBy.ToLower())
				.Select(e => new GetMemberListViewModel
				{
					MemberId = e.MemberId,
					MemberName = e.MemberName
				}).FirstOrDefault();
			return resultList;
		}
		public List<CostCentersViewModel> GetCostCentersModelList(int MemberId)
		{
			List<CostCentersViewModel> list = new List<CostCentersViewModel>();

			APIAccessBLL apiBLL = new APIAccessBLL();
			var CostCenters = apiBLL.GetCostCenters("Organization/CostCenters/", MemberId);
			if (CostCenters.Count() > 0)
			{
				list = CostCenters.Where(e => e.IsDeleted == false).Select(e => new CostCentersViewModel
				{
					Description = e.Description,
					CostCenterId = e.CostCenterId,
					Code = e.Code
				}).ToList();
			}
			return list;
		}
		private ApprovalMartixViewModel GetApprovalMatrixFromAPI(EntertainmentGiftFormViewModel viewModel, FlowType ftype, string Memberid)
		{
			ApprovalMartixViewModel approverModel = new ApprovalMartixViewModel();
			string Option = "";
			if (ftype == FlowType.PreApproval)
			{
				if (viewModel.BudgetType == ClaimTypes.Entertainment.ToString())
				{
					if (viewModel.BudgetAmount >= EntertainmentAmount)
					{
						Option = "O";
					}
					else
					{
						Option = "B";
					}
				}
				else
				{
					if (viewModel.BudgetAmount >= GiftAmount)
					{
						Option = "O";
					}
					else
					{
						Option = "B";
					}
				}
			}
			else
			{
				Option = "R";
			}
			APIAccessBLL apiBLL = new APIAccessBLL();
			approverModel = apiBLL.GetApprovers("Organization/ApprovalMartixApprovers/", Memberid.ToString(), EntertainmentGiftCode, Option);
			return approverModel;

		}
		//public ActionResult checkboxWorkflowAction(List<string> list,string Type)
		//{
		//	return null;
		//	try
		//	{
		//		if (Type == "EntertainmentGift")
		//		{
		//			foreach (var item in list)
		//			{
		//				List<string> datalist = new List<string>(item.Split('-'));
		//				int ID = int.Parse(datalist[0]);
		//				string WFAction = datalist[1];

		//				var EntertainmentGiftBLL = new EntertainmentGiftBLL();
		//				var CommonBLL = new CommonBLL();
		//				ApprovalActionHistoryActions ApprovalActionHistoryAction = ApprovalActionHistoryActions.Empty;
		//				WorkflowActions WorkflowAction;
		//				bool Success = true;
		//				string Message = "Submitted";
		//				string Text = "";
		//				//string ProcessCode = "AC04";
		//				string ProcessCode = EntertainmentGiftCode;
		//				int StepID = -1;
		//				string ActionType = "PendingCase";
		//				string step = "";
		//				string NextWFAction = "";
		//				string FormAction = "Approve";
		//				string pdfString = "";
		//				Dictionary<string, object> _Datafields = new Dictionary<string, object>();
		//				Enum.TryParse(FormAction, out ApprovalActionHistoryAction);
		//				Enum.TryParse(WFAction, out WorkflowAction);

		//				APIAccessBLL apiBLL = new APIAccessBLL();
		//				var record = EntertainmentGiftBLL.Get(ID);
		//				var model = new EntertainmentGiftFormViewModel();
		//				MapDBRecordToEntertainmentGiftPreApprovalFormViewModel(record, model);

		//				if (model.IsCompleted || model.ActionList.Where(e => e == "ReimbursementSubmit").FirstOrDefault() != null)
		//				{
		//					step = "Reimbursement";
		//				}
		//				else
		//				{
		//					step = "PreApproval";
		//				}

							
		//				var CurrentStep = GeneralWorkflowBLL.GetCurrentStep(record.ID, GetFormTypeID());
		//				var NextStep = GeneralWorkflowBLL.GetNextStep(record.ID, GetFormTypeID());
		//				var NextStepID = GeneralWorkflowBLL.GetNextStepID(record.ID, GetFormTypeID());
		//				var LastStep = GeneralWorkflowBLL.CheckLastStep(record.ID, GetFormTypeID());
		//				//StepID = NextStepID.HasValue ? (int)NextStepID : -1;
		//				GeneralWorkflowBLL.ResetWFActivityDetailFromStepName(record.ID, GetFormTypeID(), CurrentStep.ToString());
		//				ApprovalMartixViewModel approverModelReimbursement = new ApprovalMartixViewModel();
		//				ApprovalMartixViewModel approverModelPreApproval = new ApprovalMartixViewModel();
		//				approverModelReimbursement = GetApprovalMatrixFromAPI(model, FlowType.Reimbursement, record.MemberID);
		//				approverModelPreApproval = GetApprovalMatrixFromAPI(model, FlowType.PreApproval, record.MemberID);
		//				string NextApprover = GeneralWorkflowBLL.GetNextStepUser(record.ID, GetFormTypeID()).ADName;
		//				string NextApproverdisplayName = "";
		//				if (NextApprover != null)
		//				{
		//					NextApprover = NextApprover.Split(';').FirstOrDefault();
		//					var NextApprovermodel = apiBLL.GetMember(@"Organization/Members/", NextApprover.Split('\\')[1]).FirstOrDefault();


		//					if (NextApprovermodel != null)
		//					{
		//						NextApproverdisplayName = NextApprovermodel.MemberName;
		//					}
		//				}
		//				using (TransactionScope scope = new TransactionScope())
		//				{
		//					GeneralWorkflowBLL.CompleteTask(model.ID, GetFormTypeID(), User.Identity.Name, WFAction);
		//					NextWFAction = WorkflowActions.Approve.ToString();
		//					if (LastStep)
		//					{
		//						ActionType = "ApprovedCase";
		//						_Datafields[GeneralWFDataFields.HasNextStep.ToString()] = "False";
		//						_Datafields[GeneralWFDataFields.StatusCode.ToString()] = EntertainmentGiftRecordStatusCode.Completed.ToString();

		//						NextWFAction = "Completed";
		//					}
		//					else
		//					{
		//						//if (NextStep == WorkflowSteps.Reimbursement_Requester)
		//						//{
		//						//    record.FormStatusID = EntertainmentGiftBLL.GetStatusID(EntertainmentGiftRecordStatusCode.Filling_Reimbursement);

		//						//    EntertainmentGiftBLL.SaveRecord(record);
		//						//}
		//						_Datafields[GeneralWFDataFields.HasNextStep.ToString()] = "True";
		//						_Datafields[GeneralWFDataFields.NextApprover.ToString()] = NextApprover;
		//						/////
		//						NextApprover = NextApprover.Split(';').FirstOrDefault();
		//						var NextApprovermodel = apiBLL.GetMember(@"Organization/Members/", NextApprover.Split('\\')[1]).FirstOrDefault();
		//						if (NextApprovermodel != null)
		//						{
		//							NextApproverdisplayName = NextApprovermodel.MemberName;
		//						}

		//					}
		//					if (CurrentStep == WorkflowSteps.Reimbursement_ACAP)
		//					{
		//						pdfString = pdfString.Replace("data:application/pdf;base64,", "");
		//						Dictionary<string, byte[]> dictionaryByte = EntertainmentGiftBLL.GetFilesList(ProcessCode, model.ID);
		//						string filePathList = GenPDF.MergeMemoStream(pdfString, record.FormNo, dictionaryByte);
		//						MainService sapServices = new MainService(WS_eFiling_URL);
		//						sapServices.SaveFileListToSAP(filePathList, VitalDocUserName, VitalDocUserPassword, "997181", "TestFileUpload_Old_v2", "", "", "", "The file upload");
		//						NextWFAction = "Completed";
		//					}
		//					if (NextStep == WorkflowSteps.Reimbursement_Requester)
		//					{
		//						NextWFAction = WorkflowActions.ReimbursementSubmit.ToString();
		//					}
		//					record = EntertainmentGiftBLL.Get(model.ID);
		//					record.Step = NextApproverdisplayName + "-" + NextWFAction + "-" + step;
		//					EntertainmentGiftBLL.SaveRecord(record);
		//					///////
		//					CommonBLL.InsertApprovalHistroy(model.ID, FormTypes.EntertainmentGift, GetUserInfo,
		//						ApprovalActionHistoryAction,
		//						ApprovalActionHistoryRoles.AccountApproval,
		//						"",
		//						ApprovalActionHistorySections.PreApproval.ToString());

		//					if (CurrentStep == WorkflowSteps.Reimbursement_ACRV1 ||
		//	   CurrentStep == WorkflowSteps.Reimbursement_ACRV2 ||
		//	   CurrentStep == WorkflowSteps.Reimbursement_ACAP ||
		//	   (CurrentStep == WorkflowSteps.Empty && model.FormNo != null))
		//					{
		//						List<PaymentReimbursementGridViewModel> PaymentReimbursementList = GetPaymentReimbursementList(ID, CurrentStep.ToString());
		//						if (PaymentReimbursementList != null && PaymentReimbursementList.Count > 0)
		//						{
		//							List<Voucher> voucherList = PaymentReimbursementListToDBRecord(PaymentReimbursementList, model.ID);
		//							EntertainmentGiftBLL.SetVoucher(voucherList);
		//						}
		//					}
		//					if (model.PaymentReimbursement != null && model.PaymentReimbursement.SAPDocNo != null)
		//					{
		//						EntertainmentGiftBLL.upadteSAP(model.ID, model.PaymentReimbursement.SAPDocNo, model.PaymentReimbursement.SAPRemark);
		//					}


		//					var emailmodel = EmailNotifactionBLL.GetEmailContent(ProcessCode, StepID, ActionType, FormTypes.EntertainmentGift.ToString(), NextStep.ToString(), record.FormNo, record.StaffName, "#");
		//					if (emailmodel != null && IsDev)
		//					{
		//						_Datafields[GeneralWFDataFields.Subject.ToString()] = emailmodel.Subject;
		//						_Datafields[GeneralWFDataFields.Body.ToString()] = emailmodel.Body;
		//						_Datafields[GeneralWFDataFields.ToUsers.ToString()] = NextApprover;
		//					}
		//					else
		//					{
		//						//skip send email
		//						_Datafields[GeneralWFDataFields.ToUsers.ToString()] = "";
		//					}

		//					WFControl.ExecuteWorkflowAction(GetUserInfo.SAMAccountWithDomain, model.K2SN, WFAction, _Datafields);

		//					scope.Complete();
		//			}

		//		}
		//		}

		//	}
		//	catch (Exception ex)
		//	{
		//		this.CreateErrorLog(ex);

		//		return Json(new { Success = false, Title = Resources.Resource_DialogMessage.UpdatedFailMessage });
		//	}
		//		return Json(new { Success = true, Title = "" });
		//}

		public List<PaymentReimbursementGridViewModel> GetPaymentReimbursementList( int ID, string CurrentStep)
		{
			EntertainmentGiftFormViewModel model = new EntertainmentGiftFormViewModel();
			int Number = 1;
			decimal Amount = 0;
			List<PaymentReimbursementGridViewModel> result = new List<PaymentReimbursementGridViewModel>();
			var EntertainmentGiftBLL = new EntertainmentGiftBLL();
			var list = EntertainmentGiftBLL.GetVoucher(ID);
			if ((CurrentStep == WorkflowSteps.Reimbursement_ACRV1.ToString() || CurrentStep == WorkflowSteps.Reimbursement_ACRV2.ToString()) && list.Count() == 0)
			{

				var record = EntertainmentGiftBLL.Get(ID);
				var ReimburseRecord = EntertainmentGiftBLL.GetReimburseRecord(ID);
				if (ReimburseRecord != null)
				{
					record.EntertainmentGiftReimburseRecord = ReimburseRecord;
				}

				MapDBRecordToEntertainmentGiftPreApprovalFormViewModel(record, model);

				if (model.BudgetType == ClaimTypes.Entertainment.ToString())
				{
					Amount = model.PaymentReimbursement.AmountHKEntertain != null ?
						(decimal)model.PaymentReimbursement.AmountHKEntertain : 0;
				}
				else
				{
					Amount = model.PaymentReimbursement.AmountHKGift != null ?
						(decimal)model.PaymentReimbursement.AmountHKGift : 0;
				}
				int count = model.PaymentReimbursement.CostCenterList.Count();
				foreach (var item in model.PaymentReimbursement.CostCenterList)
				{
					PaymentReimbursementGridViewModel resultDRModel = new PaymentReimbursementGridViewModel();
					resultDRModel.ID = 0;
					resultDRModel.Centre = item.Code;
					resultDRModel.AcctCode = "405500701";
					resultDRModel.Number = Number;
					resultDRModel.Dr_Cr = "Dr";
					resultDRModel.Payee_AcctType = "Entertatiment";
					resultDRModel.PostKey = "40";
					resultDRModel.Amount = Amount == 0 ? 0 : Amount / count;
					Number++;
					result.Add(resultDRModel);
				}

				PaymentReimbursementGridViewModel resultModel = new PaymentReimbursementGridViewModel();
				resultModel.ID = 0;
				resultModel.Centre = model.PaymentReimbursement.CostCenterCode;
				resultModel.AcctCode = "S" + model.UserInfo.StaffID;
				resultModel.Number = Number;
				resultModel.Dr_Cr = "Cr";
				resultModel.Payee_AcctType = model.UserInfo.PayeeName;
				resultModel.PostKey = "31";
				resultModel.Amount = Amount;
				result.Add(resultModel);
			}
			else
			{
				foreach (var item in list)
				{
					PaymentReimbursementGridViewModel resultDRModel = new PaymentReimbursementGridViewModel();
					resultDRModel.ID = item.ID;
					resultDRModel.Centre = item.Centre;
					resultDRModel.AcctCode = item.AcctCode;
					resultDRModel.Number = Number;
					resultDRModel.Dr_Cr = item.DrCr;
					resultDRModel.Payee_AcctType = item.PayeeAcctType;
					resultDRModel.PostKey = item.PostKey;
					resultDRModel.Amount = item.Amount != null ? (decimal)item.Amount : 0;
					resultDRModel.Text = item.Text;
					Number++;
					result.Add(resultDRModel);
				}
			}
			return result;
		}
		public List<Voucher> PaymentReimbursementListToDBRecord(List<PaymentReimbursementGridViewModel> list, int FormID)
		{
			List<Voucher> resultList = new List<Voucher>();
			var UserInfo = GetUserInfo;
			resultList = list.Select(e => new Voucher
			{
				ID = e.ID,
				FormID = FormID,
				DrCr = e.Dr_Cr,
				PostKey = e.PostKey,
				AcctCode = e.AcctCode,
				PayeeAcctType = e.Payee_AcctType,
				Amount = e.Amount,
				Centre = e.Centre,
				Text = e.Text,
				CreatedBy = UserInfo.MemberId,
				CreatedDate = DateTime.Now,
				ModifiedBy = UserInfo.MemberId,
				ModifiedDate = DateTime.Now
			}).ToList();
			return resultList;
		}
		////

		public string GetApproverDetails(List<ApproversDetails> ApproverDetails)
		{
			string ApproverDetailsText = "";
			foreach (var item in ApproverDetails)
			{
				ApproverDetailsText += item.DisplayName+",";//+ "("+ item.ADName.Split('\\')[1] + "),";
			}
			ApproverDetailsText = ApproverDetailsText.Remove(ApproverDetailsText.Length - 1, 1);
			return ApproverDetailsText;
		}
		public List<MEHK.Workflow.CommonWeb.ViewModels.SearchGridViewModel> GetSearchByRight(List<MEHK.Workflow.CommonWeb.ViewModels.SearchGridViewModel> list)
		{
			List<MEHK.Workflow.CommonWeb.ViewModels.SearchGridViewModel> ret = new List<MEHK.Workflow.CommonWeb.ViewModels.SearchGridViewModel>();
			APIAccessBLL apiBLL = new APIAccessBLL();
			Dictionary<string, object> dir = new Dictionary<string, object>();
			dir.Add("SAMACCOUNT", GetUserInfo.SAMAccountWithDomain.Split('\\')[1]);
			dir.Add("AppID", CommonConstcs.AppID);
			var Rights = apiBLL.Fun_CommonAPICALL(@"Common/GetUserRight/", dir, new Right());
			bool VIEWOWN_ENTERTAINMENT_GIT_RIGHT=  Rights.Where(e => e.RightName == RightConst.VIEWOWN_ENTERTAINMENT_GIT_RIGHT).ToList().Count()>0;
			bool VIEWDEPARTMENT_ENTERTAINMENT_GIT_RIGHT = Rights.Where(e => e.RightName == RightConst.VIEWDEPARTMENT_ENTERTAINMENT_GIT_RIGHT).ToList().Count() > 0;
			bool VIEWALL_ENTERTAINMENT_GIT_RIGHT = Rights.Where(e => e.RightName == RightConst.VIEWALL_ENTERTAINMENT_GIT_RIGHT).ToList().Count() > 0;
			if (VIEWALL_ENTERTAINMENT_GIT_RIGHT)
			{
				ret = list;
			}
			else
			{
				if (VIEWDEPARTMENT_ENTERTAINMENT_GIT_RIGHT)
				{
					List<Unit> Unitlist = GetUserInfo.BelowUnits;
					List<int> unitlistis = GetUserInfo.BelowUnits.Select(e => e.UnitId).ToList();
					if (Unitlist != null)
					{
						ret = list.Where(e => Unitlist.Any(n => n.UnitId == e.DepartmentID)==true).ToList();
						//var result = (from SearchList in list
						//			  join unitlist in Unitlist
						//				  on SearchList.DepartmentID equals unitlist.UnitId
						//			  select new SearchGridViewModel()
						//			  {

						//			  }).ToList();
					}
				}
				else
				{
					if (VIEWOWN_ENTERTAINMENT_GIT_RIGHT)
					{
						ret = list.Where(e => e.MemberId == GetUserInfo.MemberId).ToList();
					}
				}
			}

			return ret;
		}
	}
}
﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MEHK.Workflow.BLL;
using MEHK.Workflow.Data.Enum;
using MEHK.Workflow.CommonWeb.ViewModels;
using Resources;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MEHK.Workflow.Data.Model;
using MEHK.Workflow.Data.DTO;
using MEHK.Workflow.Data.Const;
using System.Transactions;
using MEHK.Workflow.SAPService;
using MEHK.Workflow.BLL.Culture;
using Newtonsoft.Json;
using System.Configuration;

namespace MEHK.Workflow.CommonWeb.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MyDraft()
        {
            SetBreadcrumbs(Resource_Common.MyDraft);

            return View();
        }

        public void SetCulture(string culture)
        {
            // Validate input
            culture = CultureHelper.GetImplementedCulture(culture);
            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
                cookie.Value = culture;   // update cookie value
            else
            {
                cookie = new HttpCookie("_culture");
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
        }
        public ActionResult MyTask()
        {
            SetBreadcrumbs(Resource_Common.MyTask);
            ViewBag.CheckboxesEnable = true;
            ViewBag.IsSearch = false;
            var TypeList = CommonBLL.GetFormTypes().Select(n => new SelectListItem() { Text = n.Description, Value = n.Code });
            ViewBag.TypeList = TypeList;

            var statusList = CommonBLL.GetStatusGroups().Where(e => e.Code != "Draft" && e.Code != "Approved")
                .OrderBy(n => n.SortOrder)
                .Select(n => new SelectListItem() { Text = n.Name, Value = n.ID.ToString() });

            ViewBag.StatusList = statusList;
            return View();
        }

        public ActionResult MySubmission()
        {
            SetBreadcrumbs(Resource_Common.MySubmission);

            var TypeList = CommonBLL.GetFormTypes().Select(n => new SelectListItem() { Text = n.Description, Value = n.Code });
            ViewBag.TypeList = TypeList;

            var statusList = CommonBLL.GetStatusGroups().Where(e => e.Code != "Draft")
                .OrderBy(n => n.SortOrder)
                .Select(n => new SelectListItem() { Text = n.Name, Value = n.ID.ToString() });

            ViewBag.StatusList = statusList;

            ViewBag.SearchFunction = "GetMySubmission";

            return View("Search");

            //return View();
        }

        public ActionResult Search()
        {
            SetBreadcrumbs("Home", "Search - All");
            ViewBag.CheckboxesEnable = false;
            ViewBag.IsSearch = true;
            var TypeList = CommonBLL.GetFormTypes().Select(n => new SelectListItem() { Text = n.Description, Value = n.Code });
            ViewBag.TypeList = TypeList;

            var statusList = CommonBLL.GetStatusGroups().Where(e => e.Code != "Draft")
                .OrderBy(n => n.SortOrder)
                .Select(n => new SelectListItem() { Text = n.Name, Value = n.ID.ToString() });

            ViewBag.StatusList = statusList;

            ViewBag.SearchFunction = "GetSearch";
            APIAccessBLL apiBLL = new APIAccessBLL();
            var rights = apiBLL.GetUserRight(User.Identity.Name.Split('\\')[1], CommonConstcs.AppID.ToString());
            ViewBag.IsACCount = rights.Any(p => p.RightName == RightConst.ACCESS_ACCOUNT_ADMIN_PAGE_RIGHT);
            //CommonSearch();

            //var searchAllInfoList = EClaimsEntites.MyActionSettings.Where(q => q.SearchAllEnabled && !q.IsDeleted).ToList();

            //ViewBag.Mode = "SearchAll";

            return View();
        }


        public ActionResult GetMySubmission([DataSourceRequest]DataSourceRequest request
                , string workflow = null
                , string serialNo = null
                , string requestBy = null
                , string creator = null
                , string applicantStaffNo = null
                , int? statusId = null
                , DateTime? submissionFrom = null
                , DateTime? submissionTo = null
                , string mode = null
                )
        {
            var MEHKWorkflowEntities = new MEHKWorkflowEntities();

            var result = new List<SearchGridViewModel>();

            var TypeList = CommonBLL.GetFormTypes();
            if (!String.IsNullOrEmpty(workflow))
            {
                TypeList = TypeList.Where(n => n.Code == workflow).ToList();
            }

            foreach (var wfInfo in TypeList)
            {
                try
                {



                    var sql = CommonBLL.GetFormTypeMySubmissionSQL(wfInfo.Code);
                    var list = MEHKWorkflowEntities.Database
                            .SqlQuery<SearchGridViewModel>(
                            $"{sql} @user, @lang, @serialNo, @requestBy, @creator, @applicantStaffNo, @statusId, @submissionFrom, @submissionTo",
                            new SqlParameter("user", GetUserInfo.MemberId),
                            new SqlParameter("lang", ""),
                            new SqlParameter("serialNo", (object)serialNo ?? DBNull.Value),
                            new SqlParameter("requestBy", (object)requestBy ?? DBNull.Value),
                            new SqlParameter("creator", (object)creator ?? DBNull.Value),
                            new SqlParameter("applicantStaffNo", (object)applicantStaffNo ?? DBNull.Value),
                            new SqlParameter("statusId", (object)statusId ?? DBNull.Value),
                            new SqlParameter("submissionFrom", (object)submissionFrom ?? DBNull.Value),
                            new SqlParameter("submissionTo", (object)submissionTo ?? DBNull.Value)
                            ).ToList();

                    result.AddRange(list);
                }
                catch (Exception ex)
                {
                    this.CreateErrorLog(ex);
                }
            }

            result = result.OrderByDescending(q => q.SubmissionDate).ToList();

            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSearch([DataSourceRequest]DataSourceRequest request
            , string workflow = null
            , string serialNo = null
            , string requestBy = null
            , string creator = null
            , string applicantStaffNo = null
            , int? statusId = null
            , List<string> statusIdList = null
            , DateTime? submissionFrom = null
            , DateTime? submissionTo = null
            , DateTime? postDateFrom = null
            , DateTime? postDateTo = null
            , string costCenter = null
            , string mode = null
            )
        {
            var MEHKWorkflowEntities = new MEHKWorkflowEntities();
            var result = new List<SearchGridViewModel>();
            var workflowList = MEHKWorkflowEntities.MyActionSettings.Where(q => q.SearchMyActivityEnabled && !q.IsDeleted).ToList();
            if (workflow != null)
            {
                workflowList = workflowList.Where(e => e.ProcessCode == workflow).ToList();
            }


            string UnitUserStr = " ";


            foreach (var item in workflowList)
            {

                if (!String.IsNullOrEmpty(item.SPTaskInfo))
                {
                    try
                    {


                        var workflowItemList = MEHKWorkflowEntities.Database
                           .SqlQuery<SearchGridViewModel>($"{item.SPSearchMyActivity} @user,@lang, @serialNo, @requestBy, @creator, @applicantStaffNo, @statusId, @submissionFrom, @submissionTo , @postDateFrom, @postDateTo,@costCenter,@Right,@UnitUserStr",
                                        new SqlParameter("user", GetUserInfo.MemberId),
                                        new SqlParameter("lang", ""),
                                        //new SqlParameter("workflow", "EntertainmentGift"),
                                        new SqlParameter("serialNo", (object)serialNo ?? DBNull.Value),
                                        new SqlParameter("requestBy", (object)requestBy ?? DBNull.Value),
                                        new SqlParameter("creator", (object)creator ?? DBNull.Value),
                                        new SqlParameter("applicantStaffNo", (object)applicantStaffNo ?? DBNull.Value),
                                        //new SqlParameter("statusId", (object)statusId ?? DBNull.Value),
                                        new SqlParameter("statusId", DBNull.Value),
                                        new SqlParameter("submissionFrom", (object)submissionFrom ?? DBNull.Value),
                                        new SqlParameter("submissionTo", (object)submissionTo ?? DBNull.Value),
                                        new SqlParameter("postDateFrom", (object)postDateFrom ?? DBNull.Value),
                                        new SqlParameter("postDateTo", (object)postDateTo ?? DBNull.Value),
                                        new SqlParameter("costCenter", (object)costCenter ?? DBNull.Value),
                                        new SqlParameter("Right", ""),
                                        new SqlParameter("UnitUserStr", UnitUserStr)
                                        ).ToList();
                        result.AddRange(workflowItemList);


                    }
                    catch (Exception ex)
                    {
                        this.CreateErrorLog(ex);
                    }
                }
            }

            foreach (var item in result)
            {
                var myTaskInfo = workflowList.Where(q => q.ProcessDisplayName == item.Type).FirstOrDefault();



                if (myTaskInfo != null)
                {
                    var tempUrl = myTaskInfo.ViewUrl;
                    foreach (var property in item.GetType().GetProperties())
                    {
                        var value = Convert.ToString(property.GetValue(item, null));
                        tempUrl = tempUrl.Replace($"#{property.Name}#", value);
                    }

                    item.URL = tempUrl;
                }
            }

            if (statusIdList != null)
            {
                var list = new List<SearchGridViewModel>();
                foreach (var item in statusIdList)
                {
                    statusId = int.Parse(item);
                    list = list.Union(result.Where(e => e.StatusGroupID == statusId).ToList()).ToList();
                }
                result = list;
            }
            result = GetSearchByRight(result);
            result = result.OrderByDescending(q => q.SubmissionDate).ToList();


            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        public ActionResult GetWorkflowLog([DataSourceRequest]DataSourceRequest request, int FormID, int FormTypeID)
        {
            var list = CommonBLL.GetApprovalHistory(FormID, FormTypeID);

            return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public string GetStepApp(string Step)
        {
            string StepApp = "";
            List<string> list = new List<string>(Step.Split('-'));
            StepApp = list.FirstOrDefault();
            return StepApp;
        }

        public ActionResult GetMyTasks([DataSourceRequest]DataSourceRequest request,
                string workflow = null
                , string serialNo = null
                , int? statusId = null
                , string requestBy = null
                , string creator = null
                , string applicantStaffNo = null
                , DateTime? submissionFrom = null
                , DateTime? submissionTo = null
                , string mode = null)
        {

            //var list = CommonBLL.GetMyTasks();

            var k2UserID = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.k2UserID);

            var ProcessName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.GenenalProcessName);


            //todo set User Identity Name
            //  var ProcessItemList = WFControl.GetWorkList(GetUserInfo.SAMAccount, new string[] { ProcessName });
            var ProcessItemList = WFControl.GetWorkList(User.Identity.Name, new string[] { ProcessName });
            //////
            var allTaskList = new List<MyTasksModel>();
            var workflowList = MEHKWorkflowEntities.MyActionSettings.Where(q => q.TaskEnabled && !q.IsDeleted).ToList();
            if (workflow != null)
            {
                workflowList = workflowList.Where(e => e.ProcessCode == workflow).ToList();
            }
            foreach (var item in workflowList)
            {

                if (!String.IsNullOrEmpty(item.SPTaskInfo))
                {
                    try
                    {

                        var workflowItemList = MEHKWorkflowEntities.Database
                           .SqlQuery<MyTasksModel>($"{item.SPTaskInfo}"
                           //new SqlParameter("user", GetUserInfo.MemberId),
                           //new SqlParameter("lang", "")
                           ).ToList();
                        allTaskList.AddRange(workflowItemList);
                    }
                    catch (Exception ex)
                    {
                        this.CreateErrorLog(ex);
                    }
                }
            }

            var result = (from task in allTaskList
                          join k2item in ProcessItemList
                              on task.ProcInstID equals (int?)k2item.ProcInstID
                          select new MyTaskGridViewModel()
                          {
                              ID = task.ID,
                              FormType = task.FormType,
                              FormNo = task.FormNo,
                              StatusGroupID = task.StatusGroupID == null ? 0 : (int)task.StatusGroupID,
                              FormStatus = task.FormStatus,
                              Applicant = task.Payee,
                              CreatedBy = task.CreatedBy,
                              SubmissionDate = task.SubmissionDate,
                              Step = task.Step != null ? GetStepApp(task.Step) : "",

                              ProcInstID = k2item.ProcInstID,
                              //Title = task.Title,
                              FormStatusDisplayName = task.FormStatusDisplayName,
                              K2ItemSerialNo = k2item.SN,
                              FormTypeDisplayName = task.FormTypeDisplayName,

                          }).ToList();
            //URL
            //foreach (var item in result)
            //{
            //	MyActionSetting model = workflowList.Where(e => e.ProcessDisplayName == item.FormTypeDisplayName).FirstOrDefault();
            //	if (model != null)
            //	{
            //		item.URL = model.TaskURL;
            //	}
            //}

            foreach (var item in result)
            {
                var myTaskInfo = workflowList.Where(q => q.ProcessDisplayName == item.FormTypeDisplayName).FirstOrDefault();



                if (myTaskInfo != null)
                {
                    var tempUrl = myTaskInfo.TaskURL;
                    foreach (var property in item.GetType().GetProperties())
                    {
                        var value = Convert.ToString(property.GetValue(item, null));
                        tempUrl = tempUrl.Replace($"#{property.Name}#", value);
                    }

                    item.URL = tempUrl;
                }
                if (item.FormType == FormTypes.EntertainmentGift.ToString())
                {
                    item.isAcrvOrAcap = new CommonBLL().isAcrvOrAcap(item.ID);
                }

            }

            ///

            //var result = (from task in list
            //                       join k2item in ProcessItemList
            //                           on task.ProcInstID equals (int?)k2item.ProcInstID
            //                       select new MyTaskGridViewModel()
            //                       {
            //                           ID = task.ID,
            //                           FormType = task.FormType,
            //                           FormNo = task.FormNo,
            //                           StatusGroupID = task.StatusGroupID == null ? 0 : (int)task.StatusGroupID,
            //                           FormStatus = task.FormStatus,
            //                           Applicant = task.Payee,
            //                           CreatedBy = task.CreatedBy,
            //                           SubmissionDate = task.SubmissionDate,

            //                           ProcInstID = k2item.ProcInstID,
            //                           //Title = task.Title,
            //                           FormStatusDisplayName = task.FormStatusDisplayName,
            //                           K2ItemSerialNo = k2item.SN,
            //                           FormTypeDisplayName = task.FormTypeDisplayName,
            //                       }).ToList();
            if (workflow != "" && workflow != null)
            {
                result = result.Where(e => e.FormType == workflow).ToList();
            }
            if (serialNo != "" && serialNo != null)
            {
                result = result.Where(e => e.FormNo.ToLower().IndexOf(serialNo.Trim().ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            if (statusId != 0 && statusId != null)
            {
                result = result.Where(e => e.StatusGroupID == statusId).ToList();
            }
            if (requestBy != "" && requestBy != null)
            {
                result = result.Where(e => e.Applicant.ToLower().IndexOf(requestBy.Trim().ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            if (creator != "" && creator != null)
            {
                result = result.Where(e => e.CreatedBy.ToLower().IndexOf(creator.Trim().ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            if (submissionFrom != null)
            {
                result = result.Where(e => e.SubmissionDate >= submissionFrom).ToList();
            }
            if (submissionTo != null)
            {
                result = result.Where(e => e.SubmissionDate <= submissionTo).ToList();
            }


            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMyDrafts([DataSourceRequest]DataSourceRequest request)
        {
            CommonBLL CommonBLL = new CommonBLL();
            var UserInfo = GetUserInfo;
            var list = CommonBLL.GetMyDrafts().Where(e => e.CreatedBy == UserInfo.SAMAccountWithDomain).Select(n => new MyDraftGridViewModel()
            {
                ID = n.ID,
                FormType = n.FormType,
                FormTypeDisplayName = n.FormTypeDisplayName,
                FormStatus = n.FormStatus,
                CreatedBy = n.CreatedBy,
                CreatedDate = n.CreatedDate
            });

            return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";

        //    return View();
        //}

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}

        public ActionResult GetApprovalMatrixGrid([DataSourceRequest]DataSourceRequest request, int MemberId, string ApprovalMartixOptionCode)
        {
            string ProcessCode = EntertainmentGiftCode;
            List<ApprovalMartixGridViewModel> result = new List<ApprovalMartixGridViewModel>();
            APIAccessBLL apiBLL = new APIAccessBLL();
            var CostCenters = apiBLL.GetApprovers("Organization/ApprovalMartixApprovers/", MemberId.ToString(), ProcessCode, ApprovalMartixOptionCode);
            if (CostCenters != null)
            {
                if (CostCenters.Details != null)
                {
                    result = CostCenters.Details.Where(e => e.ApproverDetails != null && e.ApproverDetails.Count > 0).Select(e => new ApprovalMartixGridViewModel
                    {
                        ApprovalLevelCode = e.ApprovalLevelCode,
                        ApprovalLevelName = e.ApprovalLevelName,
                        Approvers = e.Approvers,
                        ApproversString = GetApproverDetails(e.ApproverDetails) //string.Join(",", e.Approvers)

                    }).ToList();
                }
            }

            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetMemberList(string text, int? nid)
        {
            APIAccessBLL apiBLL = new APIAccessBLL();
            List<UserInfo> resultList = apiBLL.GetMember().ToList();
            if (text != "" && text != null)
            {
                resultList = resultList.Where(e => e.MemberName.ToLower().IndexOf(text.Trim().ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            if (nid > 0)
            {
                // UserInfo user = apiBLL.GetMemberById(Convert.ToInt32(nid));
                resultList = resultList.Where(e => e.MemberId != nid).ToList();
            }
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CreateDelegation(DelegationViewModel model)
        {
            List<DelegationViewModel> resultuf = new List<DelegationViewModel>();
            try
            {
                CommonBLL bll = new CommonBLL();
                List<DelegationViewModel> Delegation = bll.GetDelegationByID(model.UserID, model.AssignUserID, model.ProcessCode);
                if (Delegation != null)
                {
                    foreach (var item in Delegation)
                    {
                        if (item.ToDate < model.FormDate || item.FormDate > model.ToDate) { }
                        else
                            return Json("Error:Date Repetition", JsonRequestBehavior.AllowGet); //date Repetition
                    }
                }
                APIAccessBLL apiBLL = new APIAccessBLL();

                //activeUser
                UserInfo activeUser = apiBLL.GetMemberById(model.UserID);
                Delegation db = new Delegation();
                db.UserName = activeUser.MemberName;
                db.UserID = activeUser.MemberId;
                db.UserADName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.loginDomain) + "\\" + activeUser.SAMAccount;

                db.ProcessCode = model.ProcessCode;
                db.ProcessName = model.ProcessName;

                //assignUser
                UserInfo assignUser = apiBLL.GetMemberById(model.AssignUserID);
                db.AssignUserID = assignUser.MemberId;
                db.AssignUserADName = CommonBLL.GetOrgManagementSystemParameter(OrgManagement_SystemParameterAppKey.loginDomain) + "\\" + assignUser.SAMAccount;
                db.AssignUserName = assignUser.MemberName;

                db.CreatedBy = GetUserInfo.SAMAccountWithDomain;
                db.CreatedDate = DateTime.Now;

                db.FormDate = model.FormDate;
                db.ToDate = model.ToDate;
                db = bll.CreateDelegation(db);
                model = bll.GetDelegation().Where(e => e.Id == db.ID).FirstOrDefault();
                resultuf.Add(model);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json("success", JsonRequestBehavior.AllowGet);

        }
        //[MEHKAuthorize(PageRight = RightConst.ACCESS_IS_ADMIN_PAGE_RIGHT)]
        public ActionResult Delegation()
        {
            ViewBag.UserName = GetUserInfo.MemberName;
            ViewBag.UserID = GetUserInfo.MemberId;
            SetBreadcrumbs("Home", "Delegation");
            return View();
        }

        public ActionResult GetDelegation([DataSourceRequest]DataSourceRequest request)
        {
            List<DelegationViewModel> result = new List<DelegationViewModel>();
            CommonBLL bll = new CommonBLL();
            result = bll.GetDelegationByUserID(GetUserInfo.MemberId);
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDelegationByCrateby([DataSourceRequest]DataSourceRequest request)
        {
            List<DelegationViewModel> result = new List<DelegationViewModel>();
            CommonBLL bll = new CommonBLL();
            result = bll.GetDelegationByCrateby(GetUserInfo.SAMAccountWithDomain);
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteDelegation([DataSourceRequest] DataSourceRequest request, DelegationViewModel model)
        {
            List<DelegationViewModel> resultuf = new List<DelegationViewModel>();
            try
            {
                Delegation db = new Delegation();
                db.ID = model.Id;
                CommonBLL bll = new CommonBLL();
                db = bll.DeleteDelegation(db);
            }
            catch (Exception e)
            {
                this.CreateErrorLog(e);
            }
            return Json(resultuf.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        /////
        private ApprovalMartixViewModel GetApprovalMatrixFromAPI(EntertainmentGiftRecord viewModel, FlowType ftype, string Memberid)
        {
            ApprovalMartixViewModel approverModel = new ApprovalMartixViewModel();


            string Option = "";
            decimal BudgetAmount = 0;
            if (viewModel.BudgetType == EntertainmentGiftBudgetTypes.Entertainment.ToString())
            {
                BudgetAmount = viewModel.EntertainBudget != null ? (decimal)viewModel.EntertainBudget : 0;
            }
            else if (viewModel.BudgetType == EntertainmentGiftBudgetTypes.Gift.ToString())
            {
                BudgetAmount = viewModel.GiftBudget != null ? (decimal)viewModel.GiftBudget : 0;
            }
            if (ftype == FlowType.PreApproval)
            {
                if (viewModel.BudgetType == ClaimTypes.Entertainment.ToString())
                {
                    if (BudgetAmount >= EntertainmentAmount)
                    {
                        Option = "O";
                    }
                    else
                    {
                        Option = "B";
                    }
                }
                else
                {
                    if (BudgetAmount >= GiftAmount)
                    {
                        Option = "O";
                    }
                    else
                    {
                        Option = "B";
                    }
                }
            }
            else
            {
                Option = "R";
            }
            APIAccessBLL apiBLL = new APIAccessBLL();
            approverModel = apiBLL.GetApprovers("Organization/ApprovalMartixApprovers/", Memberid.ToString(), EntertainmentGiftCode, Option);
            return approverModel;

        }
        public string GetNextApproverdisplayName(string NextApprover)
        {
            string NextApproverdisplayName = "";
            if (NextApprover != null)
            {
                APIAccessBLL apiBLL = new APIAccessBLL();
                NextApprover = NextApprover.Split(';').FirstOrDefault();
                var NextApprovermodel = apiBLL.GetMember(@"Organization/Members/", NextApprover.Split('\\')[1]).FirstOrDefault();

                if (NextApprovermodel != null)
                {
                    NextApproverdisplayName = NextApprovermodel.MemberName;
                }

            }
            return NextApproverdisplayName;
        }
        public ActionResult BatchApprove(string Models, string Type)
        {

            try
            {
                List<BatchAprrovalViewModel> list = JsonConvert.DeserializeObject<List<BatchAprrovalViewModel>>(Models);
                string NextApprover = "";
                FormTypes _Type = (FormTypes)Enum.Parse(typeof(FormTypes), Type);
                foreach (var model in list)
                {
                    switch (_Type)
                    {
                        case FormTypes.EntertainmentGift:


                            EntertainmentGiftBLL entertainmentGiftBLL = new EntertainmentGiftBLL();
                            NextApprover = entertainmentGiftBLL.BatchUpdateEntertainmentGift(model.ID, User.Identity.Name, GetUserInfo);
                            if (NextApprover == "")
                            {
                                break;
                            }
                            break;
                        case FormTypes.PO:
                            break;

                    }
                    Dictionary<string, object> _Datafields = new Dictionary<string, object>();
                    _Datafields[GeneralWFDataFields.HasNextStep.ToString()] = "True";
                    _Datafields[GeneralWFDataFields.NextApprover.ToString()] = NextApprover;
                    WFControl.ExecuteWorkflowAction(GetUserInfo.SAMAccountWithDomain, model.SNNum, "Approve", _Datafields);

                }



            }
            catch (Exception ex)
            {
                this.CreateErrorLog(ex);

                return Json(new { Success = false, Title = Resources.Resource_DialogMessage.UpdatedFailMessage });
            }
            return Json(new { Success = true, Title = "" });
        }

        //public List<PaymentReimbursementViewModel> GetPaymentReimbursementList(int ID, string CurrentStep)
        //{
        //	EntertainmentGift.ViewModels.EntertainmentGiftFormViewModel model = new EntertainmentGift.ViewModels.EntertainmentGiftFormViewModel();
        //	int Number = 1;
        //	decimal Amount = 0;
        //	List<PaymentReimbursementViewModel> result = new List<PaymentReimbursementViewModel>();
        //	var EntertainmentGiftBLL = new EntertainmentGiftBLL();
        //	var list = EntertainmentGiftBLL.GetVoucher(ID);
        //	if ((CurrentStep == WorkflowSteps.Reimbursement_ACRV1.ToString() || CurrentStep == WorkflowSteps.Reimbursement_ACRV2.ToString()) && list.Count() == 0)
        //	{

        //		var record = EntertainmentGiftBLL.Get(ID);
        //		var ReimburseRecord = EntertainmentGiftBLL.GetReimburseRecord(ID);
        //		if (ReimburseRecord != null)
        //		{
        //			record.EntertainmentGiftReimburseRecord = ReimburseRecord;
        //		}

        //		MapDBRecordToEntertainmentGiftPreApprovalFormViewModel(record, model);

        //		if (model.BudgetType == ClaimTypes.Entertainment.ToString())
        //		{
        //			Amount = model.PaymentReimbursement.AmountHKEntertain != null ?
        //				(decimal)model.PaymentReimbursement.AmountHKEntertain : 0;
        //		}
        //		else
        //		{
        //			Amount = model.PaymentReimbursement.AmountHKGift != null ?
        //				(decimal)model.PaymentReimbursement.AmountHKGift : 0;
        //		}
        //		int count = model.PaymentReimbursement.CostCenterList.Count();
        //		foreach (var item in model.PaymentReimbursement.CostCenterList)
        //		{
        //			PaymentReimbursementViewModel resultDRModel = new PaymentReimbursementViewModel();
        //			resultDRModel.ID = 0;
        //			resultDRModel.Centre = item.Code;
        //			resultDRModel.AcctCode = "405500701";
        //			resultDRModel.Number = Number;
        //			resultDRModel.Dr_Cr = "Dr";
        //			resultDRModel.Payee_AcctType = "Entertatiment";
        //			resultDRModel.PostKey = "40";
        //			resultDRModel.Amount = Amount == 0 ? 0 : Amount / count;
        //			Number++;
        //			result.Add(resultDRModel);
        //		}

        //		PaymentReimbursementViewModel resultModel = new PaymentReimbursementViewModel();
        //		resultModel.ID = 0;
        //		resultModel.Centre = model.PaymentReimbursement.CostCenterCode;
        //		resultModel.AcctCode = "S" + model.UserInfo.StaffID;
        //		resultModel.Number = Number;
        //		resultModel.Dr_Cr = "Cr";
        //		resultModel.Payee_AcctType = model.UserInfo.PayeeName;
        //		resultModel.PostKey = "31";
        //		resultModel.Amount = Amount;
        //		result.Add(resultModel);
        //	}
        //	else
        //	{
        //		foreach (var item in list)
        //		{
        //			PaymentReimbursementViewModel resultDRModel = new PaymentReimbursementViewModel();
        //			resultDRModel.ID = item.ID;
        //			resultDRModel.Centre = item.Centre;
        //			resultDRModel.AcctCode = item.AcctCode;
        //			resultDRModel.Number = Number;
        //			resultDRModel.Dr_Cr = item.DrCr;
        //			resultDRModel.Payee_AcctType = item.PayeeAcctType;
        //			resultDRModel.PostKey = item.PostKey;
        //			resultDRModel.Amount = item.Amount != null ? (decimal)item.Amount : 0;
        //			resultDRModel.Text = item.Text;
        //			Number++;
        //			result.Add(resultDRModel);
        //		}
        //	}
        //	return result;
        //}
        public List<Voucher> PaymentReimbursementListToDBRecord(List<PaymentReimbursementViewModel> list, int FormID)
        {
            List<Voucher> resultList = new List<Voucher>();
            var UserInfo = GetUserInfo;
            resultList = list.Select(e => new Voucher
            {
                ID = e.ID,
                FormID = FormID,
                DrCr = e.Dr_Cr,
                PostKey = e.PostKey,
                AcctCode = e.AcctCode,
                PayeeAcctType = e.Payee_AcctType,
                Amount = e.Amount,
                Centre = e.Centre,
                Text = e.Text,
                CreatedBy = UserInfo.MemberId,
                CreatedDate = DateTime.Now,
                ModifiedBy = UserInfo.MemberId,
                ModifiedDate = DateTime.Now
            }).ToList();
            return resultList;
        }

        public JsonResult GetCostCentersList(int? ID)
        {
            List<CostCentersViewModel> list = new List<CostCentersViewModel>();
            list = GetCostCentersModelList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public List<CostCentersViewModel> GetCostCentersModelList()
        {
            List<CostCentersViewModel> list = new List<CostCentersViewModel>();

            APIAccessBLL apiBLL = new APIAccessBLL();
            var CostCenters = apiBLL.GetCostCenters("Organization/CostCenters/", 0);
            if (CostCenters.Count() > 0)
            {
                list = CostCenters.Where(e => e.IsDeleted == false).Select(e => new CostCentersViewModel
                {
                    Description = e.Description,
                    CostCenterId = e.CostCenterId,
                    Code = e.Code
                }).ToList();
            }
            return list;
        }
    }

}
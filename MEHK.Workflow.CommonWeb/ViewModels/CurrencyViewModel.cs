﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.CommonWeb.ViewModels
{
	public class CurrencyViewModel
	{
		public string CurrencyID { get; set; }
		public string CurrencyName { get; set; }
		public int SortSeq { get; set; }
		public string Remark { get; set; }
	}
}
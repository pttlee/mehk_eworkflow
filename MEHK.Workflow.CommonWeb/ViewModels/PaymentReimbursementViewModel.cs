﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.CommonWeb.ViewModels
{
	public class PaymentReimbursementViewModel
	{
		public int ID { get; set; }
		public int Number { get; set; }
		public string Dr_Cr { get; set; }
		public string PostKey { get; set; }
		public string AcctCode { get; set; }
		public string Payee_AcctType { get; set; }
		public decimal Amount { get; set; }
		public string Centre { get; set; }
		public string Text { get; set; }
	}
}
﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.CommonWeb.ViewModels
{
    public class SearchGridViewModel
    {
        public int ID { get; set; }
        public string Type { get; set; }

        [Display(Name = nameof(Resource_Common.SerialNumber), ResourceType = typeof(Resource_Common))]
        public string SerialNumber { get; set; }

        [Display(Name = nameof(Resource_Common.SubmissionDate), ResourceType = typeof(Resource_Common))]
        public DateTime? SubmissionDate { get; set; }

        [Display(Name = nameof(Resource_Common.Creator), ResourceType = typeof(Resource_Common))]
        public string CreatorName { get; set; }

        [Display(Name = nameof(Resource_Common.Applicant), ResourceType = typeof(Resource_Common))]
        public string ApplicantName { get; set; }

        [Display(Name = nameof(Resource_Common.ApplicantStaffNo), ResourceType = typeof(Resource_Common))]
        public string ApplicantStaffNo { get; set; }

        [Display(Name = nameof(Resource_Common.Status), ResourceType = typeof(Resource_Common))]
        public string Status { get; set; }
        public int StatusGroupID { get; set; }

        public string URL { get; set; }
        public string Step { get; set; }
		public int? MemberId { get; set; }
		public int? DepartmentID { get; set; }

       // [Display(Name = nameof(Resource_Common.SubmissionDate), ResourceType = typeof(Resource_Common))]
        public DateTime? PostDate { get; set; }

    }
}
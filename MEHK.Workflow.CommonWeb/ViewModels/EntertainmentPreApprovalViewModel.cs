﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MEHK.Workflow.CommonWeb.ViewModels
{
    public class EntertainmentPreApprovalViewModel
    {
        //[Display(Name = "Reason, purpose")]
        [Required]
        [Display(Name = "Purpose", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public string Purpose { get; set; }

        [Required]
        public DateTime? EventDateOn { get; set; }

        [Required]
        public DateTime? EventDateTo { get; set; }

        //[Display(Name = "Late application reason")]
        [Display(Name = "LateApplicationReason", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public string LateApplicationReason { get; set; }

        [Required]
        //[Display(Name = "Place / Nature of gift")]
        [Display(Name = "NatureOfGift", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public string NatureOfGift { get; set; }

        [Required]
        //[Display(Name = "Guest(s) invited")]
        [Display(Name = "Guest", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public string Guest { get; set; }

        //[Display(Name = "Total")]

        [Display(Name = "GuestTotal", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public int? GuestTotal { get; set; }

        [Required]
        [Display(Name = "Company", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public string Company { get; set; }

        [Display(Name = "Attendant", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public string Attendant { get; set; }

        //[Display(Name = "Total")]
        [Display(Name = "AttendantTotal", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public int? AttendantTotal { get; set; }

        [Display(Name = "ReimbursedByMELCO", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public bool ReimbursedByMELCO { get; set; }

    }
}
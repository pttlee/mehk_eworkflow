﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.CommonWeb.ViewModels
{
	public class GetMemberListViewModel
	{
		public int MemberId { get; set; }
		public string MemberName { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.Workflow.SAPService
{
    public class MainService
    {

        private string sapServerUrl { get; set; }



        public MainService(string sapServerUrl)
        {
            this.sapServerUrl = sapServerUrl;
        }

        public void SubmitPOApproverSAP(string param_po_no, string param_mem_id, string param_approved, string param_rejected,
            string param_completed, string status)
        {
            using (sapWS ws = new sapWS())
            {
                ws.Url = sapServerUrl;

                ws.submitPOApproverSAP(param_po_no, param_mem_id, param_approved, param_rejected, param_completed, status);
            }
        }

        public void SaveFileListToSAP(string FormID, string username, string password, string targetDirectory, string subjectname, string datefrom, string dateto, string reference, string remark)
        {
            try
            {
                using (eFilingWS ws = new eFilingWS())
                {
                    ws.Url = sapServerUrl;
                    ws.save2eFilingForK2(FormID, username, password, targetDirectory, subjectname, datefrom, dateto, remark);
                }
            }catch(Exception ex)
            {
                return;
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;

namespace CommonHelper
{
	public class DynamicHelper
    {
        
            //public static ExpandoObject convertToExpando(object obj)
            //{
            //    //Get Properties Using Reflections
            //    BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
            //    PropertyInfo[] properties = obj.GetType().GetProperties(flags);

            //    //Add Them to a new Expando
            //   // ExpandoObject expando = new ExpandoObject();
            //    dynamic expando = new ExpandoObject();
            //    var dictionary = (IDictionary<string, object>)expando;

            //foreach (PropertyInfo property in properties)
            //    {
            //    dictionary.Add( property.Name, property.GetValue(obj));
            //    }

            //    return expando;
            //}

            public static void AddProperty(ExpandoObject expando, string propertyName, object propertyValue)
            {
                //Take use of the IDictionary implementation
                var expandoDict = (IDictionary<string, object>)expando;
                if (expandoDict.ContainsKey(propertyName))
                    expandoDict[propertyName] = propertyValue;
                else
                    expandoDict.Add(propertyName, propertyValue);
            }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonHelper.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToFinancialYear(this DateTime dateTime)
        {
            return (dateTime.Month >= 4 ? dateTime.Year : dateTime.Year - 1)  + "/" + (dateTime.Month >= 4 ? dateTime.Year + 1 : dateTime.Year);
        }

        public static string ToFinancialYearShort(this DateTime dateTime)
        {
            return (dateTime.Month >= 4 ? dateTime.ToString("yy") : dateTime.AddYears(-1).ToString("yy")) + "/" + (dateTime.Month >= 4 ? dateTime.AddYears(1).ToString("yy") : dateTime.ToString("yy"));
            //return "FY" + (dateTime.Month >= 4 ? dateTime.AddYears(1).ToString("yy") : dateTime.ToString("yy"));
        }
    }
}
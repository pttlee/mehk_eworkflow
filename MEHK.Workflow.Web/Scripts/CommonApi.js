﻿var LinkCommonApi = {
    GetUser: CommonApiURL + 'api/Organization/GetUsers',
    GetUserByID: CommonApiURL + 'api/Organization/GetUserByID',
    GetApproversByLevel: CommonApiURL + 'api/Organization/GetApproversByLevel',
    GetPettyCashApprover: CommonApiURL + 'api/Organization/GetPettyCashApprover',
    GetDistrict: CommonApiURL + 'api/Common/GetDistricts',
    GetStaffWelfareApprover: CommonApiURL + 'api/Organization/GetStaffWelfareApprover',
}
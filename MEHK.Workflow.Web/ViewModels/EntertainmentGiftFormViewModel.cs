﻿using MEHK.Workflow.Data.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Web.ViewModels
{
    public class EntertainmentGiftFormViewModel : BaseFormViewModel
    {
        public EntertainmentGiftFormViewModel()
        {
            PreApprovalHistory = new ApprovalHistoryViewModel() {
                Title = "PreApproval History",
                Section = ApprovalActionHistorySections.PreApproval.ToString()
            };

            PaymentReimbursementHistory = new ApprovalHistoryViewModel()
            {
                Title = "Payment Reimbursement History",
                Section = ApprovalActionHistorySections.Reimbursement.ToString()
            };
			PaymentReimbursement = new EntertainmentPaymentReimbursementViewModel();

		}

        //[Display(Name = "Staff ID 職員號碼")]

        [Display(Name = "StaffID", ResourceType =typeof(Resources.Resource_Form))]
        public string StaffID { get; set; }

        //[Display(Name = "Staff Name 職員姓名")]
        [Display(Name = "StaffName", ResourceType = typeof(Resources.Resource_Form))]
        public string StaffName { get; set; }

        //[Display(Name = "Division 處")]
        [Display(Name = "Division", ResourceType = typeof(Resources.Resource_Form))]
        public string Division { get; set; }

        //[Display(Name = "Department 部")]
        [Display(Name = "Department", ResourceType = typeof(Resources.Resource_Form))]
        public string Department { get; set; }

        [Display(Name = "Date of Application" )]
        public DateTime? ApplicationDate { get; set; }

        [Display(Name = "FormStatus", ResourceType = typeof(Resources.Resource_Form))]
        public string FormStatus { get; set; }

        [Display(Name = "Payee")]
        public int? Payee { get; set; }

        //[Required(ErrorMessage = "Budget Type is required")]
        [Required]
        [Display(Name = "Budget Type")]
        public string BudgetType { get; set; }

        [Required]
        [Display(Name = "Budget Amount")]
        public decimal? BudgetAmount { get; set; }

        //[UIHint("TextArea")][
        //[Required]

        public EntertainmentPreApprovalViewModel PreApproval { get; set; }

        public EntertainmentPaymentReimbursementViewModel PaymentReimbursement { get; set; }

        public ApprovalHistoryViewModel PreApprovalHistory { get; set; }

        public ApprovalHistoryViewModel PaymentReimbursementHistory { get; set; }


        public bool EnablePreApprovalPart { get; set; }

        public bool EnablePaymentReimbursementPart { get; set; }



        public int CurrentUserID { get; set; }
        public string Remark { get; set; }
        public string SAMAccount { get; set; }


        public string __RequestVerificationToken { get; set; }




    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Web.ViewModels
{
    public class EntertainmentPaymentReimbursementViewModel
    {
        
        [Display(Name = "PostingInstruction",ResourceType =typeof(Resources.Resource_EntertainmentGift))]
        public string PostingInstruction { get; set; }



        //[Display(Name = "Reason, purpose")]
        [Display(Name = "Purpose", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public string Purpose { get; set; }
		//public string EventDateOnString { get; set; }
		public DateTime? EventDateOn { get; set; }

        //public string EventDateToString { get; set; }
        public DateTime? EventDateTo { get; set; }

        //[Display(Name = "Late application reason")]

        [Display(Name = "LateApplicationReason", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public string LateApplicationReason { get; set; }

        //[Display(Name = "Place / Nature of gift")]
        [Display(Name = "NatureOfGift", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public string NatureOfGift { get; set; }

        //[Display(Name = "Guest(s) invited")]
        [Display(Name = "Guest", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public string Guest { get; set; }

        //[Display(Name = "Total")]
        [Display(Name = "GuestTotal", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public int? GuestTotal { get; set; }

        [Display(Name = "Company", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public string Company { get; set; }

        [Display(Name = "Attendant", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public string Attendant { get; set; }

        //[Display(Name = "Total")]
        [Display(Name = "AttendantTotal", ResourceType = typeof(Resources.Resource_EntertainmentGift))]
        public int? AttendantTotal { get; set; }

        public string Currency { get; set; }

        public decimal? Amount { get; set; }

        public decimal? ExchangeRate { get; set; }

        [Display(Name = "HK$")]
        public decimal? AmountHK { get; set; }

        [Display(Name = "Amount exceed reason")]
        public string AmountExceedReason { get; set; }

        [Display(Name = "Payment method")]
        public string PaymentMethod { get; set; }

        [Display(Name = "Pay to")]
        public string PayTo { get; set; }

        [Display(Name = "No. of document or receipt attached")]
        public int? NoOfDocumentOrReceiptAttached { get; set; }
		public string CostCenter { get; set; }

		public decimal? AmountHKEntertain { get; set; }
		public decimal? AmountHKGift { get; set; }
		


	}
}
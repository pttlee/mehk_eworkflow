﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MEHK.Workflow.Web.ViewModels
{
    public class BaseFormViewModel
    {
        public BaseFormViewModel()
        {
            this.ActionList = new List<string>();
        }

        public int ID { get; set; }
		public Guid GID { get; set; }

		public int FormTypeID { get; set; }

        [Display(Name = "Form No")]
        public string FormNo { get; set; }

        [Display(Name  ="Date of Submission")]
        public DateTime? SubmissionDate { get; set; }

        public bool EnableForm { get; set; }

        public bool EnableUpload { get; set; }

        public bool EnableComment { get; set; }
		public bool IsCompleted { get; set; }

		public List<string> ActionList { get; set; }

        public string Mode { get; set; }

        public string K2SN { get; set; }







    }
}
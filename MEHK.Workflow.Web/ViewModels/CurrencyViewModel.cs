﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.Workflow.Web.ViewModels
{
	public class CurrencyViewModel
	{
		public int CurrencyID { get; set; }
		public string CurrencyName { get; set; }
		public int SortSeq { get; set; }
		public string Remark { get; set; }
	}
}